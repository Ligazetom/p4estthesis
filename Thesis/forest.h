#ifndef FOREST_H
#define FOREST_H

#include "generaltypes.h"

#include <p4est.h>
#include <p4est_iterate.h>
#include <p4est_mesh.h>

#include <vector>
#include <set>

namespace Forest
{
    void GetOrientedRegionAroundQuadrant(p4est_t *p4est,
                                         p4est_ghost_t *ghostLayer,
                                         SQuadrantData *ghostData,
                                         p4est_mesh_t *mesh,
                                         p4est_quadrant_t *quad,
                                         int which_tree,
                                         std::vector<SOrientedQuadrant> &regionQuadData);

    void GetOrientedRegionAroundQuadrant(p4est_t *p4est,
                                         p4est_ghost_t *ghostLayer,
                                         SQuadrantData *ghostData,
                                         p4est_quadrant_t *quad,
                                         int which_tree,
                                         std::vector<SOrientedQuadrant> &regionQuadData);

    void GetFaceNeighbors(p4est_t *p4est,
                          p4est_ghost_t *ghostLayer,
                          SQuadrantData *ghostData,
                          p4est_quadrant_t *quad,
                          int which_tree,
                          std::vector<SOrientedQuadrant> &neighbors,
                          NeighborOrientation::ENeighborOrientation neighborOrientation,
                          FS::EFaceSide orientation);

    SQuadrantData* RetrieveLocalOrGhostData(p4est_ghost_t *ghostLayer,
                                            SQuadrantData *ghostData,
                                            p4est_quadrant_t *quadrant,
                                            int which_tree,
                                            sc_array_t *which_corner,
                                            sc_array_t *which_proc,
                                            sc_array_t *quadrants,
                                            p4est_quadrant_t &outQuadrant);

    void GetQuadrantsInRadius(p4est_t *p4est,
                              p4est_ghost_t *ghostLayer,
                              SQuadrantData *ghostData,
                              p4est_quadrant_t *quad,
                              int which_tree,
                              int pixelRadius,   //radius in pixels, not quadrants
                              std::vector<SQuadrantDataSeparated> &regionQuads);

    int RefineAccordingToGlobalOTSUThresholdCallback(p4est_t *p4est, p4est_topidx_t which_tree, p4est_quadrant_t *quadrant);
    int RefineAccordingToMaxMinDiffThresholdCallback(p4est_t *p4est, p4est_topidx_t which_tree, p4est_quadrant_t *quadrant);
    int RefineAccordingToBernsenThresholdCallback(p4est_t *p4est, p4est_topidx_t which_tree, p4est_quadrant_t *quadrant);
    int RefineAccordingToNiblackThresholdCallback(p4est_t *p4est, p4est_topidx_t which_tree, p4est_quadrant_t *quadrant);
    int RefineAccordingToSauvolaThresholdCallback(p4est_t *p4est, p4est_topidx_t which_tree, p4est_quadrant_t *quadrant);
    int AlwaysRefineUpToMaxLevelCallback(p4est_t*, p4est_topidx_t, p4est_quadrant_t *quadrant);
    int RefineUpToLevelCallback(p4est_t*, p4est_topidx_t, p4est_quadrant_t *quadrant);

    void ExtractQuadrantExportImageDataCallback(p4est_iter_volume_info_t *info, void *user_data);
    void ExtractQuadrantExportVTKDataCallback(p4est_iter_volume_info_t *info, void *user_data);
    void ExtractQuadrantSourceDataCallback(p4est_iter_volume_info_t *info, void *user_data);
    void ExtractQuadrantExportVTKBufferCallback(p4est_iter_volume_info_t *info, void *user_data);

    void InitializeQuadrantDataCallback(p4est_iter_volume_info_t *info, void*);

    void ExchangeGhostData(p4est_t *p4est, p4est_ghost_t *ghostLayer, SQuadrantData *ghostData);
    void PartitionForest(p4est_t *p4est);
    void MapQuadrantDataToQuadrants(p4est_t *p4est, SQuadrantData *quadrantData);
    void RemapQuadrantDataAfterRefinement(p4est_t *p4est, SQuadrantData *quadrantData);
}

#endif // FOREST_H
