#include "parallelstopwatch.h"

ParallelStopwatch::ParallelStopwatch(MPI_Comm comm, int procRank, const std::string &stopwatchName)
{
    m_comm = comm;
    m_procRank = procRank;
    m_stopwatchName = stopwatchName;

    m_startPoint = std::chrono::steady_clock::now();
}

void ParallelStopwatch::ParallelReset()
{
    m_startPoint = std::chrono::steady_clock::now();
}

void ParallelStopwatch::ParallelPrintTimeDiff()
{
    std::chrono::steady_clock::time_point now = std::chrono::steady_clock::now();

    std::cout << m_stopwatchName << " - Rank: " << m_procRank << ": ";
    std::cout << std::chrono::duration_cast<std::chrono::milliseconds>(now - m_startPoint).count() << " ms " << std::endl;
}

void ParallelStopwatch::CollectiveReset()
{
    MPI_Barrier(m_comm);
    m_startPoint = std::chrono::steady_clock::now();
}

void ParallelStopwatch::CollectivePrintTimeDiff()
{
    MPI_Barrier(m_comm);

    if (m_procRank == 0)
    {
        std::chrono::steady_clock::time_point now = std::chrono::steady_clock::now();

        std::cout << m_stopwatchName << ": ";
        std::cout << std::chrono::duration_cast<std::chrono::milliseconds>(now - m_startPoint).count() << " ms " << std::endl;
    }
}
