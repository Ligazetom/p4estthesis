#ifndef MPIUTILS_H
#define MPIUTILS_H

#include <p4est.h>
#include <p4est_vtk.h>
#include <p4est_ghost.h>

#include <vector>
#include <string>

#include "generaltypes.h"
#include "wimage.h"

//taking care mostly of what runs on what processes and wrapping preparations for mpi function calls
//these functions are meant to be called from every mpi process
namespace MPIUtils
{
    void InitializeMPIDatatypes();
    void FreeMPIDatatypes();

    void GenerateFilteredFilePathsList(std::vector<std::string> &filePaths);
    void BroadcastFileName(const std::string &filePath);

    void GetNumOfQuadrantsForEachProcess(std::vector<int> &quadCount, p4est_t *p4est);
    void GetAllTreesTopLeftCoords(std::vector<SImageCoords> &coords, const std::vector<int> &procToTreeCount, p4est_t *p4est);

    void LoadImage(const char *imgPath, std::vector<unsigned char> &imageData);
    void ScatterFragmentedImageData(const std::vector<unsigned char> &fragmentedImage, const std::vector<int> &procToTreeCount, p4est_t *p4est);
    void GatherQuadrantsExportData(p4est_t *p4est, std::vector<SQuadrantExportData> &qData, int pipelineIndex);

    void ComputeAndBroadcastHistogram(const WImage &img, p4est_t *p4est);
    void ComputeAndBroadcastThresholdGlobalOTSU(p4est_t *p4est, unsigned int imagePixelCount);

    void CreateOutputFolder();
    void CreateFolderInOutputFolder(const char *folderName);
    p4est_vtk_context_t *WriteVtkHeaderSequentially(p4est_vtk_context_t *context);
    p4est_vtk_context_t *WriteVtkCellDataSequentially(p4est_vtk_context_t *context,
                                                       int write_tree, int write_level,
                                                       int write_rank, int wrap_rank,
                                                       int num_cell_scalars, int num_cell_vectors,
                                                       const char* fieldnames[], sc_array_t *values[]);
    int WriteVtkFooterSequentially(p4est_vtk_context_t *context);
};


#endif // MPIUTILS_H
