#include "forest.h"
#include "generaltypes.h"
#include "wimage.h"
#include "utility.h"
#include "globals.h"

#include <iostream>
#include <limits>
#include <cmath>

#include <p4est_bits.h>
#include <p4est_ghost.h>
#include <p4est_communication.h>



//left and right are always relative to looking at the edge from the inside of the quadrant
static int GetSubLeftChildIndexAccordingToFaceSide(FS::EFaceSide fs)
{
    switch (fs)
    {
    case FS::XN: return 3;
    case FS::XP: return 0;
    case FS::YN: return 2;
    case FS::YP: return 1;
    default: return -1;
    }
}

static int GetSubRightChildIndexAccordingToFaceSide(FS::EFaceSide fs)
{
    switch (fs)
    {
    case FS::XN: return 1;
    case FS::XP: return 2;
    case FS::YN: return 3;
    case FS::YP: return 0;
    default: return -1;
    }
}

static NeighborOrientation::ENeighborOrientation GetNeighborOrientationFromFaceSide(int nface)
{
    switch(nface)
    {
    case FS::XN: return NeighborOrientation::LEFT;
    case FS::XP: return NeighborOrientation::RIGHT;
    case FS::YN: return NeighborOrientation::TOP;
    case FS::YP: return NeighborOrientation::BOTTOM;
    default: return NeighborOrientation::UNKNOWN;
    }
}

void Forest::GetOrientedRegionAroundQuadrant(p4est_t *p4est,
                                     p4est_ghost_t *ghostLayer,
                                     SQuadrantData *ghostData,
                                     p4est_mesh_t *mesh,
                                     p4est_quadrant_t *quad,
                                     int which_tree,
                                     std::vector<SOrientedQuadrant> &regionQuadData)
{
    p4est_mesh_face_neighbor_t neighborIterator;
    p4est_mesh_face_neighbor_init(&neighborIterator, p4est, ghostLayer, mesh, which_tree, quad);
    int ntree, nface, nrank, quadID;
    p4est_quadrant_t *curQuad = nullptr;

    bool validSides[4] = { false, false, false, false };
    std::vector<std::vector<SOrientedQuadrant>> sides(4);

    std::vector<SOrientedQuadrant> &leftNeighbors(sides[FS::XN]);
    std::vector<SOrientedQuadrant> &rightNeighbors(sides[FS::XP]);
    std::vector<SOrientedQuadrant> &topNeighbors(sides[FS::YN]);
    std::vector<SOrientedQuadrant> &botNeighbors(sides[FS::YP]);

    while ((curQuad = p4est_mesh_face_neighbor_next(&neighborIterator, &ntree, &quadID, &nface, &nrank)) != NULL)
    {
        SOrientedQuadrant temp;
        temp.orientation = GetNeighborOrientationFromFaceSide(neighborIterator.face - 1);
        temp.quadrant = *curQuad;
        temp.quadrant.p.which_tree = ntree;
        temp.data = reinterpret_cast<SQuadrantData*>(p4est_mesh_face_neighbor_data(&neighborIterator, ghostData));

        sides[neighborIterator.face - 1].push_back(std::move(temp));
        validSides[neighborIterator.face - 1] = true;
    }

    if (validSides[FS::YN] && validSides[FS::XN])
    {
        std::vector<SOrientedQuadrant> topLeft;
        std::vector<SOrientedQuadrant> leftTop;
        Forest::GetFaceNeighbors(p4est, ghostLayer, ghostData, &(topNeighbors.front().quadrant), topNeighbors.front().quadrant.p.which_tree, topLeft, NeighborOrientation::TOP_LEFT, FS::XN);
        Forest::GetFaceNeighbors(p4est, ghostLayer, ghostData, &(leftNeighbors.back().quadrant), leftNeighbors.back().quadrant.p.which_tree, leftTop, NeighborOrientation::TOP_LEFT, FS::YN);

        if (!topLeft.empty() && !leftTop.empty())
        {
            if (p4est_quadrant_is_equal(&(leftTop.back().quadrant), &(topLeft.front().quadrant))) regionQuadData.push_back(std::move(leftTop.back()));
        }
    }

    if (validSides[FS::YN] && validSides[FS::XP])
    {
        std::vector<SOrientedQuadrant> topRight;
        std::vector<SOrientedQuadrant> rightTop;

        Forest::GetFaceNeighbors(p4est, ghostLayer, ghostData, &(topNeighbors.back().quadrant), topNeighbors.back().quadrant.p.which_tree, topRight, NeighborOrientation::TOP_RIGHT, FS::XP);
        Forest::GetFaceNeighbors(p4est, ghostLayer, ghostData, &(rightNeighbors.front().quadrant), rightNeighbors.front().quadrant.p.which_tree, rightTop, NeighborOrientation::TOP_RIGHT, FS::YN);

        if (!topRight.empty() && !rightTop.empty())
        {
            if (p4est_quadrant_is_equal(&(topRight.back().quadrant), &(rightTop.front().quadrant))) regionQuadData.push_back(std::move(rightTop.front()));
        }
    }

    if (validSides[FS::YP] && validSides[FS::XP])
    {
        std::vector<SOrientedQuadrant> botRight;
        std::vector<SOrientedQuadrant> rightBot;

        Forest::GetFaceNeighbors(p4est, ghostLayer, ghostData, &(botNeighbors.front().quadrant), botNeighbors.front().quadrant.p.which_tree, botRight, NeighborOrientation::BOT_RIGHT, FS::XP);
        Forest::GetFaceNeighbors(p4est, ghostLayer, ghostData, &(rightNeighbors.back().quadrant), rightNeighbors.back().quadrant.p.which_tree, rightBot, NeighborOrientation::BOT_RIGHT, FS::YP);

        if (!botRight.empty() && !rightBot.empty())
        {
            if (p4est_quadrant_is_equal(&(botRight.front().quadrant), &(rightBot.back().quadrant))) regionQuadData.push_back(std::move(botRight.front()));
        }
    }

    if (validSides[FS::YP] && validSides[FS::XN])
    {
        std::vector<SOrientedQuadrant> botLeft;
        std::vector<SOrientedQuadrant> leftBot;

        Forest::GetFaceNeighbors(p4est, ghostLayer, ghostData, &(botNeighbors.back().quadrant), botNeighbors.back().quadrant.p.which_tree, botLeft, NeighborOrientation::BOT_LEFT, FS::XN);
        Forest::GetFaceNeighbors(p4est, ghostLayer, ghostData, &(leftNeighbors.front().quadrant), leftNeighbors.front().quadrant.p.which_tree, leftBot, NeighborOrientation::BOT_LEFT, FS::YP);

        if (!botLeft.empty() && !leftBot.empty())
        {
            if (p4est_quadrant_is_equal(&(botLeft.back().quadrant), &(leftBot.front().quadrant))) regionQuadData.push_back(std::move(leftBot.front()));
        }
    }

    size_t sum = 0;
    for (size_t i = 0; i < sides.size(); ++i)
    {
        sum += sides[i].size();
    }

    regionQuadData.reserve(regionQuadData.size() + sum);

    for (size_t i = 0; i < sides.size(); ++i)
    {
        std::move(std::begin(sides[i]), std::end(sides[i]), std::back_inserter(regionQuadData));
    }
}

//forest must be balanced
void Forest::GetOrientedRegionAroundQuadrant(p4est_t *p4est,
                                             p4est_ghost_t *ghostLayer,
                                             SQuadrantData *ghostData,
                                             p4est_quadrant_t *quad,
                                             int which_tree,
                                             std::vector<SOrientedQuadrant> &regionQuadData)
{
    std::vector<std::vector<SOrientedQuadrant>> sides(4);

   // std::cout << "\nStarting LEFT neighbor\n";
    std::vector<SOrientedQuadrant> &leftNeighbors(sides[FS::XN]);
    Forest::GetFaceNeighbors(p4est, ghostLayer, ghostData, quad, which_tree, leftNeighbors, NeighborOrientation::LEFT, FS::XN);

   // std::cout << "\nStarting RIGHT neighbor\n";
    std::vector<SOrientedQuadrant> &rightNeighbors(sides[FS::XP]);
    Forest::GetFaceNeighbors(p4est, ghostLayer, ghostData, quad, which_tree, rightNeighbors, NeighborOrientation::RIGHT, FS::XP);

   // std::cout << "\nStarting TOP neighbor\n";
    std::vector<SOrientedQuadrant> &topNeighbors(sides[FS::YN]);
    Forest::GetFaceNeighbors(p4est, ghostLayer, ghostData, quad, which_tree, topNeighbors, NeighborOrientation::TOP, FS::YN);

   // std::cout << "\nStarting BOT neighbor\n";
    std::vector<SOrientedQuadrant> &botNeighbors(sides[FS::YP]);
    Forest::GetFaceNeighbors(p4est, ghostLayer, ghostData, quad, which_tree, botNeighbors, NeighborOrientation::BOTTOM, FS::YP);

    bool validSides[4] = { false, false, false, false };    //edge of forest wont get any neighbors, so no need to compute diagonal neighbor

    for(size_t i = 0; i < sides.size(); ++i)
    {
        for(size_t j = 0; j < sides[i].size(); ++j)
        {
            if (p4est_quadrant_is_valid(&(sides[i][j].quadrant)))
            {
                validSides[i] = true;
                break;
            }
        }
    }

   // std::cout << "\nStarting TOPLEFT neighbor\n";
    if (validSides[FS::YN] && validSides[FS::XN])
    {
        std::vector<SOrientedQuadrant> topLeft; //quad on the top of our original and its left neighbors
        std::vector<SOrientedQuadrant> leftTop; //quad on the left of our original and its top neighbors

        //topNeighbors.front() is either the only quadrant present or the left one of the two hanging ones
        Forest::GetFaceNeighbors(p4est, ghostLayer, ghostData, &(topNeighbors.front().quadrant), topNeighbors.front().quadrant.p.which_tree, topLeft, NeighborOrientation::TOP_LEFT, FS::XN);

        //leftNeighbors.back() is either the only quadrant present or the right one of the two hanging ones
        Forest::GetFaceNeighbors(p4est, ghostLayer, ghostData, &(leftNeighbors.back().quadrant), leftNeighbors.back().quadrant.p.which_tree, leftTop, NeighborOrientation::TOP_LEFT, FS::YN);

        if (!topLeft.empty() && !leftTop.empty())
        {
            if (p4est_quadrant_is_equal(&(leftTop.back().quadrant), &(topLeft.front().quadrant)))   //if top and left see the same neighbor we add it to the region
            {
                regionQuadData.push_back(std::move(leftTop.back()));
            }
        }
    }

   // std::cout << "\nStarting TOPRIGHT neighbor\n";
    if (validSides[FS::YN] && validSides[FS::XP])
    {
        std::vector<SOrientedQuadrant> topRight; //quad on the top of our original and its right neighbors
        std::vector<SOrientedQuadrant> rightTop; //quad on the right of our original and its top neighbors

        //topNeighbors.back() is either the only quadrant present or the right one of the two hanging ones
        Forest::GetFaceNeighbors(p4est, ghostLayer, ghostData, &(topNeighbors.back().quadrant), topNeighbors.back().quadrant.p.which_tree, topRight, NeighborOrientation::TOP_RIGHT, FS::XP);

        //rightNeighbors.front() is either the only quadrant present or the left one of the two hanging ones
        Forest::GetFaceNeighbors(p4est, ghostLayer, ghostData, &(rightNeighbors.front().quadrant), rightNeighbors.front().quadrant.p.which_tree, rightTop, NeighborOrientation::TOP_RIGHT, FS::YN);

        if (!topRight.empty() && !rightTop.empty())
        {
            if (p4est_quadrant_is_equal(&(topRight.back().quadrant), &(rightTop.front().quadrant)))   //if top and right see the same neighbor we add it to the region
            {
                regionQuadData.push_back(std::move(rightTop.front()));
            }
        }
    }

   // std::cout << "\nStarting BOTRIGHT neighbor\n";
    if (validSides[FS::YP] && validSides[FS::XP])
    {
        std::vector<SOrientedQuadrant> botRight; //quad on the bot of our original and its right neighbors
        std::vector<SOrientedQuadrant> rightBot; //quad on the right of our original and its bot neighbors

        //botNeighbors.front() is either the only quadrant present or the left one of the two hanging ones
        Forest::GetFaceNeighbors(p4est, ghostLayer, ghostData, &(botNeighbors.front().quadrant), botNeighbors.front().quadrant.p.which_tree, botRight, NeighborOrientation::BOT_RIGHT, FS::XP);

        //rightNeighbors.back() is either the only quadrant present or the right one of the two hanging ones
        Forest::GetFaceNeighbors(p4est, ghostLayer, ghostData, &(rightNeighbors.back().quadrant), rightNeighbors.back().quadrant.p.which_tree, rightBot, NeighborOrientation::BOT_RIGHT, FS::YP);

        if (!botRight.empty() && !rightBot.empty())
        {
            if (p4est_quadrant_is_equal(&(botRight.front().quadrant), &(rightBot.back().quadrant)))   //if bot and right see the same neighbor we add it to the region
            {
                regionQuadData.push_back(std::move(botRight.front()));
            }
        }
    }

   // std::cout << "\nStarting BOTLEFT neighbor\n";
    if (validSides[FS::YP] && validSides[FS::XN])
    {
        std::vector<SOrientedQuadrant> botLeft; //quad on the bot of our original and its left neighbors
        std::vector<SOrientedQuadrant> leftBot; //quad on the left of our original and its bot neighbors

        //botNeighbors.back() is either the only quadrant present or the right one of the two hanging ones
        Forest::GetFaceNeighbors(p4est, ghostLayer, ghostData, &(botNeighbors.back().quadrant), botNeighbors.back().quadrant.p.which_tree, botLeft, NeighborOrientation::BOT_LEFT, FS::XN);

        //leftNeighbors.front() is either the only quadrant present or the left one of the two hanging ones
        Forest::GetFaceNeighbors(p4est, ghostLayer, ghostData, &(leftNeighbors.front().quadrant), leftNeighbors.front().quadrant.p.which_tree, leftBot, NeighborOrientation::BOT_LEFT, FS::YP);

        if (!botLeft.empty() && !leftBot.empty())
        {
            if (p4est_quadrant_is_equal(&(botLeft.back().quadrant), &(leftBot.front().quadrant)))   //if bot and left see the same neighbor we add it to the region
            {
                regionQuadData.push_back(std::move(leftBot.front()));
            }
        }
    }

    size_t sum = 0;
    for (size_t i = 0; i < sides.size(); ++i)
    {
        sum += sides[i].size();
    }

    regionQuadData.reserve(regionQuadData.size() + sum);

    for (size_t i = 0; i < sides.size(); ++i)
    {
        std::move(std::begin(sides[i]), std::end(sides[i]), std::back_inserter(regionQuadData));
    }
}

void Forest::GetFaceNeighbors(p4est_t *p4est,
                              p4est_ghost_t *ghostLayer,
                              SQuadrantData *ghostData,
                              p4est_quadrant_t *quad,
                              int which_tree,
                              std::vector<SOrientedQuadrant> &neighbors,
                              NeighborOrientation::ENeighborOrientation neighborOrientation,
                              FS::EFaceSide orientation)
{
    p4est_quadrant_t neighbor;
    int nface, exists;
    int ntree = p4est_quadrant_face_neighbor_extra(quad, which_tree, orientation, &neighbor, &nface, p4est->connectivity);

    sc_array_t *which_corner = sc_array_new_count(sizeof(int), 0);
    sc_array_t *which_proc = sc_array_new_count(sizeof(int), 0);
    sc_array_t *quads = sc_array_new_count(sizeof(p4est_quadrant_t), 0);

    if (ntree != -1)
    {
       // std::cout << "Quadrant has neighbor\n";

        exists = p4est_quadrant_exists(p4est, ghostLayer, ntree, &neighbor, which_corner, which_proc, quads);

        if (exists == 1)
        {
         //   std::cout << "Neighbor is the same size\n";
            SOrientedQuadrant temp;

            temp.orientation = neighborOrientation;
            temp.data = Forest::RetrieveLocalOrGhostData(ghostLayer, ghostData, &neighbor, ntree, which_corner, which_proc, quads, temp.quadrant);

            neighbors.push_back(temp);
        }
        else    //does not exist ---> might be hanging or bigger
        {
            int childrenCount = 0;
          //  std::cout << "Neighbor is NOT the same size\n";
            p4est_quadrant_t neighborSmaller;

            p4est_quadrant_child(&neighbor, &neighborSmaller, GetSubLeftChildIndexAccordingToFaceSide(orientation));

            exists = p4est_quadrant_exists(p4est, ghostLayer, ntree, &neighborSmaller, which_corner, which_proc, quads);

            if (exists == 1)
            {
                ++childrenCount;
              //  std::cout << "Subleft smaller neighbor exists\n";

                SOrientedQuadrant temp;

                temp.orientation = neighborOrientation;
                temp.data = Forest::RetrieveLocalOrGhostData(ghostLayer, ghostData, &neighborSmaller, ntree, which_corner, which_proc, quads, temp.quadrant);

                neighbors.push_back(temp);
            }

            p4est_quadrant_child(&neighbor, &neighborSmaller, GetSubRightChildIndexAccordingToFaceSide(orientation));
            exists = p4est_quadrant_exists(p4est, ghostLayer, ntree, &neighborSmaller, which_corner, which_proc, quads);

            if (exists == 1)
            {
                ++childrenCount;
              //  std::cout << "Subright smaller neighbor exists\n";

                SOrientedQuadrant temp;

                temp.orientation = neighborOrientation;
                temp.data = Forest::RetrieveLocalOrGhostData(ghostLayer, ghostData, &neighborSmaller, ntree, which_corner, which_proc, quads, temp.quadrant);

                neighbors.push_back(temp);
            }

            if (childrenCount !=2)
            {
                p4est_quadrant_t neighborBigger;

                p4est_quadrant_ancestor(&neighbor, neighbor.level - 1, &neighborBigger);

                exists = p4est_quadrant_exists(p4est, ghostLayer, ntree, &neighborBigger, which_corner, which_proc, quads);

                if (exists == 1)
                {
                  //  std::cout << "Bigger neighbor exists\n";

                    SOrientedQuadrant temp;

                    temp.orientation = neighborOrientation;
                    temp.data = Forest::RetrieveLocalOrGhostData(ghostLayer, ghostData, &neighborBigger, ntree, which_corner, which_proc, quads, temp.quadrant);

                    neighbors.push_back(temp);
                }
            }
        }
    }
    else
    {
       // std::cout << "Quadrant has no neighbor on the " << Utility::GetOrientationString(orientation) << std::endl;
    }

    sc_array_destroy(which_corner);
    sc_array_destroy(which_proc);
    sc_array_destroy(quads);
}

SQuadrantData* Forest::RetrieveLocalOrGhostData(p4est_ghost_t *ghostLayer,
                                                SQuadrantData *ghostData,
                                                p4est_quadrant_t *quadrant,
                                                int which_tree,
                                                sc_array_t *which_corner,
                                                sc_array_t *which_proc,
                                                sc_array_t *quadrants,
                                                p4est_quadrant_t &outQuadrant)
{
    unsigned int validIndex = 0;
    for(unsigned int i = 0; i < which_corner->elem_count; ++i)
    {
        if (*reinterpret_cast<int*>(which_corner->array + sizeof(int)* i) == 1)
        {
            validIndex = i;
        }
    }

    int neighborProc = *reinterpret_cast<int*>(which_proc->array + sizeof(int) * validIndex);
    p4est_quadrant_t *tempQuad = p4est_quadrant_array_index(quadrants, validIndex);
    tempQuad->p.which_tree = which_tree;
    outQuadrant = *tempQuad;

    if (neighborProc == Globals::MPI::Rank)     //our neighbor is stored locally on this proc
    {
      //  std::cout << "Quadrant is local\n";
        return reinterpret_cast<SQuadrantData*>(tempQuad->p.user_data);
    }
    else    //neighbor is ghost
    {
      //  std::cout << "Quadrant is ghost\n";
        int offset = p4est_ghost_bsearch(ghostLayer, neighborProc, which_tree, quadrant);

        if (offset != -1)
        {
         //   std::cout << "Ghost data found\n";
            return ghostData + offset;
        }
    }

    return nullptr;
}

static bool IsAtLeasOneQuadCornerInRadius(double (&origin)[2], double (&coords)[3], double quadSideLength, double radius)
{
    double diff[2] = { fabs(origin[0] - coords[0]), fabs(origin[1] - coords[1]) };
    if (diff[0] <= radius && diff[1] <= radius) return true;    //botleft
    double temp = diff[0];

    diff[0] = fabs(origin[0] - (coords[0] + quadSideLength));
    if (diff[0] <= radius && diff[1] <= radius) return true;    //botright

    diff[1] = fabs(origin[1] - (coords[1] - quadSideLength));
    if (diff[0] <= radius && diff[1] <= radius) return true;    //topright

    if (temp <= radius && diff[1] <= radius) return true;       //topleft

    return false;
}

void Forest::GetQuadrantsInRadius(p4est_t *p4est,
                             p4est_ghost_t *ghostLayer,
                             SQuadrantData *ghostData,
                             p4est_quadrant_t *quad,
                             int which_tree,
                             int pixelRadius,
                             std::vector<SQuadrantDataSeparated> &quadrants)
{    
    double originalQuadSideHalfLength = (Globals::Forest::TreeSideLength >> quad->level) / 2.f;
    double radius = static_cast<double>(pixelRadius);

    if (originalQuadSideHalfLength > radius) return;    //quadrant is bigger than the radius so we don't have to search anything

    double originalCoords[3];
    p4est_qcoord_to_vertex(p4est->connectivity, which_tree, quad->x, quad->y, originalCoords);
    double origin[2] = { originalCoords[0] + originalQuadSideHalfLength, originalCoords[1] - originalQuadSideHalfLength };

    for(int treeId = p4est->first_local_tree; treeId <= p4est->last_local_tree; ++treeId)
    {
        p4est_tree_t * tree = p4est_tree_array_index(p4est->trees, treeId);

        for(size_t quadId = 0; quadId < tree->quadrants.elem_count; ++quadId)
        {
            p4est_quadrant_t *curQuad = p4est_quadrant_array_index(&(tree->quadrants), quadId);
            double coords[3];
            p4est_qcoord_to_vertex(p4est->connectivity, treeId, curQuad->x, curQuad->y, coords);
            double quadSideLength = static_cast<double>(Globals::Forest::TreeSideLength >> curQuad->level);

            if (IsAtLeasOneQuadCornerInRadius(origin, coords, quadSideLength, radius))
            {
                SQuadrantDataSeparated temp;
                temp.quadrant = *curQuad;
                temp.data = reinterpret_cast<SQuadrantData*>(curQuad->p.user_data);

                quadrants.push_back(std::move(temp));
            }
        }
    }

    for(size_t quadId = 0; quadId < ghostLayer->ghosts.elem_count; ++quadId)
    {
        p4est_quadrant_t *curQuad = p4est_quadrant_array_index(&(ghostLayer->ghosts), quadId);
        double coords[3];
        p4est_qcoord_to_vertex(p4est->connectivity, curQuad->p.which_tree, curQuad->x, curQuad->y, coords);
        double quadSideLength = static_cast<double>(Globals::Forest::TreeSideLength >> curQuad->level);

        if (IsAtLeasOneQuadCornerInRadius(origin, coords, quadSideLength, radius))
        {
            SQuadrantDataSeparated temp;
            temp.quadrant = *curQuad;
            temp.data = ghostData + quadId;

            quadrants.push_back(std::move(temp));
        }
    }
}

int Forest::RefineAccordingToGlobalOTSUThresholdCallback(p4est_t * p4est, p4est_topidx_t which_tree, p4est_quadrant_t * quadrant)
{
    SGlobalOTSUThresholdParams *params = reinterpret_cast<SGlobalOTSUThresholdParams*>(Globals::Refinement::RefinementParams[Globals::Refinement::CurrentAlgorithmIndex]);

    if (quadrant->level >= Globals::Refinement::MaxRefinementLevel ||
            quadrant->level < params->refineFromQuadLevel) return 0;

    SForestData *forestData = reinterpret_cast<SForestData*>(p4est->user_pointer);
    int quadrantSideLength = Globals::Forest::TreeSideLength >> quadrant->level;

    double dxyz[3];
    p4est_qcoord_to_vertex(p4est->connectivity, which_tree, quadrant->x, quadrant->y, dxyz); //gives left bottom vertex coordinate
    int xyz[3];
    Utility::TransformQuadCoordsToLocalTopLeftImageCoords(dxyz, xyz, quadrantSideLength, Globals::Forest::TreeSideLength);
    WImage img = Utility::GetFragmentedImageFromArray(reinterpret_cast<SForestData*>(p4est->user_pointer)->pImageData,
                                                      which_tree - p4est->first_local_tree,
                                                      Globals::Forest::ExtendedTreeSideLength,
                                                      Globals::Forest::NumOfBytesPerTreeImageFragment);

    unsigned char globalOTSUThreshold = forestData->forestValueArray[ForestValue::GLOBAL_OTSU_THRESHOLD];

    for (int j = xyz[1]; j < xyz[1] + quadrantSideLength; ++j)
    {
        for (int i = xyz[0]; i < xyz[0] + quadrantSideLength; ++i)
        {
            if (img.GetPixel(j, i) >= globalOTSUThreshold) return 1;
        }
    }

    return 0;
}

int Forest::RefineAccordingToMaxMinDiffThresholdCallback(p4est_t * p4est, p4est_topidx_t which_tree, p4est_quadrant_t * quadrant)
{
    SMaxMinDiffThresholdParams *params = reinterpret_cast<SMaxMinDiffThresholdParams*>(Globals::Refinement::RefinementParams[Globals::Refinement::CurrentAlgorithmIndex]);

    if (quadrant->level >= Globals::Refinement::MaxRefinementLevel ||
            quadrant->level < params->refineFromQuadLevel) return 0;

    int quadrantSideLength = Globals::Forest::TreeSideLength >> quadrant->level;

    double dxyz[3];
    p4est_qcoord_to_vertex(p4est->connectivity, which_tree, quadrant->x, quadrant->y, dxyz); //gives left bottom vertex coordinate
    int xyz[3];
    Utility::TransformQuadCoordsToLocalTopLeftImageCoords(dxyz, xyz, quadrantSideLength, Globals::Forest::TreeSideLength);
    WImage img = Utility::GetFragmentedImageFromArray(reinterpret_cast<SForestData*>(p4est->user_pointer)->pImageData,
                                                      which_tree - p4est->first_local_tree,
                                                      Globals::Forest::ExtendedTreeSideLength,
                                                      Globals::Forest::NumOfBytesPerTreeImageFragment);

    int maxVal = 0, minVal = 0;
    for (int j = xyz[1]; j < xyz[1] + quadrantSideLength; ++j)
    {
        for (int i = xyz[0]; i < xyz[0] + quadrantSideLength; ++i)
        {
            unsigned char curVal = img.GetPixel(j, i);

            if (curVal > maxVal) maxVal = curVal;
            if (curVal < minVal) minVal = curVal;
        }
    }

    if (maxVal - minVal >= params->maxMinDiffThreshold) return 1;

    return 0;
}

int Forest::RefineAccordingToBernsenThresholdCallback(p4est_t *p4est, p4est_topidx_t which_tree, p4est_quadrant_t *quadrant)
{
    SBernsenThresholdParams *params = reinterpret_cast<SBernsenThresholdParams*>(Globals::Refinement::RefinementParams[Globals::Refinement::CurrentAlgorithmIndex]);

    if (quadrant->level >= Globals::Refinement::MaxRefinementLevel ||
            quadrant->level < params->refineFromQuadLevel) return 0;

    int quadrantSideLength = Globals::Forest::TreeSideLength >> quadrant->level;

    double dxyz[3];
    p4est_qcoord_to_vertex(p4est->connectivity, which_tree, quadrant->x, quadrant->y, dxyz); //gives left bottom vertex coordinate
    int xyz[3];
    Utility::TransformQuadCoordsToLocalTopLeftImageCoords(dxyz, xyz, quadrantSideLength, Globals::Forest::TreeSideLength);
    WImage img = Utility::GetFragmentedImageFromArray(reinterpret_cast<SForestData*>(p4est->user_pointer)->pImageData,
                                                      which_tree - p4est->first_local_tree,
                                                      Globals::Forest::ExtendedTreeSideLength,
                                                      Globals::Forest::NumOfBytesPerTreeImageFragment);

    for (int j = xyz[1]; j < xyz[1] + quadrantSideLength; ++j)
    {
        for (int i = xyz[0]; i < xyz[0] + quadrantSideLength; ++i)
        {
            int minValue = std::numeric_limits<int>::max();
            int maxValue = std::numeric_limits<int>::min();

            for (int jMask = j - params->radius; jMask <= j + params->radius; ++jMask)
            {
                for(int iMask = i - params->radius; iMask <= i + params->radius; ++iMask)
                {
                    int maskPixelValue = img.GetPixel(jMask, iMask);

                    if (maskPixelValue < minValue) minValue = maskPixelValue;
                    if (maskPixelValue > maxValue) maxValue = maskPixelValue;
                }
            }

            int contrast = maxValue - minValue;
            int pixelValue = img.GetPixel(j, i);
            int midValue = (minValue + maxValue) / 2;

            if (contrast >= params->contrastThreshold)
            {
                if (pixelValue >= midValue) return 1;
            }
            else
            {
                if (midValue >= params->backgroundForegroundThreshold) return 1;
            }
        }
    }

    return 0;
}

int Forest::RefineAccordingToNiblackThresholdCallback(p4est_t *p4est, p4est_topidx_t which_tree, p4est_quadrant_t *quadrant)
{
    SNiblackThresholdParams *params = reinterpret_cast<SNiblackThresholdParams*>(Globals::Refinement::RefinementParams[Globals::Refinement::CurrentAlgorithmIndex]);

    if (quadrant->level >= Globals::Refinement::MaxRefinementLevel ||
            quadrant->level < params->refineFromQuadLevel) return 0;

    int quadrantSideLength = Globals::Forest::TreeSideLength >> quadrant->level;

    double dxyz[3];
    p4est_qcoord_to_vertex(p4est->connectivity, which_tree, quadrant->x, quadrant->y, dxyz); //gives left bottom vertex coordinate
    int xyz[3];
    Utility::TransformQuadCoordsToLocalTopLeftImageCoords(dxyz, xyz, quadrantSideLength, Globals::Forest::TreeSideLength);
    WImage img = Utility::GetFragmentedImageFromArray(reinterpret_cast<SForestData*>(p4est->user_pointer)->pImageData,
                                                      which_tree - p4est->first_local_tree,
                                                      Globals::Forest::ExtendedTreeSideLength,
                                                      Globals::Forest::NumOfBytesPerTreeImageFragment);

    for (int j = xyz[1]; j < xyz[1] + quadrantSideLength; ++j)
    {
        for (int i = xyz[0]; i < xyz[0] + quadrantSideLength; ++i)
        {
            int maskAvg = 0;
            int maskPower2Avg = 0;

            for (int jMask = j - params->radius; jMask <= j + params->radius; ++jMask)
            {
                for(int iMask = i - params->radius; iMask <= i + params->radius; ++iMask)
                {
                    int maskPixelValue = img.GetPixel(jMask, iMask);

                    maskAvg += maskPixelValue;
                    maskPower2Avg += maskPixelValue * maskPixelValue;
                }
            }

            int maskSideLength = params->radius + params->radius + 1;
            int numOfPixelsInMask = maskSideLength * maskSideLength;

            maskAvg /= numOfPixelsInMask;
            maskPower2Avg /= numOfPixelsInMask;

            double sigmaR = sqrt(maskPower2Avg - (maskAvg * maskAvg));
            double threshold = maskAvg + params->kappa * sigmaR + params->meanOffset;

            if (img.GetPixel(j, i) > threshold) return 1;
        }
    }

    return 0;
}

int Forest::RefineAccordingToSauvolaThresholdCallback(p4est_t *p4est, p4est_topidx_t which_tree, p4est_quadrant_t *quadrant)
{
    SSauvolaThresholdParams *params = reinterpret_cast<SSauvolaThresholdParams*>(Globals::Refinement::RefinementParams[Globals::Refinement::CurrentAlgorithmIndex]);

    if (quadrant->level >= Globals::Refinement::MaxRefinementLevel ||
            quadrant->level < params->refineFromQuadLevel) return 0;

    int quadrantSideLength = Globals::Forest::TreeSideLength >> quadrant->level;

    double dxyz[3];
    p4est_qcoord_to_vertex(p4est->connectivity, which_tree, quadrant->x, quadrant->y, dxyz); //gives left bottom vertex coordinate
    int xyz[3];
    Utility::TransformQuadCoordsToLocalTopLeftImageCoords(dxyz, xyz, quadrantSideLength, Globals::Forest::TreeSideLength);
    WImage img = Utility::GetFragmentedImageFromArray(reinterpret_cast<SForestData*>(p4est->user_pointer)->pImageData,
                                                      which_tree - p4est->first_local_tree,
                                                      Globals::Forest::ExtendedTreeSideLength,
                                                      Globals::Forest::NumOfBytesPerTreeImageFragment);

    for (int j = xyz[1]; j < xyz[1] + quadrantSideLength; ++j)
    {
        for (int i = xyz[0]; i < xyz[0] + quadrantSideLength; ++i)
        {
            double maskAvg = 0.;
            double maskPower2Avg = 0.;

            for (int jMask = j - params->radius; jMask <= j + params->radius; ++jMask)
            {
                for(int iMask = i - params->radius; iMask <= i + params->radius; ++iMask)
                {
                    int maskPixelValue = img.GetPixel(jMask, iMask);

                    maskAvg += maskPixelValue;
                    maskPower2Avg += maskPixelValue * maskPixelValue;
                }
            }

            int maskSideLength = params->radius + params->radius + 1;
            int numOfPixelsInMask = maskSideLength * maskSideLength;

            maskAvg /= numOfPixelsInMask;
            maskPower2Avg /= numOfPixelsInMask;

            double sigmaR = sqrt(maskPower2Avg - (maskAvg * maskAvg));
            double threshold = maskAvg * (1 + params->kappa * ((sigmaR / params->sigmaMax) - 1));

            if (img.GetPixel(j, i) > threshold) return 1;
        }
    }

    return 0;
}

int Forest::AlwaysRefineUpToMaxLevelCallback(p4est_t*, p4est_topidx_t, p4est_quadrant_t * quadrant)
{
    if (quadrant->level >= Globals::Refinement::MaxRefinementLevel) return 0;
    return 1;
}

int Forest::RefineUpToLevelCallback(p4est_t*, p4est_topidx_t, p4est_quadrant_t *quadrant)
{
    SUpToRefinementParams *params = reinterpret_cast<SUpToRefinementParams*>(Globals::Refinement::RefinementParams[Globals::Refinement::CurrentAlgorithmIndex]);

    if (quadrant->level >= Globals::Refinement::MaxRefinementLevel || quadrant->level >= params->quadLevel) return 0;
    return 1;
}

void Forest::ExtractQuadrantExportImageDataCallback(p4est_iter_volume_info_t *info, void *user_data)
{
    SImageExportData *data = reinterpret_cast<SImageExportData*>(user_data);
    SQuadrantData *qData = reinterpret_cast<SQuadrantData*>(info->quad->p.user_data);
    SQuadrantExportData &curExportData = data->exportData[qData->quadLocalOffset];

    double temp = qData->pipeline[data->exportPipelineIndex].volumeData.value / qData->pipeline[data->exportPipelineIndex].volumeData.maxValue; //0-1 space
    temp *= 255.;   //back to 0-255, in case we are exporting pipeline from square output..this is done only for image outputs
    curExportData.val = round(temp);
    double xyz[3];
    p4est_qcoord_to_vertex(info->p4est->connectivity, info->treeid, info->quad->x, info->quad->y, xyz);

    curExportData.xCoord = static_cast<int>(xyz[0]);
    curExportData.yCoord = static_cast<int>(xyz[1]);
    curExportData.level = info->quad->level;
}

void Forest::ExtractQuadrantExportVTKDataCallback(p4est_iter_volume_info_t *info, void *user_data)
{
    SVTKExportData *data = reinterpret_cast<SVTKExportData*>(user_data);
    SQuadrantData *qData = reinterpret_cast<SQuadrantData*>(info->quad->p.user_data);

    double *arrMember = reinterpret_cast<double*>(sc_array_index(data->exportArray, qData->quadLocalOffset));
    *arrMember = static_cast<double>(qData->pipeline[data->exportPipelineIndex].volumeData.value);
}

void Forest::ExtractQuadrantSourceDataCallback(p4est_iter_volume_info_t *info, void *user_data)
{
    SQuadrantData *qData = reinterpret_cast<SQuadrantData*>(info->quad->p.user_data);
    sc_array_t *dataArray = reinterpret_cast<sc_array_t*>(user_data);

    double *arrMember = reinterpret_cast<double*>(sc_array_index(dataArray, qData->quadLocalOffset));
    *arrMember = static_cast<double>(qData->source);
}

void Forest::ExtractQuadrantExportVTKBufferCallback(p4est_iter_volume_info_t *info, void *user_data)
{
    SVTKExportData *data = reinterpret_cast<SVTKExportData*>(user_data);
    SQuadrantData *qData = reinterpret_cast<SQuadrantData*>(info->quad->p.user_data);

    double *arrMember = reinterpret_cast<double*>(sc_array_index(data->exportArray, qData->quadLocalOffset));
    *arrMember = static_cast<double>(qData->buffers[data->exportBufferIndex]);
}

void Forest::InitializeQuadrantDataCallback(p4est_iter_volume_info_t *info, void*)
{
    p4est_t *p4est = info->p4est;
    p4est_quadrant_t *quadrant = info->quad;
    p4est_topidx_t which_tree = info->treeid;
    SQuadrantData *qData = reinterpret_cast<SQuadrantData*>(quadrant->p.user_data);
    bool assignPixelMeanToPipeline = true;  //source always have to be assigned for export purposes

    if (Globals::Processing::SetBackgroundQuadrantValue)
    {
        int quadrantLevelForOTSUBackgroundValue = Globals::Processing::QuadrantLevelForOTSUBackgroundValue;

        if (quadrantLevelForOTSUBackgroundValue < 0)
        {
            quadrantLevelForOTSUBackgroundValue = Globals::Refinement::MaxRefinementLevel + quadrantLevelForOTSUBackgroundValue;    //subtract negative value from maximum level
        }

        if (quadrant->level <= quadrantLevelForOTSUBackgroundValue)    //if quadrants are coarse enough we assign background value
        {
            unsigned char valueToAssign;

            if (Globals::Processing::AutoBackgroundQuadrantValue) valueToAssign = reinterpret_cast<SForestData*>(p4est->user_pointer)->forestValueArray[ForestValue::GLOBAL_OTSU_BACKGROUND_MEAN];
            else valueToAssign = Globals::Processing::BackgroundQuadrantValue;

            for(int i = 0; i < MAX_PIPELINE_NUMBER; ++i)
            {
                qData->pipeline[i].volumeData.value = valueToAssign;
                qData->pipeline[i].volumeData.maxValue = 255.;
            }

            assignPixelMeanToPipeline = false;
        }
    }

    int quadrantSideLength = Globals::Forest::TreeSideLength >> quadrant->level;

    double dxyz[3];
    p4est_qcoord_to_vertex(p4est->connectivity, which_tree, quadrant->x, quadrant->y, dxyz); //gives left bottom vertex coordinate
    int xyz[3];
    Utility::TransformQuadCoordsToLocalTopLeftImageCoords(dxyz, xyz, quadrantSideLength, Globals::Forest::TreeSideLength);

    WImage img = Utility::GetFragmentedImageFromArray(reinterpret_cast<SForestData*>(p4est->user_pointer)->pImageData,
                                                      which_tree - p4est->first_local_tree,     //offset for local process
                                                      Globals::Forest::ExtendedTreeSideLength,
                                                      Globals::Forest::NumOfBytesPerTreeImageFragment);
    int totalTimes = 0;
    int totalValue = 0;

    for (int j = xyz[1]; j < xyz[1] + quadrantSideLength; ++j)
    {
        for (int i = xyz[0]; i < xyz[0] + quadrantSideLength; ++i)
        {
            totalValue += img.GetPixel(j, i);
            ++totalTimes;
        }
    }

    if (assignPixelMeanToPipeline)
    {
        for (int i = 0; i < MAX_PIPELINE_NUMBER; ++i)
        {
            qData->pipeline[i].volumeData.value = (totalValue / totalTimes);
            qData->pipeline[i].volumeData.maxValue = 255.;
        }
    }

    qData->source = totalValue / totalTimes;
}

void Forest::ExchangeGhostData(p4est_t *p4est, p4est_ghost_t *ghostLayer, SQuadrantData *ghostData)
{
    std::vector<void*> mirrorsData(ghostLayer->mirrors.elem_count);

    for(size_t i = 0; i < ghostLayer->mirrors.elem_count; ++i)
    {
        p4est_quadrant_t *mirror = p4est_quadrant_array_index(&(ghostLayer->mirrors), i);
        int which_tree = mirror->p.piggy3.which_tree;
        p4est_tree_t *tree = p4est_tree_array_index(p4est->trees, which_tree);
        int which_quad = mirror->p.piggy3.local_num - tree->quadrants_offset;

        mirrorsData[i] = p4est_quadrant_array_index(&(tree->quadrants), which_quad)->p.user_data;
    }

    p4est_ghost_exchange_custom(p4est, ghostLayer, sizeof(SQuadrantData), mirrorsData.data(), ghostData);
}

void Forest::PartitionForest(p4est_t *p4est)
{
    SForestData *forestData = reinterpret_cast<SForestData*>(p4est->user_pointer);
    SQuadrantData *oldData = forestData->pQuadrantData;

    if (oldData == nullptr)
    {
        p4est_partition(p4est, 0, NULL);
        return;
    }

    std::vector<p4est_gloidx_t> cumulativeCountBefore(Globals::MPI::WorldSize + 1);

    for(int i = 0; i < Globals::MPI::WorldSize + 1; ++i)
    {
        cumulativeCountBefore[i] = p4est->global_first_quadrant[i];
    }

    p4est_partition(p4est, 0, NULL);

    SQuadrantData *newData = new SQuadrantData[p4est->local_num_quadrants];

    p4est_transfer_fixed(p4est->global_first_quadrant, cumulativeCountBefore.data(), Globals::MPI::Comm, 0, newData, oldData, sizeof(SQuadrantData));

    delete[] oldData;

    Forest::MapQuadrantDataToQuadrants(p4est, newData);
    forestData->pQuadrantData = newData;
}

void Forest::MapQuadrantDataToQuadrants(p4est_t *p4est, SQuadrantData *quadrantData)
{
    int count = 0;
    for(int treeId = p4est->first_local_tree; treeId <= p4est->last_local_tree; ++treeId)
    {
        p4est_tree_t * tree = p4est_tree_array_index(p4est->trees, treeId);

        for(size_t quadId = 0; quadId < tree->quadrants.elem_count; ++quadId)
        {
            p4est_quadrant_t *curQuad = p4est_quadrant_array_index(&(tree->quadrants), quadId);
            curQuad->p.user_data = quadrantData + count;
            reinterpret_cast<SQuadrantData*>(curQuad->p.user_data)->quadLocalOffset = count;    //saving quad offset on current proc
            ++count;
        }
    }

    P4EST_ASSERT(p4est->local_num_quadrants == count);
}

void Forest::RemapQuadrantDataAfterRefinement(p4est_t *p4est, SQuadrantData *quadrantData)
{
    int count = 0;
    for(int treeId = p4est->first_local_tree; treeId <= p4est->last_local_tree; ++treeId)
    {
        p4est_tree_t * tree = p4est_tree_array_index(p4est->trees, treeId);

        for(size_t quadId = 0; quadId < tree->quadrants.elem_count; ++quadId)
        {
            p4est_quadrant_t *curQuad = p4est_quadrant_array_index(&(tree->quadrants), quadId);
            SQuadrantData *qData = reinterpret_cast<SQuadrantData*>(curQuad->p.user_data);

            quadrantData[count] = *qData;

            if (qData->quadLocalOffset == -1)
            {
                P4EST_FREE(qData);
            }

            curQuad->p.user_data = quadrantData + count;
            reinterpret_cast<SQuadrantData*>(curQuad->p.user_data)->quadLocalOffset = count;
            ++count;
        }
    }

    P4EST_ASSERT(p4est->local_num_quadrants == count);
}
