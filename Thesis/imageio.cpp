#include "imageio.h"

#include <fstream>
#include <iostream>

#include "wimage.h"

static bool LoadPGMImageAscii(std::ifstream &instream, std::vector<unsigned char> &imageData)
{
    int maxIntensity, height, width = 0;
    std::string line;

    while(true)
    {
        std::getline(instream, line);

        if (line.size() == 0) continue;
        if (line[0] != '#') break;
    }

    instream.seekg(-static_cast<int>(line.size() + 1), std::ios_base::cur);

    instream >> width;
    instream >> height;
    instream >> maxIntensity;

    imageData.resize(width * height);
    WImage image(width, height, imageData.data());

    for(int j = 0; j < height; ++j)
    {
        for(int i = 0; i < width; ++i)
        {
            int buff;
            instream >> buff;
            image.SetPixel(j, i, static_cast<unsigned char>(buff));
        }
    }

    instream.close();

    return true;
}

static bool LoadPGMImageBinary(std::ifstream &instream, std::vector<unsigned char> &imageData)
{
    int maxIntensity, height, width = 0;
    std::string line;

    while(true)
    {
        std::getline(instream, line);

        if (line.size() == 0) continue;
        if (line[0] != '#') break;
    }

    instream.seekg(-static_cast<int>(line.size() + 1), std::ios_base::cur);

    instream >> width;
    instream >> height;
    instream >> maxIntensity;

    imageData.resize(width * height);
    WImage image(width, height, imageData.data());

    char bytesPerPixel = maxIntensity > 255 ? 2 : 1;

    if (bytesPerPixel == 2)
    {
        imageData.resize(0);
        instream.close();
        std::cout << "Cannot load .pgm image. Application supports 8-bit colors only!\n";
        return false;
    }

    for(int j = 0; j < height; ++j)
    {
        for(int i = 0; i < width; ++i)
        {
            char buff;
            instream.read(&buff, 1);
            image.SetPixel(j, i, static_cast<unsigned char>(buff));
        }
    }

    instream.close();

    return true;
}

bool ImageIO::LoadPGMImage(const char *imgPath, std::vector<unsigned char> &imageData)
{
    std::ifstream instream(imgPath);

    if (!instream.is_open()) { std::cout << "Image file could not be opened!\n"; return false; }

    std::string line;
    std::getline(instream, line);

    if (line == "P2")
    {
        return LoadPGMImageAscii(instream, imageData);
    }
    else if (line == "P5")
    {
        return LoadPGMImageBinary(instream, imageData);
    }
    else
    {
        instream.close();
        std::cout << "Unknown PGM format!" << std::endl;
        return false;
    }
}

SImageSize ImageIO::GetImageSizeFromPGMFile(const char *imgPath)
{
    SImageSize sizeBuff;
    std::ifstream in(imgPath);

    if (!in.is_open())
    {
        sizeBuff.height = 0;
        sizeBuff.width = 0;
        std::cout << "Image file " << imgPath << " could not be opened!\n";
        return sizeBuff;
    }

    std::string buff;
    std::getline(in, buff);

    while(true)
    {
        std::getline(in, buff);

        if (buff.size() == 0) continue;
        if (buff[0] != '#') break;
    }

    in.seekg(-static_cast<int>(buff.size() + 1), std::ios_base::cur);

    in >> sizeBuff.width >> sizeBuff.height;

    in.close();

    return sizeBuff;
}

static bool WritePGMImageAscii(const char *imgPath, const WImage &img)
{
    std::ofstream out(imgPath);

    if (!out.is_open()) return false;

    out << "P2" << std::endl;
    out << img.Width() << " " << img.Height() << std::endl;
    out << 255 << std::endl;

    for (int j = 0; j < img.Height(); ++j)
    {
        for(int i = 0; i < img.Width(); ++i)
        {
            out << static_cast<int>(img.GetPixel(j, i)) << " ";
        }

        out << std::endl;
    }

    out.close();
    return true;
}

static bool WritePGMImageBinary(const char *imgPath, const WImage &img)
{
    std::ofstream out(imgPath);

    if (!out.is_open()) return false;

    out << "P5" << std::endl;
    out << img.Width() << " " << img.Height() << std::endl;
    out << 255 << std::endl;

    for (int j = 0; j < img.Height(); ++j)
    {
        for(int i = 0; i < img.Width(); ++i)
        {
            out.write(reinterpret_cast<const char *>(&(img.GetPixel(j, i))), 1);
        }
    }

    out.close();
    return true;
}

bool ImageIO::WritePGMImage(const char *imgPath, const WImage &img, bool ascii)
{
    if (ascii)
    {
        return WritePGMImageAscii(imgPath, img);
    }
    else return WritePGMImageBinary(imgPath, img);
}
