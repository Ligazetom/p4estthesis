#include "processingoperation.h"
#include "forest.h"
#include "utility.h"
#include "globals.h"
#include "mpiutils.h"
#include "core.h"

#include <cmath>

////////////////////////////////////
/// *Private* functions - START
////////////////////////////////////

static float ComputeBackgroundWeight(unsigned char threshold, const unsigned int (&histogram)[256], unsigned int pixelCount, int &backgroundSum)
{
    backgroundSum = 0;

    for(int i = 0; i < threshold; ++i)
    {
        backgroundSum += histogram[i];
    }

    return backgroundSum / static_cast<float>(pixelCount);
}

static float ComputeForegroundWeight(unsigned char threshold, const unsigned int (&histogram)[256], unsigned int pixelCount, int &foregroundSum)
{
    foregroundSum = 0;

    for(int i = 255; i >= threshold; --i)
    {
        foregroundSum += histogram[i];
    }

    return foregroundSum / static_cast<float>(pixelCount);
}

static float ComputeBackgroundMean(unsigned char threshold, const unsigned int (&histogram)[256], int backgroundSum)
{
    int sum = 0;

    for(int i = 0; i < threshold; ++i)
    {
        sum += histogram[i] * i;
    }

    return sum / static_cast<float>(backgroundSum);
}

static float ComputeForegroundMean(unsigned char threshold, const unsigned int (&histogram)[256], int foregroundSum)
{
    int sum = 0;

    for(int i = 255; i >= threshold; --i)
    {
        sum += histogram[i] * i;
    }

    return sum / static_cast<float>(foregroundSum);
}


////////////////////////////////////
/// *Private* functions - END
////////////////////////////////////

void ProcessingOperation::ComputeHistogram(const WImage &img, unsigned int (&histogram)[256])
{
    const unsigned char * rawData = img.GetRawData();
    memset(histogram, 0, 256 * sizeof(unsigned int));

    for(size_t i = 0; i < img.GetNumOfPixels(); ++i)
    {
        ++histogram[rawData[i]];
    }
}

void ProcessingOperation::ComputeThresholdGlobalOTSU(p4est_t *p4est, unsigned int imagePixelCount)
{
    SForestData *forestData = reinterpret_cast<SForestData*>(p4est->user_pointer);
    float maxVariance = 0.f;
    unsigned char threshold = 0;
    float backgroundMeanFinal = 0.f;

    for(unsigned int i = 0; i < 256; ++i)
    {
        unsigned char tempThreshold = static_cast<unsigned char>(i);
        int sumBuff = 0;

        float backgroundWeight = ComputeBackgroundWeight(tempThreshold, forestData->histogram, imagePixelCount, sumBuff);
        float backgroundMean = ComputeBackgroundMean(tempThreshold, forestData->histogram, sumBuff);

        float foregroundWeight = ComputeForegroundWeight(tempThreshold, forestData->histogram, imagePixelCount, sumBuff);
        float foregroundMean = ComputeForegroundMean(tempThreshold, forestData->histogram, sumBuff);

        float variance = backgroundWeight * foregroundWeight * (backgroundMean - foregroundMean) * (backgroundMean - foregroundMean);

        if (variance > maxVariance)
        {
            maxVariance = variance;
            threshold = i;
            backgroundMeanFinal = backgroundMean;
        }
    }

    forestData->forestValueArray[ForestValue::GLOBAL_OTSU_THRESHOLD] = threshold;
    forestData->forestValueArray[ForestValue::GLOBAL_OTSU_BACKGROUND_MEAN] = static_cast<unsigned char>(round(backgroundMeanFinal));
}

void ProcessingOperation::ExtendImage(const WImage &originalImage, std::vector<unsigned char> &extendedImageData, const SImageSize &extendedImageSize, int edgeRadius, unsigned char extraPixelValue)
{
    extendedImageData.resize(extendedImageSize.width * extendedImageSize.height);
    WImage extendedImage(extendedImageSize.width, extendedImageSize.height, extendedImageData.data());

    //copy original
    for(int j = edgeRadius; j < originalImage.Height(); ++j)
    {
        for(int i = edgeRadius; i < originalImage.Width(); ++i)
        {
            extendedImage.SetPixel(j, i, originalImage.GetPixel(j, i));
        }
    }

    //fill 2^n extended values
    for(int j = originalImage.Height() + edgeRadius; j < extendedImage.Height() - edgeRadius; ++j)
    {
        for(int i = originalImage.Width() + edgeRadius; i < extendedImage.Width() - edgeRadius; ++i)
        {
            extendedImage.SetPixel(j, i, extraPixelValue);
        }
    }

    //mirror edges

    //top
    for(int fromJ = edgeRadius, toJ = edgeRadius - 1; fromJ < 2 * edgeRadius; ++fromJ, --toJ)
    {
        for(int i = edgeRadius; i < extendedImage.Width() - edgeRadius; ++i)
        {
            extendedImage.SetPixel(toJ, i, extendedImage.GetPixel(fromJ, i));
        }
    }

    //bot
    for(int fromJ = extendedImage.Height() - edgeRadius * 2, toJ = extendedImage.Height() - 1; fromJ < extendedImage.Height() - edgeRadius; ++fromJ, --toJ)
    {
        for(int i = edgeRadius; i < extendedImage.Width() - edgeRadius; ++i)
        {
            extendedImage.SetPixel(toJ, i, extendedImage.GetPixel(fromJ, i));
        }
    }

    //left
    for(int j = edgeRadius; j < extendedImage.Height() - edgeRadius; ++j)
    {
        for(int fromI = edgeRadius, toI = edgeRadius - 1; fromI < 2 * edgeRadius; ++fromI, --toI)
        {
            extendedImage.SetPixel(j, toI, extendedImage.GetPixel(j, fromI));
        }
    }

    //right
    for(int j = edgeRadius; j < extendedImage.Height() - edgeRadius; ++j)
    {
        for(int fromI = extendedImage.Width() - edgeRadius * 2, toI = extendedImage.Width() - 1; fromI < extendedImage.Width() - edgeRadius; ++fromI, --toI)
        {
            extendedImage.SetPixel(j, toI, extendedImage.GetPixel(j, fromI));
        }
    }

    //leftTop
    for(int fromJ = edgeRadius, toJ = edgeRadius - 1; fromJ < edgeRadius * 2; ++fromJ, --toJ)
    {
        for(int fromI = edgeRadius, toI = edgeRadius - 1; fromI < edgeRadius * 2; ++fromI, --toI)
        {
            extendedImage.SetPixel(toJ, toI, extendedImage.GetPixel(fromJ, fromI));
        }
    }

    //rightTop
    for(int fromJ = edgeRadius, toJ = edgeRadius - 1; fromJ < edgeRadius * 2; ++fromJ, --toJ)
    {
        for(int fromI = extendedImage.Width() - edgeRadius * 2, toI = extendedImage.Width() - 1; fromI < extendedImage.Width() - edgeRadius; ++fromI, --toI)
        {
            extendedImage.SetPixel(toJ, toI, extendedImage.GetPixel(fromJ, fromI));
        }
    }

    //leftBot
    for(int fromJ = extendedImage.Height() - edgeRadius * 2, toJ = extendedImage.Height() - 1; fromJ < extendedImage.Height() - edgeRadius; ++fromJ, --toJ)
    {
        for(int fromI = edgeRadius, toI = edgeRadius - 1; fromI < edgeRadius * 2; ++fromI, --toI)
        {
            extendedImage.SetPixel(toJ, toI, extendedImage.GetPixel(fromJ, fromI));
        }
    }

    //rightBot
    for(int fromJ = extendedImage.Height() - edgeRadius * 2, toJ = extendedImage.Height() - 1; fromJ < extendedImage.Height() - edgeRadius; ++fromJ, --toJ)
    {
        for(int fromI = extendedImage.Width() - edgeRadius * 2, toI = extendedImage.Width() - 1; fromI < extendedImage.Width() - edgeRadius; ++fromI, --toI)
        {
            extendedImage.SetPixel(toJ, toI, extendedImage.GetPixel(fromJ, fromI));
        }
    }
}

//fragment original image to square pieces, linearly in contiguous memory to be sent to processes with corresponding trees
void ProcessingOperation::FragmentImage(const WImage &originalImg,
                                   std::vector<unsigned char> &fractionedImageData,
                                   const std::vector<SImageCoords> &coords,
                                   const int fragmentSideLength,
                                   const int edgeRadius)
{
    int extendedSide = fragmentSideLength + 2 * edgeRadius;
    int bytesPerFragment = extendedSide * extendedSide;
    int imageSideLength = fragmentSideLength + edgeRadius * 2;

    fractionedImageData.resize(coords.size() * bytesPerFragment);

    for(size_t quad = 0; quad < coords.size(); ++quad)
    {        
        WImage buff(imageSideLength, imageSideLength, fractionedImageData.data() + quad * bytesPerFragment);
        int x = coords[quad].x + edgeRadius;
        int y = coords[quad].y + edgeRadius;

        for(int gRow = y - edgeRadius, lRow = 0; gRow < y + fragmentSideLength + edgeRadius; ++gRow, ++lRow)
        {
            for(int gCol = x - edgeRadius, lCol = 0; gCol < x + fragmentSideLength + edgeRadius; ++gCol, ++lCol)
            {
                buff.SetPixel(lRow, lCol, originalImg.GetPixel(gRow, gCol));
            }
        }
    }
}

///////////////////////////////
/// FlatThreshold functions ///
///////////////////////////////
void ProcessingOperation::FlatThreshold(p4est_t *p4est, size_t algorithmIndex)
{
    SFlatThresholdParams *params = reinterpret_cast<SFlatThresholdParams*>(Globals::Processing::AlgorithmParams[algorithmIndex]);

    p4est_iterate(p4est, NULL, params, ProcessingOperation::FlatThresholdVolumeCallback, NULL, NULL);
}

void ProcessingOperation::FlatThresholdVolumeCallback(p4est_iter_volume_info_t *info, void *user_data)
{
    SFlatThresholdParams *params = reinterpret_cast<SFlatThresholdParams*>(user_data);
    SQuadrantData *qData = reinterpret_cast<SQuadrantData*>(info->quad->p.user_data);

    if (qData->pipeline[params->pipelineIN].volumeData.value >= params->threshold)
    {
        qData->pipeline[params->pipelineOUT].volumeData.value = qData->pipeline[params->pipelineIN].volumeData.maxValue;
    }
    else
    {
        qData->pipeline[params->pipelineOUT].volumeData.value = 0.;
    }

    qData->pipeline[params->pipelineOUT].volumeData.maxValue = qData->pipeline[params->pipelineIN].volumeData.maxValue;
}

//////////////////////////////////
/// ExpandRefinement functions ///
//////////////////////////////////
void ProcessingOperation::ExpandRefinement(p4est_t *p4est)
{
    int recursive = 1;
    p4est_refine_ext(p4est, recursive, -1, ExpandRefinementRefineCallback, ExpandRefinementInitCallback, ExpandRefinementReplaceCallback);
    p4est_balance_ext(p4est, P4EST_CONNECT_FACE, ExpandRefinementInitCallback, ExpandRefinementReplaceCallback);

    SQuadrantData *tempData = new SQuadrantData[p4est->local_num_quadrants];
    Forest::RemapQuadrantDataAfterRefinement(p4est, tempData);

    SForestData *forestData = reinterpret_cast<SForestData*>(p4est->user_pointer);
    delete[] forestData->pQuadrantData;
    forestData->pQuadrantData = tempData;
}

int ProcessingOperation::ExpandRefinementRefineCallback(p4est_t*, p4est_topidx_t, p4est_quadrant_t *quadrant)
{
    return quadrant->level == Globals::Refinement::MaxRefinementLevel - 1;
}

void ProcessingOperation::ExpandRefinementInitCallback(p4est_t*, p4est_topidx_t, p4est_quadrant_t *quadrant)
{
    quadrant->p.user_data = P4EST_ALLOC(SQuadrantData, 1);
    reinterpret_cast<SQuadrantData*>(quadrant->p.user_data)->quadLocalOffset = -1;
}

void ProcessingOperation::ExpandRefinementReplaceCallback(p4est_t*, p4est_topidx_t, int, p4est_quadrant_t *outgoing[], int num_incoming, p4est_quadrant_t *incoming[])
{
    //during refining outgoing is always 1 and incoming always 4

    for (int i = 0; i < num_incoming; ++i)
    {
        SQuadrantData *qData = reinterpret_cast<SQuadrantData*>(incoming[i]->p.user_data);
        int buff = qData->quadLocalOffset;

        *qData = *reinterpret_cast<SQuadrantData*>(outgoing[0]->p.user_data);
        qData->quadLocalOffset = buff;
    }
}

///////////////////////////////////
/// RefineByThreshold functions ///
///////////////////////////////////
void ProcessingOperation::RefineByThreshold(p4est_t *p4est)
{
    int recursive = 1;
    p4est_refine_ext(p4est, recursive, -1, RefineByThresholdCallback, RefineByThresholdInitCallback, RefineByThresholdReplaceCallback);
    p4est_balance_ext(p4est, P4EST_CONNECT_FACE, RefineByThresholdInitCallback, RefineByThresholdReplaceCallback);

    SQuadrantData *tempData = new SQuadrantData[p4est->local_num_quadrants];
    Forest::RemapQuadrantDataAfterRefinement(p4est, tempData);

    SForestData *forestData = reinterpret_cast<SForestData*>(p4est->user_pointer);
    delete[] forestData->pQuadrantData;
    forestData->pQuadrantData = tempData;
}

int ProcessingOperation::RefineByThresholdCallback(p4est_t*, p4est_topidx_t, p4est_quadrant_t *quadrant)
{
    if (quadrant->level >= Globals::Refinement::MaxRefinementLevel) return 0;

     SRefineByThresholdParams *params = reinterpret_cast<SRefineByThresholdParams*>(Globals::Processing::AlgorithmParams[Globals::Processing::CurrentAlgorithmIndex]);
     SQuadrantData *qData = reinterpret_cast<SQuadrantData*>(quadrant->p.user_data);

     return qData->pipeline[params->pipeline].volumeData.value >= params->threshold;
}

void ProcessingOperation::RefineByThresholdInitCallback(p4est_t*, p4est_topidx_t, p4est_quadrant_t *quadrant)
{
    quadrant->p.user_data = P4EST_ALLOC(SQuadrantData, 1);
    reinterpret_cast<SQuadrantData*>(quadrant->p.user_data)->quadLocalOffset = -1;
}

void ProcessingOperation::RefineByThresholdReplaceCallback(p4est_t*, p4est_topidx_t, int, p4est_quadrant_t *outgoing[], int num_incoming, p4est_quadrant_t *incoming[])
{
    //during refining outgoing is always 1 and incoming always 4
    SQuadrantData *parentData = reinterpret_cast<SQuadrantData*>(outgoing[0]->p.user_data);

    for (int i = 0; i < num_incoming; ++i)
    {
        SQuadrantData *qData = reinterpret_cast<SQuadrantData*>(incoming[i]->p.user_data);
        int buff = qData->quadLocalOffset;

        *qData = *parentData;
        qData->quadLocalOffset = buff;
    }

    if (parentData->quadLocalOffset == -1) P4EST_FREE(outgoing[0]->p.user_data);    //this quadrant was already refined by us before
}

////////////////////////
/// Square functions ///
////////////////////////
void ProcessingOperation::Square(p4est_t *p4est, size_t algorithmIndex)
{
    SSquareParams *params = reinterpret_cast<SSquareParams*>(Globals::Processing::AlgorithmParams[algorithmIndex]);

    p4est_iterate(p4est, NULL, params, ProcessingOperation::SquareVolumeCallback, NULL, NULL);
}

void ProcessingOperation::SquareVolumeCallback(p4est_iter_volume_info_t *info, void *user_data)
{
    SSquareParams *params = reinterpret_cast<SSquareParams*>(user_data);
    SQuadrantData *qData = reinterpret_cast<SQuadrantData*>(info->quad->p.user_data);
    qData->pipeline[params->pipelineOUT].volumeData.value = qData->pipeline[params->pipelineIN].volumeData.value * qData->pipeline[params->pipelineIN].volumeData.value;
    qData->pipeline[params->pipelineOUT].volumeData.maxValue = qData->pipeline[params->pipelineIN].volumeData.maxValue * qData->pipeline[params->pipelineIN].volumeData.maxValue;
}

/////////////////////
/// Max functions ///
/////////////////////
void ProcessingOperation::Max(p4est_t *p4est, size_t algorithmIndex)
{
    SMaxParams *params = reinterpret_cast<SMaxParams*>(Globals::Processing::AlgorithmParams[algorithmIndex]);

    p4est_iterate(p4est, NULL, params, ProcessingOperation::MaxVolumeCallback, NULL, NULL);
}

void ProcessingOperation::MaxVolumeCallback(p4est_iter_volume_info_t *info, void *user_data)
{
    SMaxParams *params = reinterpret_cast<SMaxParams*>(user_data);
    SQuadrantData *qData = reinterpret_cast<SQuadrantData*>(info->quad->p.user_data);

    double val1 = qData->pipeline[params->pipelineIN1].volumeData.value;
    double val2 = qData->pipeline[params->pipelineIN2].volumeData.value;

    if (val1 > val2)
    {
        qData->pipeline[params->pipelineOUT].volumeData.value = val1;
        qData->pipeline[params->pipelineOUT].volumeData.maxValue = qData->pipeline[params->pipelineIN1].volumeData.maxValue;
    }
    else
    {
        qData->pipeline[params->pipelineOUT].volumeData.value = val2;
        qData->pipeline[params->pipelineOUT].volumeData.maxValue = qData->pipeline[params->pipelineIN1].volumeData.maxValue;
    }
}

/////////////////////////////
/// LHEExplicit functions ///
/////////////////////////////
void ProcessingOperation::LHEExplicit(p4est_t *p4est, size_t algorithmIndex)
{
    SLHEExplicitParams *params = reinterpret_cast<SLHEExplicitParams*>(Globals::Processing::AlgorithmParams[algorithmIndex]);
    SVTKExportData exportData;
    exportData.exportArray = nullptr;

    p4est_ghost_t *ghostLayer = p4est_ghost_new(p4est, P4EST_CONNECT_FACE);
    SQuadrantData *ghostData = P4EST_ALLOC(SQuadrantData, ghostLayer->ghosts.elem_count);

    params->ghostData = ghostData;
    p4est_iterate(p4est, NULL, params, ProcessingOperation::LHEExplicitInitializeVolumeCallback, NULL, NULL);

    if (params->bExportAnimation)
    {
        exportData.exportArray = sc_array_new_count(sizeof(double), p4est->local_num_quadrants);
        exportData.exportBufferIndex = LHEExplicitBuffs::CURRENT_TIME_ITER;
    }

    for(int iter = 0; iter < params->numOfSteps; ++iter)
    {
        Forest::ExchangeGhostData(p4est, ghostLayer, ghostData);
        p4est_iterate(p4est, ghostLayer, params, ProcessingOperation::LHEExplicitVolumeCallback, ProcessingOperation::LHEExplicitFaceCallback, NULL);

        if (params->bRefine && iter % params->numOfIterationsToRefineAfter == 0 && iter != 0)
        {
            //TODO
        }

        if (params->bExportAnimation)
        {
            p4est_iterate(p4est, NULL, &exportData, Forest::ExtractQuadrantExportVTKBufferCallback, NULL, NULL);
            Core::WriteVtkAnimationOutput(p4est, params->animationFileName.c_str(), iter, exportData.exportArray);
        }
    }

    p4est_iterate(p4est, ghostLayer, params, ProcessingOperation::LHEExplicitFinalizeVolumeCallback, NULL, NULL);

    if (params->bRefine)    //always refine last step
    {
        //TODO
    }

    if (params->bExportAnimation)
    {
        p4est_iterate(p4est, NULL, &exportData, Forest::ExtractQuadrantExportVTKBufferCallback, NULL, NULL);

        Core::WriteVtkAnimationOutput(p4est, params->animationFileName.c_str(), params->numOfSteps, exportData.exportArray);

        sc_array_destroy(exportData.exportArray);
    }

    p4est_ghost_destroy(ghostLayer);
    P4EST_FREE(ghostData);
}

void ProcessingOperation::LHEExplicitInitializeVolumeCallback(p4est_iter_volume_info_t *info, void *user_data)
{
    SQuadrantData *quadData = reinterpret_cast<SQuadrantData*>(info->quad->p.user_data);
    SLHEExplicitParams *params = reinterpret_cast<SLHEExplicitParams*>(user_data);

    quadData->buffers[LHEExplicitBuffs::CURRENT_TIME_ITER] = quadData->pipeline[params->pipelineIN].volumeData.value;
    quadData->buffers[LHEExplicitBuffs::SUM_BUFFER] = 0.;
}

void ProcessingOperation::LHEExplicitVolumeCallback(p4est_iter_volume_info_t *info, void *user_data)
{
    int quadSideLength = Globals::Forest::TreeSideLength >> info->quad->level;
    SQuadrantData *qData = reinterpret_cast<SQuadrantData*>(info->quad->p.user_data);
    SLHEExplicitParams *params = reinterpret_cast<SLHEExplicitParams*>(user_data);

    qData->buffers[LHEExplicitBuffs::CURRENT_TIME_ITER] = qData->buffers[LHEExplicitBuffs::CURRENT_TIME_ITER] +
                                                          (params->timeStep / (quadSideLength * quadSideLength)) * qData->buffers[LHEExplicitBuffs::SUM_BUFFER];
    qData->buffers[LHEExplicitBuffs::SUM_BUFFER] = 0.;   //serves as buffer for partial sums
}

void ProcessingOperation::LHEExplicitFaceCallback(p4est_iter_face_info_t *info, void *user_data)
{
    void* ghostData = reinterpret_cast<SLHEExplicitParams*>(user_data)->ghostData;

    if (info->sides.elem_count == 1)        //edge of image, partial sum is zero --> nothing to do
    {
    }
    else if (info->sides.elem_count == 2)
    {
        p4est_iter_face_side_t* sides = reinterpret_cast<p4est_iter_face_side_t*>(info->sides.array);
        SQuadrantData *qData[4];     //qData[0], qData[1] are for sides[0], qData[2], qData[3] are for sides[1], if both faces are full, only qData[0] and qData[2] are populated
        int numOfQuadrants = 2;
        Utility::RetrieveQuadrantDataFromFaceSides(sides, qData, numOfQuadrants, ghostData);

        if (numOfQuadrants == 2)    //both quadrants are full
        {
            double buff = qData[0]->buffers[LHEExplicitBuffs::CURRENT_TIME_ITER] - qData[2]->buffers[LHEExplicitBuffs::CURRENT_TIME_ITER];
            qData[2]->buffers[LHEExplicitBuffs::SUM_BUFFER] += buff;
            qData[0]->buffers[LHEExplicitBuffs::SUM_BUFFER] += -buff;
        }
        else if (numOfQuadrants == 3) //there was a hanging face
        {
            int hangingIndex = sides[0].is_hanging ? 0 : 2;
            int fullIndex = hangingIndex == 2 ? 0 : 2;
            double buff1, buff2;

            buff1 = (qData[fullIndex]->buffers[LHEExplicitBuffs::CURRENT_TIME_ITER] - qData[hangingIndex]->buffers[LHEExplicitBuffs::CURRENT_TIME_ITER]) * (2./3.);
            buff2 = (qData[fullIndex]->buffers[LHEExplicitBuffs::CURRENT_TIME_ITER] - qData[hangingIndex + 1]->buffers[LHEExplicitBuffs::CURRENT_TIME_ITER]) * (2./3.);
            qData[hangingIndex]->buffers[LHEExplicitBuffs::SUM_BUFFER] += buff1;
            qData[hangingIndex + 1]->buffers[LHEExplicitBuffs::SUM_BUFFER] += buff2;
            qData[fullIndex]->buffers[LHEExplicitBuffs::SUM_BUFFER] += -buff1 - buff2;
        }
    }
}

void ProcessingOperation::LHEExplicitFinalizeVolumeCallback(p4est_iter_volume_info_t *info, void *user_data)
{
    int quadSideLength = Globals::Forest::TreeSideLength >> info->quad->level;
    SQuadrantData *qData = reinterpret_cast<SQuadrantData*>(info->quad->p.user_data);
    SLHEExplicitParams *params = reinterpret_cast<SLHEExplicitParams*>(user_data);

    qData->buffers[LHEExplicitBuffs::CURRENT_TIME_ITER] = qData->buffers[LHEExplicitBuffs::CURRENT_TIME_ITER] +
                                                           (params->timeStep / (quadSideLength * quadSideLength)) * qData->buffers[LHEExplicitBuffs::SUM_BUFFER];
    qData->buffers[LHEExplicitBuffs::SUM_BUFFER] = 0.;   //serves as buffer for partial sums

    qData->pipeline[params->pipelineOUT].volumeData.value = qData->buffers[LHEExplicitBuffs::CURRENT_TIME_ITER];
    qData->pipeline[params->pipelineOUT].volumeData.maxValue = qData->pipeline[params->pipelineIN].volumeData.maxValue; //once the max value changes every successive pipeline must preserve it
}

/////////////////////////////
/// Niblack functions ///
/////////////////////////////
void ProcessingOperation::Niblack(p4est_t *p4est, size_t algorithmIndex)
{
    SNiblackThresholdParams *params = reinterpret_cast<SNiblackThresholdParams*>(Globals::Processing::AlgorithmParams[algorithmIndex]);

    p4est_ghost_t *ghostLayer = nullptr;
    SQuadrantData *ghostData = nullptr;

    if (params->radius > 0)
    {
        ghostLayer = p4est_ghost_new(p4est, P4EST_CONNECT_FULL);        

        for(int i = 1; i < params->radius; ++i)
        {
            p4est_ghost_expand(p4est, ghostLayer);
        }

        ghostData = P4EST_ALLOC(SQuadrantData, ghostLayer->ghosts.elem_count);

        Forest::ExchangeGhostData(p4est, ghostLayer, ghostData);
    }

    params->ghostData = ghostData;

    if (params->radius > 0) p4est_iterate(p4est, ghostLayer, params, ProcessingOperation::NiblackNonZeroRadiusVolumeCallback, NULL, NULL);
    else p4est_iterate(p4est, NULL, params, ProcessingOperation::NiblackZeroRadiusVolumeCallback, NULL, NULL);

    if (params->radius > 0)
    {
        p4est_ghost_destroy(ghostLayer);
        P4EST_FREE(ghostData);
    }
}

void ProcessingOperation::NiblackZeroRadiusVolumeCallback(p4est_iter_volume_info_t *info, void *user_data)
{
    SNiblackThresholdParams *params = reinterpret_cast<SNiblackThresholdParams*>(user_data);
    SQuadrantData *qData = reinterpret_cast<SQuadrantData*>(info->quad->p.user_data);

    double u = qData->pipeline[params->pipelineINProcessing].volumeData.value;
    double uSqrd = qData->pipeline[params->pipelineINProcessingSquared].volumeData.value;
    double sigmaR = sqrt(uSqrd - (u * u));
    if(sigmaR < 20 && u<70) sigmaR+= +30;
    if(sigmaR < 20 && u>=70) sigmaR+=params->meanOffset;

    if (sigmaR<0)sigmaR=0;
    if (sigmaR>255)sigmaR=255;
    double threshold = u + params->kappa * sigmaR;

    qData->pipeline[params->pipelineOUT].volumeData.value = qData->pipeline[params->pipelineINThresholding].volumeData.value >= threshold ? 255 : 0;
    qData->pipeline[params->pipelineOUT].volumeData.maxValue = 255.; //thresholding algorithms always return 0-255 no matter what the input is
}

void ProcessingOperation::NiblackNonZeroRadiusVolumeCallback(p4est_iter_volume_info_t *info, void *user_data)
{
    SNiblackThresholdParams *params = reinterpret_cast<SNiblackThresholdParams*>(user_data);
    SQuadrantData *qData = reinterpret_cast<SQuadrantData*>(info->quad->p.user_data);
    std::vector<SQuadrantDataSeparated> regionQuads(1);

    SQuadrantDataSeparated temp;    //we don't get the quadrant itself from 'GetQuadrantsInRadius' function. It's on purpose because the function can be given even ghost quadrant
    temp.data = qData;              //as input and that means we don't get the local data and it shall be searched for in ghost layer, so it's easier to just add it ourselves
    temp.quadrant = *(info->quad);
    regionQuads[0] = temp;

    Forest::GetQuadrantsInRadius(info->p4est, info->ghost_layer, reinterpret_cast<SQuadrantData*>(params->ghostData), info->quad, info->treeid, params->radius, regionQuads);

    double avgVal = 0.;
    double avgValPower2 = 0.;

    for(size_t i = 0; i < regionQuads.size(); ++i)
    {
        avgVal = regionQuads[i].data->pipeline[params->pipelineINProcessing].volumeData.value;
        avgValPower2 = regionQuads[i].data->pipeline[params->pipelineINProcessing].volumeData.value * regionQuads[i].data->pipeline[params->pipelineINProcessing].volumeData.value;
    }

    avgVal /= regionQuads.size();
    avgValPower2 /= regionQuads.size();

    double threshold = avgVal + params->kappa * sqrt(avgValPower2 - (avgVal * avgVal)) + params->meanOffset;

    qData->pipeline[params->pipelineOUT].volumeData.value = qData->pipeline[params->pipelineINThresholding].volumeData.value >= threshold ? 255 : 0;
    qData->pipeline[params->pipelineOUT].volumeData.maxValue = 255.; //thresholding algorithms always return 0-255 no matter what the input is
}

/////////////////////////////////
///    LHEImplicit functions  ///
/////////////////////////////////
void ProcessingOperation::LHEImplicit(p4est_t *p4est, size_t algorithmIndex)
{
    SLHEImplicitParams *params = reinterpret_cast<SLHEImplicitParams*>(Globals::Processing::AlgorithmParams[algorithmIndex]);
    SVTKExportData exportData;
    exportData.exportArray = nullptr;

    p4est_ghost_t *ghostLayer = p4est_ghost_new(p4est, P4EST_CONNECT_FACE);
    SQuadrantData *ghostData = P4EST_ALLOC(SQuadrantData, ghostLayer->ghosts.elem_count);    
    double *residualsSquared = new double[p4est->local_num_quadrants];

    params->ghostData = ghostData;    
    params->residualsSquared = residualsSquared;

    p4est_iterate(p4est, NULL, params, ProcessingOperation::LHEImplicitInitializeVolumeCallback, NULL, NULL);
    Forest::ExchangeGhostData(p4est, ghostLayer, ghostData);
    p4est_iterate(p4est, ghostLayer, params, NULL, ProcessingOperation::LHEImplicitSigmaUpdateFaceCallback, NULL);

    if (params->bExportAnimation)
    {
        exportData.exportArray = sc_array_new_count(sizeof(double), p4est->local_num_quadrants);
        exportData.exportBufferIndex = LHEImplicitBuffs::PREVIOUS_TIME_ITER;

        p4est_iterate(p4est, NULL, &exportData, Forest::ExtractQuadrantExportVTKBufferCallback, NULL, NULL);

        Core::WriteVtkAnimationOutput(p4est, params->animationFileName.c_str(), 0, exportData.exportArray);
    }

    for(int i = 0; i < params->numOfSteps; ++i)
    {        
        double currentRes = 0.;

        do
        {
            p4est_iterate(p4est, NULL, params, ProcessingOperation::LHEImplicitJacobiIterationUpdateVolumeCallback, NULL, NULL);
            Forest::ExchangeGhostData(p4est, ghostLayer, ghostData);
            p4est_iterate(p4est, ghostLayer, params, NULL, ProcessingOperation::LHEImplicitSigmaUpdateFaceCallback, NULL);
            p4est_iterate(p4est, NULL, params, ProcessingOperation::LHEImplicitComputeResidualsVolumeCallback, NULL, NULL);

            double sumResSquared = 0.;
            for (int i = 0; i < p4est->local_num_quadrants; ++i)
            {
                sumResSquared += residualsSquared[i];
            }

            MPI_Allreduce(&sumResSquared, &currentRes, 1, MPI_DOUBLE, MPI_SUM, Globals::MPI::Comm);

            currentRes = sqrt(currentRes);
        }
        while(currentRes > params->tol);

        p4est_iterate(p4est, NULL, NULL, ProcessingOperation::LHEImplicitTimeIterationVolumeCallback, NULL, NULL);

        if (params->bRefine && i % params->numOfIterationsToRefineAfter == 0 && i != 0)
        {
            //TODO
        }

        if (params->bExportAnimation)
        {
            p4est_iterate(p4est, NULL, &exportData, Forest::ExtractQuadrantExportVTKBufferCallback, NULL, NULL);

            Core::WriteVtkAnimationOutput(p4est, params->animationFileName.c_str(), i + 1, exportData.exportArray);
        }
    }

    p4est_iterate(p4est, NULL, params, ProcessingOperation::LHEImplicitFinalizeVolumeCallback, NULL, NULL);

    if (params->bRefine && params->numOfSteps % params->numOfIterationsToRefineAfter != 0)  //always refine last, but not if we already did inside of a loop
    {
        //TODO
        //dont have to care about ghost data anymore
    }

    if (params->bExportAnimation)
    {
        sc_array_destroy(exportData.exportArray);
    }

    P4EST_FREE(ghostData);
    p4est_ghost_destroy(ghostLayer);
    delete[] residualsSquared;
}

void ProcessingOperation::LHEImplicitInitializeVolumeCallback(p4est_iter_volume_info_t *info, void *user_data)
{
    SQuadrantData *quadData = reinterpret_cast<SQuadrantData*>(info->quad->p.user_data);
    SLHEImplicitParams *params = reinterpret_cast<SLHEImplicitParams*>(user_data);

    quadData->buffers[LHEImplicitBuffs::CURRENT_JACOBI_ITER] = quadData->pipeline[params->pipelineIN].volumeData.value;
    quadData->buffers[LHEImplicitBuffs::PREVIOUS_TIME_ITER] = quadData->pipeline[params->pipelineIN].volumeData.value;
}

void ProcessingOperation::LHEImplicitSigmaUpdateFaceCallback(p4est_iter_face_info_t *info, void *user_data)
{
    void* ghostData = reinterpret_cast<SLHEImplicitParams*>(user_data)->ghostData;

    if (info->sides.elem_count == 1)        //edge of image, partial sum is zero --> nothing to do
    {
        p4est_iter_face_side_t* sides = reinterpret_cast<p4est_iter_face_side_t*>(info->sides.array);
        SQuadrantData *qData = reinterpret_cast<SQuadrantData*>(sides[0].is.full.quad->p.user_data);

        qData->edgeData[sides[0].face].sigma = qData->buffers[LHEImplicitBuffs::CURRENT_JACOBI_ITER];
    }
    else if (info->sides.elem_count == 2)
    {
        p4est_iter_face_side_t* sides = reinterpret_cast<p4est_iter_face_side_t*>(info->sides.array);
        SQuadrantData *qData[4];     //qData[0], qData[1] are for sides[0], qData[2], qData[3] are for sides[1], if both faces are full, only qData[0] and qData[2] are populated
        int numOfQuadrants = 2;
        Utility::RetrieveQuadrantDataFromFaceSides(sides, qData, numOfQuadrants, ghostData);

        if (numOfQuadrants == 2)    //both quadrants are full
        {
            double buff = (qData[0]->buffers[LHEImplicitBuffs::CURRENT_JACOBI_ITER] + qData[2]->buffers[LHEImplicitBuffs::CURRENT_JACOBI_ITER]) / 2.;
            qData[0]->edgeData[sides[0].face].sigma = buff;
            qData[2]->edgeData[sides[1].face].sigma = buff;
        }
        else if (numOfQuadrants == 3) //there was a hanging face
        {
            int hangingDataIndex = 2;       //default if sides[1].is_hanging == true
            int fullDataIndex = 0;          //
            int hangingSideIndex = 1;       //
            int fullSideIndex = 0;          //

            if (sides[0].is_hanging)
            {
                hangingDataIndex = 0;
                fullDataIndex = 2;
                hangingSideIndex = 0;
                fullSideIndex = 1;
            }

            double buff = (1./3.) * qData[fullDataIndex]->buffers[LHEImplicitBuffs::CURRENT_JACOBI_ITER];

            qData[fullDataIndex]->edgeData[sides[fullSideIndex].face].sigma = (1./3.) * (qData[hangingDataIndex]->buffers[LHEImplicitBuffs::CURRENT_JACOBI_ITER] +
                                                     qData[hangingDataIndex + 1]->buffers[LHEImplicitBuffs::CURRENT_JACOBI_ITER]) + buff;
            qData[hangingDataIndex]->edgeData[sides[hangingSideIndex].face].sigma = buff + (2./3.) * qData[hangingDataIndex]->buffers[LHEImplicitBuffs::CURRENT_JACOBI_ITER];
            qData[hangingDataIndex + 1]->edgeData[sides[hangingSideIndex].face].sigma = buff + (2./3.) * qData[hangingDataIndex + 1]->buffers[LHEImplicitBuffs::CURRENT_JACOBI_ITER];
        }
    }
}

void ProcessingOperation::LHEImplicitJacobiIterationUpdateVolumeCallback(p4est_iter_volume_info_t *info, void *user_data)
{
    SLHEImplicitParams *params = reinterpret_cast<SLHEImplicitParams*>(user_data);
    SQuadrantData *qData = reinterpret_cast<SQuadrantData*>(info->quad->p.user_data);

    int quadrantSideLength = Globals::Forest::TreeSideLength >> info->quad->level;
    double tauOverSize = params->timeStep / (quadrantSideLength * quadrantSideLength);

    double fluxSum = 0.;

    for (int i = 0; i < 4; ++i)
    {
        fluxSum += qData->edgeData[i].sigma;
    }

    double numerator = qData->buffers[LHEImplicitBuffs::PREVIOUS_TIME_ITER] + tauOverSize * fluxSum * 2;
    double denominator = 1. + tauOverSize * 8;

    qData->buffers[LHEImplicitBuffs::CURRENT_JACOBI_ITER] = numerator / denominator;
}

void ProcessingOperation::LHEImplicitComputeResidualsVolumeCallback(p4est_iter_volume_info_t *info, void *user_data)
{
    SLHEImplicitParams *params = reinterpret_cast<SLHEImplicitParams*>(user_data);
    SQuadrantData *qData = reinterpret_cast<SQuadrantData*>(info->quad->p.user_data);

    int quadrantSideLength = Globals::Forest::TreeSideLength >> info->quad->level;
    double tauOverSize = params->timeStep / (quadrantSideLength * quadrantSideLength);

    double fluxSum = 0.;

    for (int i = 0; i < 4; ++i)
    {
        fluxSum += qData->edgeData[i].sigma;
    }

    double numerator = qData->buffers[LHEImplicitBuffs::PREVIOUS_TIME_ITER] + tauOverSize * fluxSum * 2;
    double denominator = 1. + tauOverSize * 8;

    double residual = qData->buffers[LHEImplicitBuffs::CURRENT_JACOBI_ITER] * denominator - numerator;
    residual /= qData->pipeline[params->pipelineIN].volumeData.maxValue;   //go to 0-1 range since tol is small number
    residual /= quadrantSideLength * quadrantSideLength;
    params->residualsSquared[qData->quadLocalOffset] = residual * residual;
}

void ProcessingOperation::LHEImplicitTimeIterationVolumeCallback(p4est_iter_volume_info_t *info, void*)
{    
    SQuadrantData *qData = reinterpret_cast<SQuadrantData*>(info->quad->p.user_data);
    qData->buffers[LHEImplicitBuffs::PREVIOUS_TIME_ITER] = qData->buffers[LHEImplicitBuffs::CURRENT_JACOBI_ITER];
}

void ProcessingOperation::LHEImplicitFinalizeVolumeCallback(p4est_iter_volume_info_t *info, void *user_data)
{
    SQuadrantData *qData = reinterpret_cast<SQuadrantData*>(info->quad->p.user_data);
    SLHEImplicitParams *params = reinterpret_cast<SLHEImplicitParams*>(user_data);

    qData->pipeline[params->pipelineOUT].volumeData.value = qData->buffers[LHEImplicitBuffs::PREVIOUS_TIME_ITER];
    qData->pipeline[params->pipelineOUT].volumeData.maxValue = qData->pipeline[params->pipelineIN].volumeData.maxValue; //once the max value changes every successive pipeline must preserve it
}


/////////////////////
/// MCF functions ///
/////////////////////
void ProcessingOperation::MCF(p4est_t *p4est, size_t algorithmIndex)
{
    SMCFParams *params = reinterpret_cast<SMCFParams*>(Globals::Processing::AlgorithmParams[algorithmIndex]);
    SVTKExportData exportData;
    exportData.exportArray = nullptr;

    p4est_ghost_t *ghostLayer = p4est_ghost_new(p4est, P4EST_CONNECT_FACE);
    SQuadrantData *ghostData = P4EST_ALLOC(SQuadrantData, ghostLayer->ghosts.elem_count);
    double *residualsSquared = new double[p4est->local_num_quadrants];

    params->ghostData = ghostData;  
    params->residualsSquared = residualsSquared;

    p4est_iterate(p4est, NULL, params, ProcessingOperation::MCFInitializeVolumeCallback, NULL, NULL);
    Forest::ExchangeGhostData(p4est, ghostLayer, ghostData);
    p4est_iterate(p4est, ghostLayer, params, NULL, ProcessingOperation::MCFInitializeSigmaFaceCallback, NULL);

    if (params->bExportAnimation)
    {
        exportData.exportArray = sc_array_new_count(sizeof(double), p4est->local_num_quadrants);
        exportData.exportBufferIndex = MCFBuffs::PREVIOUS_TIME_ITER;

        p4est_iterate(p4est, NULL, &exportData, Forest::ExtractQuadrantExportVTKBufferCallback, NULL, NULL);

        Core::WriteVtkAnimationOutput(p4est, params->animationFileName.c_str(), 0, exportData.exportArray);
    }

    for(int i = 0; i < params->numOfSteps; ++i)
    {
        double currentRes = 0.;

        p4est_iterate(p4est, NULL, params, ProcessingOperation::MCFVolumeGradientUpdateVolumeCallback, NULL, NULL);
        Forest::ExchangeGhostData(p4est, ghostLayer, ghostData);
        p4est_iterate(p4est, ghostLayer, params, NULL, ProcessingOperation::MCFSigmaUpdateFaceCallback, NULL);

        do
        {
            p4est_iterate(p4est, ghostLayer, params, ProcessingOperation::MCFJacobiIterationUpdateVolumeCallback, NULL, NULL);
            Forest::ExchangeGhostData(p4est, ghostLayer, ghostData);
            p4est_iterate(p4est, ghostLayer, params, NULL, ProcessingOperation::MCFSigmaUpdateFaceCallback, NULL);
            p4est_iterate(p4est, NULL, params, ProcessingOperation::MCFComputeResidualsVolumeCallback, NULL, NULL);

            double sumResSquared = 0.;
            for (int i = 0; i < p4est->local_num_quadrants; ++i)
            {
                sumResSquared += residualsSquared[i];
            }

            MPI_Allreduce(&sumResSquared, &currentRes, 1, MPI_DOUBLE, MPI_SUM, Globals::MPI::Comm);

            currentRes = sqrt(currentRes);
        }
        while(currentRes > params->tol);

        p4est_iterate(p4est, ghostLayer, params, ProcessingOperation::MCFTimeIterationVolumeCallback, NULL, NULL);

        if (params->bRefine && i % params->numOfIterationsToRefineAfter == 0 && i != 0)
        {
            //TODO
        }

        if (params->bExportAnimation)
        {
            p4est_iterate(p4est, NULL, &exportData, Forest::ExtractQuadrantExportVTKBufferCallback, NULL, NULL);

            Core::WriteVtkAnimationOutput(p4est, params->animationFileName.c_str(), i + 1, exportData.exportArray);
        }
    }

    p4est_iterate(p4est, NULL, params, ProcessingOperation::MCFFinalizeVolumeCallback, NULL, NULL);

    if (params->bRefine && params->numOfSteps % params->numOfIterationsToRefineAfter != 0)  //always refine last, but not if we already did inside of a loop
    {
        //TODO
        //dont have to care about ghost data anymore
    }

    if (params->bExportAnimation)
    {
        sc_array_destroy(exportData.exportArray);
    }

    P4EST_FREE(ghostData);
    p4est_ghost_destroy(ghostLayer);
    delete[] residualsSquared;
}

void ProcessingOperation::MCFInitializeVolumeCallback(p4est_iter_volume_info_t *info, void *user_data)
{
    SQuadrantData *quadData = reinterpret_cast<SQuadrantData*>(info->quad->p.user_data);
    SMCFParams *params = reinterpret_cast<SMCFParams*>(user_data);

    quadData->buffers[MCFBuffs::CURRENT_JACOBI_ITER] = quadData->pipeline[params->pipelineIN].volumeData.value;
    quadData->buffers[MCFBuffs::PREVIOUS_TIME_ITER] = quadData->pipeline[params->pipelineIN].volumeData.value;
}

void ProcessingOperation::MCFInitializeSigmaFaceCallback(p4est_iter_face_info_t *info, void *user_data)
{
    void* ghostData = reinterpret_cast<SMCFParams*>(user_data)->ghostData;

    if (info->sides.elem_count == 1)        //edge of image, partial sum is zero --> nothing to do
    {
        p4est_iter_face_side_t* sides = reinterpret_cast<p4est_iter_face_side_t*>(info->sides.array);
        SQuadrantData *qData = reinterpret_cast<SQuadrantData*>(sides[0].is.full.quad->p.user_data);

        qData->edgeData[sides[0].face].sigma = qData->buffers[MCFBuffs::CURRENT_JACOBI_ITER];
    }
    else if (info->sides.elem_count == 2)
    {
        p4est_iter_face_side_t* sides = reinterpret_cast<p4est_iter_face_side_t*>(info->sides.array);
        SQuadrantData *qData[4];     //qData[0], qData[1] are for sides[0], qData[2], qData[3] are for sides[1], if both faces are full, only qData[0] and qData[2] are populated
        int numOfQuadrants = 2;
        Utility::RetrieveQuadrantDataFromFaceSides(sides, qData, numOfQuadrants, ghostData);

        if (numOfQuadrants == 2)    //both quadrants are full
        {
            double buff = (qData[0]->buffers[MCFBuffs::CURRENT_JACOBI_ITER] + qData[2]->buffers[MCFBuffs::CURRENT_JACOBI_ITER]) / 2.;
            qData[0]->edgeData[sides[0].face].sigma = buff;
            qData[2]->edgeData[sides[1].face].sigma = buff;
        }
        else if (numOfQuadrants == 3) //there was a hanging face
        {
            int hangingDataIndex = 2;       //default if sides[1].is_hanging == true
            int fullDataIndex = 0;          //
            int hangingSideIndex = 1;       //
            int fullSideIndex = 0;          //

            if (sides[0].is_hanging)
            {
                hangingDataIndex = 0;
                fullDataIndex = 2;
                hangingSideIndex = 0;
                fullSideIndex = 1;
            }

            double buff = (1./3.) * qData[fullDataIndex]->buffers[MCFBuffs::CURRENT_JACOBI_ITER];

            qData[fullDataIndex]->edgeData[sides[fullSideIndex].face].sigma = (1./3.) * (qData[hangingDataIndex]->buffers[MCFBuffs::CURRENT_JACOBI_ITER] +
                                                     qData[hangingDataIndex + 1]->buffers[MCFBuffs::CURRENT_JACOBI_ITER]) + buff;
            qData[hangingDataIndex]->edgeData[sides[hangingSideIndex].face].sigma = buff + (2./3.) * qData[hangingDataIndex]->buffers[MCFBuffs::CURRENT_JACOBI_ITER];
            qData[hangingDataIndex + 1]->edgeData[sides[hangingSideIndex].face].sigma = buff + (2./3.) * qData[hangingDataIndex + 1]->buffers[MCFBuffs::CURRENT_JACOBI_ITER];
        }
    }
}

void ProcessingOperation::MCFVolumeGradientUpdateVolumeCallback(p4est_iter_volume_info_t *info, void *user_data)
{
    SMCFParams *params = reinterpret_cast<SMCFParams*>(user_data);
    SQuadrantData *qData = reinterpret_cast<SQuadrantData*>(info->quad->p.user_data);
    int quadrantSideLength = Globals::Forest::TreeSideLength >> info->quad->level;
    int quadArea = quadrantSideLength * quadrantSideLength;

    double sum = 0.;

    for (int i = 0; i < 4; ++i)
    {
        double diff = qData->edgeData[i].sigma - qData->buffers[MCFBuffs::PREVIOUS_TIME_ITER];
        sum += diff * diff;
    }

    sum *= 2;
    sum /= quadArea;

    qData->buffers[MCFBuffs::CURRENT_VOLUME_GRADIENT] = sqrt(params->epsilon * params->epsilon + sum);
}

void ProcessingOperation::MCFSigmaUpdateFaceCallback(p4est_iter_face_info_t *info, void *user_data)
{
    void* ghostData = reinterpret_cast<SMCFParams*>(user_data)->ghostData;

    if (info->sides.elem_count == 1)        //edge of image, partial sum is zero --> nothing to do
    {
        p4est_iter_face_side_t* sides = reinterpret_cast<p4est_iter_face_side_t*>(info->sides.array);
        SQuadrantData *qData = reinterpret_cast<SQuadrantData*>(sides[0].is.full.quad->p.user_data);

        qData->edgeData[sides[0].face].sigma = qData->buffers[MCFBuffs::CURRENT_JACOBI_ITER];
    }
    else if (info->sides.elem_count == 2)
    {
        p4est_iter_face_side_t* sides = reinterpret_cast<p4est_iter_face_side_t*>(info->sides.array);
        SQuadrantData *qData[4];     //qData[0], qData[1] are for sides[0], qData[2], qData[3] are for sides[1], if both faces are full, only qData[0] and qData[2] are populated
        int numOfQuadrants = 2;
        Utility::RetrieveQuadrantDataFromFaceSides(sides, qData, numOfQuadrants, ghostData);

        if (numOfQuadrants == 2)    //both quadrants are full
        {
            double sigma = qData[0]->buffers[MCFBuffs::CURRENT_JACOBI_ITER] * qData[2]->buffers[MCFBuffs::CURRENT_VOLUME_GRADIENT] +
                          qData[2]->buffers[MCFBuffs::CURRENT_JACOBI_ITER] * qData[0]->buffers[MCFBuffs::CURRENT_VOLUME_GRADIENT];
            sigma /= qData[0]->buffers[MCFBuffs::CURRENT_VOLUME_GRADIENT] + qData[2]->buffers[MCFBuffs::CURRENT_VOLUME_GRADIENT];

            qData[0]->edgeData[sides[0].face].sigma = sigma;
            qData[2]->edgeData[sides[1].face].sigma = sigma;
        }
        else if (numOfQuadrants == 3) //there was a hanging face
        {
            int hangingDataIndex = 2;       //default if sides[1].is_hanging == true
            int fullDataIndex = 0;          //
            int hangingSideIndex = 1;       //
            int fullSideIndex = 0;          //

            if (sides[0].is_hanging)
            {
                hangingDataIndex = 0;
                fullDataIndex = 2;
                hangingSideIndex = 0;
                fullSideIndex = 1;
            }

            double sigma1 = qData[fullDataIndex]->buffers[MCFBuffs::CURRENT_JACOBI_ITER] * qData[hangingDataIndex]->buffers[MCFBuffs::CURRENT_VOLUME_GRADIENT];
            double sigma2 = qData[fullDataIndex]->buffers[MCFBuffs::CURRENT_JACOBI_ITER] * qData[hangingDataIndex + 1]->buffers[MCFBuffs::CURRENT_VOLUME_GRADIENT];

            sigma1 += 2 * qData[fullDataIndex]->buffers[MCFBuffs::CURRENT_VOLUME_GRADIENT] * qData[hangingDataIndex]->buffers[MCFBuffs::CURRENT_JACOBI_ITER];
            sigma2 += 2 * qData[fullDataIndex]->buffers[MCFBuffs::CURRENT_VOLUME_GRADIENT] * qData[hangingDataIndex + 1]->buffers[MCFBuffs::CURRENT_JACOBI_ITER];

            sigma1 /= 2 * qData[fullDataIndex]->buffers[MCFBuffs::CURRENT_VOLUME_GRADIENT] + qData[hangingDataIndex]->buffers[MCFBuffs::CURRENT_VOLUME_GRADIENT];
            sigma2 /= 2 * qData[fullDataIndex]->buffers[MCFBuffs::CURRENT_VOLUME_GRADIENT] + qData[hangingDataIndex + 1]->buffers[MCFBuffs::CURRENT_VOLUME_GRADIENT];

            qData[fullDataIndex]->edgeData[sides[fullSideIndex].face].sigma = 0.5 * (sigma1 + sigma2);
            qData[hangingDataIndex]->edgeData[sides[hangingSideIndex].face].sigma = sigma1;
            qData[hangingDataIndex + 1]->edgeData[sides[hangingSideIndex].face].sigma = sigma2;
        }
    }
}

void ProcessingOperation::MCFJacobiIterationUpdateVolumeCallback(p4est_iter_volume_info_t *info, void *user_data)
{
    SMCFParams *params = reinterpret_cast<SMCFParams*>(user_data);
    SQuadrantData *qData = reinterpret_cast<SQuadrantData*>(info->quad->p.user_data);

    int quadrantSideLength = Globals::Forest::TreeSideLength >> info->quad->level;
    double tauOverSize = params->timeStep / (quadrantSideLength * quadrantSideLength);

    double fluxSum = 0.;

    for (int i = 0; i < 4; ++i)
    {
        fluxSum += qData->edgeData[i].sigma;
    }

    double numerator = qData->buffers[MCFBuffs::PREVIOUS_TIME_ITER] + tauOverSize * fluxSum * 2;
    double denominator = 1. + tauOverSize * 8;

    qData->buffers[MCFBuffs::CURRENT_JACOBI_ITER] = numerator / denominator;
}

void ProcessingOperation::MCFComputeResidualsVolumeCallback(p4est_iter_volume_info_t *info, void *user_data)
{
    SMCFParams *params = reinterpret_cast<SMCFParams*>(user_data);
    SQuadrantData *qData = reinterpret_cast<SQuadrantData*>(info->quad->p.user_data);

    int quadrantSideLength = Globals::Forest::TreeSideLength >> info->quad->level;
    double tauOverSize = params->timeStep / (quadrantSideLength * quadrantSideLength);

    double fluxSum = 0.;

    for (int i = 0; i < 4; ++i)
    {
        fluxSum += qData->edgeData[i].sigma;
    }

    double numerator = qData->buffers[MCFBuffs::PREVIOUS_TIME_ITER] + tauOverSize * fluxSum * 2;
    double denominator = 1. + tauOverSize * 8;

    double residual = qData->buffers[MCFBuffs::CURRENT_JACOBI_ITER] * denominator - numerator;
    residual /= qData->pipeline[params->pipelineIN].volumeData.maxValue;   //go to 0-1 range since tol is small number
    params->residualsSquared[qData->quadLocalOffset] = residual * residual;
}

void ProcessingOperation::MCFTimeIterationVolumeCallback(p4est_iter_volume_info_t *info, void*)
{
    SQuadrantData *qData = reinterpret_cast<SQuadrantData*>(info->quad->p.user_data);
    qData->buffers[MCFBuffs::PREVIOUS_TIME_ITER] = qData->buffers[MCFBuffs::CURRENT_JACOBI_ITER];
}

void ProcessingOperation::MCFFinalizeVolumeCallback(p4est_iter_volume_info_t *info, void *user_data)
{
    SQuadrantData *qData = reinterpret_cast<SQuadrantData*>(info->quad->p.user_data);
    SMCFParams *params = reinterpret_cast<SMCFParams*>(user_data);

    qData->pipeline[params->pipelineOUT].volumeData.value = qData->buffers[MCFBuffs::PREVIOUS_TIME_ITER];
    qData->pipeline[params->pipelineOUT].volumeData.maxValue = qData->pipeline[params->pipelineIN].volumeData.maxValue; //once the max value changes every successive pipeline must preserve it
}

/////////////////////////
/// SUBSURF functions ///
/////////////////////////
void ProcessingOperation::Subsurf(p4est_t *p4est, size_t algorithmIndex)
{
    SSubsurfParams *params = reinterpret_cast<SSubsurfParams*>(Globals::Processing::AlgorithmParams[algorithmIndex]);
    SVTKExportData exportData;
    exportData.exportArray = nullptr;

    p4est_ghost_t *ghostLayer = p4est_ghost_new(p4est, P4EST_CONNECT_FACE);
    SQuadrantData *ghostData = P4EST_ALLOC(SQuadrantData, ghostLayer->ghosts.elem_count);
    double *residualsSquared = new double[p4est->local_num_quadrants];

    params->ghostData = ghostData;
    params->residualsSquared = residualsSquared;

    p4est_iterate(p4est, NULL, params, ProcessingOperation::SubsurfInitializeVolumeCallback, NULL, NULL);
    Forest::ExchangeGhostData(p4est, ghostLayer, ghostData);

    p4est_iterate(p4est, ghostLayer, params, NULL, ProcessingOperation::SubsurfInitializeGFunctionSigmaFaceCallback, NULL);
    p4est_iterate(p4est, NULL, params, SubsurfGFunctionInitVolumeCallback, NULL, NULL);
    p4est_iterate(p4est, ghostLayer, params, NULL, ProcessingOperation::SubsurfInitializeSigmaFaceCallback, NULL);

    if (params->bExportAnimation)
    {
        exportData.exportArray = sc_array_new_count(sizeof(double), p4est->local_num_quadrants);
        exportData.exportBufferIndex = SubsurfBuffs::PREVIOUS_TIME_ITER;

        p4est_iterate(p4est, NULL, &exportData, Forest::ExtractQuadrantExportVTKBufferCallback, NULL, NULL);

        Core::WriteVtkAnimationOutput(p4est, params->animationFileName.c_str(), 0, exportData.exportArray);
    }

    for(int i = 0; i < params->numOfSteps; ++i)
    {
        double currentRes = 0.;

        p4est_iterate(p4est, NULL, params, ProcessingOperation::SubsurfVolumeGradientUpdateVolumeCallback, NULL, NULL);
        Forest::ExchangeGhostData(p4est, ghostLayer, ghostData);
        p4est_iterate(p4est, ghostLayer, params, NULL, ProcessingOperation::SubsurfSigmaUpdateFaceCallback, NULL);

        do
        {
            p4est_iterate(p4est, ghostLayer, params, ProcessingOperation::SubsurfJacobiIterationUpdateVolumeCallback, NULL, NULL);
            Forest::ExchangeGhostData(p4est, ghostLayer, ghostData);
            p4est_iterate(p4est, ghostLayer, params, NULL, ProcessingOperation::SubsurfSigmaUpdateFaceCallback, NULL);
            p4est_iterate(p4est, NULL, params, ProcessingOperation::SubsurfComputeResidualsVolumeCallback, NULL, NULL);

            double sumResSquared = 0.;
            for (int i = 0; i < p4est->local_num_quadrants; ++i)
            {
                sumResSquared += residualsSquared[i];
            }

            MPI_Allreduce(&sumResSquared, &currentRes, 1, MPI_DOUBLE, MPI_SUM, Globals::MPI::Comm);

            currentRes = sqrt(currentRes);
        }
        while(currentRes > params->tol);

        p4est_iterate(p4est, ghostLayer, params, ProcessingOperation::SubsurfTimeIterationVolumeCallback, NULL, NULL);

        if (params->bRefine && i % params->numOfIterationsToRefineAfter == 0 && i != 0)
        {
            //TODO
        }

        if (params->bExportAnimation)
        {
            p4est_iterate(p4est, NULL, &exportData, Forest::ExtractQuadrantExportVTKBufferCallback, NULL, NULL);

            Core::WriteVtkAnimationOutput(p4est, params->animationFileName.c_str(), i + 1, exportData.exportArray);
        }
    }

    p4est_iterate(p4est, NULL, params, ProcessingOperation::SubsurfFinalizeVolumeCallback, NULL, NULL);

    if (params->bRefine && params->numOfSteps % params->numOfIterationsToRefineAfter != 0)  //always refine last, but not if we already did inside of a loop
    {
        //TODO
        //dont have to care about ghost data anymore
    }

    if (params->bExportAnimation)
    {
        sc_array_destroy(exportData.exportArray);
    }

    P4EST_FREE(ghostData);
    p4est_ghost_destroy(ghostLayer);
    delete[] residualsSquared;
}

double ProcessingOperation::SubsurfGFunction(double K, double sSquared)
{
    return 1. / (1. + K * sSquared);
}

void ProcessingOperation::SubsurfInitializeGFunctionSigmaFaceCallback(p4est_iter_face_info_t *info, void *user_data)
{
    SSubsurfParams *params = reinterpret_cast<SSubsurfParams*>(user_data);
    void* ghostData = params->ghostData;

    if (info->sides.elem_count == 1)        //edge of image, partial sum is zero --> nothing to do
    {
        p4est_iter_face_side_t* sides = reinterpret_cast<p4est_iter_face_side_t*>(info->sides.array);
        SQuadrantData *qData = reinterpret_cast<SQuadrantData*>(sides[0].is.full.quad->p.user_data);

        qData->edgeData[sides[0].face].sigma = qData->pipeline[params->pipelineIN_I0].volumeData.value;
    }
    else if (info->sides.elem_count == 2)
    {
        p4est_iter_face_side_t* sides = reinterpret_cast<p4est_iter_face_side_t*>(info->sides.array);
        SQuadrantData *qData[4];     //qData[0], qData[1] are for sides[0], qData[2], qData[3] are for sides[1], if both faces are full, only qData[0] and qData[2] are populated
        int numOfQuadrants = 2;
        Utility::RetrieveQuadrantDataFromFaceSides(sides, qData, numOfQuadrants, ghostData);

        if (numOfQuadrants == 2)    //both quadrants are full
        {
            double buff = (qData[0]->pipeline[params->pipelineIN_I0].volumeData.value + qData[2]->pipeline[params->pipelineIN_I0].volumeData.value) / 2.;
            qData[0]->edgeData[sides[0].face].sigma = buff;
            qData[2]->edgeData[sides[1].face].sigma = buff;
        }
        else if (numOfQuadrants == 3) //there was a hanging face
        {
            int hangingDataIndex = 2;       //default if sides[1].is_hanging == true
            int fullDataIndex = 0;          //
            int hangingSideIndex = 1;       //
            int fullSideIndex = 0;          //

            if (sides[0].is_hanging)
            {
                hangingDataIndex = 0;
                fullDataIndex = 2;
                hangingSideIndex = 0;
                fullSideIndex = 1;
            }

            double buff = (1./3.) * qData[fullDataIndex]->pipeline[params->pipelineIN_I0].volumeData.value;

            qData[fullDataIndex]->edgeData[sides[fullSideIndex].face].sigma = (1./3.) * (qData[hangingDataIndex]->pipeline[params->pipelineIN_I0].volumeData.value +
                                                     qData[hangingDataIndex + 1]->pipeline[params->pipelineIN_I0].volumeData.value) + buff;
            qData[hangingDataIndex]->edgeData[sides[hangingSideIndex].face].sigma = buff + (2./3.) * qData[hangingDataIndex]->pipeline[params->pipelineIN_I0].volumeData.value;
            qData[hangingDataIndex + 1]->edgeData[sides[hangingSideIndex].face].sigma = buff + (2./3.) * qData[hangingDataIndex + 1]->pipeline[params->pipelineIN_I0].volumeData.value;
        }
    }
}

void ProcessingOperation::SubsurfGFunctionInitVolumeCallback(p4est_iter_volume_info_t *info, void *user_data)
{
    SSubsurfParams *params = reinterpret_cast<SSubsurfParams*>(user_data);
    SQuadrantData *qData = reinterpret_cast<SQuadrantData*>(info->quad->p.user_data);
    int quadrantSideLength = Globals::Forest::TreeSideLength >> info->quad->level;
    int quadArea = quadrantSideLength * quadrantSideLength;

    double sum = 0.;

    for (int i = 0; i < 4; ++i)
    {
        double diff = qData->edgeData[i].sigma - qData->pipeline[params->pipelineIN_I0].volumeData.value;
        sum += diff * diff;
    }

    sum *= 2;
    sum /= quadArea;

    qData->buffers[SubsurfBuffs::G_ZERO_RESULT] = ProcessingOperation::SubsurfGFunction(params->K, params->epsilon * params->epsilon + sum);
}

void ProcessingOperation::SubsurfInitializeVolumeCallback(p4est_iter_volume_info_t *info, void *user_data)
{
    SQuadrantData *quadData = reinterpret_cast<SQuadrantData*>(info->quad->p.user_data);
    SSubsurfParams *params = reinterpret_cast<SSubsurfParams*>(user_data);

    quadData->buffers[MCFBuffs::CURRENT_JACOBI_ITER] = quadData->pipeline[params->pipelineIN_U0].volumeData.value;
    quadData->buffers[MCFBuffs::PREVIOUS_TIME_ITER] = quadData->pipeline[params->pipelineIN_U0].volumeData.value;
}

void ProcessingOperation::SubsurfInitializeSigmaFaceCallback(p4est_iter_face_info_t *info, void *user_data)
{
    void* ghostData = reinterpret_cast<SSubsurfParams*>(user_data)->ghostData;

    if (info->sides.elem_count == 1)        //edge of image, partial sum is zero --> nothing to do
    {
        p4est_iter_face_side_t* sides = reinterpret_cast<p4est_iter_face_side_t*>(info->sides.array);
        SQuadrantData *qData = reinterpret_cast<SQuadrantData*>(sides[0].is.full.quad->p.user_data);

        qData->edgeData[sides[0].face].sigma = qData->buffers[SubsurfBuffs::CURRENT_JACOBI_ITER];
    }
    else if (info->sides.elem_count == 2)
    {
        p4est_iter_face_side_t* sides = reinterpret_cast<p4est_iter_face_side_t*>(info->sides.array);
        SQuadrantData *qData[4];     //qData[0], qData[1] are for sides[0], qData[2], qData[3] are for sides[1], if both faces are full, only qData[0] and qData[2] are populated
        int numOfQuadrants = 2;
        Utility::RetrieveQuadrantDataFromFaceSides(sides, qData, numOfQuadrants, ghostData);

        if (numOfQuadrants == 2)    //both quadrants are full
        {
            double buff = (qData[0]->buffers[SubsurfBuffs::CURRENT_JACOBI_ITER] + qData[2]->buffers[SubsurfBuffs::CURRENT_JACOBI_ITER]) / 2.;
            qData[0]->edgeData[sides[0].face].sigma = buff;
            qData[2]->edgeData[sides[1].face].sigma = buff;
        }
        else if (numOfQuadrants == 3) //there was a hanging face
        {
            int hangingDataIndex = 2;       //default if sides[1].is_hanging == true
            int fullDataIndex = 0;          //
            int hangingSideIndex = 1;       //
            int fullSideIndex = 0;          //

            if (sides[0].is_hanging)
            {
                hangingDataIndex = 0;
                fullDataIndex = 2;
                hangingSideIndex = 0;
                fullSideIndex = 1;
            }

            double buff = (1./3.) * qData[fullDataIndex]->buffers[SubsurfBuffs::CURRENT_JACOBI_ITER];

            qData[fullDataIndex]->edgeData[sides[fullSideIndex].face].sigma = (1./3.) * (qData[hangingDataIndex]->buffers[SubsurfBuffs::CURRENT_JACOBI_ITER] +
                                                     qData[hangingDataIndex + 1]->buffers[SubsurfBuffs::CURRENT_JACOBI_ITER]) + buff;
            qData[hangingDataIndex]->edgeData[sides[hangingSideIndex].face].sigma = buff + (2./3.) * qData[hangingDataIndex]->buffers[SubsurfBuffs::CURRENT_JACOBI_ITER];
            qData[hangingDataIndex + 1]->edgeData[sides[hangingSideIndex].face].sigma = buff + (2./3.) * qData[hangingDataIndex + 1]->buffers[SubsurfBuffs::CURRENT_JACOBI_ITER];
        }
    }
}

void ProcessingOperation::SubsurfVolumeGradientUpdateVolumeCallback(p4est_iter_volume_info_t *info, void *user_data)
{
    SSubsurfParams *params = reinterpret_cast<SSubsurfParams*>(user_data);
    SQuadrantData *qData = reinterpret_cast<SQuadrantData*>(info->quad->p.user_data);
    int quadrantSideLength = Globals::Forest::TreeSideLength >> info->quad->level;
    int quadArea = quadrantSideLength * quadrantSideLength;

    double sum = 0.;

    for (int i = 0; i < 4; ++i)
    {
        double diff = qData->edgeData[i].sigma - qData->buffers[SubsurfBuffs::PREVIOUS_TIME_ITER];
        sum += diff * diff;
    }

    sum *= 2;
    sum /= quadArea;

    qData->buffers[SubsurfBuffs::CURRENT_VOLUME_GRADIENT] = sqrt(params->epsilon * params->epsilon + sum);
}

void ProcessingOperation::SubsurfSigmaUpdateFaceCallback(p4est_iter_face_info_t *info, void *user_data)
{
    void* ghostData = reinterpret_cast<SSubsurfParams*>(user_data)->ghostData;

    if (info->sides.elem_count == 1)        //edge of image, partial sum is zero --> nothing to do
    {
        p4est_iter_face_side_t* sides = reinterpret_cast<p4est_iter_face_side_t*>(info->sides.array);
        SQuadrantData *qData = reinterpret_cast<SQuadrantData*>(sides[0].is.full.quad->p.user_data);

        qData->edgeData[sides[0].face].sigma = qData->buffers[SubsurfBuffs::CURRENT_JACOBI_ITER];
    }
    else if (info->sides.elem_count == 2)
    {
        p4est_iter_face_side_t* sides = reinterpret_cast<p4est_iter_face_side_t*>(info->sides.array);
        SQuadrantData *qData[4];     //qData[0], qData[1] are for sides[0], qData[2], qData[3] are for sides[1], if both faces are full, only qData[0] and qData[2] are populated
        int numOfQuadrants = 2;
        Utility::RetrieveQuadrantDataFromFaceSides(sides, qData, numOfQuadrants, ghostData);

        if (numOfQuadrants == 2)    //both quadrants are full
        {
            double sigma = qData[0]->buffers[SubsurfBuffs::G_ZERO_RESULT] * qData[0]->buffers[SubsurfBuffs::CURRENT_JACOBI_ITER] * qData[2]->buffers[SubsurfBuffs::CURRENT_VOLUME_GRADIENT] +
                          qData[2]->buffers[SubsurfBuffs::G_ZERO_RESULT] * qData[2]->buffers[SubsurfBuffs::CURRENT_JACOBI_ITER] * qData[0]->buffers[SubsurfBuffs::CURRENT_VOLUME_GRADIENT];

            sigma /= qData[2]->buffers[SubsurfBuffs::G_ZERO_RESULT] * qData[0]->buffers[SubsurfBuffs::CURRENT_VOLUME_GRADIENT] +
                    qData[0]->buffers[SubsurfBuffs::G_ZERO_RESULT] * qData[2]->buffers[SubsurfBuffs::CURRENT_VOLUME_GRADIENT];

            qData[0]->edgeData[sides[0].face].sigma = sigma;
            qData[2]->edgeData[sides[1].face].sigma = sigma;
        }
        else if (numOfQuadrants == 3) //there was a hanging face
        {
            int hangingDataIndex = 2;       //default if sides[1].is_hanging == true
            int fullDataIndex = 0;          //
            int hangingSideIndex = 1;       //
            int fullSideIndex = 0;          //

            if (sides[0].is_hanging)
            {
                hangingDataIndex = 0;
                fullDataIndex = 2;
                hangingSideIndex = 0;
                fullSideIndex = 1;
            }

            double sigma1 = qData[fullDataIndex]->buffers[SubsurfBuffs::CURRENT_JACOBI_ITER] * qData[hangingDataIndex]->buffers[SubsurfBuffs::CURRENT_VOLUME_GRADIENT];
            double sigma2 = qData[fullDataIndex]->buffers[SubsurfBuffs::CURRENT_JACOBI_ITER] * qData[hangingDataIndex + 1]->buffers[SubsurfBuffs::CURRENT_VOLUME_GRADIENT];

            sigma1 *= qData[fullDataIndex]->buffers[SubsurfBuffs::G_ZERO_RESULT];
            sigma2 *= qData[fullDataIndex]->buffers[SubsurfBuffs::G_ZERO_RESULT];

            //second part of numerator
            double temp1 = 2 * qData[fullDataIndex]->buffers[SubsurfBuffs::CURRENT_VOLUME_GRADIENT] * qData[hangingDataIndex]->buffers[SubsurfBuffs::CURRENT_JACOBI_ITER];
            double temp2 = 2 * qData[fullDataIndex]->buffers[SubsurfBuffs::CURRENT_VOLUME_GRADIENT] * qData[hangingDataIndex + 1]->buffers[SubsurfBuffs::CURRENT_JACOBI_ITER];

            sigma1 += temp1 * qData[hangingDataIndex]->buffers[SubsurfBuffs::G_ZERO_RESULT];
            sigma2 += temp2 * qData[hangingDataIndex + 1]->buffers[SubsurfBuffs::G_ZERO_RESULT];

            //first part of denominator
            temp1 = qData[fullDataIndex]->buffers[SubsurfBuffs::G_ZERO_RESULT] * qData[hangingDataIndex]->buffers[SubsurfBuffs::CURRENT_VOLUME_GRADIENT];
            temp2 = qData[fullDataIndex]->buffers[SubsurfBuffs::G_ZERO_RESULT] * qData[hangingDataIndex + 1]->buffers[SubsurfBuffs::CURRENT_VOLUME_GRADIENT];

            //second part of denominator
            temp1 += 2 * qData[hangingDataIndex]->buffers[SubsurfBuffs::G_ZERO_RESULT] * qData[fullDataIndex]->buffers[SubsurfBuffs::CURRENT_VOLUME_GRADIENT];
            temp2 += 2 * qData[hangingDataIndex + 1]->buffers[SubsurfBuffs::G_ZERO_RESULT] * qData[fullDataIndex]->buffers[SubsurfBuffs::CURRENT_VOLUME_GRADIENT];

            sigma1 /= temp1;
            sigma2 /= temp2;

            qData[fullDataIndex]->edgeData[sides[fullSideIndex].face].sigma = 0.5 * (sigma1 + sigma2);
            qData[hangingDataIndex]->edgeData[sides[hangingSideIndex].face].sigma = sigma1;
            qData[hangingDataIndex + 1]->edgeData[sides[hangingSideIndex].face].sigma = sigma2;
        }
    }
}

void ProcessingOperation::SubsurfJacobiIterationUpdateVolumeCallback(p4est_iter_volume_info_t *info, void *user_data)
{
    SSubsurfParams *params = reinterpret_cast<SSubsurfParams*>(user_data);
    SQuadrantData *qData = reinterpret_cast<SQuadrantData*>(info->quad->p.user_data);

    int quadrantSideLength = Globals::Forest::TreeSideLength >> info->quad->level;
    double tauOverSize = params->timeStep / (quadrantSideLength * quadrantSideLength);

    double fluxSum = 0.;

    for (int i = 0; i < 4; ++i)
    {
        fluxSum += qData->edgeData[i].sigma;
    }

    double numerator = qData->buffers[SubsurfBuffs::PREVIOUS_TIME_ITER] + tauOverSize * fluxSum * 2 * qData->buffers[SubsurfBuffs::G_ZERO_RESULT];
    double denominator = 1. + tauOverSize * 8 * qData->buffers[SubsurfBuffs::G_ZERO_RESULT];

    qData->buffers[SubsurfBuffs::CURRENT_JACOBI_ITER] = numerator / denominator;
}

void ProcessingOperation::SubsurfComputeResidualsVolumeCallback(p4est_iter_volume_info_t *info, void *user_data)
{
    SSubsurfParams *params = reinterpret_cast<SSubsurfParams*>(user_data);
    SQuadrantData *qData = reinterpret_cast<SQuadrantData*>(info->quad->p.user_data);

    int quadrantSideLength = Globals::Forest::TreeSideLength >> info->quad->level;
    double tauOverSize = params->timeStep / (quadrantSideLength * quadrantSideLength);

    double fluxSum = 0.;

    for (int i = 0; i < 4; ++i)
    {
        fluxSum += qData->edgeData[i].sigma;
    }

    double numerator = qData->buffers[SubsurfBuffs::PREVIOUS_TIME_ITER] + tauOverSize * fluxSum * 2 * qData->buffers[SubsurfBuffs::G_ZERO_RESULT];
    double denominator = 1. + tauOverSize * 8 * qData->buffers[SubsurfBuffs::G_ZERO_RESULT];

    double residual = qData->buffers[SubsurfBuffs::CURRENT_JACOBI_ITER] * denominator - numerator;
    residual /= qData->pipeline[params->pipelineIN_U0].volumeData.maxValue;   //go to 0-1 range since tol is small number
    params->residualsSquared[qData->quadLocalOffset] = residual * residual;
}

void ProcessingOperation::SubsurfTimeIterationVolumeCallback(p4est_iter_volume_info_t *info, void*)
{
    SQuadrantData *qData = reinterpret_cast<SQuadrantData*>(info->quad->p.user_data);
    qData->buffers[SubsurfBuffs::PREVIOUS_TIME_ITER] = qData->buffers[SubsurfBuffs::CURRENT_JACOBI_ITER];
}

void ProcessingOperation::SubsurfFinalizeVolumeCallback(p4est_iter_volume_info_t *info, void *user_data)
{
    SQuadrantData *qData = reinterpret_cast<SQuadrantData*>(info->quad->p.user_data);
    SSubsurfParams *params = reinterpret_cast<SSubsurfParams*>(user_data);

    qData->pipeline[params->pipelineOUT].volumeData.value = qData->buffers[SubsurfBuffs::PREVIOUS_TIME_ITER];
    qData->pipeline[params->pipelineOUT].volumeData.maxValue = qData->pipeline[params->pipelineIN_U0].volumeData.maxValue; //once the max value changes every successive pipeline must preserve it
}
