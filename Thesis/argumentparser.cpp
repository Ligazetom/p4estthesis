#include "argumentparser.h"
#include "globals.h"

#include <qstring.h>

#include "utility.h"

#include <cmath>
#include <algorithm>
#include <string>
#include <sstream>

ArgumentParser::ArgumentParser(int argc, char **argv, PLogger &logger) : m_logger(logger)
{    
    m_logger << "\n";   //just so error messages are easier to see
    m_logger.Log();

    if (argc == 1)
    {
        m_logger << "For more help run with parameter:\n";
        m_logger << "help\n";
        m_logger << "example\n";
        m_logger << "processing\n";
        m_logger << "refinement\n";
        m_logger.Log();
        MPI_Abort(Globals::MPI::Comm, 1);
    }
    else if (argc == 2)
    {
        if (std::string(argv[1]) == "help")
        {
            PrintHelpInfo();
            MPI_Abort(Globals::MPI::Comm, 1);
        }
        else if (std::string(argv[1]) == "example")
        {
            PrintExample();
            MPI_Abort(Globals::MPI::Comm, 1);
        }
        else if (std::string(argv[1]) == "processing")
        {
            PrintAvailableProcessingMethods();
            MPI_Abort(Globals::MPI::Comm, 1);
        }
        else if (std::string(argv[1]) == "refinement")
        {
            PrintAvailableRefinementMethods();
            MPI_Abort(Globals::MPI::Comm, 1);
        }
    }

    Globals::FileIO::FolderBatchPath = nullptr;
    Globals::FileIO::ImageLoadPath = nullptr;
    Globals::Forest::AutoTreeSideLength = true;
    Globals::Processing::AutoExtraPixelValue = true;
    Globals::Forest::BleedingPixelEdgeRadius = 0;

    Globals::FileIO::VTKExportPipelineNames.reserve(2);
    Globals::FileIO::VTKExportPipelineNumbers.reserve(2);

    Globals::FileIO::VTKExportPipelineNames.push_back("original");
    Globals::FileIO::VTKExportPipelineNames.push_back("result");
    Globals::FileIO::VTKExportPipelineNumbers.push_back(0);
    Globals::FileIO::VTKExportPipelineNumbers.push_back(1);
    Globals::FileIO::ImageExportPipelineNames.push_back("result");
    Globals::FileIO::ImageExportPipelineNumbers.push_back(1);

    PickFlagsAndArgs(argc, argv);

    for(size_t i = 0; i < m_currentFlags.size(); ++i)
    {
        switch(m_currentFlags[i])
        {
        case 'f': Parse_f_flag(); break;
        case 'i': Parse_i_flag(); break;
        case 'c': Parse_c_flag(); break;
        case 'C': Parse_C_flag(); break;
        case 'o': Parse_o_flag(); break;
        case 'O': Parse_O_flag(); break;
        case 'X': Parse_X_flag(); break;
        case 'B': Parse_B_flag(); break;
        case 'I': Parse_I_flag(); break;
        case 'D': Parse_D_flag(); break;
        case 'T': Parse_T_flag(); break;
        case 'r': Parse_r_flag(); break;
        case 'm': Parse_m_flag(); break;            
        default:
        {
            m_logger << "Unknown flag " << m_currentFlags[i] << ". Program will abort!" << std::endl;
            m_logger.LogError();
            MPI_Abort(Globals::MPI::Comm, 1);
        }
        }
    }

    if (!IsValid())
    {
        m_logger << "Mandatory flags missing: \"" << PrintMissingFlags() << "\"" << std::endl;
        m_logger.LogError();
        MPI_Abort(Globals::MPI::Comm, 2);
    }

    if (HasDuplicates(m_animationNames))
    {
        m_logger << "Animation names cannot be identical with each other!\n";
        m_logger.LogError();
        MPI_Abort(Globals::MPI::Comm, 1);
    }

    if (!Globals::Forest::AutoTreeSideLength)
    {
        Globals::Forest::ExtendedTreeSideLength = Globals::Forest::TreeSideLength + 2 * Globals::Forest::BleedingPixelEdgeRadius;
        Globals::Forest::NumOfBytesPerTreeImageFragment = Globals::Forest::ExtendedTreeSideLength * Globals::Forest::ExtendedTreeSideLength;
    }
}

bool ArgumentParser::IsValid() const
{
    for(int i = 0; i < NUMBER_OF_MANDATORY_FLAGS; ++i)
    {
        if (m_flagParams.find(m_mandatoryFlags[i]) == m_flagParams.end())
        {
            return false;
        }
    }

    bool f_flag_present = m_flagParams.find('f') != m_flagParams.end();
    bool i_flag_present = m_flagParams.find('i') != m_flagParams.end();

    return f_flag_present || i_flag_present;
}

std::string ArgumentParser::PrintMissingFlags() const
{
    std::stringstream ss;

    for(int i = 0; i < NUMBER_OF_MANDATORY_FLAGS; ++i)
    {
        if (m_flagParams.find(m_mandatoryFlags[i]) == m_flagParams.end())
        {
            ss << m_mandatoryFlags[i] << " ";
        }
    }

    bool f_flag_present = m_flagParams.find('f') != m_flagParams.end();
    bool i_flag_present = m_flagParams.find('i') != m_flagParams.end();

    if (!(f_flag_present || i_flag_present)) ss << "i/f ";

    std::string temp(ss.str());

    return temp.substr(0, temp.size() - 1); //remove last " "
}

void ArgumentParser::PickFlagsAndArgs(int argc, char **argv)
{
    for(int i = 1; i < argc; ++i)
    {
        if (argv[i][0] == '-' && ((argv[i][1] >= 'A' && argv[i][1] <= 'Z') || (argv[i][1] >= 'a' && argv[i][1] <= 'z')))
        {
            m_currentFlags.push_back(argv[i][1]);

            if (argv[i][1] == 'D' || argv[i][1] == 'I' || argv[i][1] == 'C')
            {
            }
            else ExtractFlagParams(i, argc, argv);
        }
    }
}

void ArgumentParser::Parse_f_flag() const
{
    QString params(m_flagParams.at('f'));
    QStringList paramList(params.split(' ', QString::SkipEmptyParts));

    if (paramList.length() != 1)
    {
        m_logger << "Wrong number of parameters for flag '-f'. Expected is 1." << std::endl;
        m_logger.Log();
        MPI_Abort(Globals::MPI::Comm, 1);
    }

    Globals::FileIO::FolderBatchPath = m_flagParams.at('f');
}

void ArgumentParser::Parse_i_flag() const
{
    QString params(m_flagParams.at('i'));
    QStringList paramList(params.split(' ', QString::SkipEmptyParts));

    if (paramList.length() != 1)
    {
        m_logger << "Wrong number of parameters for flag '-i'. Expects: *path* to file to be processed." << std::endl;
        m_logger.LogError();
        MPI_Abort(Globals::MPI::Comm, 1);
    }

    Globals::FileIO::ImageLoadPath = m_flagParams.at('i');
    Globals::FileIO::ImageFileName = Utility::RetrieveFileNameFromPath(paramList[0].toStdString().c_str());
}

void ArgumentParser::Parse_c_flag() const
{
    QString params(m_flagParams.at('c'));
    QStringList paramList(params.split(' ', QString::SkipEmptyParts));

    if (paramList.length() != 1)
    {
        m_logger << "Wrong number of parameters for flag '-c'. Expects: *path* to file where connectivity .inp file will be created." << std::endl;
        m_logger.LogError();
        MPI_Abort(Globals::MPI::Comm, 1);
    }

    QString extension = paramList[0].section('.', -1);
    QString result(paramList[0]);

    if (extension != "inp")
    {
        result = paramList[0].append(".inp");
    }

    Globals::FileIO::ConnectivityInpFilePath = result.toStdString();
}

void ArgumentParser::Parse_C_flag() const
{
    m_logger.TurnOffColor();
}

void ArgumentParser::Parse_o_flag() const
{
    QString params(m_flagParams.at('o'));
    QStringList paramList(params.split(' ', QString::SkipEmptyParts));

    if (paramList.length() != 1)
    {
        m_logger << "Wrong number of parameters for flag '-o'. Expects: *path* to folder where .vtk output will be created." << std::endl;
        m_logger.LogError();
        MPI_Abort(Globals::MPI::Comm, 1);
    }

    params.replace('\\','/');
    if (params.back() == '/')
    {
        params.chop(1);
    }

    Globals::FileIO::OutputPath = params.toStdString();

    if (!Utility::IsValidDirectory(Globals::FileIO::OutputPath.c_str()))
    {
        m_logger << "Specified output directory does not exist!" << std::endl;
        m_logger.LogError();
        MPI_Abort(Globals::MPI::Comm, 1);
    }
}

void ArgumentParser::Parse_O_flag() const
{
    QString params(m_flagParams.at('O'));
    QStringList paramList(params.split(' ', QString::SkipEmptyParts));

    if (paramList.length() > 3)
    {
        m_logger << "Wrong number of parameters for flag '-O'. Expects: *vtk* for vtk output, *image* for image output along with optional *binary* as output pgm format." << std::endl;
        m_logger.LogError();
        MPI_Abort(Globals::MPI::Comm, 1);
    }

    bool presentFlag[3] = { false, false, false };
    for(int i = 0; i < paramList.length(); ++i)
    {
        if (paramList[i] == "vtk") presentFlag[0] = true;
        else if (paramList[i] == "image") presentFlag[1] = true;
        else if (paramList[i] == "binary") presentFlag[2] = true;
    }

    Globals::FileIO::OutputVTKFormat = presentFlag[0];
    Globals::FileIO::OutputImageFormat = presentFlag[1];
    Globals::FileIO::OutputASCII = !presentFlag[2];
}

void ArgumentParser::Parse_X_flag() const
{
    Globals::FileIO::VTKExportPipelineNames.clear();
    Globals::FileIO::VTKExportPipelineNumbers.clear();
    Globals::FileIO::ImageExportPipelineNames.clear();
    Globals::FileIO::ImageExportPipelineNumbers.clear();
    std::vector<std::string> outputNames;

    QString params(m_flagParams.at('X'));
    QStringList paramList(params.split(',', QString::SkipEmptyParts));

    if (paramList.length() != 2)
    {
        m_logger << "Wrong number of parameters for flag '-X'. Expects: *pipeline export options* vtk export options, *pipeline export options* image export options.";
        m_logger << "You can specify as many pipelines to be exported as needed. Default is '0 original 1 result, 1 result'." << std::endl;
        m_logger.LogError();
        MPI_Abort(Globals::MPI::Comm, 1);
    }

    QStringList vtkParams(paramList[0].split(' ', QString::SkipEmptyParts));
    QStringList imageParams(paramList[1].split(' ', QString::SkipEmptyParts));

    if (vtkParams.length() % 2 == 1)    //we need pair number-name, so length must be even number
    {
        m_logger << "Wrong number of parameters for flag '-X' vtk part. Expects: *non-negative number < " << MAX_PIPELINE_NUMBER_STRING << "* pipeline number, *string* pipeline name.";
        m_logger << "You can specify as many pipelines to be exported as needed. Default is '0 original 1 result'." << std::endl;
        m_logger.LogError();
        MPI_Abort(Globals::MPI::Comm, 1);
    }

    if (imageParams.length() % 2 == 1)    //we need pair number-name, so length must be even number
    {
        m_logger << "Wrong number of parameters for flag '-X' image part. Expects: *non-negative number < " << MAX_PIPELINE_NUMBER_STRING << "* pipeline number, *string* pipeline name.";
        m_logger << "You can specify as many pipelines to be exported as needed. Default is '1 result'." << std::endl;
        m_logger.LogError();
        MPI_Abort(Globals::MPI::Comm, 1);
    }

    int numOfPipelinesToExport = vtkParams.length() / 2;

    Globals::FileIO::VTKExportPipelineNames.reserve(numOfPipelinesToExport);
    Globals::FileIO::VTKExportPipelineNumbers.reserve(numOfPipelinesToExport);

    //VTK export options parsing
    for (int i = 0; i < numOfPipelinesToExport; ++i)
    {
        bool conversionOK = false;
        int pipelineNumber = vtkParams[2 * i].toInt(&conversionOK);

        if (!conversionOK || pipelineNumber < 0 || pipelineNumber >= MAX_PIPELINE_NUMBER)
        {
            m_logger << "Wrong parameter (" << 2 * i << ") format for flag '-X' vtk part. Expects: *non-negative number < " << MAX_PIPELINE_NUMBER_STRING << "* pipeline number." << std::endl;
            m_logger.LogError();
            MPI_Abort(Globals::MPI::Comm, 1);
        }

        Globals::FileIO::VTKExportPipelineNumbers.push_back(pipelineNumber);
        std::string outputName(vtkParams[2 * i + 1].toStdString());
        Globals::FileIO::VTKExportPipelineNames.push_back(outputName);
        outputNames.push_back(outputName);
    }

    for(size_t i = 0; i < outputNames.size(); ++i)
    {
        if (outputNames[i] == "source")
        {
            m_logger << "VTK pipeline outputs cannot have the name 'source'!\n";
            m_logger.LogError();
            MPI_Abort(Globals::MPI::Comm, 1);
        }
    }

    if (HasDuplicates(outputNames))
    {
        m_logger << "VTK pipeline outputs cannot have the same name!\n";
        m_logger.LogError();
        MPI_Abort(Globals::MPI::Comm, 1);
    }

    outputNames.clear();
    numOfPipelinesToExport = imageParams.length() / 2;

    //Image export options parsing
    for (int i = 0; i < numOfPipelinesToExport; ++i)
    {
        bool conversionOK = false;
        int pipelineNumber = imageParams[2 * i].toInt(&conversionOK);

        if (!conversionOK || pipelineNumber < 0 || pipelineNumber >= MAX_PIPELINE_NUMBER)
        {
            m_logger << "Wrong parameter (" << 2 * i << ") format for flag '-X' image part. Expects: *non-negative number < " << MAX_PIPELINE_NUMBER_STRING << "* pipeline number." << std::endl;
            m_logger.LogError();
            MPI_Abort(Globals::MPI::Comm, 1);
        }

        Globals::FileIO::ImageExportPipelineNumbers.push_back(pipelineNumber);
        std::string outputName(imageParams[2 * i + 1].toStdString());
        Globals::FileIO::ImageExportPipelineNames.push_back(outputName);
        outputNames.push_back(outputName);
    }

    if (HasDuplicates(outputNames))
    {
        m_logger << "IMAGE pipeline outputs cannot have the same name!\n";
        m_logger.LogError();
        MPI_Abort(Globals::MPI::Comm, 1);
    }
}

void ArgumentParser::Parse_B_flag() const
{
    QString params(m_flagParams.at('B'));
    QStringList paramList(params.split(' ', QString::SkipEmptyParts));

    if (paramList.length() == 0 || paramList.length() > 2)
    {
        m_logger << "Wrong number of parameters for flag '-B'. Expects: *number* level up to which quadrants will be assigned OTSU's background mean value, ";
        m_logger << "instead of mean of image part corresponding to their position (negative values will be subtracted from maximum level), ";
        m_logger << "*non-negative number <= 255* optional value to be assigned instead of OTSU's." << std::endl;
        m_logger.LogError();
        MPI_Abort(Globals::MPI::Comm, 1);
    }

    bool conversionOK = false;
    int quadLevel = paramList[0].toInt(&conversionOK);

    if (!conversionOK)
    {
        m_logger << "Wrong first parameter format for flag '-B'. Expects: *number* level up to which quadrants will be assigned OTSU's background mean value, ";
        m_logger << "instead of mean of image part corresponding to their position. Negative values will be subtracted from maximum level." << std::endl;
        m_logger.LogError();
        MPI_Abort(Globals::MPI::Comm, 1);
    }

    int customValue = 0;

    if (paramList.length() == 2)
    {
        customValue = paramList[1].toInt(&conversionOK);
        Globals::Processing::AutoBackgroundQuadrantValue = false;

        if (!conversionOK || customValue < 0 || customValue > 255)
        {
            m_logger << "Wrong second parameter format for flag '-B'. Expects: *non-negative number <= 255* optional value to be assigned instead of OTSU's." << std::endl;
            m_logger.LogError();
            MPI_Abort(Globals::MPI::Comm, 1);
        }
    }

    Globals::Processing::SetBackgroundQuadrantValue = true;
    Globals::Processing::QuadrantLevelForOTSUBackgroundValue = quadLevel;
    Globals::Processing::BackgroundQuadrantValue = static_cast<unsigned char>(customValue);
}

void ArgumentParser::Parse_I_flag() const
{
    Globals::FileIO::OutputOriginalImageCopy = true;
}

void ArgumentParser::Parse_D_flag() const
{
    Globals::FileIO::CreateNewOutputDirectory = true;
}

void ArgumentParser::Parse_T_flag() const
{
    QString params(m_flagParams.at('T'));
    QStringList paramList(params.split(' ', QString::SkipEmptyParts));

    if (paramList.length() == 0 || paramList.length() > 2)
    {
        m_logger << "Wrong number of parameters for flag '-T'. Expects: 2^n *positive number* of initial tree side length with optional *non-negative number* value for extra pixels.";
        m_logger << " If initial tree side length is zero, application will try to find the most optimal value. If second parameter is not specified mean of original image is used instead." << std::endl;
        m_logger.LogError();
        MPI_Abort(Globals::MPI::Comm, 1);
    }

    bool conversionOK = false;
    int treeSideLength = paramList[0].toInt(&conversionOK);

    if (!conversionOK || treeSideLength < 0)
    {
        m_logger << "Wrong first parameter format for -T flag. Expects: 2^n * positive number* of initial tree side length.";
        m_logger << " If initial tree side length is zero, application will try to find the most optimal value." << std::endl;
        m_logger.LogError();
        MPI_Abort(Globals::MPI::Comm, 1);
    }

    if (!IsPowerOfTwo(treeSideLength))
    {
        m_logger << "First parameter for -T flag is not 2^n positive number. Expects: 2^n *positive number* of initial tree side length.";
        m_logger << " If initial tree side length is zero, application will try to find the most optimal value." << std::endl;
        m_logger.LogError();
        MPI_Abort(Globals::MPI::Comm, 1);
    }

    Globals::Forest::AutoTreeSideLength = treeSideLength == 0;

    Globals::Forest::TreeSideLength = treeSideLength;
    Globals::Forest::NumOfBytesPerTreeImageFragment = treeSideLength * treeSideLength;
    Globals::Refinement::MaxRefinementLevel = static_cast<int>(log2(treeSideLength));

    Globals::Processing::AutoExtraPixelValue = true;

    if (paramList.length() == 2)
    {
        int extraPixelValue = paramList[1].toInt(&conversionOK);

        if (extraPixelValue > 255 && conversionOK)
        {
            m_logger << "Value for extra pixels exceeds value of 255. Results might not be what you expect" << std::endl;
            m_logger.Log();
        }

        if (!conversionOK || extraPixelValue < 0)
        {
            m_logger << "Wrong second parameter format for -T flag. Expects: *non-negative number* value for extra pixels.";
            m_logger << " If second parameter is not specified, mean of original image is used instead." << std::endl;
            m_logger.LogError();
            MPI_Abort(Globals::MPI::Comm, 1);
        }

        Globals::Processing::AutoExtraPixelValue = false;
        Globals::Processing::ExtraPixelValue = extraPixelValue;
    }
}

void ArgumentParser::Parse_r_flag() const
{
    QString all(m_flagParams.at('r'));
    QStringList methodsWithParams(all.split(',', QString::SkipEmptyParts));  //different methods

    for (int i = 0; i < methodsWithParams.length(); ++i)
    {
        QString methodWithParams(methodsWithParams[i]);
        QStringList tokens(methodWithParams.trimmed().split(' ', QString::SkipEmptyParts));

        if (tokens.length() == 0)
        {
            m_logger << "Wrong input for -r flag, method " << i << ". No parameters given." << std::endl;
            m_logger.LogError();
            MPI_Abort(Globals::MPI::Comm, 1);
        }

        QString method(tokens[0]);

        //TODO: use negative numbers to specify refinement level from the maximum one

        if (method == "GLOBAL_OTSU") Parse_GLOBAL_OTSU_args(tokens, i);
        else if (method == "MAX_MIN_DIFF") Parse_MAX_MIN_DIFF_args(tokens, i);
        else if (method == "BERNSEN") Parse_BERNSEN_args(tokens, i);
        else if (method == "SAUVOLA") Parse_SAUVOLA_args(tokens, i);
        else if (method == "NIBLACK") Parse_NIBLACK_REFINE_args(tokens, i);
        else if (method == "TOTAL") Parse_TOTAL_args(tokens, i);
        else if (method == "UP_TO") Parse_UP_TO_args(tokens, i);
        else
        {
            m_logger << "Unknown method passed to -r flag!" << std::endl;
            m_logger.LogError();
            MPI_Abort(Globals::MPI::Comm, 1);
        }
    }
}

void ArgumentParser::Parse_GLOBAL_OTSU_args(const QStringList &tokens, int orderIndex) const
{
    Globals::Refinement::RefinementAlgorithms.push_back(ERefinementAlgorithm::GLOBAL_OTSU);

    if (tokens.length() != 2)
    {
        m_logger << "Wrong number of parameters for GLOBAL_OTSU(" << orderIndex << ") method. Expects: *non-negative number* quadrant level to refine from" << std::endl;
        m_logger.LogError();
        MPI_Abort(Globals::MPI::Comm, 1);
    }

    bool conversionOK = false;
    int quadrantLevel = tokens[1].toInt(&conversionOK);

    if (!conversionOK || quadrantLevel < 0)
    {
        m_logger << "Wrong first parameter format for GLOBAL_OTSU(" << orderIndex << ") method. Expects: *non-negative number* quadrant level to refine from." << std::endl;
        m_logger.LogError();
        MPI_Abort(Globals::MPI::Comm, 1);
    }

    SGlobalOTSUThresholdParams *params = new SGlobalOTSUThresholdParams;
    params->refineFromQuadLevel = quadrantLevel;

    Globals::Refinement::RefinementParams.push_back(params);
}

void ArgumentParser::Parse_MAX_MIN_DIFF_args(const QStringList &tokens, int orderIndex) const
{
    Globals::Refinement::RefinementAlgorithms.push_back(ERefinementAlgorithm::MAX_MIN_DIFF_THRESHOLD);

    if (tokens.length() != 3)
    {
        m_logger << "Wrong number of parameters for MAX_MIN_DIFF(" << orderIndex << ") method. Expects: *non-negative number* quadrant level to refine from, *non-negative number* threshold." << std::endl;
        m_logger.LogError();
        MPI_Abort(Globals::MPI::Comm, 1);
    }

    bool conversionOK = false;
    int quadrantLevel = tokens[1].toInt(&conversionOK);

    if (!conversionOK || quadrantLevel < 0)
    {
        m_logger << "Wrong first parameter format for MAX_MIN_DIFF(" << orderIndex << ") method. Expects: *non-negative number* quadrant level to refine from." << std::endl;
        m_logger.LogError();
        MPI_Abort(Globals::MPI::Comm, 1);
    }

    int maxMinDiffThreshold = tokens[2].toInt(&conversionOK);

    if (!conversionOK || maxMinDiffThreshold < 0)
    {
        m_logger << "Wrong second parameter format for MAX_MIN_DIFF(" << orderIndex << ") method. Expects: *non-negative number* threshold." << std::endl;
        m_logger.LogError();
        MPI_Abort(Globals::MPI::Comm, 1);
    }

    SMaxMinDiffThresholdParams *params = new SMaxMinDiffThresholdParams;
    params->refineFromQuadLevel = quadrantLevel;
    params->maxMinDiffThreshold = maxMinDiffThreshold;

    Globals::Refinement::RefinementParams.push_back(params);
}

void ArgumentParser::Parse_BERNSEN_args(const QStringList &tokens, int orderIndex) const
{
    Globals::Refinement::RefinementAlgorithms.push_back(ERefinementAlgorithm::BERNSEN);

    if (tokens.length() != 5)
    {
        m_logger << "Wrong number of parameters for BERNSEN(" << orderIndex << ") method. Expects: ";
        m_logger << "*non-negative number* quadrant level to refine from, *non-negative number* radius, *non-negative number* contrast threshold, *non-negative number* background-foreground threshold." << std::endl;
        m_logger.LogError();
        MPI_Abort(Globals::MPI::Comm, 1);
    }

    bool conversionOK = false;
    int minQuadLevel = tokens[1].toInt(&conversionOK);

    if (!conversionOK || minQuadLevel < 0)
    {
        m_logger << "Wrong first parameter format for BERNSEN(" << orderIndex << ") method. Expects: *non-negative number* quadrant level to refine from." << std::endl;
        m_logger.LogError();
        MPI_Abort(Globals::MPI::Comm, 1);
    }

    int radius = tokens[2].toInt(&conversionOK);

    if (!conversionOK || radius < 0)
    {
        m_logger << "Wrong second parameter format for BERNSEN(" << orderIndex << ") method. Expects: *non-negative number* radius." << std::endl;
        m_logger.LogError();
        MPI_Abort(Globals::MPI::Comm, 1);
    }


    if (radius > Globals::Forest::BleedingPixelEdgeRadius) Globals::Forest::BleedingPixelEdgeRadius = radius;

    double contrastThreshold = tokens[3].toDouble(&conversionOK);

    if (!conversionOK || contrastThreshold < 0.)
    {
        m_logger << "Wrong third parameter format for BERNSEN(" << orderIndex << ") method. Expects: *non-negative number* contrast threshold." << std::endl;
        m_logger.LogError();
        MPI_Abort(Globals::MPI::Comm, 1);
    }

    double backgroundForegroundThreshold = tokens[4].toDouble(&conversionOK);

    if (!conversionOK || backgroundForegroundThreshold < 0.)
    {
        m_logger << "Wrong fourth parameter format for BERNSEN(" << orderIndex << ") method. Expects: *non-negative number* background-foreground threshold." << std::endl;
        m_logger.LogError();
        MPI_Abort(Globals::MPI::Comm, 1);
    }

    SBernsenThresholdParams *params = new SBernsenThresholdParams;
    params->radius = radius;
    params->contrastThreshold = contrastThreshold;
    params->backgroundForegroundThreshold = backgroundForegroundThreshold;
    params->refineFromQuadLevel = minQuadLevel;

    Globals::Refinement::RefinementParams.push_back(params);
}

void ArgumentParser::Parse_SAUVOLA_args(const QStringList &tokens, int orderIndex) const
{
    Globals::Refinement::RefinementAlgorithms.push_back(ERefinementAlgorithm::SAUVOLA);

    if (tokens.length() != 5)
    {
        m_logger << "Wrong number of parameters for SAUVOLA(" << orderIndex << ") method. Expects: ";
        m_logger << "*non-negative number* quadrant level to refine from, *non-negative number* radius, *non-negative number* kappa coefficient, *positive number* maximal standard deviation." << std::endl;
        m_logger.LogError();
        MPI_Abort(Globals::MPI::Comm, 1);
    }

    bool conversionOK = false;
    int minQuadLevel = tokens[1].toInt(&conversionOK);

    if (!conversionOK || minQuadLevel < 0)
    {
        m_logger << "Wrong first parameter format for SAUVOLA(" << orderIndex << ") method. Expects: *non-negative number* quadrant level to refine from." << std::endl;
        m_logger.LogError();
        MPI_Abort(Globals::MPI::Comm, 1);
    }

    int radius = tokens[2].toInt(&conversionOK);

    if (!conversionOK || radius < 0)
    {
        m_logger << "Wrong second parameter format for SAUVOLA(" << orderIndex << ") method. Expects: *non-negative number* radius." << std::endl;
        m_logger.LogError();
        MPI_Abort(Globals::MPI::Comm, 1);
    }

    if (radius > Globals::Forest::BleedingPixelEdgeRadius) Globals::Forest::BleedingPixelEdgeRadius = radius;

    double kappa = tokens[3].toDouble(&conversionOK);

    if (!conversionOK || kappa < 0.)
    {
        m_logger << "Wrong third parameter format for SAUVOLA(" << orderIndex << ") method. Expects: *non-negative number* kappa coefficient." << std::endl;
        m_logger.LogError();
        MPI_Abort(Globals::MPI::Comm, 1);
    }

    double sigmaMax = tokens[4].toDouble(&conversionOK);

    if (!conversionOK || sigmaMax <= 0.)
    {
        m_logger << "Wrong fourth parameter format for SAUVOLA(" << orderIndex << ") method. Expects: *positive number* maximal standard deviation." << std::endl;
        m_logger.LogError();
        MPI_Abort(Globals::MPI::Comm, 1);
    }

    SSauvolaThresholdParams *params = new SSauvolaThresholdParams;
    params->radius = radius;
    params->kappa = kappa;
    params->sigmaMax = sigmaMax;
    params->refineFromQuadLevel = minQuadLevel;

    Globals::Refinement::RefinementParams.push_back(params);
}

void ArgumentParser::Parse_NIBLACK_REFINE_args(const QStringList &tokens, int orderIndex) const
{
    Globals::Refinement::RefinementAlgorithms.push_back(ERefinementAlgorithm::NIBLACK);

    if (tokens.length() != 5)
    {
        m_logger << "Wrong number of parameters for NIBLACK-refine(" << orderIndex << ") method. Expects: ";
        m_logger << "*non-negative number* quadrant level to refine from, *non-negative number* radius, *number* kappa coefficient, *number* mean offset." << std::endl;
        m_logger.LogError();
        MPI_Abort(Globals::MPI::Comm, 1);
    }

    bool conversionOK = false;
    int minQuadLevel = tokens[1].toInt(&conversionOK);

    if (!conversionOK || minQuadLevel < 0)
    {
        m_logger << "Wrong first parameter format for NIBLACK-refine(" << orderIndex << ") method. Expects: *non-negative number* quadrant level to refine from." << std::endl;
        m_logger.LogError();
        MPI_Abort(Globals::MPI::Comm, 1);
    }

    int radius = tokens[2].toInt(&conversionOK);

    if (!conversionOK || radius < 0)
    {
        m_logger << "Wrong second parameter format for NIBLACK-refine(" << orderIndex << ") method. Expects: *non-negative number* radius." << std::endl;
        m_logger.LogError();
        MPI_Abort(Globals::MPI::Comm, 1);
    }

    if (radius > Globals::Forest::BleedingPixelEdgeRadius) Globals::Forest::BleedingPixelEdgeRadius = radius;

    double kappa = tokens[3].toDouble(&conversionOK);

    if (!conversionOK)
    {
        m_logger << "Wrong third parameter format for NIBLACK-refine(" << orderIndex << ") method. Expects: *number* kappa coefficient." << std::endl;
        m_logger.LogError();
        MPI_Abort(Globals::MPI::Comm, 1);
    }

    double meanOffset = tokens[4].toDouble(&conversionOK);

    if (!conversionOK)
    {
        m_logger << "Wrong fourth parameter format for NIBLACK-refine(" << orderIndex << ") method. Expects: *number* mean offset." << std::endl;
        m_logger.LogError();
        MPI_Abort(Globals::MPI::Comm, 1);
    }

    SNiblackThresholdParams *params = new SNiblackThresholdParams;
    params->radius = radius;
    params->kappa = kappa;
    params->meanOffset = meanOffset;
    params->refineFromQuadLevel = minQuadLevel;

    Globals::Refinement::RefinementParams.push_back(params);
}

void ArgumentParser::Parse_TOTAL_args(const QStringList &tokens, int orderIndex) const
{
    Globals::Refinement::RefinementAlgorithms.push_back(ERefinementAlgorithm::TOTAL);

    if (tokens.length() != 1)
    {
        m_logger << "Wrong number of parameters for TOTAL(" << orderIndex << ") refine method. Expects: nothing ";
        m_logger.LogError();
        MPI_Abort(Globals::MPI::Comm, 1);
    }

    Globals::Refinement::RefinementParams.push_back(nullptr);
}

void ArgumentParser::Parse_UP_TO_args(const QStringList &tokens, int orderIndex) const
{
    Globals::Refinement::RefinementAlgorithms.push_back(ERefinementAlgorithm::UP_TO);

    if (tokens.length() != 2)
    {
        m_logger << "Wrong number of parameters for UP_TO(" << orderIndex << ") refine method. Expects: *non-negative number* quadrant level up to which to refine the forest.\n";
        m_logger.LogError();
        MPI_Abort(Globals::MPI::Comm, 1);
    }

    bool conversionOK = false;
    int quadLevel = tokens[1].toInt(&conversionOK);

    if (!conversionOK || quadLevel < 0)
    {
        m_logger << "Wrong first parameter format for UP_TO(" << orderIndex << ") refine method. Expects: *non-negative number* quadrant level up to which to refine the forest." << std::endl;
        m_logger.LogError();
        MPI_Abort(Globals::MPI::Comm, 1);
    }

    SUpToRefinementParams *params = new SUpToRefinementParams;
    params->quadLevel = quadLevel;
    Globals::Refinement::RefinementParams.push_back(params);
}

void ArgumentParser::Parse_m_flag()
{
    QString all(m_flagParams.at('m'));
    QStringList methodsWithParams(all.split(',', QString::SkipEmptyParts));   //different methods

    for(int i = 0; i < methodsWithParams.length(); ++i)
    {
        QString allParams(methodsWithParams[i].trimmed());
        QStringList processingAndRefinement(allParams.split('*', QString::SkipEmptyParts));

        QString methodWithParams;

        if (processingAndRefinement.length() > 0)
        {
            methodWithParams = processingAndRefinement[0];
        }
        else
        {
            m_logger << "Empty section for '-m' flag(" << i << ")!\n";
            m_logger.LogError();
            MPI_Abort(Globals::MPI::Comm, 1);
        }

        int methodEnd = methodWithParams.indexOf(' ');
        QString method;

        if (methodEnd == -1) method = methodWithParams;
        else method = QString(methodWithParams.left(methodEnd));

        if (processingAndRefinement.length() > 2)
        {
            m_logger << "Multiple continuous refinements for " << method.toStdString() << " method set!\n";
            m_logger.LogError();
            MPI_Abort(Globals::MPI::Comm, 1);
        }

        int pipelineStart = methodWithParams.indexOf('{', 0);
        int pipelineEnd = methodWithParams.indexOf('}', pipelineStart);
        QStringList tokens;
        QStringList refinementTokens;

        if (pipelineStart != -1 && pipelineEnd != -1)
        {
            int pipelineLength = pipelineEnd - pipelineStart;
            QString pipeline(methodWithParams.mid(pipelineStart + 1, pipelineLength - 1));
            tokens = methodWithParams.right(methodWithParams.length() - pipelineEnd - 1).trimmed().split(' ', QString::SkipEmptyParts);
            tokens.prepend(pipeline);

            if (processingAndRefinement.length() == 2)
            {
                refinementTokens = processingAndRefinement[1].split(' ', QString::SkipEmptyParts);
            }
        }        

        if (method == "LHE_EXPLICIT") Parse_LHE_EXPLICIT_args(tokens, refinementTokens, i);
        else if (method == "LHE_IMPLICIT") Parse_LHE_IMPLICIT_args(tokens, refinementTokens, i);
        else if (method == "NIBLACK") Parse_NIBLACK_IMAGE_args(tokens, i);
        else if (method == "MCF") Parse_MCF_args(tokens, refinementTokens, i);
        else if (method == "SUBSURF") Parse_SUBSURF_args(tokens, refinementTokens, i);
        else if (method == "SQUARE") Parse_SQUARE_args(tokens, i);
        else if (method == "MAX") Parse_MAX_args(tokens, i);
        else if (method == "EXPAND_REFINEMENT") Parse_EXPAND_REFINEMENT_args(tokens, i);
        else if (method == "REFINE_BY_THRESHOLD") Parse_REFINE_BY_THRESHOLD_args(tokens, i);
        else if (method == "FLAT_THRESHOLD") Parse_FLAT_THRESHOLD_args(tokens, i);
        else
        {
            m_logger << "Unknown method passed to -m flag!" << std::endl;
            m_logger.LogError();
            MPI_Abort(Globals::MPI::Comm, 1);
        }
    }
}

void ArgumentParser::Parse_LHE_EXPLICIT_args(const QStringList &tokens, const QStringList &refinementTokens, int orderIndex)
{
    Globals::Processing::Algorithms.push_back(EProcessingAlgorithm::LHE_EXPLICIT);

    if (tokens.length() != 3 && tokens.length() != 5)
    {
        m_logger << "Wrong number of parameters for LHE_EXPLICIT(" << orderIndex << ") method. Expects: *pipeline* input#output, *positive number* time step, *non-negative number* number of steps. ";
        m_logger << "Accepts optional 'anim' and *animation name* parameters.\n";
        m_logger.LogError();
        MPI_Abort(Globals::MPI::Comm, 1);
    }

    bool conversionOK = false;
    QStringList pipeline(tokens[0].split('#', QString::SkipEmptyParts));

    if (pipeline.size() != 2)
    {
        m_logger << "Wrong first parameter format for LHE_EXPLICIT(" << orderIndex << ") method. Expects: *pipeline* input#output." << std::endl;
        m_logger.LogError();
        MPI_Abort(Globals::MPI::Comm, 1);
    }

    int pipelineIN = pipeline[0].trimmed().toInt(&conversionOK);

    if (!conversionOK || pipelineIN >= MAX_PIPELINE_NUMBER || pipelineIN < 0)
    {
        m_logger << "Wrong pipeline input format for LHE_EXPLICIT(" << orderIndex << ") method. Expects: *non-negative number < " << MAX_PIPELINE_NUMBER_STRING << "*." << std::endl;
        m_logger.LogError();
        MPI_Abort(Globals::MPI::Comm, 1);
    }

    int pipelineOUT = pipeline[1].trimmed().toInt(&conversionOK);

    if (!conversionOK || pipelineOUT >= MAX_PIPELINE_NUMBER || pipelineOUT < 0)
    {
        m_logger << "Wrong pipeline output format for LHE_EXPLICIT(" << orderIndex << ") method. Expects: *non-negative number < " << MAX_PIPELINE_NUMBER_STRING << "*." << std::endl;
        m_logger.LogError();
        MPI_Abort(Globals::MPI::Comm, 1);
    }

    double timeStep = tokens[1].toDouble(&conversionOK);

    if (!conversionOK || timeStep <= 0.)
    {
        m_logger << "Wrong second parameter format for LHE_EXPLICIT(" << orderIndex << ") method. Expects: *positive number* time step." << std::endl;
        m_logger.LogError();
        MPI_Abort(Globals::MPI::Comm, 1);
    }

    int numOfSteps = tokens[2].toInt(&conversionOK);
    if (!conversionOK || numOfSteps < 0)
    {
        m_logger << "Wrong third parameter format for LHE_EXPLICIT(" << orderIndex << ") method. Expects: *non-negative number* number of steps." << std::endl;
        m_logger.LogError();
        MPI_Abort(Globals::MPI::Comm, 1);
    }

    bool bRefine = false;
    int numOfIterationsToRefineAfter = 0;

    if (refinementTokens.length() == 1)
    {
        bRefine = true;
        numOfIterationsToRefineAfter = refinementTokens[0].toInt(&conversionOK);

        if (!conversionOK || numOfIterationsToRefineAfter <= 0)
        {
            m_logger << "Wrong first parameter format in refinement options for LHE_EXPLICIT(" << orderIndex << ") method! ";
            m_logger << "Expects: *positive number* how often (iteration-wise) should refinement occur.\n";
            m_logger.LogError();
            MPI_Abort(Globals::MPI::Comm, 2);
        }
    }
    else if (refinementTokens.length() > 1)
    {
        m_logger << "Too many arguments for continuous refinement in LHE_EXPLICIT(" << orderIndex << ") method!\n";
        m_logger.LogError();
        MPI_Abort(Globals::MPI::Comm, 2);
    }

    SLHEExplicitParams *params = new SLHEExplicitParams;
    params->timeStep = timeStep;
    params->numOfSteps = numOfSteps;
    params->pipelineIN = pipelineIN;
    params->pipelineOUT = pipelineOUT;
    params->bRefine = bRefine;
    params->numOfIterationsToRefineAfter = numOfIterationsToRefineAfter;

    ParseAnimationArgs(tokens, "LHE_EXPLICIT", orderIndex, 3, params->animationFileName, params->bExportAnimation);

    Globals::Processing::AlgorithmParams.push_back(params);
}

void ArgumentParser::Parse_LHE_IMPLICIT_args(const QStringList &tokens, const QStringList &refinementTokens, int orderIndex)
{
    Globals::Processing::Algorithms.push_back(EProcessingAlgorithm::LHE_IMPLICIT);

    if (tokens.length() != 4 && tokens.length() != 6)
    {
        m_logger << "Wrong number of parameters for LHE_IMPLICIT(" << orderIndex << ") method. Expects: *pipeline* input#output, ";
        m_logger << "*positive number* time step, *non-negative number* number of steps, *non-negative number* tolerance. ";
        m_logger << "Accepts optional 'anim' and *animation name* parameters.\n";;
        m_logger.LogError();
        MPI_Abort(Globals::MPI::Comm, 1);
    }

    bool conversionOK = false;
    QStringList pipeline(tokens[0].split('#', QString::SkipEmptyParts));

    if (pipeline.size() != 2)
    {
        m_logger << "Wrong first parameter format for LHE_IMPLICIT(" << orderIndex << ") method. Expects: *pipeline* input#output." << std::endl;
        m_logger.LogError();
        MPI_Abort(Globals::MPI::Comm, 1);
    }

    int pipelineIN = pipeline[0].trimmed().toInt(&conversionOK);

    if (!conversionOK || pipelineIN >= MAX_PIPELINE_NUMBER || pipelineIN < 0)
    {
        m_logger << "Wrong pipeline input format for LHE_IMPLICIT(" << orderIndex << ") method. Expects: *non-negative number < " << MAX_PIPELINE_NUMBER_STRING << "*." << std::endl;
        m_logger.LogError();
        MPI_Abort(Globals::MPI::Comm, 1);
    }

    int pipelineOUT = pipeline[1].trimmed().toInt(&conversionOK);

    if (!conversionOK || pipelineOUT >= MAX_PIPELINE_NUMBER || pipelineOUT < 0)
    {
        m_logger << "Wrong pipeline output format for LHE_IMPLICIT(" << orderIndex << ") method. Expects: *non-negative number < " << MAX_PIPELINE_NUMBER_STRING << "*." << std::endl;
        m_logger.LogError();
        MPI_Abort(Globals::MPI::Comm, 1);
    }

    double timeStep = tokens[1].toDouble(&conversionOK);

    if (!conversionOK || timeStep <= 0.)
    {
        m_logger << "Wrong second parameter format for LHE_IMPLICIT(" << orderIndex << ") method. Expects: *positive number* time step." << std::endl;
        m_logger.LogError();
        MPI_Abort(Globals::MPI::Comm, 1);
    }

    int numOfSteps = tokens[2].toInt(&conversionOK);
    if (!conversionOK || numOfSteps < 0)
    {
        m_logger << "Wrong third parameter format for LHE_IMPLICIT(" << orderIndex << ") method. Expects: *non-negative number* number of steps." << std::endl;
        m_logger.LogError();
        MPI_Abort(Globals::MPI::Comm, 1);
    }

    double tol = tokens[3].toDouble(&conversionOK);
    if (!conversionOK || tol < 0.)
    {
        m_logger << "Wrong fourth parameter format for LHE_IMPLICIT(" << orderIndex << ") method. Expects: *non-negative number* tolerance." << std::endl;
        m_logger.LogError();
        MPI_Abort(Globals::MPI::Comm, 1);
    }

    bool bRefine = false;
    int numOfIterationsToRefineAfter = 0;

    if (refinementTokens.length() == 1)
    {
        bRefine = true;
        numOfIterationsToRefineAfter = refinementTokens[0].toInt(&conversionOK);

        if (!conversionOK || numOfIterationsToRefineAfter <= 0)
        {
            m_logger << "Wrong first parameter format in refinement options for LHE_IMPLICIT(" << orderIndex << ") method! ";
            m_logger << "Expects: *positive number* how often (iteration-wise) should refinement occur.\n";
            m_logger.LogError();
            MPI_Abort(Globals::MPI::Comm, 2);
        }
    }
    else if (refinementTokens.length() > 1)
    {
        m_logger << "Too many arguments for continuous refinement in LHE_IMPLICIT(" << orderIndex << ") method!\n";
        m_logger.LogError();
        MPI_Abort(Globals::MPI::Comm, 2);
    }

    SLHEImplicitParams *params = new SLHEImplicitParams;
    params->timeStep = timeStep;
    params->numOfSteps = numOfSteps;
    params->tol = tol;
    params->pipelineIN = pipelineIN;
    params->pipelineOUT = pipelineOUT;
    params->bRefine = bRefine;
    params->numOfIterationsToRefineAfter = numOfIterationsToRefineAfter;

    ParseAnimationArgs(tokens, "LHE_IMPLICIT", orderIndex, 4, params->animationFileName, params->bExportAnimation);

    Globals::Processing::AlgorithmParams.push_back(params);
}

void ArgumentParser::Parse_NIBLACK_IMAGE_args(const QStringList &tokens, int orderIndex) const
{
    Globals::Processing::Algorithms.push_back(EProcessingAlgorithm::NIBLACK);

    if (tokens.length() != 4)
    {
        m_logger << "Wrong number of parameters for NIBLACK-image(" << orderIndex << ") method. Expects: *pipeline* input1(intensity) input2(intensitySquared) input3(thresholding)#output, ";
        m_logger << "*non-negative number* radius, *number* kappa coefficient, *number* mean offset.";
        m_logger << " If radius > 0, input2(intensitySquared) is ignored and value gets computed using neighbors." << std::endl;
        m_logger.LogError();
        MPI_Abort(Globals::MPI::Comm, 1);
    }

    bool conversionOK = false;
    QStringList pipeline(tokens[0].split('#', QString::SkipEmptyParts));

    if (pipeline.size() != 2)
    {
        m_logger << "Wrong first parameter format for NIBLACK-image(" << orderIndex << ") method. Expects: *pipeline* input1(intensity) input2(intensitySquared) input3(thresholding)#output." << std::endl;
        m_logger.LogError();
        MPI_Abort(Globals::MPI::Comm, 1);
    }

    QStringList inputs(pipeline[0].split(' ', QString::SkipEmptyParts));

    if (inputs.size() != 3)
    {
        m_logger << "Wrong pipeline input format for NIBLACK-image(" << orderIndex << ") method. Expects: *non-negative number < " << MAX_PIPELINE_NUMBER_STRING << "* (intensity) *non-negative number < ";
        m_logger << MAX_PIPELINE_NUMBER_STRING << "* (intensitySquared) ";
        m_logger << "*non-negative number < "<< MAX_PIPELINE_NUMBER_STRING << "* (thresholding)." << std::endl;
        m_logger.LogError();
        MPI_Abort(Globals::MPI::Comm, 1);
    }

    int pipelineINProcessing = inputs[0].trimmed().toInt(&conversionOK);

    if (!conversionOK || pipelineINProcessing >= MAX_PIPELINE_NUMBER || pipelineINProcessing < 0)
    {
        m_logger << "Wrong pipeline input format for NIBLACK-image(" << orderIndex << ") method. Expects: *non-negative number < " << MAX_PIPELINE_NUMBER_STRING << "* (intensity) *non-negative number < ";
        m_logger << MAX_PIPELINE_NUMBER_STRING << "* (intensitySquared) ";
        m_logger << "*non-negative number < "<< MAX_PIPELINE_NUMBER_STRING << "* (thresholding)." << std::endl;
        m_logger.LogError();
        MPI_Abort(Globals::MPI::Comm, 1);
    }

    int pipelineINProcessingSquared = inputs[1].trimmed().toInt(&conversionOK);

    if (!conversionOK || pipelineINProcessingSquared >= MAX_PIPELINE_NUMBER || pipelineINProcessingSquared < 0)
    {
        m_logger << "Wrong pipeline input format for NIBLACK-image(" << orderIndex << ") method. Expects: *non-negative number < " << MAX_PIPELINE_NUMBER_STRING << "* (intensity) *non-negative number < ";
        m_logger << MAX_PIPELINE_NUMBER_STRING << "* (intensitySquared) ";
        m_logger << "*non-negative number < "<< MAX_PIPELINE_NUMBER_STRING << "* (thresholding)." << std::endl;
        m_logger.LogError();
        MPI_Abort(Globals::MPI::Comm, 1);
    }

    int pipelineINThresholding = inputs[2].trimmed().toInt(&conversionOK);

    if (!conversionOK || pipelineINThresholding >= MAX_PIPELINE_NUMBER || pipelineINThresholding < 0)
    {
        m_logger << "Wrong pipeline input format for NIBLACK-image(" << orderIndex << ") method. Expects: *non-negative number < " << MAX_PIPELINE_NUMBER_STRING << "* (intensity) *non-negative number < ";
        m_logger << MAX_PIPELINE_NUMBER_STRING << "* (intensitySquared) ";
        m_logger << "*non-negative number < "<< MAX_PIPELINE_NUMBER_STRING << "* (thresholding)." << std::endl;
        m_logger.LogError();
        MPI_Abort(Globals::MPI::Comm, 1);
    }

    int pipelineOUT = pipeline[1].trimmed().toInt(&conversionOK);

    if (!conversionOK || pipelineOUT >= MAX_PIPELINE_NUMBER || pipelineOUT < 0)
    {
        m_logger << "Wrong pipeline output format for NIBLACK-image(" << orderIndex << ") method. Expects: *non-negative number < " << MAX_PIPELINE_NUMBER_STRING << "*." << std::endl;
        m_logger.LogError();
        MPI_Abort(Globals::MPI::Comm, 1);
    }

    int radius = tokens[1].toInt(&conversionOK);

    if (!conversionOK || radius < 0)
    {
        m_logger << "Wrong second parameter format for NIBLACK-image(" << orderIndex << ") method. Expects: *non-negative number* radius." << std::endl;
        m_logger << " If radius > 0, input2(intensitySquared) is ignored and value gets computed using neighbors." << std::endl;
        m_logger.LogError();
        MPI_Abort(Globals::MPI::Comm, 1);
    }

    double kappa = tokens[2].toDouble(&conversionOK);

    if (!conversionOK)
    {
        m_logger << "Wrong third parameter format for NIBLACK-image(" << orderIndex << ") method. Expects: *number* kappa coefficient." << std::endl;
        m_logger.LogError();
        MPI_Abort(Globals::MPI::Comm, 1);
    }

    double meanOffset = tokens[3].toDouble(&conversionOK);

    if (!conversionOK)
    {
        m_logger << "Wrong fourth parameter format for NIBLACK-image(" << orderIndex << ") method. Expects: *number* mean offset." << std::endl;
        m_logger.LogError();
        MPI_Abort(Globals::MPI::Comm, 1);
    }

    SNiblackThresholdParams *params = new SNiblackThresholdParams;
    params->radius = radius;
    params->kappa = kappa;
    params->meanOffset = meanOffset;
    params->refineFromQuadLevel = 0;                    //just to have it initialized
    params->pipelineINProcessing = pipelineINProcessing;
    params->pipelineINProcessingSquared = pipelineINProcessingSquared;
    params->pipelineINThresholding = pipelineINThresholding;
    params->pipelineOUT = pipelineOUT;

    Globals::Processing::AlgorithmParams.push_back(params);
}

void ArgumentParser::Parse_MCF_args(const QStringList &tokens, const QStringList &refinementTokens, int orderIndex)
{
    Globals::Processing::Algorithms.push_back(EProcessingAlgorithm::MCF);

    if (tokens.length() != 5 && tokens.length() != 7)
    {
        m_logger << "Wrong number of parameters for MCF(" << orderIndex << ") method. Expects: *pipeline* input#output, ";
        m_logger << "*positive number* time step, *non-negative number* number of steps, *non-negative number* tolerance, *non-negative number* epsilon. ";
        m_logger << "Accepts optional 'anim' and *animation name* parameters.\n";
        m_logger.LogError();
        MPI_Abort(Globals::MPI::Comm, 1);
    }

    bool conversionOK = false;
    QStringList pipeline(tokens[0].split('#', QString::SkipEmptyParts));

    if (pipeline.size() != 2)
    {
        m_logger << "Wrong first parameter format for MCF(" << orderIndex << ") method. Expects: *pipeline* input#output." << std::endl;
        m_logger.LogError();
        MPI_Abort(Globals::MPI::Comm, 1);
    }

    int pipelineIN = pipeline[0].trimmed().toInt(&conversionOK);

    if (!conversionOK || pipelineIN >= MAX_PIPELINE_NUMBER || pipelineIN < 0)
    {
        m_logger << "Wrong pipeline input format for MCF(" << orderIndex << ") method. Expects: *non-negative number < " << MAX_PIPELINE_NUMBER_STRING << "*." << std::endl;
        m_logger.LogError();
        MPI_Abort(Globals::MPI::Comm, 1);
    }

    int pipelineOUT = pipeline[1].trimmed().toInt(&conversionOK);

    if (!conversionOK || pipelineOUT >= MAX_PIPELINE_NUMBER || pipelineOUT < 0)
    {
        m_logger << "Wrong pipeline output format for MCF(" << orderIndex << ") method. Expects: *non-negative number < " << MAX_PIPELINE_NUMBER_STRING << "*." << std::endl;
        m_logger.LogError();
        MPI_Abort(Globals::MPI::Comm, 1);
    }

    double timeStep = tokens[1].toDouble(&conversionOK);

    if (!conversionOK || timeStep <= 0.)
    {
        m_logger << "Wrong second parameter format for MCF(" << orderIndex << ") method. Expects: *positive number* time step." << std::endl;
        m_logger.LogError();
        MPI_Abort(Globals::MPI::Comm, 1);
    }

    int numOfSteps = tokens[2].toInt(&conversionOK);
    if (!conversionOK || numOfSteps < 0)
    {
        m_logger << "Wrong third parameter format for MCF(" << orderIndex << ") method. Expects: *non-negative number* number of steps." << std::endl;
        m_logger.LogError();
        MPI_Abort(Globals::MPI::Comm, 1);
    }

    double tol = tokens[3].toDouble(&conversionOK);
    if (!conversionOK || tol < 0.)
    {
        m_logger << "Wrong fourth parameter format for MCF(" << orderIndex << ") method. Expects: *non-negative number* tolerance." << std::endl;
        m_logger.LogError();
        MPI_Abort(Globals::MPI::Comm, 1);
    }

    double epsilon = tokens[4].toDouble(&conversionOK);
    if (!conversionOK || epsilon < 0.)
    {
        m_logger << "Wrong fifth parameter format for MCF(" << orderIndex << ") method. Expects: *non-negative number* epsilon." << std::endl;
        m_logger.LogError();
        MPI_Abort(Globals::MPI::Comm, 1);
    }

    bool bRefine = false;
    int numOfIterationsToRefineAfter = 0;

    if (refinementTokens.length() == 1)
    {
        bRefine = true;
        numOfIterationsToRefineAfter = refinementTokens[0].toInt(&conversionOK);

        if (!conversionOK || numOfIterationsToRefineAfter <= 0)
        {
            m_logger << "Wrong first parameter format in refinement options for MCF(" << orderIndex << ") method! ";
            m_logger << "Expects: *positive number* how often (iteration-wise) should refinement occur.\n";
            m_logger.LogError();
            MPI_Abort(Globals::MPI::Comm, 2);
        }
    }
    else if (refinementTokens.length() > 1)
    {
        m_logger << "Too many arguments for continuous refinement in MCF(" << orderIndex << ") method!\n";
        m_logger.LogError();
        MPI_Abort(Globals::MPI::Comm, 2);
    }

    SMCFParams *params = new SMCFParams;
    params->timeStep = timeStep;
    params->numOfSteps = numOfSteps;
    params->tol = tol;
    params->epsilon = epsilon;
    params->pipelineIN = pipelineIN;
    params->pipelineOUT = pipelineOUT;
    params->bRefine = bRefine;
    params->numOfIterationsToRefineAfter = numOfIterationsToRefineAfter;

    ParseAnimationArgs(tokens, "MCF", orderIndex, 5, params->animationFileName, params->bExportAnimation);

    Globals::Processing::AlgorithmParams.push_back(params);
}

void ArgumentParser::Parse_SUBSURF_args(const QStringList &tokens, const QStringList &refinementTokens, int orderIndex)
{
    Globals::Processing::Algorithms.push_back(EProcessingAlgorithm::SUBSURF);

    if (tokens.length() != 6 && tokens.length() != 8)
    {
        m_logger << "Wrong number of parameters for SUBSURF(" << orderIndex << ") method. Expects: *pipeline* input1(I0) input2(U0)#output, ";
        m_logger << "*positive number* time step, *non-negative number* number of steps, *non-negative number* tolerance, *non-negative number* epsilon, *non-negative number* K. ";
        m_logger << "Accepts optional 'anim' and *animation name* parameters.\n";
        m_logger.LogError();
        MPI_Abort(Globals::MPI::Comm, 1);
    }

    bool conversionOK = false;
    QStringList pipeline(tokens[0].split('#', QString::SkipEmptyParts));

    if (pipeline.size() != 2)
    {
        m_logger << "Wrong first parameter format for SUBSURF(" << orderIndex << ") method. Expects: *pipeline* *pipeline* input1(I0) input2(U0)#output." << std::endl;
        m_logger.LogError();
        MPI_Abort(Globals::MPI::Comm, 1);
    }

    QStringList input(pipeline[0].split(' ', QString::SkipEmptyParts));

    if (input.length() != 2)
    {
        m_logger << "Wrong pipeline input format for SUBSURF(" << orderIndex << ") method. Expects: *non-negative number < " << MAX_PIPELINE_NUMBER_STRING << "* input1(I0)";
        m_logger << "*non-negative number < " << MAX_PIPELINE_NUMBER_STRING << "* input2(U0)."<< std::endl;
        m_logger.LogError();
        MPI_Abort(Globals::MPI::Comm, 1);
    }

    int pipelineIN_I0 = input[0].trimmed().toInt(&conversionOK);

    if (!conversionOK || pipelineIN_I0 >= MAX_PIPELINE_NUMBER || pipelineIN_I0 < 0)
    {
        m_logger << "Wrong pipeline input1(I0) format for SUBSURF(" << orderIndex << ") method. Expects: *non-negative number < " << MAX_PIPELINE_NUMBER_STRING << "*." << std::endl;
        m_logger.LogError();
        MPI_Abort(Globals::MPI::Comm, 1);
    }

    int pipelineIN_U0 = input[1].trimmed().toInt(&conversionOK);

    if (!conversionOK || pipelineIN_U0 >= MAX_PIPELINE_NUMBER || pipelineIN_U0 < 0)
    {
        m_logger << "Wrong pipeline input2(U0) format for SUBSURF(" << orderIndex << ") method. Expects: *non-negative number < " << MAX_PIPELINE_NUMBER_STRING << "*." << std::endl;
        m_logger.LogError();
        MPI_Abort(Globals::MPI::Comm, 1);
    }

    int pipelineOUT = pipeline[1].trimmed().toInt(&conversionOK);

    if (!conversionOK || pipelineOUT >= MAX_PIPELINE_NUMBER || pipelineOUT < 0)
    {
        m_logger << "Wrong pipeline output format for SUBSURF(" << orderIndex << ") method. Expects: *non-negative number < " << MAX_PIPELINE_NUMBER_STRING << "*." << std::endl;
        m_logger.LogError();
        MPI_Abort(Globals::MPI::Comm, 1);
    }

    double timeStep = tokens[1].toDouble(&conversionOK);

    if (!conversionOK || timeStep <= 0.)
    {
        m_logger << "Wrong second parameter format for SUBSURF(" << orderIndex << ") method. Expects: *positive number* time step." << std::endl;
        m_logger.LogError();
        MPI_Abort(Globals::MPI::Comm, 1);
    }

    int numOfSteps = tokens[2].toInt(&conversionOK);
    if (!conversionOK || numOfSteps < 0)
    {
        m_logger << "Wrong third parameter format for SUBSURF(" << orderIndex << ") method. Expects: *non-negative number* number of steps." << std::endl;
        m_logger.LogError();
        MPI_Abort(Globals::MPI::Comm, 1);
    }

    double tol = tokens[3].toDouble(&conversionOK);
    if (!conversionOK || tol < 0.)
    {
        m_logger << "Wrong fourth parameter format for SUBSURF(" << orderIndex << ") method. Expects: *non-negative number* tolerance." << std::endl;
        m_logger.LogError();
        MPI_Abort(Globals::MPI::Comm, 1);
    }

    double epsilon = tokens[4].toDouble(&conversionOK);
    if (!conversionOK || epsilon < 0.)
    {
        m_logger << "Wrong fifth parameter format for SUBSURF(" << orderIndex << ") method. Expects: *non-negative number* epsilon." << std::endl;
        m_logger.LogError();
        MPI_Abort(Globals::MPI::Comm, 1);
    }

    double K = tokens[5].toDouble(&conversionOK);
    if (!conversionOK || K < 0.)
    {
        m_logger << "Wrong sixth parameter format for SUBSURF(" << orderIndex << ") method. Expects: *non-negative number* K." << std::endl;
        m_logger.LogError();
        MPI_Abort(Globals::MPI::Comm, 1);
    }

    bool bRefine = false;
    int numOfIterationsToRefineAfter = 0;

    if (refinementTokens.length() == 1)
    {
        bRefine = true;
        numOfIterationsToRefineAfter = refinementTokens[0].toInt(&conversionOK);

        if (!conversionOK || numOfIterationsToRefineAfter <= 0)
        {
            m_logger << "Wrong first parameter format in refinement options for SUBSURF(" << orderIndex << ") method! ";
            m_logger << "Expects: *positive number* how often (iteration-wise) should refinement occur.\n";
            m_logger.LogError();
            MPI_Abort(Globals::MPI::Comm, 2);
        }
    }
    else if (refinementTokens.length() > 1)
    {
        m_logger << "Too many arguments for continuous refinement in SUBSURF(" << orderIndex << ") method!\n";
        m_logger.LogError();
        MPI_Abort(Globals::MPI::Comm, 2);
    }

    SSubsurfParams *params = new SSubsurfParams;
    params->timeStep = timeStep;
    params->numOfSteps = numOfSteps;
    params->tol = tol;
    params->epsilon = epsilon;
    params->K = K;
    params->pipelineIN_I0 = pipelineIN_I0;
    params->pipelineIN_U0 = pipelineIN_U0;
    params->pipelineOUT = pipelineOUT;
    params->bRefine = bRefine;
    params->numOfIterationsToRefineAfter = numOfIterationsToRefineAfter;

    ParseAnimationArgs(tokens, "SUBSURF", orderIndex, 6, params->animationFileName, params->bExportAnimation);

    Globals::Processing::AlgorithmParams.push_back(params);
}

void ArgumentParser::Parse_SQUARE_args(const QStringList &tokens, int orderIndex) const
{
    Globals::Processing::Algorithms.push_back(EProcessingAlgorithm::SQUARE);

    if (tokens.length() != 1)
    {
        m_logger << "Wrong number of parameters for SQUARE(" << orderIndex << ") method. Expects: *pipeline* input/output.\n";
        m_logger.LogError();
        MPI_Abort(Globals::MPI::Comm, 1);
    }

    bool conversionOK = false;
    QStringList pipeline(tokens[0].split('#', QString::SkipEmptyParts));

    if (pipeline.size() != 2)
    {
        m_logger << "Wrong first parameter format for SQUARE(" << orderIndex << ") method. Expects: *pipeline* input/output." << std::endl;
        m_logger.LogError();
        MPI_Abort(Globals::MPI::Comm, 1);
    }

    int pipelineIN = pipeline[0].trimmed().toInt(&conversionOK);

    if (!conversionOK || pipelineIN >= MAX_PIPELINE_NUMBER || pipelineIN < 0)
    {
        m_logger << "Wrong pipeline input format for SQUARE(" << orderIndex << ") method. Expects: *non-negative number < " << MAX_PIPELINE_NUMBER_STRING << "*." << std::endl;
        m_logger.LogError();
        MPI_Abort(Globals::MPI::Comm, 1);
    }

    int pipelineOUT = pipeline[1].trimmed().toInt(&conversionOK);

    if (!conversionOK || pipelineOUT >= MAX_PIPELINE_NUMBER || pipelineOUT < 0)
    {
        m_logger << "Wrong pipeline output format for SQUARE(" << orderIndex << ") method. Expects: *non-negative number < " << MAX_PIPELINE_NUMBER_STRING << "*." << std::endl;
        m_logger.LogError();
        MPI_Abort(Globals::MPI::Comm, 1);
    }

    SSquareParams *params = new SSquareParams;
    params->pipelineIN = pipelineIN;
    params->pipelineOUT = pipelineOUT;

    Globals::Processing::AlgorithmParams.push_back(params);
}

void ArgumentParser::Parse_MAX_args(const QStringList &tokens, int orderIndex) const
{
    Globals::Processing::Algorithms.push_back(EProcessingAlgorithm::MAX);

    if (tokens.length() != 1)
    {
        m_logger << "Wrong number of parameters for MAX(" << orderIndex << ") method. Expects: *pipeline* input1 input2/output.\n";
        m_logger.LogError();
        MPI_Abort(Globals::MPI::Comm, 1);
    }

    bool conversionOK = false;
    QStringList pipeline(tokens[0].split('#', QString::SkipEmptyParts));

    if (pipeline.size() != 2)
    {
        m_logger << "Wrong first parameter format for MAX(" << orderIndex << ") method. Expects: *pipeline* input1 input2/output." << std::endl;
        m_logger.LogError();
        MPI_Abort(Globals::MPI::Comm, 1);
    }

    QStringList input(pipeline[0].split(' ', QString::SkipEmptyParts));

    if (input.length() != 2)
    {
        m_logger << "Wrong pipeline input format for MAX(" << orderIndex << ") method. Expects: *non-negative number < " << MAX_PIPELINE_NUMBER_STRING << "* input1";
        m_logger << "*non-negative number < " << MAX_PIPELINE_NUMBER_STRING << "* input2."<< std::endl;
        m_logger.LogError();
        MPI_Abort(Globals::MPI::Comm, 1);
    }

    int pipelineIN1 = input[0].trimmed().toInt(&conversionOK);

    if (!conversionOK || pipelineIN1 >= MAX_PIPELINE_NUMBER || pipelineIN1 < 0)
    {
        m_logger << "Wrong pipeline input1 format for MAX(" << orderIndex << ") method. Expects: *non-negative number < " << MAX_PIPELINE_NUMBER_STRING << "*." << std::endl;
        m_logger.LogError();
        MPI_Abort(Globals::MPI::Comm, 1);
    }

    int pipelineIN2 = input[1].trimmed().toInt(&conversionOK);

    if (!conversionOK || pipelineIN2 >= MAX_PIPELINE_NUMBER || pipelineIN2 < 0)
    {
        m_logger << "Wrong pipeline input2 format for MAX(" << orderIndex << ") method. Expects: *non-negative number < " << MAX_PIPELINE_NUMBER_STRING << "*." << std::endl;
        m_logger.LogError();
        MPI_Abort(Globals::MPI::Comm, 1);
    }

    int pipelineOUT = pipeline[1].trimmed().toInt(&conversionOK);

    if (!conversionOK || pipelineOUT >= MAX_PIPELINE_NUMBER || pipelineOUT < 0)
    {
        m_logger << "Wrong pipeline output format for MAX(" << orderIndex << ") method. Expects: *non-negative number < " << MAX_PIPELINE_NUMBER_STRING << "*." << std::endl;
        m_logger.LogError();
        MPI_Abort(Globals::MPI::Comm, 1);
    }

    SMaxParams *params = new SMaxParams;
    params->pipelineIN1 = pipelineIN1;
    params->pipelineIN2 = pipelineIN2;
    params->pipelineOUT = pipelineOUT;

    Globals::Processing::AlgorithmParams.push_back(params);
}

void ArgumentParser::Parse_EXPAND_REFINEMENT_args(const QStringList &tokens, int orderIndex) const
{
    Globals::Processing::Algorithms.push_back(EProcessingAlgorithm::EXPAND_REFINEMENT);

    if (tokens.length() != 0)
    {
        m_logger << "Wrong number of parameters for EXPAND_REFINEMENT(" << orderIndex << ") method. Expects: nothing.\n";
        m_logger.LogError();
        MPI_Abort(Globals::MPI::Comm, 1);
    }

    Globals::Processing::AlgorithmParams.push_back(nullptr);
}

void ArgumentParser::Parse_REFINE_BY_THRESHOLD_args(const QStringList &tokens, int orderIndex) const
{
    Globals::Processing::Algorithms.push_back(EProcessingAlgorithm::REFINE_BY_THRESHOLD);

    if (tokens.length() != 2)
    {
        m_logger << "Wrong number of parameters for REFINE_BY_THRESHOLD(" << orderIndex << ") method.";
        m_logger << " Expects: *pipeline* input, *number* threshold.\n";
        m_logger.LogError();
        MPI_Abort(Globals::MPI::Comm, 1);
    }

    bool conversionOK = false;
    int pipeline = tokens[0].trimmed().toInt(&conversionOK);

    if (!conversionOK || pipeline >= MAX_PIPELINE_NUMBER || pipeline < 0)
    {
        m_logger << "Wrong pipeline parameter format for REFINE_BY_THRESHOLD(" << orderIndex << ") method. Expects: *non-negative number < " << MAX_PIPELINE_NUMBER_STRING << "*." << std::endl;
        m_logger.LogError();
        MPI_Abort(Globals::MPI::Comm, 1);
    }

    double threshold = tokens[1].trimmed().toDouble(&conversionOK);

    if (!conversionOK)
    {
        m_logger << "Wrong second parameter format for REFINE_BY_THRESHOLD(" << orderIndex << ") method. Expects: *number*." << std::endl;
        m_logger.LogError();
        MPI_Abort(Globals::MPI::Comm, 1);
    }

    SRefineByThresholdParams *params = new SRefineByThresholdParams;
    params->pipeline = pipeline;
    params->threshold = threshold;

    Globals::Processing::AlgorithmParams.push_back(params);
}

void ArgumentParser::Parse_FLAT_THRESHOLD_args(const QStringList &tokens, int orderIndex) const
{
    Globals::Processing::Algorithms.push_back(EProcessingAlgorithm::FLAT_THRESHOLD);

    if (tokens.length() != 2)
    {
        m_logger << "Wrong number of parameters for FLAT_THRESHOLD(" << orderIndex << ") method.";
        m_logger << " Expects: *pipeline* input/output, *number* threshold.\n";
        m_logger.LogError();
        MPI_Abort(Globals::MPI::Comm, 1);
    }

    bool conversionOK = false;
    QStringList pipeline(tokens[0].split('#', QString::SkipEmptyParts));

    if (pipeline.size() != 2)
    {
        m_logger << "Wrong first parameter format for FLAT_THRESHOLD(" << orderIndex << ") method. Expects: *pipeline* input/output." << std::endl;
        m_logger.LogError();
        MPI_Abort(Globals::MPI::Comm, 1);
    }

    int pipelineIN = pipeline[0].trimmed().toInt(&conversionOK);

    if (!conversionOK || pipelineIN >= MAX_PIPELINE_NUMBER || pipelineIN < 0)
    {
        m_logger << "Wrong pipeline input format for FLAT_THRESHOLD(" << orderIndex << ") method. Expects: *non-negative number < " << MAX_PIPELINE_NUMBER_STRING << "*." << std::endl;
        m_logger.LogError();
        MPI_Abort(Globals::MPI::Comm, 1);
    }

    int pipelineOUT = pipeline[1].trimmed().toInt(&conversionOK);

    if (!conversionOK || pipelineOUT >= MAX_PIPELINE_NUMBER || pipelineOUT < 0)
    {
        m_logger << "Wrong pipeline output format for FLAT_THRESHOLD(" << orderIndex << ") method. Expects: *non-negative number < " << MAX_PIPELINE_NUMBER_STRING << "*." << std::endl;
        m_logger.LogError();
        MPI_Abort(Globals::MPI::Comm, 1);
    }

    double threshold = tokens[1].trimmed().toDouble(&conversionOK);

    if (!conversionOK)
    {
        m_logger << "Wrong second parameter format for FLAT_THRESHOLD(" << orderIndex << ") method. Expects: *number*." << std::endl;
        m_logger.LogError();
        MPI_Abort(Globals::MPI::Comm, 1);
    }

    SFlatThresholdParams *params = new SFlatThresholdParams;
    params->pipelineIN = pipelineIN;
    params->pipelineOUT = pipelineOUT;
    params->threshold = threshold;

    Globals::Processing::AlgorithmParams.push_back(params);
}

void ArgumentParser::ParseAnimationArgs(const QStringList &tokens, const char *method,
                                        int orderIndex, int atIndex,
                                        std::string &animationName, bool &exportAnimation)
{
    if (tokens.length() <= atIndex)
    {
        exportAnimation = false;
        animationName = "";
        return;
    }

    if (tokens[atIndex] == "anim")
    {
        if (tokens.length() == atIndex + 1)
        {
            m_logger << "Missing animation name for " << method << "(" << orderIndex << ") method.\n";
            m_logger.LogError();
            MPI_Abort(Globals::MPI::Comm, 1);
        }

        if (tokens.length() > atIndex + 2)
        {
            m_logger << "More arguments than expected for " << method << "(" << orderIndex << ") method.\n";
            m_logger.LogError();
            MPI_Abort(Globals::MPI::Comm, 1);
        }

        m_animationNames.push_back(tokens[atIndex + 1].toStdString());
        animationName = tokens[atIndex + 1].toStdString();
        exportAnimation = true;
    }
    else
    {
        m_logger << "Wrong animation parameter format for " << method << "(" << orderIndex << ") method.\n";
        m_logger.LogError();
        MPI_Abort(Globals::MPI::Comm, 1);
    }
}

bool ArgumentParser::HasDuplicates(std::vector<std::string> &names) const
{
    if (names.size() == 0) return false;

    std::sort(names.begin(), names.end());

    for (size_t i = 0; i < names.size() - 1; ++i)
    {
        if (names[i] == names[i + 1]) return true;
    }

    return false;
}

void ArgumentParser::PrintHelpInfo() const
{
    m_logger << "\n\n********** HELP **********\n";
    m_logger << "Maximum available pipelines: " << MAX_PIPELINE_NUMBER_STRING << " (zero based indexes)" << std::endl;
    m_logger << "Available flags:\n";

    m_logger << "-f - path to folder for batch processing\n";
    m_logger << "-i - path to source image **(1)**\n";
    m_logger << "-c - connectivity path **(2)**\n";
    m_logger << "-C - turns off color for logging error messages\n";
    m_logger << "-o - path to output folder\n";
    m_logger << "-O - output format: vtk or pgm image or both **(3)**\n";
    m_logger << "-X - pipeline export options **(4)**\n";
    m_logger << "-B - background initialization settings **(5)**\n";
    m_logger << "-I - create copy of source image in output path\n";
    m_logger << "-D - create new folder for output of every source image in output path\n";
    m_logger << "-T - initial tree side length, with optional extended pixel value **(6)**\n";
    m_logger << "-r - refinement methods and their parameters. Different methods separated by comma ','\n";
    m_logger << "-m - image processing methods with their parameters. Different methods separated by comma ','\n";

    m_logger << "\n**(1)** - If both -i flag and -f flag are specified, -f flag has priority and -i flag will be ignored.\n";
    m_logger << "**(2)** - If not specified will create 'custom_conn.inp'.\n";
    m_logger << "**(3)** - 'ascii' or 'binary' may be specified after pgm, but is not mandatory. Default behavior exports vtk and pgm with ascii format. Example -O \"vtk image ascii\". ";
    m_logger << "VTK export (except animations) always contains 'level', 'mpirank', 'treeid' and 'source' fields. 'source' values are computed after initial refinement methods ";
    m_logger << "have been called. 'source' might be different from the starting pipeline values as 'source' is always computed by averaging image pixels corresponding to quadrant's position.\n";
    m_logger << "**(4)** - '*vtk settings*, *image settings*. Settings expect *pipeline number* followed by desired name. Default is '0 original 1 result, 1 result'.\n";
    m_logger << "**(5)** - level(negative value will be subtracted from maximum level) up to which will quadrants be assigned OTSU's background mean value, instead of mean of image part corresponding to their position, ";
    m_logger << "followed by optional value to be assigned instead of OTSU's background mean value. By default all quadrants are assigned values based on source image.\n";
    m_logger << "**(6)** - extended pixel value gets assigned to pixels that were added to fit the 2^n side length of image, if not specified, mean of original image is used.\n";

    m_logger << "\nIf flag expects parameters, they have to be enclosed in \"\".\n";
    m_logger << "Every iterative algorithm accepts two optional arguments after all of the mandatory ones. ";
    m_logger << "First is 'anim' which tells the method to output every iteration of it's algorithm to .vtu file, ";
    m_logger << "second one is the name of the animation file. Animation file names must be different for different animations.\n";
    m_logger << "********** HELP **********\n\n";

    m_logger.Log();
}

void ArgumentParser::PrintAvailableRefinementMethods() const
{
    m_logger << "\n\n********** REFINEMENT METHODS **********\n";
    m_logger << "GLOBAL_OTSU - refine grid according to OTSU's algorithm\n";
    m_logger << "MAX_MIN_DIFF - finds maximum and minimum pixel intensity for quadrant, if difference is bigger or equal to ";
    m_logger << "user provided threshold, quadrant will get refined\n";
    m_logger << "BERNSEN - refine grid according to BERNSEN's algorithm\n";
    m_logger << "SAUVOLA - refine grid according to SAUVOLA's algorithm\n";
    m_logger << "NIBLACK - refine grid according to NIBLACK's algorithm\n";
    m_logger << "TOTAL - refines every quadrant to the maximum possible level of refinement\n";
    m_logger << "UP_TO - refines every quadrant to no more than the specified quadrant level\n";
    m_logger << "********** REFINEMENT METHODS **********\n\n";

    m_logger.Log();
}

void ArgumentParser::PrintAvailableProcessingMethods() const
{
    m_logger << "\n\n********** IMAGE PROCESSING METHODS **********\n";
    m_logger << "LHE_EXPLICIT - apply LHE solved by explicit scheme\n";
    m_logger << "LHE_IMPLICIT - apply LHE solved by implicit scheme\n";
    m_logger << "NIBLACK - apply NIBLACK's algorithm. If radius = 0, expects squared values as second input\n";
    m_logger << "MCF - apply MEAN CURVATURE FLOW algorithm solved by semi-implicit scheme\n";
    m_logger << "SUBSURF - apply SUBSURF solved by semi-implicit scheme\n";
    m_logger << "SQUARE - modifies values by the power of two\n";
    m_logger << "MAX - outputs higher one out of the two values\n";
    m_logger << "EXPAND_REFINEMENT - refines every second highest level quadrants by one level and then balances the forest\n";
    m_logger << "REFINE_BY_THRESHOLD - refines every quadrants which pipeline values is equal or higher than the threshold\n";
    m_logger << "FLAT_THRESHOLD - thresholds image based on user provided threshold value\n";
    m_logger << "********** IMAGE PROCESSING METHODS **********\n\n";

    m_logger.Log();
}

void ArgumentParser::PrintExample() const
{
    m_logger << "\n\n********** RUN EXAMPLE **********\n";
    m_logger << "Application will be run on 4 cores.\n\n";
    m_logger << "Our working directory consists of our executable 'Thesis', image to process 'input_data.pgm' and folder for output 'outFolder'.\n";
    m_logger << "Connectivity file should be created in working directory with name 'my_connectivity.inp'.\n";
    m_logger << "We want to copy the source image to our output folder. Image values will be assigned to quadrants with level higher than 4, rest will be assigned ";
    m_logger << "background value computed from Otsu's algorithm (does not have anything to do with refining according to 'GLOBAL_OTSU' method).\n";
    m_logger << "Tree side length should be computed automatically and to each extended pixel we want to assign value of '10'.\n";
    m_logger << "We want to create new directory for output of our processed image. As output we want vtk along with pgm binary image.\n";
    m_logger << "For VTK, data will be exported from pipeline '0' with name 'original', pipeline '3' with name 'mid' and pipeline '5' with name 'result'\n";
    m_logger << "For IMAGE, data will be exported from pipeline '5' with name 'imageResult'\n\n";
    m_logger << "Grid will be refined by 'GLOBAL_OTSU' method from quadrant level '0' followed by 'MAX_MIN_DIFF' method from quadrant level '3' with threshold of '30'.\n\n";
    m_logger << "To segment image by 'NIBLACK' method, we need 3 inputs. First, regular smoothed 'u', second, smoothed 'u^2' and third is original for actual thresholding.\n";
    m_logger << "Image will be first processed by 'LHE_EXPLICIT', where input pipeline will be '0' and output pipeline will be '1', with timestep '0.15' and number of iterations equal to '20'. ";
    m_logger << "We also want to see the iterations of this method, so we export animation with name 'lhe_explicit_anim'.\n";
    m_logger << "This will be our first input for 'NIBLACK'. Then we use 'SQUARE' method, where input pipeline will be '0' and output pipeline will be '2'.\n";
    m_logger << "Then we apply apply 'LHE_IMPLICIT' method on these squared data, so input pipeline will be '2' (result of 'SQUARE' method) and output pipeline will be '3',";
    m_logger << " with timestep '0.5', number of iterations '10' and tolerance equal to '0.01'.\n";
    m_logger << "Now we can call 'NIBLACK' method, where input pipelines will be '1', '2' '0', output pipeline will be '5', radius can be equal to '0' because of all the smoothing ";
    m_logger << "done by LHE methods, with kappa coefficient '0.14' and mean offset equal to '20'.\n\n";


    m_logger << "mpiexec -n 4 ./Thesis -i \"./input_data.pgm\" -o \"./outFolder/\" -c \"./my_connectivity.inp\" -I -B \"4\" -T \"0 10\" -D -O \"vtk image binary\" ";
    m_logger << "-X \"0 original 3 mid 5 result, 5 imageResult\" -r \"GLOBAL_OTSU 0, MAX_MIN_DIFF 3 30\" -m \"LHE_EXPLICIT {0#1} 0.15 20 anim lhe_explicit_anim, SQUARE {0#2}, ";
    m_logger << "LHE_IMPLICIT {2#3} 0.5 10 0.01, NIBLACK {1 2 0#5} 0 0.14 20\"\n";
    m_logger << "\n********** RUN EXAMPLE **********\n\n";
    m_logger.Log();
}

bool ArgumentParser::IsPowerOfTwo(int number) const
{
    if (number == 0) return true;

    double res = log2(number);
    return ceil(res) - floor(res) == 0.;
}

void ArgumentParser::ExtractFlagParams(int i, int argc, char **argv)
{
    if (i + 1 >= argc)
    {
        m_logger << "Missing parameter/s for flag " << argv[i][1] << std::endl;
        m_logger.LogError();
        MPI_Abort(Globals::MPI::Comm, 1);
    }
    else if (i + 2 < argc && argv[i + 2][0] != '-')
    {
        m_logger << "Multiple parameters for single flag must be inside \" \". Fix required for flag " << argv[i][1] << std::endl;
        m_logger.LogError();
        MPI_Abort(Globals::MPI::Comm, 1);
    }

    m_flagParams[argv[i][1]] = argv[i + 1];
}

