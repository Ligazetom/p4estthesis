 #ifndef GLOBALS_H
#define GLOBALS_H

#include "generaltypes.h"
#include <p4est.h>

#include <string>
#include <vector>

//values given by user via command line arguments and other useful constants
//these values (some exceptions for automatically computed ones if user does not specify them) do NOT change after initialization
//in case of batch processing ImageLoadPath along with ImageFileName is updated at the beginning of loop

extern const char *MAX_PIPELINE_NUMBER_STRING;

namespace Globals
{
    void DeallocateAlgorithmAndParamsVectors();

    namespace FileIO
    {
        extern const char* FolderBatchPath;
        extern const char* ImageLoadPath;
        extern std::string ImageFileName;   //no extension
        extern std::string ConnectivityInpFilePath;
        extern std::string OutputPath;
        extern bool OutputVTKFormat;
        extern bool OutputImageFormat;
        extern bool OutputOriginalImageCopy;
        extern bool OutputASCII;
        extern bool CreateNewOutputDirectory;
        extern std::vector<int> VTKExportPipelineNumbers;
        extern std::vector<std::string> VTKExportPipelineNames;
        extern std::vector<int> ImageExportPipelineNumbers;
        extern std::vector<std::string> ImageExportPipelineNames;
    }

    namespace Forest
    {
        extern bool AutoTreeSideLength;
        extern int TreeSideLength;
        extern int ExtendedTreeSideLength;
        extern int NumOfBytesPerTreeImageFragment;
        extern int BleedingPixelEdgeRadius;      //how many edge pixels outside of actual fragmented image we need to mirror/add
    }

    namespace Refinement
    {        
        extern int CurrentAlgorithmIndex;
        extern int MaxRefinementLevel;        
        extern std::vector<void*> RefinementParams;
        extern std::vector<ERefinementAlgorithm> RefinementAlgorithms;
    }

    namespace Processing
    {        
        extern int CurrentAlgorithmIndex;
        extern bool AutoExtraPixelValue;
        extern unsigned char ExtraPixelValue;
        extern int QuadrantLevelForOTSUBackgroundValue;
        extern unsigned char BackgroundQuadrantValue;
        extern bool AutoBackgroundQuadrantValue;
        extern bool SetBackgroundQuadrantValue;
        extern std::vector<void*> AlgorithmParams;
        extern std::vector<EProcessingAlgorithm> Algorithms;
    }

    namespace MPI
    {
        extern MPI_Comm Comm;
        extern int Rank;
        extern int WorldSize;
        extern MPI_Datatype QuadrantExportData;
    }
};

#endif // GLOBALS_H
