#ifndef IMAGELOADER_H
#define IMAGELOADER_H

#include <vector>
#include "generaltypes.h"
#include "wimage.h"

namespace ImageIO
{
    bool LoadPGMImage(const char *imgPath, std::vector<unsigned char> &imageData);
    SImageSize GetImageSizeFromPGMFile(const char *imgPath);
    bool WritePGMImage(const char *imgPath, const WImage &img, bool ascii = true);
};

#endif // IMAGELOADER_H
