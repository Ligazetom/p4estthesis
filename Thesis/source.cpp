#include <vector>
#include <string>
#include <iostream>

#include "processingoperation.h"
#include "generaltypes.h"
#include "forest.h"
#include "utility.h"
#include "mpiutils.h"
#include "core.h"
#include "plogger.h"
#include "argumentparser.h"
#include "globals.h"
#include "parallelstopwatch.h"


int main (int argc, char **argv)
{
    int mpiret;
    p4est_t *p4est;

    ///////////////////
    //  FIRST INIT   //
    ///////////////////

    mpiret = MPI_Init (&argc, &argv);
    SC_CHECK_MPI (mpiret);
    Globals::MPI::Comm = MPI_COMM_WORLD;
    MPI_Comm_rank(Globals::MPI::Comm, &Globals::MPI::Rank);
    MPI_Comm_size(Globals::MPI::Comm, &Globals::MPI::WorldSize);

    sc_init(Globals::MPI::Comm, 1, 1, NULL, SC_LP_ESSENTIAL);
    p4est_init(NULL, 7);//SC_LP_PRODUCTION); //7

    PLogger logger(Globals::MPI::Rank);
    ParallelStopwatch globalStopwatch(Globals::MPI::Comm, Globals::MPI::Rank, "globalStopwatch");
    ArgumentParser argParser(argc, argv, logger);   //every process gets the same arguments

    MPIUtils::InitializeMPIDatatypes();
    globalStopwatch.CollectiveReset();

    ////////////////////////////////////////////
    //  Generating file list for processing   //
    ////////////////////////////////////////////

    unsigned int numberOfFiles = 0;
    std::vector<std::string> filePaths;

    MPIUtils::GenerateFilteredFilePathsList(filePaths); //only supported image file extensions      //TODO: refactor to std::list

    numberOfFiles = static_cast<unsigned int>(filePaths.size());
    MPI_Bcast(&numberOfFiles, 1, MPI_INT, 0, Globals::MPI::Comm);

    /////////////
    //  CORE   //
    /////////////

    for (unsigned int i = 0; i < numberOfFiles; ++i)
    {
        ParallelStopwatch preInitStopwatch(Globals::MPI::Comm, Globals::MPI::Rank, "preInitStopwatch");
        preInitStopwatch.CollectiveReset();

        SC_CHECK_MPI(MPI_Barrier(Globals::MPI::Comm));        //if there is some issue for the next image, calling MPI_Abort can interrupt processes working on previous image

        if (Globals::MPI::Rank == 0)
        {
            logger << "\n" << i + 1 << "/" << numberOfFiles << " :" << "Processing image " << filePaths[i] << std::endl << std::endl;
            logger.Log();
        }

        MPIUtils::BroadcastFileName(filePaths[i]);      //needed for file output
        p4est_connectivity_t *connectivity = NULL;

        /////////////////////////////////
        //  Connectivity generation    //
        /////////////////////////////////

        SImageSize originalImageSize;
        SImageSize extendedImageSize;

        Core::GenerateTreeRoots(originalImageSize, extendedImageSize, connectivity);
        Utility::PrintPixelRedundancy(logger, originalImageSize, extendedImageSize);

        if(Globals::Forest::AutoTreeSideLength)
        {
            MPI_Bcast(&(Globals::Forest::TreeSideLength), 1, MPI_INT, 0, Globals::MPI::Comm);
            MPI_Bcast(&(Globals::Forest::NumOfBytesPerTreeImageFragment), 1, MPI_INT, 0, Globals::MPI::Comm);
            MPI_Bcast(&(Globals::Refinement::MaxRefinementLevel), 1, MPI_INT, 0, Globals::MPI::Comm);
            MPI_Bcast(&(Globals::Forest::ExtendedTreeSideLength ), 1, MPI_INT, 0, Globals::MPI::Comm);
        }

        /////////////////////////////////////////////////
        //  SECOND INIT AFTER ACQUIRED CONNECTIVITY    //
        /////////////////////////////////////////////////

        p4est = p4est_new (Globals::MPI::Comm, connectivity, 0 /*sizeof(SQuadrantData)*/, NULL, NULL);

        p4est->user_pointer = new SForestData;
        SForestData *forestData = reinterpret_cast<SForestData*>(p4est->user_pointer);            //p4est custom data available for this process
        forestData->pQuadrantData = nullptr;

        //at this point there are only trees/treeRoots, so local_num_quadrants has number of trees for the current process
        forestData->pImageData = new unsigned char[p4est->local_num_quadrants * Globals::Forest::NumOfBytesPerTreeImageFragment];    //array to store image tree fragments for this process

        ///////////////////////////////////////////////////////////////
        //  Image load, fragmentation and broadcast of basic info    //
        ///////////////////////////////////////////////////////////////

        std::vector<unsigned char> originalImageData;
        Core::InitializeImageData(originalImageData, originalImageSize, extendedImageSize, p4est);

        WImage originalImage(originalImageSize, originalImageData.data());
        MPIUtils::ComputeAndBroadcastHistogram(originalImage, p4est);
        MPIUtils::ComputeAndBroadcastThresholdGlobalOTSU(p4est, originalImage.GetNumOfPixels());

        preInitStopwatch.CollectivePrintTimeDiff();

        /////////////////////////////////////////////////////////
        //  Refinement / Coarsening? ... forest grid adjusting //
        /////////////////////////////////////////////////////////

        ParallelStopwatch forestGenerationStopwatch(Globals::MPI::Comm, Globals::MPI::Rank, "forestGenerationStopwatch");
        forestGenerationStopwatch.CollectiveReset();

        for(size_t i = 0; i < Globals::Refinement::RefinementAlgorithms.size(); ++i)
        {
            Globals::Refinement::CurrentAlgorithmIndex = static_cast<int>(i);     //refine functions do not get any *user_data

            logger << "Refining according to " << Utility::RefinementAlgorithmToString(Globals::Refinement::RefinementAlgorithms[i]) << std::endl;
            logger.Log();

            switch(Globals::Refinement::RefinementAlgorithms[i])
            {
            case ERefinementAlgorithm::MAX_MIN_DIFF_THRESHOLD: Core::RefineAccordingToMaxMinDiffThreshold(p4est); break;
            case ERefinementAlgorithm::GLOBAL_OTSU: Core::RefineAccordingToGlobalOTSUThreshold(p4est); break;
            case ERefinementAlgorithm::BERNSEN: Core::RefineAccordingToBernsenThreshold(p4est); break;
            case ERefinementAlgorithm::NIBLACK: Core::RefineAccordingToNiblackThreshold(p4est); break;
            case ERefinementAlgorithm::SAUVOLA: Core::RefineAccordingToSauvolaThreshold(p4est); break;
            case ERefinementAlgorithm::TOTAL: Core::TotalRefinement(p4est); break;
            case ERefinementAlgorithm::UP_TO: Core::RefineUpToLevel(p4est); break;
            default:
                break;
            }
        }

        //keep 2:1 ratio after last refinement. Forest will not change from now on.
        p4est_balance (p4est, P4EST_CONNECT_FACE, NULL);

        if (Globals::MPI::Rank == 0)
        {
            std::cout << "Initial discretization generated with quadrant count: " << p4est->global_num_quadrants << std::endl;
        }

        forestData->pQuadrantData = new SQuadrantData[p4est->local_num_quadrants];
        Forest::MapQuadrantDataToQuadrants(p4est, forestData->pQuadrantData);

        Core::InitializeQuadrantData(p4est);    //we are done with image data, from now on working only with quadrant data
        delete[] forestData->pImageData;        //because of partitioning, image tree fragments are no longer valid

        Forest::PartitionForest(p4est);    //divide equally for all available processes, custom function since we keep data in application memory

        forestGenerationStopwatch.CollectivePrintTimeDiff();

        ////////////////////////////
        //  Core Image processing //
        ////////////////////////////

        ParallelStopwatch imgProcessingStopwatch(Globals::MPI::Comm, Globals::MPI::Rank, "imgProcessingStopwatch");
        imgProcessingStopwatch.CollectiveReset();

        for(size_t i = 0; i < Globals::Processing::Algorithms.size(); ++i)            
        {
            ParallelStopwatch lastOperationStopwatch(Globals::MPI::Comm, Globals::MPI::Rank, "lastOperationStopwatch");
            lastOperationStopwatch.CollectiveReset();

            Globals::Processing::CurrentAlgorithmIndex = static_cast<int>(i);   //for refinements during processing

            logger << "Processing image by " << Utility::ImageAlgorithmToString(Globals::Processing::Algorithms[i]);

            if (Globals::Processing::Algorithms[i] != EProcessingAlgorithm::EXPAND_REFINEMENT)
                logger << ". Pipeline: " << Utility::ImageAlgorithmPipelineToString(Globals::Processing::Algorithms[i], Globals::Processing::AlgorithmParams[i]) << std::endl;
            else logger << ".\n";

            logger.Log();

            switch(Globals::Processing::Algorithms[i])
            {
            case EProcessingAlgorithm::LHE_EXPLICIT: ProcessingOperation::LHEExplicit(p4est, i); break;
            case EProcessingAlgorithm::LHE_IMPLICIT: ProcessingOperation::LHEImplicit(p4est, i); break;
            case EProcessingAlgorithm::NIBLACK: ProcessingOperation::Niblack(p4est, i); break;
            case EProcessingAlgorithm::MCF: ProcessingOperation::MCF(p4est, i); break;
            case EProcessingAlgorithm::SUBSURF: ProcessingOperation::Subsurf(p4est, i); break;
            case EProcessingAlgorithm::SQUARE: ProcessingOperation::Square(p4est, i); break;
            case EProcessingAlgorithm::MAX: ProcessingOperation::Max(p4est, i); break;
            case EProcessingAlgorithm::EXPAND_REFINEMENT: ProcessingOperation::ExpandRefinement(p4est); break;
            case EProcessingAlgorithm::REFINE_BY_THRESHOLD: ProcessingOperation::RefineByThreshold(p4est); break;
            case EProcessingAlgorithm::FLAT_THRESHOLD: ProcessingOperation::FlatThreshold(p4est, i); break;
            default:
                break;
            }

            lastOperationStopwatch.CollectivePrintTimeDiff();
        }

        imgProcessingStopwatch.CollectivePrintTimeDiff();

        //////////////////
        //  Output      //
        //////////////////

        ParallelStopwatch exportStopwatch(Globals::MPI::Comm, Globals::MPI::Rank, "exportStopwatch");
        exportStopwatch.CollectiveReset();

        if (Globals::FileIO::OutputVTKFormat) Core::WriteVtkOutput(p4est);
        if (Globals::FileIO::OutputImageFormat) Core::WriteImageOutput(p4est, originalImageSize);
        if (Globals::FileIO::OutputOriginalImageCopy) Core::WriteOriginalImage(originalImage);

        //////////////
        //  DEINIT  //
        //////////////

        delete[] forestData->pQuadrantData;
        delete reinterpret_cast<SForestData*>(p4est->user_pointer);
        p4est_destroy (p4est);
        p4est_connectivity_destroy (connectivity);

        exportStopwatch.CollectivePrintTimeDiff();
    }

    Globals::DeallocateAlgorithmAndParamsVectors();
    MPIUtils::FreeMPIDatatypes();
    sc_finalize ();

    globalStopwatch.CollectivePrintTimeDiff();
    logger.Log("\nDone\n");

    mpiret = MPI_Finalize ();
    SC_CHECK_MPI (mpiret);
    return 0;
}
