#ifndef ARGUMENTPARSER_H
#define ARGUMENTPARSER_H

#include "generaltypes.h"
#include <qlist.h>

#include <vector>
#include <map>
#include "plogger.h"

constexpr int NUMBER_OF_MANDATORY_FLAGS = 1;

class ArgumentParser
{
public:
    ArgumentParser(int argc, char **argv, PLogger &logger);
    bool IsValid() const;
    std::string PrintMissingFlags() const;

private:
    PLogger &m_logger;
    bool m_bIsValid = false;
    std::vector<char> m_currentFlags;
    std::map<char, char*> m_flagParams;
    std::vector<std::string> m_animationNames;

    const char m_mandatoryFlags[NUMBER_OF_MANDATORY_FLAGS] = {'o'}; //{f, i} only one of the must be present

    void PickFlagsAndArgs(int argc, char **argv);
    void Parse_f_flag() const;
    void Parse_i_flag() const;
    void Parse_c_flag() const;
    void Parse_C_flag() const;
    void Parse_o_flag() const;
    void Parse_O_flag() const;
    void Parse_X_flag() const;
    void Parse_B_flag() const;
    void Parse_I_flag() const;
    void Parse_D_flag() const;
    void Parse_T_flag() const;
    void Parse_r_flag() const;
    void Parse_m_flag();

    void Parse_GLOBAL_OTSU_args(const QStringList &tokens, int orderIndex) const;
    void Parse_MAX_MIN_DIFF_args(const QStringList &tokens, int orderIndex) const;
    void Parse_BERNSEN_args(const QStringList &tokens, int orderIndex) const;
    void Parse_SAUVOLA_args(const QStringList &tokens, int orderIndex) const;
    void Parse_NIBLACK_REFINE_args(const QStringList &tokens, int orderIndex) const;
    void Parse_TOTAL_args(const QStringList &tokens, int orderIndex) const;
    void Parse_UP_TO_args(const QStringList &tokens, int orderIndex) const;

    void Parse_LHE_EXPLICIT_args(const QStringList &tokens, const QStringList &refinementTokens, int orderIndex);
    void Parse_LHE_IMPLICIT_args(const QStringList &tokens, const QStringList &refinementTokens, int orderIndex);
    void Parse_NIBLACK_IMAGE_args(const QStringList &tokens, int orderIndex) const;
    void Parse_MCF_args(const QStringList &tokens, const QStringList &refinementTokens, int orderIndex);
    void Parse_SUBSURF_args(const QStringList &tokens, const QStringList &refinementTokens, int orderIndex);
    void Parse_SQUARE_args(const QStringList &tokens, int orderIndex) const;
    void Parse_MAX_args(const QStringList &tokens, int orderIndex) const;
    void Parse_EXPAND_REFINEMENT_args(const QStringList &tokens, int orderIndex) const;
    void Parse_REFINE_BY_THRESHOLD_args(const QStringList &tokens, int orderIndex) const;
    void Parse_FLAT_THRESHOLD_args(const QStringList &tokens, int orderIndex) const;

    void ParseAnimationArgs(const QStringList &tokens, const char *method,
                            int orderIndex, int atIndex,
                            std::string &animationName, bool &exportAnimation);
    bool HasDuplicates(std::vector<std::string> &names) const;

    void PrintHelpInfo() const;
    void PrintAvailableRefinementMethods() const;
    void PrintAvailableProcessingMethods() const;
    void PrintExample() const;

    bool IsPowerOfTwo(int number) const;
    void ExtractFlagParams(int i, int argc, char **argv);

};

#endif // ARGUMENTPARSER_H

