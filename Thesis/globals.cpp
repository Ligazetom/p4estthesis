#include "globals.h"

#include <iostream>

const char *MAX_PIPELINE_NUMBER_STRING = "10";

namespace Globals
{
    namespace FileIO
    {
        const char* FolderBatchPath;
        const char* ImageLoadPath;
        std::string ImageFileName;
        std::string ConnectivityInpFilePath = "conn.inp";
        std::string  OutputPath;
        bool OutputVTKFormat = true;
        bool OutputImageFormat = true;
        bool OutputOriginalImageCopy = false;
        bool OutputASCII = true;
        bool CreateNewOutputDirectory = false;
        std::vector<int> VTKExportPipelineNumbers;
        std::vector<std::string> VTKExportPipelineNames;
        std::vector<int> ImageExportPipelineNumbers;
        std::vector<std::string> ImageExportPipelineNames;
    }

    namespace Forest
    {
        bool AutoTreeSideLength;
        int TreeSideLength;
        int ExtendedTreeSideLength;
        int NumOfBytesPerTreeImageFragment;
        int BleedingPixelEdgeRadius;
    }

    namespace Refinement
    {        
        int CurrentAlgorithmIndex;
        int MaxRefinementLevel;
        std::vector<void*> RefinementParams;
        std::vector<ERefinementAlgorithm> RefinementAlgorithms;
    }

    namespace Processing
    {        
        int CurrentAlgorithmIndex;
        bool AutoExtraPixelValue;
        unsigned char ExtraPixelValue;
        int QuadrantLevelForOTSUBackgroundValue = -1;
        unsigned char BackgroundQuadrantValue;
        bool AutoBackgroundQuadrantValue = true;
        bool SetBackgroundQuadrantValue = false;
        std::vector<void*> AlgorithmParams;
        std::vector<EProcessingAlgorithm> Algorithms;
    }

    namespace MPI
    {
        MPI_Comm Comm;
        int Rank;
        int WorldSize;
        MPI_Datatype QuadrantExportData;
    }
};

void Globals::DeallocateAlgorithmAndParamsVectors()
{
    for(size_t i = 0; i < Globals::Refinement::RefinementParams.size(); ++i)
    {
        switch(Globals::Refinement::RefinementAlgorithms[i])
        {
        case ERefinementAlgorithm::MAX_MIN_DIFF_THRESHOLD:
            delete reinterpret_cast<SMaxMinDiffThresholdParams*>(Globals::Refinement::RefinementParams[i]); break;

        case ERefinementAlgorithm::GLOBAL_OTSU:
            delete reinterpret_cast<SGlobalOTSUThresholdParams*>(Globals::Refinement::RefinementParams[i]); break;

        case ERefinementAlgorithm::BERNSEN:
            delete reinterpret_cast<SBernsenThresholdParams*>(Globals::Refinement::RefinementParams[i]); break;

        case ERefinementAlgorithm::NIBLACK:
            delete reinterpret_cast<SNiblackThresholdParams*>(Globals::Refinement::RefinementParams[i]); break;

        case ERefinementAlgorithm::SAUVOLA:
            delete reinterpret_cast<SSauvolaThresholdParams*>(Globals::Refinement::RefinementParams[i]); break;

        case ERefinementAlgorithm::TOTAL: break;    //no allocation happened

        case ERefinementAlgorithm::UP_TO:
            delete reinterpret_cast<SUpToRefinementParams*>(Globals::Refinement::RefinementParams[i]); break;
        default:
            std::cout << "Error deallocating refinement algorithm params!" << std::endl;
            break;
        }
    }

    for(size_t i = 0; i < Globals::Processing::AlgorithmParams.size(); ++i)
    {
        switch(Globals::Processing::Algorithms[i])
        {
        case EProcessingAlgorithm::LHE_EXPLICIT:
            delete reinterpret_cast<SLHEExplicitParams*>(Globals::Processing::AlgorithmParams[i]); break;

        case EProcessingAlgorithm::LHE_IMPLICIT:
            delete reinterpret_cast<SLHEImplicitParams*>(Globals::Processing::AlgorithmParams[i]); break;

        case EProcessingAlgorithm::NIBLACK:
            delete reinterpret_cast<SNiblackThresholdParams*>(Globals::Processing::AlgorithmParams[i]); break;

        case EProcessingAlgorithm::MCF:
            delete reinterpret_cast<SMCFParams*>(Globals::Processing::AlgorithmParams[i]); break;

        case EProcessingAlgorithm::SUBSURF:
            delete reinterpret_cast<SSubsurfParams*>(Globals::Processing::AlgorithmParams[i]); break;

        case EProcessingAlgorithm::SQUARE:
            delete reinterpret_cast<SSquareParams*>(Globals::Processing::AlgorithmParams[i]); break;

        case EProcessingAlgorithm::MAX:
            delete reinterpret_cast<SMaxParams*>(Globals::Processing::AlgorithmParams[i]); break;

        case EProcessingAlgorithm::EXPAND_REFINEMENT: break; //no parameters

        case EProcessingAlgorithm::REFINE_BY_THRESHOLD:
            delete reinterpret_cast<SRefineByThresholdParams*>(Globals::Processing::AlgorithmParams[i]); break;

        case EProcessingAlgorithm::FLAT_THRESHOLD:
            delete reinterpret_cast<SFlatThresholdParams*>(Globals::Processing::AlgorithmParams[i]); break;

        default:
            std::cout << "Error deallocating image algorithm params!" << std::endl;
            break;
        }
    }

    Globals::Refinement::RefinementParams.resize(0);
    Globals::Refinement::RefinementAlgorithms.resize(0);
    Globals::Processing::AlgorithmParams.resize(0);
    Globals::Processing::Algorithms.resize(0);

    Globals::Refinement::RefinementParams.shrink_to_fit();
    Globals::Refinement::RefinementAlgorithms.shrink_to_fit();
    Globals::Processing::AlgorithmParams.shrink_to_fit();
    Globals::Processing::Algorithms.shrink_to_fit();
}
