#include "utility.h"

#include <qdebug.h>
#include <qstring.h>
#include <qstringlist.h>

#include <fstream>
#include <iostream>
#include <string>
#include <sstream>
#include <set>
#include <algorithm>
#include <limits>

#include <sys/types.h>
#include <sys/stat.h>
#include <cstring>
#include <cerrno>
#include <dirent.h>

#include "globals.h"
#include "imageio.h"

SImageSize Utility::GetImageSizeFromFile(const char *imgPath)
{
    QString filePath(imgPath);
    QString fileExtension = filePath.section('.', -1, -1);

    if (fileExtension == "pgm")
    {
        return ImageIO::GetImageSizeFromPGMFile(imgPath);
    }
    else
    {
        std::cout << "Unsupported image file format for file " << imgPath << std::endl;
        SImageSize size;
        size.width = 0;
        size.height = 0;
        return size;
    }
}

bool Utility::LoadImage(const char* imgPath, std::vector<unsigned char> &imageData)
{
    QString filePath(imgPath);
    QString fileExtension = filePath.section('.', -1, -1);

    if (fileExtension == "pgm")
    {
        return ImageIO::LoadPGMImage(imgPath, imageData);
    }
    else
    {
        std::cout << "Unsupported image file format for file " << imgPath << std::endl;
        return false;
    }
}

bool Utility::CreateConnectivityInpFile(const char *filePath, const SImageSize &imgSize, int treeSideLength)
{
    std::ofstream out(filePath);

    if (!out.is_open()) { std::cout << "Could not open inp file for writing!\n"; return false; }

    out << "*HEADING" << std::endl;
    out << "p4est custom inp file" << std::endl;
    out << "*NODE" << std::endl;

    int widthStepCount = imgSize.width / treeSideLength;
    int heightStepCount = imgSize.height / treeSideLength;
    int counter = 0;

    for (int j = 0; j <= heightStepCount; ++j)
    {
        for (int i = 0; i <= widthStepCount; ++i)
        {
            ++counter;
            out << counter << "," << i * treeSideLength << "," << j * treeSideLength << ",0" << std::endl;
        }
    }

    out << "*ELEMENT, TYPE=S4R" << std::endl;

    counter = 0;
    for (int j = 0; j < heightStepCount; ++j)
    {
        for (int i = 0; i < widthStepCount; ++i)
        {
            ++counter;
            int BL = (j + 1) * (widthStepCount + 1) + i + 1;
            int TL = j * (widthStepCount + 1) + i + 1;
            out << counter << "," << BL << "," << BL + 1 << "," << TL + 1 << "," << TL << std::endl;
        }
    }

    out.close();
    return true;
}

bool Utility::CreateDirectory(const char *dirPath)
{
    struct stat status;

    if (stat(dirPath, &status) != 0)    //dir/file does not already exist, but maybe we do not have permissions for search
    {
        if (errno == EACCES)
        {
            std::cout << std::strerror(errno) << std::endl;
            return false;
        }

        if (mkdir(dirPath, 0777) != 0)
        {
            std::cout << std::strerror(errno) << std::endl;
            return false;
        }
    }
    else
    {
        if (S_ISDIR(status.st_mode))
        {
            return true;    //even if we did not create it, it exists so we are fine
        }
        else
        {
            std::cout << "Directory could not be created! File with the same name already exists!" << std::endl;
            return false;
        }
    }

    return true;
}

bool Utility::IsValidDirectory(const char *dirPath)
{
    struct stat status;

    if (stat(dirPath, &status) == 0)
    {
        return S_ISDIR(status.st_mode);
    }

    return false;
}

void Utility::GetListOfFilesInDirectory(const char *dirPath, std::vector<std::string> &fileNames)
{
    DIR *directory = opendir(dirPath);

    if (directory == NULL)
    {
        std::cout << "Error(" << std::strerror(errno) <<") opening " << dirPath << std::endl;
        return;
    }

    errno = 0;  //differentiate between no more files in dir vs error while reading info about next file

    struct dirent *fileData = readdir(directory);

    while(fileData != NULL)
    {
        if (fileData->d_type == DT_REG)     //we only care about regular files
        {
            fileNames.push_back(fileData->d_name);
        }

        fileData = readdir(directory);
    }

    if (errno != 0 && fileData == NULL)
    {
        std::cout << "Error(" << std::strerror(errno) << ") reading file info from " << dirPath << std::endl;
        fileNames.resize(0);
        closedir(directory);
        return;
    }

    closedir(directory);
}

std::string Utility::RetrieveFileNameFromPath(const char *path)
{
    QString filePath(path);
    filePath.replace("\\", "/");
    filePath = filePath.section('/', -1, -1);

    if (filePath.contains('.'))
    {
        return filePath.section('.', -2, -2).toStdString();   //remove extension (.pgm etc.)
    }
    else return filePath.toStdString();
}

void Utility::FilterAndKeepByFileExtensions(std::vector<std::string> &fileNames, int numOfSupportedExtensions, ...)
{
    va_list extensions;

    va_start(extensions, numOfSupportedExtensions);
    std::set<std::string> supportedExtensions;

    for(int i = 0; i < numOfSupportedExtensions; ++i)
    {
        std::string extension = va_arg(extensions, const char *);
        extension.erase(std::remove(extension.begin(), extension.end(), '.'), extension.end());
        supportedExtensions.insert(extension);
    }

    std::vector<std::string> filteredFileNames;
    filteredFileNames.reserve(fileNames.size());

    for(size_t i = 0; i < fileNames.size(); ++i)
    {
        QString fileName = fileNames[i].c_str();
        QStringList parts = fileName.split('.');

        if (parts.length() == 2)
        {
            if (supportedExtensions.find(parts[1].toStdString()) != supportedExtensions.end())
            {
                filteredFileNames.push_back(fileNames[i]);
            }
        }
    }

    fileNames = std::move(filteredFileNames);
    va_end(extensions);
}

void Utility::ComputeFilledImageSize(const SImageSize &oldImgSize, SImageSize &newImgSize, int treeSideLength)
{
    int width = oldImgSize.width;
    int height = oldImgSize.height;
    newImgSize.width = width;
    newImgSize.height = height;

    int widthResidual = width % treeSideLength;
    int heightResidual = height % treeSideLength;

    if (widthResidual != 0) newImgSize.width += treeSideLength - width % treeSideLength;
    if (heightResidual != 0) newImgSize.height += treeSideLength - height % treeSideLength;

    Q_ASSERT(newImgSize.width >= 0 && newImgSize.height >= 0);
}

static int FindNearestPowerOfTwoNumber(int number)
{
    double logRes = log2(number);
    int topLogRes = static_cast<int>(ceil(logRes));
    int botLogRes = static_cast<int>(floor(logRes));

    int topVal = 1 << topLogRes;
    int botVal = 1 << botLogRes;

    return fabs(topVal - number) <= fabs(botVal - number) ? topVal : botVal;
}

int Utility::FindOptimalTreeSideLength(const SImageSize &imgSize)
{
    int nearWidth = FindNearestPowerOfTwoNumber(imgSize.width);
    int nearHeight = FindNearestPowerOfTwoNumber(imgSize.height);

    int lesser = nearWidth < nearHeight ? nearWidth : nearHeight;

    int lengths[4];
    double redundancy[4];

    for(int i = 0; i < 4; ++i)  //only 3 times divided by 2 from the lower nearest power of two
    {
        lengths[i] = lesser >> i;

        if (lengths[i] == 0) continue;

        SImageSize newImgSize;
        Utility::ComputeFilledImageSize(imgSize, newImgSize, lengths[i]);
        redundancy[i] = Utility::GetPixelRedundancy(imgSize, newImgSize);
    }

    int minIndex = 0;
    double minVal = std::numeric_limits<double>::max();

    for(int i = 0; i < 4; ++i)
    {
        if (lengths[i] == 0) continue;

        if (redundancy[i] < minVal)         //no equal sign means even if we have 0% redundancy multiple times, we keep the one with biggest tree side length
        {
            minVal = redundancy[i];
            minIndex = i;
        }
    }

    return lengths[minIndex];
}

void Utility::MultiplyEveryVectorElementBy(const std::vector<int> &vecFrom, std::vector<int> &vecTo, int multiplier)
{
    vecTo.resize(vecFrom.size());

    for(size_t i = 0; i < vecFrom.size(); ++i)
    {
        vecTo[i] = vecFrom[i] * multiplier;
    }
}

int Utility::SumOverVector(const std::vector<int> &vec)
{
    int sum = 0;
    for(size_t i = 0; i < vec.size(); ++i)
    {
        sum += vec[i];
    }

    return sum;
}

void Utility::MakeDisplacementOutOfCountVector(const std::vector<int> &countVec, std::vector<int> &displVec, size_t elemSize)
{
    displVec.resize(countVec.size());

    int sum = 0;
    for(size_t i = 0; i < countVec.size(); ++i)
    {
        displVec[i] = sum;
        sum += countVec[i] * elemSize;
    }
}

SImageSize Utility::GetImageSize(int sideLength)
{
    SImageSize buff;

    buff.width = sideLength;
    buff.height = sideLength;

    return buff;
}

WImage Utility::GetFragmentedImageFromArray(unsigned char *array, int index, int treeSideLength, int bytesPerTree)
{
    //fragmented images are stored in contiguous block of memory per tree
    //there are no gaps in indexes between processes so each process has full interval of tree index values
    //image fragments are sent in the same order as were the tree coordinates received
    //so if one process has 2 trees with indexes 3 and 4 all we need to do is subtract the first index value to get indexes to interval [0, *]
    //and then we can simply index in p4est user_data ptr for the valid start of fragmented image since we know original tree side size in bytes
    return WImage(treeSideLength, treeSideLength, array + index * bytesPerTree);
}

void Utility::TransformQuadCoordsToLocalTopLeftImageCoords(const double *qCoords, int *locCoords, int quadrantSideLength, int treeSideLength)
{
    locCoords[0] = static_cast<int>(qCoords[0]) % treeSideLength + Globals::Forest::BleedingPixelEdgeRadius;
    locCoords[1] = (static_cast<int>(qCoords[1]) - quadrantSideLength) % treeSideLength + Globals::Forest::BleedingPixelEdgeRadius;
    //locCoords[2] = static_cast<int>(qCoords[2]) % treeSideLength;   //z only for 3D, depending on the coordinate system has to be changed
}

void Utility::GetLocalTreesTopLeftCoords(std::vector<SImageCoords> &coords, p4est_t *p4est)
{
    coords.resize(p4est->local_num_quadrants);

    int offset = 0;

    if (p4est->first_local_tree != -1)
    {
        for (int i = p4est->first_local_tree; i <= p4est->last_local_tree; ++i)     //tree indexes are contiguous for processes they are assigned to
        {
            p4est_tree_t *tree = p4est_tree_array_index(p4est->trees, i);

            for(uint j = 0; j < tree->quadrants.elem_count; ++j)
            {
                p4est_quadrant_t *quad = p4est_quadrant_array_index(&(tree->quadrants), j);
                double xyz[3];
                p4est_qcoord_to_vertex(p4est->connectivity, i, quad->x, quad->y, xyz);

                coords[offset].x = xyz[0];
                coords[offset].y = xyz[1] - Globals::Forest::TreeSideLength;

                ++offset;
            }
        }
    }
}

void Utility::RetrieveQuadrantDataFromFaceSides(p4est_iter_face_side_t *sides, SQuadrantData *(&qData)[4], int &numOfQuadrants, void *user_data)
{
    for(int sId = 0; sId < 2; ++sId)
    {
        if (sides[sId].is_hanging)  //only one side can be hanging at one moment
        {
            numOfQuadrants = 3;

            for(int i = 0; i < 2; ++i)
            {
                if (sides[sId].is.hanging.is_ghost[i])
                    qData[sId * 2 + i] = reinterpret_cast<SQuadrantData*>(user_data) + sides[sId].is.hanging.quadid[i];
                else
                    qData[sId * 2 + i] = reinterpret_cast<SQuadrantData*>(sides[sId].is.hanging.quad[i]->p.user_data);
            }
        }
        else    //side is not hanging
        {
            if (sides[sId].is.full.is_ghost)
                qData[sId * 2] = reinterpret_cast<SQuadrantData*>(user_data) + sides[sId].is.full.quadid;
            else
                qData[sId * 2] = reinterpret_cast<SQuadrantData*>(sides[sId].is.full.quad->p.user_data);
        }
    }
}

void Utility::PrintPixelRedundancy(PLogger &logger, const SImageSize &originalImageSize, const SImageSize& extendedImageSize)
{
    double pixelRedundancy = 0;

    if (Globals::MPI::Rank == 0)
    {
        pixelRedundancy = Utility::GetPixelRedundancy(originalImageSize, extendedImageSize);
        logger << "Current tree side length: " << Globals::Forest::TreeSideLength << ". Redundant pixel percentage: " << pixelRedundancy << "%\n";
        logger.Log();
    }
}

const char* Utility::GetOrientationString(NeighborOrientation::ENeighborOrientation orientation)
{
    switch(orientation)
    {
    case NeighborOrientation::LEFT: return "LEFT";
    case NeighborOrientation::RIGHT: return "RIGHT";
    case NeighborOrientation::TOP: return "TOP";
    case NeighborOrientation::BOTTOM: return "BOTTOM";
    case NeighborOrientation::TOP_LEFT: return "LEFT_TOP";
    case NeighborOrientation::BOT_LEFT: return "LEFT_BOT";
    case NeighborOrientation::TOP_RIGHT: return "RIGHT_TOP";
    case NeighborOrientation::BOT_RIGHT: return "RIGHT_BOT";
    default: return "UNKNOWN ORIENTATION";
    }
}

const char* Utility::GetOrientationString(FS::EFaceSide orientation)
{
    switch(orientation)
    {
    case FS::XN: return "LEFT";
    case FS::XP: return "RIGHT";
    case FS::YN: return "TOP";
    case FS::YP: return "BOTTOM";
    default: return "UNKNOWN ORIENTATION";
    }
}

double Utility::GetPixelRedundancy(const SImageSize &originalImageSize, const SImageSize &extendedImageSize)
{
    return (extendedImageSize.width * extendedImageSize.height) / static_cast<double>(originalImageSize.width * originalImageSize.height) - 1.;
}

const char* Utility::ImageAlgorithmToString(EProcessingAlgorithm algorithm)
{
    switch(algorithm)
    {
    case EProcessingAlgorithm::LHE_EXPLICIT: return "LHE_EXPLICIT";
    case EProcessingAlgorithm::LHE_IMPLICIT: return "LHE_IMPLICIT";
    case EProcessingAlgorithm::NIBLACK: return "NIBLACK";
    case EProcessingAlgorithm::MCF: return "MCF";
    case EProcessingAlgorithm::SUBSURF: return "SUBSURF";
    case EProcessingAlgorithm::SQUARE: return "SQUARE";
    case EProcessingAlgorithm::MAX: return "MAX";
    case EProcessingAlgorithm::EXPAND_REFINEMENT: return "EXPAND_REFINEMENT";
    case EProcessingAlgorithm::REFINE_BY_THRESHOLD: return "REFINE_BY_THRESHOLD";
    case EProcessingAlgorithm::FLAT_THRESHOLD: return "FLAT_THRESHOLD";
    default: return "Unknown image algorithm";
    }
}

std::string Print_LHE_EXPLICIT_Pipeline(void *methodParams)
{
    SLHEExplicitParams *params = reinterpret_cast<SLHEExplicitParams*>(methodParams);
    std::stringstream ss;

    ss << "Input->" << params->pipelineIN << " # Output->" << params->pipelineOUT;
    return ss.str();
}

std::string Print_LHE_IMPLICIT_Pipeline(void *methodParams)
{
    SLHEImplicitParams *params = reinterpret_cast<SLHEImplicitParams*>(methodParams);
    std::stringstream ss;

    ss << "Input->" << params->pipelineIN << " # Output->" << params->pipelineOUT;
    return ss.str();
}

std::string Print_NIBLACK_Pipeline(void *methodParams)
{
    SNiblackThresholdParams *params = reinterpret_cast<SNiblackThresholdParams*>(methodParams);
    std::stringstream ss;

    ss << "Input processing->" << params->pipelineINProcessing;
    ss << " Input processing squared->" << params->pipelineINProcessingSquared;
    ss << " Input thresholding->" << params->pipelineINThresholding << " # Output->" << params->pipelineOUT;
    return ss.str();
}

std::string Print_MCF_Pipeline(void *methodParams)
{
    SMCFParams *params = reinterpret_cast<SMCFParams*>(methodParams);

    std::stringstream ss;

    ss << "Input->" << params->pipelineIN << " # Output->" << params->pipelineOUT;
    return ss.str();
}

std::string Print_SUBSURF_Pipeline(void *methodParams)
{
    SSubsurfParams *params = reinterpret_cast<SSubsurfParams*>(methodParams);

    std::stringstream ss;

    ss << "Input_I0->" << params->pipelineIN_I0 << " Input_U0->" << params->pipelineIN_U0 << " # Output->" << params->pipelineOUT;
    return ss.str();
}

std::string Print_SQUARE_Pipeline(void *methodParams)
{
    SSquareParams *params = reinterpret_cast<SSquareParams*>(methodParams);
    std::stringstream ss;

    ss << "Input->" << params->pipelineIN << " # Output->" << params->pipelineOUT;
    return ss.str();
}

std::string Print_MAX_Pipeline(void *methodParams)
{
    SMaxParams *params = reinterpret_cast<SMaxParams*>(methodParams);
    std::stringstream ss;

    ss << "Input1->" << params->pipelineIN1 << " Input2->" << params->pipelineIN2 << " # Output->" << params->pipelineOUT;
    return ss.str();
}

std::string Print_REFINE_BY_THRESHOLD_Pipeline(void *methodParams)
{
    SRefineByThresholdParams *params = reinterpret_cast<SRefineByThresholdParams*>(methodParams);
    std::stringstream ss;

    ss << "Input->" << params->pipeline;
    return ss.str();
}

std::string Print_FLAT_THRESHOLD_Pipeline(void *methodParams)
{
    SFlatThresholdParams *params = reinterpret_cast<SFlatThresholdParams*>(methodParams);
    std::stringstream ss;

    ss << "Input->" << params->pipelineIN << " # Output->" << params->pipelineOUT;
    return ss.str();
}

std::string Utility::ImageAlgorithmPipelineToString(EProcessingAlgorithm algorithm, void *params)
{
    switch(algorithm)
    {
    case EProcessingAlgorithm::LHE_EXPLICIT: return Print_LHE_EXPLICIT_Pipeline(params);
    case EProcessingAlgorithm::LHE_IMPLICIT: return Print_LHE_IMPLICIT_Pipeline(params);
    case EProcessingAlgorithm::NIBLACK: return Print_NIBLACK_Pipeline(params);
    case EProcessingAlgorithm::MCF: return Print_MCF_Pipeline(params);
    case EProcessingAlgorithm::SUBSURF: return Print_SUBSURF_Pipeline(params);
    case EProcessingAlgorithm::SQUARE: return Print_SQUARE_Pipeline(params);
    case EProcessingAlgorithm::MAX: return Print_MAX_Pipeline(params);
    case EProcessingAlgorithm::REFINE_BY_THRESHOLD: return Print_REFINE_BY_THRESHOLD_Pipeline(params);
    case EProcessingAlgorithm::FLAT_THRESHOLD: return Print_FLAT_THRESHOLD_Pipeline(params);
    default: return "Unknown processing algorithm. Cannot print pipeline information.";
    }
}

const char* Utility::RefinementAlgorithmToString(ERefinementAlgorithm algorithm)
{
    switch(algorithm)
    {
    case ERefinementAlgorithm::GLOBAL_OTSU: return "GLOBAL_OTSU";
    case ERefinementAlgorithm::MAX_MIN_DIFF_THRESHOLD: return "MAX_MIN_DIFF_THRESHOLD";
    case ERefinementAlgorithm::BERNSEN: return "BERNSEN";
    case ERefinementAlgorithm::NIBLACK: return "NIBLACK";
    case ERefinementAlgorithm::SAUVOLA: return "SAUVOLA";
    case ERefinementAlgorithm::TOTAL: return "TOTAL";
    case ERefinementAlgorithm::UP_TO: return "UP_TO";
    default: return "Unknown refinement algorithm";
    }
}
