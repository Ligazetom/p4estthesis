#ifndef GENERALTYPES_H
#define GENERALTYPES_H

#include <p4est.h>
#include <string>
constexpr int MAX_PIPELINE_NUMBER = 10;  //change string in globals.cpp as well

namespace NeighborOrientation
{
    enum ENeighborOrientation
    {
        LEFT = 0,
        RIGHT,
        TOP,
        BOTTOM,
        TOP_LEFT,
        TOP_RIGHT,
        BOT_LEFT,
        BOT_RIGHT,
        UNKNOWN
    };
}

namespace FS    //face side orientation
{
    enum EFaceSide
    {
        XN = 0,     //x-negative etc.
        XP,
        YN,
        YP,
        UNKNOWN
    };
}

namespace ForestValue     //access to forestValueArray
{
    enum EForestValue
    {
        GLOBAL_OTSU_THRESHOLD = 0,
        GLOBAL_OTSU_BACKGROUND_MEAN
    };
}

enum class ERefinementAlgorithm
{
    MAX_MIN_DIFF_THRESHOLD = 0,
    GLOBAL_OTSU,
    BERNSEN,
    NIBLACK,
    SAUVOLA,
    TOTAL,
    UP_TO
};

enum class EProcessingAlgorithm
{
    LHE_EXPLICIT = 0,
    LHE_IMPLICIT,
    NIBLACK,   
    MCF,
    SUBSURF,
    SQUARE,
    MAX,
    EXPAND_REFINEMENT,
    REFINE_BY_THRESHOLD,
    FLAT_THRESHOLD
};

struct SImageSize
{
    int width;
    int height;
};

struct SImageCoords
{
    int x;
    int y;
};

struct SVolumeData
{
    double value;
    double maxValue;
};

struct SFaceData
{
    double sigma;
};

struct SCornerData
{

};

struct SPipeline
{
    SVolumeData volumeData;     //so far the only thing worth differentiate is volume data, edge and corner data are not used as inputs anywhere
};

struct SQuadrantData            //data structure generated for every quadrant
{
    double source;
    SPipeline pipeline[MAX_PIPELINE_NUMBER];
    double buffers[4];
    SFaceData edgeData[4];
    SCornerData cornerData[4];
    int quadLocalOffset;        //normally could be computed, but we need to store it to preserve quad data during refinement mid algorithm
};

struct SQuadrantExportData
{
    int xCoord;
    int yCoord;
    int level;
    int val;
};

struct SForestData
{
    unsigned char *pImageData = nullptr;
    SQuadrantData *pQuadrantData = nullptr;
    unsigned int histogram[256];
    unsigned char forestValueArray[2];
};

struct SOrientedQuadrant
{
    SQuadrantData *data;
    NeighborOrientation::ENeighborOrientation orientation;
    p4est_quadrant_t quadrant;
};

struct SQuadrantDataSeparated
{
    p4est_quadrant_t quadrant;
    SQuadrantData *data;
};

struct SImageExportData
{
    SQuadrantExportData *exportData;
    int exportPipelineIndex;
};

struct SVTKExportData
{
    sc_array_t *exportArray;

    union
    {
        int exportPipelineIndex;
        int exportBufferIndex;
    };
};

struct SLHEExplicitParams
{
    double timeStep;
    int numOfSteps;
    void *ghostData;    //cannot pass ghost data to callbacks otherwise
    int pipelineIN;
    int pipelineOUT;
    bool bExportAnimation;
    std::string animationFileName;
    bool bRefine;
    int numOfIterationsToRefineAfter;
};

namespace LHEExplicitBuffs     //aliases for quadrant buffers
{
    enum ELHEExplicitBufferName
    {
        CURRENT_TIME_ITER = 0,
        SUM_BUFFER
    };
}

struct SGlobalOTSUThresholdParams
{
    int refineFromQuadLevel;
};

struct SMaxMinDiffThresholdParams
{
    int refineFromQuadLevel;
    int maxMinDiffThreshold;
};

struct SBernsenThresholdParams
{
    int refineFromQuadLevel;
    int radius;
    double contrastThreshold;
    double backgroundForegroundThreshold;
};

struct SNiblackThresholdParams
{
    int refineFromQuadLevel;
    int radius;
    double kappa;
    double meanOffset;
    void *ghostData;    //cannot pass ghost data to callbacks otherwise
    int pipelineINProcessing;
    int pipelineINProcessingSquared;
    int pipelineINThresholding;
    int pipelineOUT;
};

struct SSauvolaThresholdParams
{
    int refineFromQuadLevel;
    int radius;
    double kappa;
    double sigmaMax;
};

struct SUpToRefinementParams
{
    int quadLevel;
};

struct SLHEImplicitParams
{    
    double *residualsSquared;
    void *ghostData;
    double timeStep;
    int numOfSteps;
    double tol;
    int pipelineIN;
    int pipelineOUT;
    bool bExportAnimation;
    std::string animationFileName;
    bool bRefine;
    int numOfIterationsToRefineAfter;
};

namespace LHEImplicitBuffs      //aliases for quadrant buffers
{
    enum ELHEImplicitBufferName
    {
        CURRENT_JACOBI_ITER = 0,
        PREVIOUS_TIME_ITER
    };
}

struct SMCFParams
{    
    double *residualsSquared;
    void *ghostData;
    double timeStep;
    int numOfSteps;
    double tol;
    double epsilon;
    int pipelineIN;
    int pipelineOUT;
    bool bExportAnimation;
    std::string animationFileName;
    bool bRefine;
    int numOfIterationsToRefineAfter;
};

namespace MCFBuffs      //aliases for quadrant buffers
{
    enum EMCFBufferName
    {
        CURRENT_JACOBI_ITER = 0,
        PREVIOUS_TIME_ITER,
        CURRENT_VOLUME_GRADIENT
    };
}

struct SSubsurfParams
{    
    double *residualsSquared;
    void *ghostData;
    double timeStep;
    int numOfSteps;
    double tol;
    double epsilon;
    double K;
    int pipelineIN_I0;
    int pipelineIN_U0;
    int pipelineOUT;
    bool bExportAnimation;
    std::string animationFileName;
    bool bRefine;
    int numOfIterationsToRefineAfter;
};

namespace SubsurfBuffs      //aliases for quadrant buffers
{
    enum ESubsurfBufferName
    {
        CURRENT_JACOBI_ITER = 0,
        PREVIOUS_TIME_ITER,
        CURRENT_VOLUME_GRADIENT,
        G_ZERO_RESULT
    };
}

struct SSquareParams
{
    int pipelineIN;
    int pipelineOUT;
};

struct SMaxParams
{
    int pipelineIN1;
    int pipelineIN2;
    int pipelineOUT;
};

struct SRefineByThresholdParams
{
    int pipeline;
    double threshold;
};

struct SFlatThresholdParams
{
    int pipelineIN;
    int pipelineOUT;
    double threshold;
};

#endif // GENERALTYPES_H
