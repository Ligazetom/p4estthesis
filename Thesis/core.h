#ifndef CORE_H
#define CORE_H

#include <vector>

#include <p4est.h>
#include <p4est_vtk.h>
#include <p4est_iterate.h>

#include "generaltypes.h"
#include "wimage.h"

namespace Core
{
    void RefineAccordingToGlobalOTSUThreshold(p4est_t *p4est);
    void RefineAccordingToMaxMinDiffThreshold(p4est_t *p4est);
    void RefineAccordingToBernsenThreshold(p4est_t *p4est);
    void RefineAccordingToNiblackThreshold(p4est_t *p4est);
    void RefineAccordingToSauvolaThreshold(p4est_t *p4est);
    void TotalRefinement(p4est_t *p4est);
    void RefineUpToLevel(p4est_t *p4est);


    void InitializeQuadrantData(p4est_t *p4est);
    void InitializeImageData(std::vector<unsigned char> &originalImageData,                       
                             const SImageSize &originalImageSize,
                             SImageSize &extendedImageSize,
                             p4est_t *p4est);
    void GenerateTreeRoots(SImageSize &originalImageSize, SImageSize &extendedImageSize, p4est_connectivity_t *&connectivity);

    void WriteVtkAnimationOutput(p4est_t *p4est, const char *fileName, int frame, sc_array_t *values);

    void WriteVtkOutput(p4est_t *p4est);
    void WriteImageOutput(p4est_t *p4est, const SImageSize &originalImageSize);
    void WriteOriginalImage(const WImage &originalImage);
};

#endif // CORE_H
