#include "core.h"

#include "forest.h"
#include "mpiutils.h"
#include "utility.h"
#include "globals.h"
#include "processingoperation.h"
#include "imageio.h"

void Core::RefineAccordingToGlobalOTSUThreshold(p4est_t *p4est)
{
    int recursive = 1;
    p4est_refine(p4est, recursive, Forest::RefineAccordingToGlobalOTSUThresholdCallback, NULL);
}

void Core::RefineAccordingToMaxMinDiffThreshold(p4est_t *p4est)
{
    int recursive = 1;
    p4est_refine(p4est, recursive, Forest::RefineAccordingToMaxMinDiffThresholdCallback, NULL);
}

void Core::RefineAccordingToBernsenThreshold(p4est_t *p4est)
{
    int recursive = 1;
    p4est_refine(p4est, recursive, Forest::RefineAccordingToBernsenThresholdCallback, NULL);
}

void Core::RefineAccordingToNiblackThreshold(p4est_t *p4est)
{
    int recursive = 1;
    p4est_refine(p4est, recursive, Forest::RefineAccordingToNiblackThresholdCallback, NULL);
}

void Core::RefineAccordingToSauvolaThreshold(p4est_t *p4est)
{
    int recursive = 1;
    p4est_refine(p4est, recursive, Forest::RefineAccordingToSauvolaThresholdCallback, NULL);
}

void Core::TotalRefinement(p4est_t *p4est)
{
    int recursive = 1;
    p4est_refine(p4est, recursive, Forest::AlwaysRefineUpToMaxLevelCallback, NULL);
}

void Core::RefineUpToLevel(p4est_t *p4est)
{
    int recursive = 1;
    p4est_refine(p4est, recursive, Forest::RefineUpToLevelCallback, NULL);
}

void Core::InitializeQuadrantData(p4est_t *p4est)
{
    p4est_iterate(p4est, NULL, NULL, Forest::InitializeQuadrantDataCallback, NULL, NULL);
}

void Core::InitializeImageData(std::vector<unsigned char> &originalImageData,                              
                               const SImageSize &originalImageSize,
                               SImageSize &extendedImageSize,
                               p4est_t *p4est)
{ 
    std::vector<int> procToTreeCount;           //how many trees does every process have
    MPIUtils::GetNumOfQuadrantsForEachProcess(procToTreeCount, p4est);      //in this moment the only quadrants are trees themselves

    std::vector<SImageCoords> treeCoords;       //top left coordinates for all trees across all processes
    MPIUtils::GetAllTreesTopLeftCoords(treeCoords, procToTreeCount, p4est);

    MPIUtils::LoadImage(Globals::FileIO::ImageLoadPath, originalImageData);

    WImage originalImage(originalImageSize, originalImageData.data());

    if (Globals::Processing::AutoExtraPixelValue && Globals::MPI::Rank == 0) Globals::Processing::ExtraPixelValue = originalImage.GetMean();

    std::vector<unsigned char> extendedImageData;
    if (Globals::MPI::Rank == 0) ProcessingOperation::ExtendImage(originalImage,
                                                             extendedImageData,
                                                             extendedImageSize,
                                                             Globals::Forest::BleedingPixelEdgeRadius,
                                                             Globals::Processing::ExtraPixelValue);

    std::vector<unsigned char> fragmentedImage;
    WImage extendedImage(extendedImageSize.width, extendedImageSize.height, extendedImageData.data());

    if (Globals::MPI::Rank == 0) ProcessingOperation::FragmentImage(extendedImage, fragmentedImage, treeCoords, Globals::Forest::TreeSideLength, Globals::Forest::BleedingPixelEdgeRadius);

    MPIUtils::ScatterFragmentedImageData(fragmentedImage, procToTreeCount, p4est);                             //send each process their corresponding image piece
}

void Core:: GenerateTreeRoots(SImageSize &originalImageSize, SImageSize &extendedImageSize, p4est_connectivity_t *&connectivity)
{
    if (Globals::MPI::Rank == 0)
    {       
        originalImageSize = Utility::GetImageSizeFromFile(Globals::FileIO::ImageLoadPath);                           //size of the original image

        if (originalImageSize.height == 0 || originalImageSize.width == 0)
        {
            std::cout << "Error while loading image size for file " << Globals::FileIO::ImageLoadPath << std::endl;
            MPI_Abort(Globals::MPI::Comm, 1);
        }

        if (originalImageSize.width < Globals::Forest::BleedingPixelEdgeRadius || originalImageSize.height < Globals::Forest::BleedingPixelEdgeRadius)
        {
            std::cout << "Radius for image fragments edge mirroring/adding is bigger than the size of the image!" << std::endl;
            MPI_Abort(Globals::MPI::Comm, 1);
        }

        if (Globals::Forest::AutoTreeSideLength)
        {
            Globals::Forest::TreeSideLength = Utility::FindOptimalTreeSideLength(originalImageSize);
            Globals::Refinement::MaxRefinementLevel = static_cast<int>(log2(Globals::Forest::TreeSideLength));
            Globals::Forest::ExtendedTreeSideLength = Globals::Forest::TreeSideLength + 2 * Globals::Forest::BleedingPixelEdgeRadius;
            Globals::Forest::NumOfBytesPerTreeImageFragment = Globals::Forest::ExtendedTreeSideLength * Globals::Forest::ExtendedTreeSideLength;
        }

        Utility::ComputeFilledImageSize(originalImageSize, extendedImageSize, Globals::Forest::TreeSideLength);  //image size after adding extra pixels to fit to 2^n tree

        extendedImageSize.width += 2 * Globals::Forest::BleedingPixelEdgeRadius;
        extendedImageSize.height += 2 * Globals::Forest::BleedingPixelEdgeRadius;

        if (!Utility::CreateConnectivityInpFile(Globals::FileIO::ConnectivityInpFilePath.c_str(), extendedImageSize, Globals::Forest::TreeSideLength))
        {
            MPI_Abort(Globals::MPI::Comm, 1);
        }

        connectivity = p4est_connectivity_read_inp(Globals::FileIO::ConnectivityInpFilePath.c_str());
    }

    connectivity = p4est_connectivity_bcast(connectivity, 0, Globals::MPI::Comm);
}

void Core::WriteVtkAnimationOutput(p4est_t *p4est, const char *fileName, int frame, sc_array_t *values)
{
    std::string vtkOutput = Globals::FileIO::OutputPath;

    MPIUtils::CreateFolderInOutputFolder(fileName);

    if (Globals::FileIO::CreateNewOutputDirectory)
    {        
        vtkOutput += "/" + Globals::FileIO::ImageFileName;
    }

    vtkOutput += std::string("/") + fileName + "/" + fileName + "_" + std::to_string(frame);

    std::vector<const char *> fieldName = {"values"};
    std::vector<sc_array_t*> array = {values};

    p4est_vtk_context_t *context = p4est_vtk_context_new(p4est, vtkOutput.c_str());
    context = MPIUtils::WriteVtkHeaderSequentially(context);
    context = MPIUtils::WriteVtkCellDataSequentially(context, 0, 0, 0, 0, 1, 0, fieldName.data(), array.data());
    MPIUtils::WriteVtkFooterSequentially(context);
}

void Core::WriteVtkOutput(p4est_t *p4est)
{
    std::vector<sc_array_t*> dataToExport(Globals::FileIO::VTKExportPipelineNumbers.size() + 1);    //+1 for source
    std::vector<const char*> fieldNames(Globals::FileIO::VTKExportPipelineNames.size() + 1);

    for (size_t i = 0; i < fieldNames.size() - 1; ++i)
    {
        fieldNames[i] = Globals::FileIO::VTKExportPipelineNames[i].c_str();
    }

    fieldNames.back() = "source";

    SVTKExportData exportData;

    for (size_t i = 0; i < dataToExport.size() - 1; ++i)
    {
        dataToExport[i] = sc_array_new_count(sizeof(double), p4est->local_num_quadrants);
        exportData.exportArray = dataToExport[i];
        exportData.exportPipelineIndex = Globals::FileIO::VTKExportPipelineNumbers[i];

        p4est_iterate(p4est, NULL, &exportData, Forest::ExtractQuadrantExportVTKDataCallback, NULL, NULL);
    }

    dataToExport.back() = sc_array_new_count(sizeof(double), p4est->local_num_quadrants);
    p4est_iterate(p4est, NULL, dataToExport.back(), Forest::ExtractQuadrantSourceDataCallback, NULL, NULL);

    std::string vtkOutput = Globals::FileIO::OutputPath;
    vtkOutput += "/" + Globals::FileIO::ImageFileName;

    if (Globals::FileIO::CreateNewOutputDirectory)
    {
        MPIUtils::CreateOutputFolder();
        vtkOutput += "/" + Globals::FileIO::ImageFileName;
    }

    p4est_vtk_context_t *context = p4est_vtk_context_new(p4est, vtkOutput.c_str());
    context = MPIUtils::WriteVtkHeaderSequentially(context);
    context = MPIUtils::WriteVtkCellDataSequentially(context, 1, 1, 1, 0, Globals::FileIO::VTKExportPipelineNumbers.size() + 1, 0, fieldNames.data(), dataToExport.data());
    MPIUtils::WriteVtkFooterSequentially(context);

    for (size_t i = 0; i < dataToExport.size(); ++i)
    {
        sc_array_destroy(dataToExport[i]);
    }

    if (Globals::MPI::Rank == 0)
    {
        std::cout << "Exported quadrant count: " << p4est->global_num_quadrants << std::endl;
    }
}

void Core::WriteImageOutput(p4est_t *p4est, const SImageSize &originalImageSize)
{
    std::vector<SQuadrantExportData> quadData;    //info about coordinates, levels and current values

    if (Globals::FileIO::CreateNewOutputDirectory) MPIUtils::CreateOutputFolder();

    for (size_t i = 0; i < Globals::FileIO::ImageExportPipelineNames.size(); ++i)
    {
        MPI_Barrier(Globals::MPI::Comm);        //to make sure everything is written before we start something that might fail

        MPIUtils::GatherQuadrantsExportData(p4est, quadData, Globals::FileIO::ImageExportPipelineNumbers[i]);

        if (Globals::MPI::Rank == 0)
        {
            std::vector<unsigned char> imageData(originalImageSize.width * originalImageSize.height);
            WImage img(originalImageSize, imageData.data());

            for (size_t i = 0; i < quadData.size(); ++i)
            {
                const SQuadrantExportData &curData = quadData[i];
                int quadrantSideLength = Globals::Forest::TreeSideLength >> curData.level;
                int xStart = curData.xCoord;                         //
                int yStart = curData.yCoord - quadrantSideLength;    //global top left
                int xEnd = xStart + quadrantSideLength - 1;
                int yEnd = yStart + quadrantSideLength - 1;

                if (xStart >= originalImageSize.width || yStart >= originalImageSize.height) continue;  //balanced quadrants in extended parts of image

                if (xEnd >= originalImageSize.width) xEnd = originalImageSize.width - 1;
                if (yEnd >= originalImageSize.height) yEnd = originalImageSize.height - 1;

                for(int row = yStart; row <= yEnd; ++row)
                {
                    for(int col = xStart; col <= xEnd; ++col)
                    {
                        img.SetPixel(row, col, static_cast<unsigned char>(curData.val));
                    }
                }
            }

            std::string imagePath = Globals::FileIO::OutputPath + "/" + Globals::FileIO::ImageFileName;
            if (Globals::FileIO::CreateNewOutputDirectory) imagePath += "/" + Globals::FileIO::ImageFileName;
            imagePath += "_" + Globals::FileIO::ImageExportPipelineNames[i];
            imagePath += ".pgm";

            ImageIO::WritePGMImage(imagePath.c_str(), img, Globals::FileIO::OutputASCII);
        }
    }
}

void Core::WriteOriginalImage(const WImage &originalImage)
{
    if (Globals::FileIO::CreateNewOutputDirectory) MPIUtils::CreateOutputFolder();

    if (Globals::MPI::Rank == 0)
    {
        std::string imagePath = Globals::FileIO::OutputPath + "/" + Globals::FileIO::ImageFileName;
        if (Globals::FileIO::CreateNewOutputDirectory) imagePath += "/" + Globals::FileIO::ImageFileName;
        imagePath += "_original_source.pgm";

        ImageIO::WritePGMImage(imagePath.c_str(), originalImage, Globals::FileIO::OutputASCII);
    }
}
