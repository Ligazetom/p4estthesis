#-------------------------------------------------
#
# Project created by QtCreator 2020-10-04T14:24:27
#
#-------------------------------------------------

QT       += core gui

QMAKE_CXX = mpicxx
QMAKE_CXX_RELEASE = $$QMAKE_CXX
QMAKE_CXX_DEBUG = $$QMAKE_CXX
QMAKE_LINK = $$QMAKE_CXX
QMAKE_CC = mpicc

QMAKE_CFLAGS += $$system(mpicc --showme:compile)
QMAKE_LFLAGS += $$system(mpicxx --showme:link)
QMAKE_CXXFLAGS += $$system(mpicxx --showme:compile) -DMPICH_IGNORE_CXX_SEEK
QMAKE_CXXFLAGS_RELEASE += $$system(mpicxx --showme:compile) -DMPICH_IGNORE_CXX_SEEK

unix {
    INCLUDEPATH += /usr/lib/gcc/x86_64-linux-gnu/9/include
}

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Thesis
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS
DEFINES += OMPI_SKIP_MPICXX=1

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0


SOURCES += \
    argumentparser.cpp \
    core.cpp \
    globals.cpp \
    imageio.cpp \
    mpiutils.cpp \
    parallelstopwatch.cpp \
    plogger.cpp \
    processingoperation.cpp \
    source.cpp \
    utility.cpp \
    wimage.cpp \
    forest.cpp

HEADERS += \
    argumentparser.h \
    core.h \
    globals.h \
    imageio.h \
    generaltypes.h \
    mpiutils.h \
    parallelstopwatch.h \
    plogger.h \
    processingoperation.h \
    utility.h \
    wimage.h \
    forest.h

FORMS +=

CONFIG(debug, debug|release)
{
    INCLUDEPATH += $$(P4EST_LIB_PATH_DEBUG)/local/include
    DEPENDPATH += $$(P4EST_LIB_PATH_DEBUG)/local/lib

    unix
    {
        LIBS += -L$$(P4EST_LIB_PATH_DEBUG)/local/lib/ -lp4est
        PRE_TARGETDEPS += $$(P4EST_LIB_PATH_DEBUG)/local/lib/libp4est.a
        LIBS += -L$$(P4EST_LIB_PATH_DEBUG)/local/lib/ -lsc
        PRE_TARGETDEPS += $$(P4EST_LIB_PATH_DEBUG)/local/lib/libsc.a
        QMAKE_RPATHDIR += $$(P4EST_LIB_PATH_DEBUG)/local/lib
    }
}

CONFIG(release, debug|release)
{
    INCLUDEPATH += $$(P4EST_LIB_PATH_RELEASE)/local/include
    DEPENDPATH += $$(P4EST_LIB_PATH_RELEASE)/local/include

    unix
    {
        LIBS += -L$$(P4EST_LIB_PATH_RELEASE)/local/lib/ -lp4est
        PRE_TARGETDEPS += $$(P4EST_LIB_PATH_RELEASE)/local/lib/libp4est.a
        LIBS += -L$$(P4EST_LIB_PATH_RELEASE)/local/lib/ -lsc
        PRE_TARGETDEPS += $$(P4EST_LIB_PATH_RELEASE)/local/lib/libsc.a
        QMAKE_RPATHDIR += $$(P4EST_LIB_PATH_RELEASE)/local/lib
    }
}
