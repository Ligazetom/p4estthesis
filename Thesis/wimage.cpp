#include "wimage.h"


WImage::WImage(int width, int height, unsigned char *data)
{
    m_width = width;
    m_height = height;
    m_pData = data;
    m_pixelCount = width * height;
}

WImage::WImage(const SImageSize &imgSize, unsigned char *data) : WImage(imgSize.width, imgSize.height, data)
{
}

WImage::~WImage()
{
    if (m_bDeallocateWhenDestroyed)
    {
        delete[] m_pData;
        m_pData = nullptr;
    }
}

const unsigned char& WImage::GetPixel(int row, int col) const
{
    return m_pData[row * m_width + col];
}

unsigned char& WImage::GetPixel(int row, int col)
{
    return m_pData[row * m_width + col];
}

void WImage::SetPixel(int row, int col, unsigned char value)
{
    GetPixel(row, col) = value;
}

WImage WImage::GetSubImage(int xStart, int yStart, int xEnd, int yEnd)
{
    WImage buff;

    buff.m_width = xEnd - xStart + 1;
    buff.m_height = yEnd - yStart + 1;
    buff.m_pData = new unsigned char[buff.m_width * buff.m_height];

    int counter = 0;

    for(int row = 0; row < buff.m_height; ++row)
    {
        for(int col = 0; col < buff.m_width; ++col)
        {
            buff.SetPixel(row, col, this->GetPixel(yStart + row, xStart + col));

            ++counter;
        }
    }

    return buff;
}

unsigned char WImage::GetMean() const
{
    unsigned int totalVal = 0;

    for(int row = 0; row < m_height; ++row)
    {
        for(int col = 0; col < m_width; ++col)
        {
            totalVal += GetPixel(row, col);
        }
    }

    return totalVal / (m_height * m_width);
}
