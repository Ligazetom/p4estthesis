#ifndef WIMAGE_H
#define WIMAGE_H

#include "generaltypes.h"

//wrapper for grayscale image
class WImage
{
public:
    WImage(int width, int height, unsigned char *data);
    WImage(const SImageSize &imgSize, unsigned char *data);
    WImage() {};
    ~WImage();
    const unsigned char& GetPixel(int row, int col) const;
    unsigned char &GetPixel(int row, int col);
    void SetPixel(int row, int col, unsigned char value);
    void DeallocateWhenDestroyed(bool b) { m_bDeallocateWhenDestroyed = b; }
    WImage GetSubImage(int xStart, int yStart, int xEnd, int yEnd);
    const unsigned char *GetRawData() const { return m_pData; }
    int Width() const { return m_width; }
    int Height() const { return m_height; }
    unsigned int GetNumOfPixels() const { return m_pixelCount; }

    unsigned char GetMean() const;

private:
    unsigned char *m_pData = nullptr;
    int m_width = 0;
    int m_height = 0;
    unsigned int m_pixelCount = 0;

    bool m_bDeallocateWhenDestroyed = false;
};

#endif // WIMAGE_H
