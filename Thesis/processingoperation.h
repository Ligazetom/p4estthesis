#ifndef IMAGEOPERATION_H
#define IMAGEOPERATION_H

#include "wimage.h"
#include "generaltypes.h"

#include <p4est.h>
#include <p4est_extended.h>
#include <p4est_iterate.h>

#include <vector>

namespace ProcessingOperation
{
    void ComputeHistogram(const WImage &img, unsigned int (&histogram)[256]);
    void ComputeThresholdGlobalOTSU(p4est_t *p4est, unsigned int imagePixelCount);

    //make image fit the 2^n boundary, also mirroring according to the highest needed radius
    void ExtendImage(const WImage &originalImage, std::vector<unsigned char> &extendedImageData, const SImageSize &extendedImageSize, int edgeRadius, unsigned char extraPixelValue);
    void FragmentImage(const WImage &originalImg,
                       std::vector<unsigned char> &fractionedImageData,
                       const std::vector<SImageCoords> &coords,
                       const int fragmentSideLength,
                       const int edgeRadius);


    ///////////////////////////////
    /// FlatThreshold functions ///
    ///////////////////////////////
    void FlatThreshold(p4est_t *p4est, size_t algorithmIndex);
    void FlatThresholdVolumeCallback(p4est_iter_volume_info_t *info, void *user_data);

    //////////////////////////////////
    /// ExpandRefinement functions ///
    //////////////////////////////////
    void ExpandRefinement(p4est_t *p4est);
    int ExpandRefinementRefineCallback(p4est_t*, p4est_topidx_t, p4est_quadrant_t *quadrant);
    void ExpandRefinementInitCallback(p4est_t*, p4est_topidx_t, p4est_quadrant_t *quadrant);
    void ExpandRefinementReplaceCallback(p4est_t*, p4est_topidx_t, int, p4est_quadrant_t *outgoing[], int num_incoming, p4est_quadrant_t *incoming[]);

    ///////////////////////////////////
    /// RefineByThreshold functions ///
    ///////////////////////////////////
    void RefineByThreshold(p4est_t *p4est);
    int RefineByThresholdCallback(p4est_t*, p4est_topidx_t, p4est_quadrant_t *quadrant);
    void RefineByThresholdInitCallback(p4est_t*, p4est_topidx_t, p4est_quadrant_t *quadrant);
    void RefineByThresholdReplaceCallback(p4est_t*, p4est_topidx_t, int, p4est_quadrant_t *outgoing[], int num_incoming, p4est_quadrant_t *incoming[]);

    ////////////////////////
    /// Square functions ///
    ////////////////////////
    void Square(p4est_t *p4est, size_t algorithmIndex);
    void SquareVolumeCallback(p4est_iter_volume_info_t *info, void *user_data);

    /////////////////////
    /// Max functions ///
    /////////////////////
    void Max(p4est_t *p4est, size_t algorithmIndex);
    void MaxVolumeCallback(p4est_iter_volume_info_t *info, void *user_data);

    /////////////////////////////
    /// LHEExplicit functions ///
    /////////////////////////////
    void LHEExplicit(p4est_t *p4est, size_t algorithmIndex);
    void LHEExplicitInitializeVolumeCallback(p4est_iter_volume_info_t *info, void *user_data);
    void LHEExplicitVolumeCallback(p4est_iter_volume_info_t *info, void *user_data);
    void LHEExplicitFaceCallback(p4est_iter_face_info_t *info, void *user_data);
    void LHEExplicitFinalizeVolumeCallback(p4est_iter_volume_info_t *info, void *user_data);

    /////////////////////////////
    /// Niblack functions ///
    /////////////////////////////
    void Niblack(p4est_t *p4est, size_t algorithmIndex);
    void NiblackZeroRadiusVolumeCallback(p4est_iter_volume_info_t *info, void *user_data);
    void NiblackNonZeroRadiusVolumeCallback(p4est_iter_volume_info_t *info, void *user_data);

    /////////////////////////////////
    /// LHEImplicit functions ///
    /////////////////////////////////
    void LHEImplicit(p4est_t *p4est, size_t algorithmIndex);
    void LHEImplicitInitializeVolumeCallback(p4est_iter_volume_info_t *info, void *user_data);
    void LHEImplicitSigmaUpdateFaceCallback(p4est_iter_face_info_t *info, void *user_data);
    void LHEImplicitJacobiIterationUpdateVolumeCallback(p4est_iter_volume_info_t *info, void *user_data);
    void LHEImplicitComputeResidualsVolumeCallback(p4est_iter_volume_info_t *info, void *user_data);
    void LHEImplicitTimeIterationVolumeCallback(p4est_iter_volume_info_t *info, void*);
    void LHEImplicitFinalizeVolumeCallback(p4est_iter_volume_info_t *info, void *user_data);

    /////////////////////
    /// MCF functions ///
    /////////////////////
    void MCF(p4est_t *p4est, size_t algorithmIndex);
    void MCFInitializeVolumeCallback(p4est_iter_volume_info_t *info, void *user_data);
    void MCFInitializeSigmaFaceCallback(p4est_iter_face_info_t *info, void *user_data);
    void MCFVolumeGradientUpdateVolumeCallback(p4est_iter_volume_info_t *info, void *user_data);
    void MCFSigmaUpdateFaceCallback(p4est_iter_face_info_t *info, void *user_data);
    void MCFJacobiIterationUpdateVolumeCallback(p4est_iter_volume_info_t *info, void *user_data);
    void MCFComputeResidualsVolumeCallback(p4est_iter_volume_info_t *info, void *user_data);
    void MCFTimeIterationVolumeCallback(p4est_iter_volume_info_t *info, void*);
    void MCFFinalizeVolumeCallback(p4est_iter_volume_info_t *info, void *user_data);

    /////////////////////////
    /// SUBSURF functions ///
    /////////////////////////
    void Subsurf(p4est_t *p4est, size_t algorithmIndex);
    double SubsurfGFunction(double K, double sSquared);
    void SubsurfInitializeGFunctionSigmaFaceCallback(p4est_iter_face_info_t *info, void *user_data);
    void SubsurfGFunctionInitVolumeCallback(p4est_iter_volume_info_t *info, void *user_data);
    void SubsurfInitializeVolumeCallback(p4est_iter_volume_info_t *info, void *user_data);
    void SubsurfInitializeSigmaFaceCallback(p4est_iter_face_info_t *info, void *user_data);
    void SubsurfVolumeGradientUpdateVolumeCallback(p4est_iter_volume_info_t *info, void *user_data);
    void SubsurfSigmaUpdateFaceCallback(p4est_iter_face_info_t *info, void *user_data);
    void SubsurfJacobiIterationUpdateVolumeCallback(p4est_iter_volume_info_t *info, void *user_data);
    void SubsurfComputeResidualsVolumeCallback(p4est_iter_volume_info_t *info, void *user_data);
    void SubsurfTimeIterationVolumeCallback(p4est_iter_volume_info_t *info, void*);
    void SubsurfFinalizeVolumeCallback(p4est_iter_volume_info_t *info, void *user_data);
};

#endif // IMAGEOPERATION_H
