#ifndef PARALLELSTOPWATCH_H
#define PARALLELSTOPWATCH_H

#include <chrono>
#include <string>
#include <mpi.h>
#include <iostream>

class ParallelStopwatch
{
public:
    ParallelStopwatch(MPI_Comm comm, int procRank, const std::string &stopwatchName = "");
    void ParallelReset();
    void ParallelPrintTimeDiff();
    void CollectiveReset();
    void CollectivePrintTimeDiff();

private:
    MPI_Comm m_comm;
    int m_procRank = -1;
    std::string m_stopwatchName;
    std::chrono::steady_clock::time_point m_startPoint;
};

#endif // PARALLELSTOPWATCH_H
