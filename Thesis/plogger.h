#ifndef PLOGGER_H
#define PLOGGER_H

#include <string>
#include <iostream>
#include <sstream>

class PLogger
{
public:
    PLogger(int procRank);

    void Log(const char* msg) const;
    void Log(const std::string &msg) const;
    void Log();
    void TurnOffColor() { m_bColorOn = false; }
    void LogError();

    std::ostream& operator<<(const std::string &str);
    std::ostream& operator<<(unsigned int i);
    std::ostream& operator<<(const char *str);

private:
    int m_procRank = -1;
    bool m_bColorOn = true;
    std::stringstream m_sstream;
};

#endif // PLOGGER_H
