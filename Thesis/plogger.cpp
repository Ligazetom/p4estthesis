#include "plogger.h"

#include "globals.h"

PLogger::PLogger(int procRank)
{
    m_procRank = procRank;
}

void PLogger::Log(const char *msg) const
{
    if (m_procRank == 0) std::cout << msg << std::endl;
}

void PLogger::Log(const std::string &msg) const
{
    if (m_procRank == 0) std::cout << msg << std::endl;
}

void PLogger::Log()
{
    if (m_procRank == 0)
    {
        std::cout << m_sstream.str();
        std::cout << std::flush;
    }

    m_sstream.str("");
}

void PLogger::LogError()
{
    if (m_procRank == 0)
    {
        if (m_bColorOn) std::cout << "\033[31m";
        std::cout << m_sstream.str();
        if (m_bColorOn) std::cout << "\033[0m";
        std::cout << std::flush;
    }

    m_sstream.str("");
};

std::ostream& PLogger::operator<<(const std::string &str)
{
    if (m_procRank == 0) return m_sstream << str;
    return m_sstream;
}

std::ostream& PLogger::operator<<(unsigned int i)
{
    if (m_procRank == 0) return m_sstream << i;
    return m_sstream;
}

std::ostream& PLogger::operator<<(const char *str)
{
    if (m_procRank == 0) return m_sstream << str;
    return m_sstream;
}
