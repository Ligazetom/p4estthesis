#ifndef UTILITY_H
#define UTILITY_H

#include <p4est.h>
#include <p4est_iterate.h>

#include <vector>
#include <string>

#include "generaltypes.h"
#include "wimage.h"
#include "plogger.h"

namespace Utility
{
    /////// FILE OPERATIONS //////////
    SImageSize GetImageSizeFromFile(const char* imgPath);
    bool LoadImage(const char* imgPath, std::vector<unsigned char> &imageData);
    bool CreateConnectivityInpFile(const char* filePath, const SImageSize &imgSize, int treeSideLength);
    bool CreateDirectory(const char *dirPath);
    bool IsValidDirectory(const char *dirPath);
    void GetListOfFilesInDirectory(const char *dirPath, std::vector<std::string> &fileNames);

    std::string RetrieveFileNameFromPath(const char *path);
    void FilterAndKeepByFileExtensions(std::vector<std::string> &fileNames, int numOfSupportedExtensions, ...);

    void ComputeFilledImageSize(const SImageSize &oldImgSize, SImageSize &newImgSize, int treeSideLength);
    int FindOptimalTreeSideLength(const SImageSize &imgSize);

    void MultiplyEveryVectorElementBy(const std::vector<int> &vecFrom, std::vector<int> &vecTo, int multiplier);
    int SumOverVector(const std::vector<int> &vec);
    void MakeDisplacementOutOfCountVector(const std::vector<int> &countVec, std::vector<int> &displVec, size_t elemSize);

    SImageSize GetImageSize(int sideLength);
    WImage GetFragmentedImageFromArray(unsigned char *array, int index, int treeSideLength, int bytesPerTree);
    void TransformQuadCoordsToLocalTopLeftImageCoords(const double *qCoords, int *locCoords, int quadrantSideLength, int treeSideLength);   //local top left image coords of actual fragment (bleeding offsetted)
    void GetLocalTreesTopLeftCoords(std::vector<SImageCoords> &coords, p4est_t *p4est);

    void RetrieveQuadrantDataFromFaceSides(p4est_iter_face_side_t *sides, SQuadrantData *(&qData)[4], int &numOfQuadrants, void *user_data);

    void PrintPixelRedundancy(PLogger &logger, const SImageSize &originalImageSize, const SImageSize& extendedImageSize);
    const char* GetOrientationString(NeighborOrientation::ENeighborOrientation orientation);
    const char* GetOrientationString(FS::EFaceSide orientation);
    double GetPixelRedundancy(const SImageSize &originalImageSize, const SImageSize &extendedImageSize);

    const char *ImageAlgorithmToString(EProcessingAlgorithm algorithm);
    std::string ImageAlgorithmPipelineToString(EProcessingAlgorithm algorithm, void *params);
    const char *RefinementAlgorithmToString(ERefinementAlgorithm algorithm);
    //////// TEMPLATE FUNCTIONS /////////

    template<typename T>
    void FreeVectorMemory(std::vector<T> &vec)
    {
        vec.resize(0);
        vec.shrink_to_fit();
    };
};

#endif // UTILITY_H
