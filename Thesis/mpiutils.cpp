#include "mpiutils.h"
#include "utility.h"
#include "processingoperation.h"
#include "globals.h"
#include "forest.h"

#include <algorithm>



void MPIUtils::InitializeMPIDatatypes()
{
    MPI_Type_contiguous(sizeof(SQuadrantExportData), MPI_CHAR, &Globals::MPI::QuadrantExportData);
    SC_CHECK_MPI(MPI_Type_commit(&Globals::MPI::QuadrantExportData));
}

void MPIUtils::FreeMPIDatatypes()
{
    SC_CHECK_MPI(MPI_Type_free(&Globals::MPI::QuadrantExportData));
}

void MPIUtils::GenerateFilteredFilePathsList(std::vector<std::string> &filePaths)
{
    if (Globals::MPI::Rank == 0)    //only root has to have access to stored data
    {
        if (Globals::FileIO::FolderBatchPath != nullptr)
        {
            if (!Utility::IsValidDirectory(Globals::FileIO::FolderBatchPath))
            {
                std::cout << "Invalid directory for batch process!\n";
                MPI_Abort(Globals::MPI::Comm, 1);
            }

            std::vector<std::string> fileNames;
            Utility::GetListOfFilesInDirectory(Globals::FileIO::FolderBatchPath, fileNames);

            Utility::FilterAndKeepByFileExtensions(fileNames, 1, ".pgm");   //support for variadic arguments

            std::string folderPath = Globals::FileIO::FolderBatchPath;
            std::replace(folderPath.begin(), folderPath.end(), '\\', '/');
            if (folderPath.back() != '/')
            {
                folderPath += '/';
            }

            filePaths.reserve(fileNames.size());
            for(size_t i = 0; i < fileNames.size(); ++i)
            {
                filePaths.push_back(folderPath + fileNames[i]);
            }
        }
        else if (Globals::FileIO::ImageLoadPath != nullptr)
        {
            filePaths.push_back(Globals::FileIO::ImageLoadPath);
        }
        else filePaths.resize(0);
    }
}

void MPIUtils::BroadcastFileName(const std::string &filePath)
{
    unsigned int fileNameLength = 0;

    if (Globals::MPI::Rank == 0)
    {
        Globals::FileIO::ImageLoadPath = filePath.c_str();
        Globals::FileIO::ImageFileName = Utility::RetrieveFileNameFromPath(filePath.c_str());
        fileNameLength = static_cast<unsigned int>(Globals::FileIO::ImageFileName.length()) + 1;        //null char
    }

    MPI_Bcast(&fileNameLength, 1, MPI_INT, 0, Globals::MPI::Comm);

    std::vector<char> fileNameBuff;     //buffer for non const chars
    fileNameBuff.resize(fileNameLength);

    if(Globals::MPI::Rank == 0) memcpy(fileNameBuff.data(), Globals::FileIO::ImageFileName.data(), fileNameLength);

    MPI_Bcast(fileNameBuff.data(), fileNameLength, MPI_CHAR, 0, Globals::MPI::Comm);

    Globals::FileIO::ImageFileName = fileNameBuff.data();
}

void MPIUtils::GetNumOfQuadrantsForEachProcess(std::vector<int> &quadCount, p4est_t *p4est)
{
     if (Globals::MPI::Rank == 0) quadCount.resize(Globals::MPI::WorldSize);

     //get num of quadrants on every process. In this part of program there are no refined quadrants, so every quadrant there is, is the tree itself.
     int mpiret = MPI_Gather(&(p4est->local_num_quadrants), 1, MPI_INT, quadCount.data(), 1, MPI_INT, 0, Globals::MPI::Comm);
     SC_CHECK_MPI(mpiret);
}

void MPIUtils::GetAllTreesTopLeftCoords(std::vector<SImageCoords> &coords, const std::vector<int> &procToTreeCount, p4est_t *p4est)
{   
    std::vector<SImageCoords> localTreesCoords;
    Utility::GetLocalTreesTopLeftCoords(localTreesCoords, p4est);

    if (Globals::MPI::Rank == 0) coords.resize(p4est->global_num_quadrants);

    std::vector<int> treeCoordsByteSize;
    if (Globals::MPI::Rank == 0) Utility::MultiplyEveryVectorElementBy(procToTreeCount, treeCoordsByteSize, sizeof(SImageCoords));

    std::vector<int> coordsDisplacement;
    if (Globals::MPI::Rank == 0) Utility::MakeDisplacementOutOfCountVector(procToTreeCount, coordsDisplacement, sizeof(SImageCoords));

    int mpiret = MPI_Gatherv(localTreesCoords.data(), localTreesCoords.size() * 2, MPI_INT, coords.data(), treeCoordsByteSize.data(), coordsDisplacement.data(), MPI_CHAR, 0, Globals::MPI::Comm);
    SC_CHECK_MPI(mpiret);
}

void MPIUtils::LoadImage(const char *imgPath, std::vector<unsigned char> &imageData)
{
    if (Globals::MPI::Rank == 0)
    {
        if (!Utility::LoadImage(imgPath, imageData))
        {
            MPI_Abort(Globals::MPI::Comm, 1);
        }
    }
}

void MPIUtils::ScatterFragmentedImageData(const std::vector<unsigned char> &fragmentedImage, const std::vector<int> &procToTreeCount, p4est_t *p4est)
{   
    SForestData *forestData = reinterpret_cast<SForestData*>(p4est->user_pointer);
    std::vector<int> fractionImageDataSize;
    if (Globals::MPI::Rank == 0) Utility::MultiplyEveryVectorElementBy(procToTreeCount, fractionImageDataSize, Globals::Forest::NumOfBytesPerTreeImageFragment);

    std::vector<int> imageDataDisplacement;
    if (Globals::MPI::Rank == 0) Utility::MakeDisplacementOutOfCountVector(procToTreeCount, imageDataDisplacement, Globals::Forest::NumOfBytesPerTreeImageFragment);

    int mpiret = MPI_Scatterv(fragmentedImage.data(),
                          fractionImageDataSize.data(),
                          imageDataDisplacement.data(),
                          MPI_CHAR,
                          forestData->pImageData,
                          p4est->local_num_quadrants * Globals::Forest::NumOfBytesPerTreeImageFragment,
                          MPI_CHAR,
                          0,
                          Globals::MPI::Comm);
    SC_CHECK_MPI(mpiret);
}

void MPIUtils::GatherQuadrantsExportData(p4est_t *p4est, std::vector<SQuadrantExportData> &qData, int pipelineIndex)
{
    std::vector<int> quadsPerProcess;
    MPIUtils::GetNumOfQuadrantsForEachProcess(quadsPerProcess, p4est);
    int globalQuadCount = Utility::SumOverVector(quadsPerProcess);

    std::vector<SQuadrantExportData> localData(p4est->local_num_quadrants);
    SImageExportData exportData;
    exportData.exportData = localData.data();
    exportData.exportPipelineIndex = pipelineIndex;

    p4est_iterate(p4est, NULL, &exportData, Forest::ExtractQuadrantExportImageDataCallback, NULL, NULL);

    std::vector<int> displacement;
    if (Globals::MPI::Rank == 0) Utility::MakeDisplacementOutOfCountVector(quadsPerProcess, displacement, 1);

    qData.resize(globalQuadCount);

    int mpiret = MPI_Gatherv(localData.data(),
                             p4est->local_num_quadrants,
                             Globals::MPI::QuadrantExportData,
                             qData.data(),
                             quadsPerProcess.data(),
                             displacement.data(),
                             Globals::MPI::QuadrantExportData,
                             0,
                             Globals::MPI::Comm);
    SC_CHECK_MPI(mpiret);
}

void MPIUtils::ComputeAndBroadcastHistogram(const WImage &img, p4est_t *p4est)
{
    SForestData *forestData = reinterpret_cast<SForestData*>(p4est->user_pointer);

    if (Globals::MPI::Rank == 0) ProcessingOperation::ComputeHistogram(img, forestData->histogram);

    int mpiret = MPI_Bcast(forestData->histogram, 256, MPI_INT, 0, Globals::MPI::Comm);
    SC_CHECK_MPI(mpiret);
}

void MPIUtils::ComputeAndBroadcastThresholdGlobalOTSU(p4est_t *p4est, unsigned int imagePixelCount)
{
    SForestData *forestData = reinterpret_cast<SForestData*>(p4est->user_pointer);

    if (Globals::MPI::Rank == 0) ProcessingOperation::ComputeThresholdGlobalOTSU(p4est, imagePixelCount);

    int mpiret = MPI_Bcast(forestData->forestValueArray + ForestValue::GLOBAL_OTSU_THRESHOLD, 1, MPI_CHAR, 0, Globals::MPI::Comm);
    SC_CHECK_MPI(mpiret);
    mpiret = MPI_Bcast(forestData->forestValueArray + ForestValue::GLOBAL_OTSU_BACKGROUND_MEAN, 1, MPI_CHAR, 0, Globals::MPI::Comm);
    SC_CHECK_MPI(mpiret);
}

void MPIUtils::CreateOutputFolder()
{
    if (Globals::MPI::Rank == 0)
    {
        if (Utility::IsValidDirectory(Globals::FileIO::OutputPath.c_str()))
        {
            if (!Utility::CreateDirectory((Globals::FileIO::OutputPath + "/" + Globals::FileIO::ImageFileName).c_str()))
            {
                std::cout << "Unable to create directory for vtk output!\n" << std::endl;
                MPI_Abort(Globals::MPI::Comm, 1);
            }
        }
    }

    SC_CHECK_MPI(MPI_Barrier(Globals::MPI::Comm));   //synchronization to wait for directory creation
}

void MPIUtils::CreateFolderInOutputFolder(const char *folderName)
{
    if (Globals::MPI::Rank == 0)
    {
        if (Utility::IsValidDirectory(Globals::FileIO::OutputPath.c_str()))
        {
            std::string outputDir = Globals::FileIO::OutputPath;

            if (Globals::FileIO::CreateNewOutputDirectory)
            {
                outputDir += std::string("/") + Globals::FileIO::ImageFileName;
            }

            if (!Utility::IsValidDirectory(outputDir.c_str()))
            {
                if (!Utility::CreateDirectory(outputDir.c_str()))
                {
                    std::cout << "Unable to create directory for vtk output!\n" << std::endl;
                    MPI_Abort(Globals::MPI::Comm, 1);
                }
            }

            outputDir += std::string("/") + folderName;

            if (!Utility::CreateDirectory(outputDir.c_str()))
            {
                std::cout << "Unable to create directory in output folder!\n" << std::endl;
                MPI_Abort(Globals::MPI::Comm, 1);
            }
        }
    }

    SC_CHECK_MPI(MPI_Barrier(Globals::MPI::Comm));   //synchronization to wait for directory creation
}

p4est_vtk_context_t *MPIUtils::WriteVtkHeaderSequentially(p4est_vtk_context_t *context)
{
    char synchFlag = 0;

    int mpiret = MPI_Barrier(Globals::MPI::Comm);   //synch for any other actions before
    SC_CHECK_MPI(mpiret);

    int sendTo = Globals::MPI::Rank + 1;
    int receiveFrom = Globals::MPI::Rank - 1;

    if (Globals::MPI::Rank == 0) receiveFrom = MPI_PROC_NULL;
    if (Globals::MPI::Rank == Globals::MPI::WorldSize - 1) sendTo = MPI_PROC_NULL;

    MPI_Status status;
    mpiret = MPI_Recv(&synchFlag, 1, MPI_CHAR, receiveFrom, MPI_ANY_TAG, Globals::MPI::Comm, &status);
    SC_CHECK_MPI(mpiret);

    context = p4est_vtk_write_header(context);

    mpiret = MPI_Send(&synchFlag, 1, MPI_CHAR, sendTo, 0, Globals::MPI::Comm);
    SC_CHECK_MPI(mpiret);

    return context;
}

p4est_vtk_context_t *MPIUtils::WriteVtkCellDataSequentially(p4est_vtk_context_t *context,
                                                   int write_tree, int write_level,
                                                   int write_rank, int wrap_rank,
                                                   int num_cell_scalars, int num_cell_vectors,
                                                   const char* fieldnames[], sc_array_t *values[])
{
    char synchFlag = 0; //just something to send

    int mpiret = MPI_Barrier(Globals::MPI::Comm);   //synch for any other actions before
    SC_CHECK_MPI(mpiret);

    int sendTo = Globals::MPI::Rank + 1;
    int receiveFrom = Globals::MPI::Rank - 1;

    if (Globals::MPI::Rank == 0) receiveFrom = MPI_PROC_NULL;
    if (Globals::MPI::Rank == Globals::MPI::WorldSize - 1) sendTo = MPI_PROC_NULL;

    MPI_Status status;
    mpiret = MPI_Recv(&synchFlag, 1, MPI_CHAR, receiveFrom, MPI_ANY_TAG, Globals::MPI::Comm, &status);
    SC_CHECK_MPI(mpiret);

    context = p4est_vtk_write_cell_data(context, write_tree, write_level, write_rank, wrap_rank, num_cell_scalars, num_cell_vectors, fieldnames, values);

    mpiret = MPI_Send(&synchFlag, 1, MPI_CHAR, sendTo, 0, Globals::MPI::Comm);
    SC_CHECK_MPI(mpiret);

    return context;
}

int MPIUtils::WriteVtkFooterSequentially(p4est_vtk_context_t *context)
{
    char synchFlag = 0;

    int mpiret = MPI_Barrier(Globals::MPI::Comm);   //synch for any other actions before
    SC_CHECK_MPI(mpiret);

    int sendTo = Globals::MPI::Rank + 1;
    int receiveFrom = Globals::MPI::Rank  - 1;

    if (Globals::MPI::Rank  == 0) receiveFrom = MPI_PROC_NULL;
    if (Globals::MPI::Rank  == Globals::MPI::WorldSize - 1) sendTo = MPI_PROC_NULL;

    MPI_Status status;
    mpiret = MPI_Recv(&synchFlag, 1, MPI_CHAR, receiveFrom, MPI_ANY_TAG, Globals::MPI::Comm, &status);
    SC_CHECK_MPI(mpiret);

    int retVal = p4est_vtk_write_footer(context);

    mpiret = MPI_Send(&synchFlag, 1, MPI_CHAR, sendTo, 0, Globals::MPI::Comm);
    SC_CHECK_MPI(mpiret);

    return retVal;
}
