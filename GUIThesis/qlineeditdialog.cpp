#include "qlineeditdialog.h"

#include <qfiledialog.h>
#include <qsettings.h>

QLineEditDialog::QLineEditDialog(const QString &labelText, const QString &pushButtonText, QWidget* parent) : QDialog(parent), ui(new Ui::LineEditDialog)
{
    QSettings settings;
    ui->setupUi(this);
    ui->label->setText(labelText);
    ui->pushButton->setText(pushButtonText);
    ui->lineEdit->setText(settings.value("p4est_app_path", "").toString());
}

void QLineEditDialog::on_pushButton_pressed()
{
    QSettings settings;
    QString folder;

    if (ui->lineEdit->text() != "")
    {
        QFileInfo fi(ui->lineEdit->text());
        folder = fi.absoluteDir().absolutePath();
    }
    else folder = settings.value("p4est_app_path", "").toString();

    QString fileFilter = "All files (*)";
    QString filePath = QFileDialog::getOpenFileName(this, "Select p4est application", folder, fileFilter);

    if (filePath != "")
    {
        settings.setValue("p4est_app_path", filePath);
        ui->lineEdit->setText(filePath);
    }
}
