#ifndef BOILERPLATEMACROS_H
#define BOILERPLATEMACROS_H


#define ESC(...) __VA_ARGS__

#define OPERATION_PUBLIC_OVERRIDE(x) virtual size_t NumberOfDoubleParameters() const override { return NUM_OF_DOUBLE_PARAMS; } \
virtual EMethod GetMethod() const override { return x; } \
virtual size_t NumberOfIntParameters() const override { return NUM_OF_INT_PARAMS; } \
virtual size_t NumberOfPipelineInputs() const override { return NUM_OF_PIPELINE_INPUTS; } \
virtual size_t NumberOfPipelineOutputs() const override { return NUM_OF_PIPELINE_OUTPUTS; } \
\
virtual const char* const * GetDoubleParametersNames() const override { return DoubleParametersNames; } \
virtual const char* const * GetIntParametersNames() const override { return IntParametersNames; } \
virtual const char* const * GetPipelineInputsNames() const override { return PipelineInputsNames; } \
virtual const char* const * GetPipelineOutputsNames() const override { return PipelineOutputsNames; } \
\
virtual double GetMinimalDoubleParamValue(size_t paramIndex) const override { return MinimalDoubleParamsValues[paramIndex]; } \
virtual double GetMaximalDoubleParamValue(size_t paramIndex) const override { return MaximalDoubleParamsValues[paramIndex]; } \
virtual double GetDefaultDoubleParamValue(size_t paramIndex) const override { return DefaultDoubleParamsValues[paramIndex]; } \
virtual double GetStepDoubleParamValue(size_t paramIndex) const override { return StepDoubleParamsValues[paramIndex]; } \
virtual int GetDecimalCountDoubleParamValue(size_t paramIndex) const override { return DecimalCountDoubleParamsValues[paramIndex]; } \
\
virtual int GetMinimalIntParamValue(size_t paramIndex) const override { return MinimalIntParamsValues[paramIndex]; } \
virtual int GetMaximalIntParamValue(size_t paramIndex) const override { return MaximalIntParamsValues[paramIndex]; } \
virtual int GetDefaultIntParamValue(size_t paramIndex) const override { return DefaultIntParamsValues[paramIndex]; } \
virtual int GetStepIntParamValue(size_t paramIndex) const override { return StepIntParamsValues[paramIndex]; } \
\
virtual int GetPipelineINValue(size_t pipeIndex) const override { return m_pipelinesIN[pipeIndex]; } \
virtual int GetPipelineOUTValue(size_t pipeIndex) const override { return m_pipelinesOUT[pipeIndex]; } \
virtual int GetIntParamValue(size_t paramIndex) const override { return m_intParams[paramIndex]; } \
virtual double GetDoubleParamValue(size_t paramIndex) const override { return m_doubleParams[paramIndex]; } \
virtual const std::string &GenerateCommandString() override; \
virtual QString GetMethodInfo() const override { return Operation::OperationInfo[x]; } \
static QString GetMethodInfoStat() { return Operation::OperationInfo[x]; }



#define OPERATION_SLOTS_OVERRIDE virtual void OnPipelineINValueChanged(int pipeIndex) override  \
{\
    m_pipelinesIN[reinterpret_cast<QCustomComboBox*>(sender())->GetCustomID()] = pipeIndex; \
} \
\
virtual void OnPipelineOUTValueChanged(int pipeIndex) override \
{\
    m_pipelinesOUT[reinterpret_cast<QCustomComboBox*>(sender())->GetCustomID()] = pipeIndex; \
}\
\
virtual void OnIntParamValueChanged(int value) override \
{\
    QCustomSpinBox *spinBox = reinterpret_cast<QCustomSpinBox*>(sender()); \
\
    m_intParams[spinBox->GetCustomID()] = value; \
}\
\
virtual void OnDoubleParamValueChanged(double value) override \
{\
    QCustomDoubleSpinBox *spinBox = reinterpret_cast<QCustomDoubleSpinBox*>(sender()); \
\
    m_doubleParams[spinBox->GetCustomID()] = value; \
}\
virtual void OnAnimationNameChanged(QString text) override \
{\
    m_animationName = text.toStdString(); \
}\
\
virtual void OnExportAnimationChecked(bool checked) override\
{\
    m_bExportAnimation = checked; \
}


#define OPERATION_PRIVATE_MEMBERS  int m_pipelinesIN[GetValidArraySize(NUM_OF_PIPELINE_INPUTS)]; \
int m_pipelinesOUT[GetValidArraySize(NUM_OF_PIPELINE_OUTPUTS)]; \
int m_intParams[GetValidArraySize(NUM_OF_INT_PARAMS)]; \
double m_doubleParams[GetValidArraySize(NUM_OF_DOUBLE_PARAMS)];


#define OPERATION_PRIVATE_MEMBERS_IMPL(x, initIN, initOUT) x::x(void *data) : Operation(data) \
{\
    int defPipeIN[NUM_OF_PIPELINE_INPUTS] = {initIN}; \
    int defPipeOUT[NUM_OF_PIPELINE_OUTPUTS] = {initOUT}; \
\
    memcpy(m_pipelinesIN, defPipeIN, sizeof(int) * NUM_OF_PIPELINE_INPUTS); \
    memcpy(m_pipelinesOUT, defPipeOUT, sizeof(int) * NUM_OF_PIPELINE_OUTPUTS); \
    memcpy(m_intParams, DefaultIntParamsValues, sizeof(int) * NUM_OF_INT_PARAMS); \
    memcpy(m_doubleParams, DefaultDoubleParamsValues, sizeof(double) * NUM_OF_DOUBLE_PARAMS); \
    \
    ProcessData(data); \
}\
\
constexpr const char* x::DoubleParametersNames[]; \
constexpr const char* x::IntParametersNames[]; \
constexpr const char* x::PipelineInputsNames[]; \
constexpr const char* x::PipelineOutputsNames[]; \
\
constexpr double x::MinimalDoubleParamsValues[]; \
constexpr double x::MaximalDoubleParamsValues[]; \
constexpr double x::DefaultDoubleParamsValues[]; \
constexpr double x::StepDoubleParamsValues[]; \
constexpr int x::DecimalCountDoubleParamsValues[]; \
\
constexpr int x::MinimalIntParamsValues[]; \
constexpr int x::MaximalIntParamsValues[]; \
constexpr int x::DefaultIntParamsValues[]; \
constexpr int x::StepIntParamsValues[]; \
\
const std::string &x::GenerateCommandString() \
{\
    std::stringstream ss; \
\
    ss << GetMethodName() << " " << GeneratePipelineArg(m_pipelinesIN, NUM_OF_PIPELINE_INPUTS, m_pipelinesOUT, NUM_OF_PIPELINE_OUTPUTS) << " "; \
    ss << GenerateParametersArg(); \
\
    m_commandString = ss.str(); \
\
    return m_commandString; \
}





#define REFINEMENT_PUBLIC_OVERRIDE(x) virtual size_t NumberOfDoubleParameters() const override { return NUM_OF_DOUBLE_PARAMS; } \
virtual EMethod GetMethod() const override { return x; } \
virtual size_t NumberOfIntParameters() const override { return NUM_OF_INT_PARAMS; } \
\
virtual const char* const * GetDoubleParametersNames() const override { return DoubleParametersNames; } \
virtual const char* const * GetIntParametersNames() const override { return IntParametersNames; } \
\
virtual double GetMinimalDoubleParamValue(size_t paramIndex) const override { return MinimalDoubleParamsValues[paramIndex]; } \
virtual double GetMaximalDoubleParamValue(size_t paramIndex) const override { return MaximalDoubleParamsValues[paramIndex]; } \
virtual double GetDefaultDoubleParamValue(size_t paramIndex) const override { return DefaultDoubleParamsValues[paramIndex]; } \
virtual double GetStepDoubleParamValue(size_t paramIndex) const override { return StepDoubleParamsValues[paramIndex]; } \
virtual int GetDecimalCountDoubleParamValue(size_t paramIndex) const override { return DecimalCountDoubleParamsValues[paramIndex]; } \
\
virtual int GetMinimalIntParamValue(size_t paramIndex) const override { return MinimalIntParamsValues[paramIndex]; } \
virtual int GetMaximalIntParamValue(size_t paramIndex) const override { return MaximalIntParamsValues[paramIndex]; } \
virtual int GetDefaultIntParamValue(size_t paramIndex) const override { return DefaultIntParamsValues[paramIndex]; } \
virtual int GetStepIntParamValue(size_t paramIndex) const override { return StepIntParamsValues[paramIndex]; } \
\
virtual int GetIntParamValue(size_t paramIndex) const override { return m_intParams[paramIndex]; } \
virtual double GetDoubleParamValue(size_t paramIndex) const override { return m_doubleParams[paramIndex]; } \
virtual const std::string &GenerateCommandString() override; \
virtual QString GetMethodInfo() const override { return Refinement::RefinementInfo[x]; } \
static QString GetMethodInfoStat() { return Refinement::RefinementInfo[x]; }



#define REFINEMENT_SLOTS_OVERRIDE virtual void OnIntParamValueChanged(int value) override \
{\
    QCustomSpinBox *spinBox = reinterpret_cast<QCustomSpinBox*>(sender()); \
\
    m_intParams[spinBox->GetCustomID()] = value; \
}\
\
virtual void OnDoubleParamValueChanged(double value) override \
{\
    QCustomDoubleSpinBox *spinBox = reinterpret_cast<QCustomDoubleSpinBox*>(sender()); \
\
    m_doubleParams[spinBox->GetCustomID()] = value; \
}


#define REFINEMENT_PRIVATE_MEMBERS int m_intParams[GetValidArraySize(NUM_OF_INT_PARAMS)]; \
double m_doubleParams[GetValidArraySize(NUM_OF_DOUBLE_PARAMS)];


#define REFINEMENT_PRIVATE_MEMBERS_IMPL(x) x::x(void *data) : Refinement(data) \
{\
    memcpy(m_intParams, DefaultIntParamsValues, sizeof(int) * NUM_OF_INT_PARAMS); \
    memcpy(m_doubleParams, DefaultDoubleParamsValues, sizeof(double) * NUM_OF_DOUBLE_PARAMS); \
    \
    ProcessData(data); \
}\
\
constexpr const char* x::DoubleParametersNames[]; \
constexpr const char* x::IntParametersNames[]; \
\
constexpr double x::MinimalDoubleParamsValues[]; \
constexpr double x::MaximalDoubleParamsValues[]; \
constexpr double x::DefaultDoubleParamsValues[]; \
constexpr double x::StepDoubleParamsValues[]; \
constexpr int x::DecimalCountDoubleParamsValues[]; \
\
constexpr int x::MinimalIntParamsValues[]; \
constexpr int x::MaximalIntParamsValues[]; \
constexpr int x::DefaultIntParamsValues[]; \
constexpr int x::StepIntParamsValues[]; \
\
const std::string &x::GenerateCommandString() \
{\
    std::stringstream ss; \
\
    ss << GetMethodName() << " " << GenerateParametersArg(); \
\
    m_commandString = ss.str(); \
\
    return m_commandString; \
}

#endif // BOILERPLATEMACROS_H
