#ifndef QLINEEDITDIALOG_H
#define QLINEEDITDIALOG_H

#include <qdialog.h>
#include <qstring.h>
#include "ui_LineEditDialog.h"

class QLineEditDialog : public QDialog
{
    Q_OBJECT
public:
    QLineEditDialog(const QString &labelText = "", const QString &pushButtonText = "", QWidget* parent = Q_NULLPTR);

private:
    Ui::LineEditDialog* ui;

private slots:
    void on_pushButton_pressed();
};

#endif // QLINEEDITDIALOG_H
