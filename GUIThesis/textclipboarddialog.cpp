#include "textclipboarddialog.h"

#include <qclipboard.h>

TextClipboardDialog::TextClipboardDialog(QWidget* parent) : QDialog(parent), ui(new Ui::TextClipboardDialog)
{
    ui->setupUi(this);
}

void TextClipboardDialog::Clear()
{
    ui->textEdit->clear();
}

void TextClipboardDialog::HideClipboardButton(bool b)
{
    ui->pushButtonCopyToClipboard->setHidden(b);
}

void TextClipboardDialog::SetTextReadOnly(bool b)
{
    ui->textEdit->setReadOnly(b);
}

QString TextClipboardDialog::GetText() const
{
    return ui->textEdit->toPlainText();
}

void TextClipboardDialog::AppendText(QString text)
{
    ui->textEdit->moveCursor(QTextCursor::End);
    ui->textEdit->insertPlainText(text);
}

void TextClipboardDialog::on_pushButtonOK_pressed()
{
    this->close();
}

void TextClipboardDialog::on_pushButtonCopyToClipboard_pressed()
{
    QClipboard *clipboard = QGuiApplication::clipboard();

    clipboard->setText(ui->textEdit->toPlainText());
}
