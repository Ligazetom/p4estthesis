#ifndef GEOMETRICSPINBOX_H
#define GEOMETRICSPINBOX_H

#include <qspinbox.h>

class QGeometricSpinBox : public QSpinBox
{
    Q_OBJECT

public:
    QGeometricSpinBox(QWidget* parent = Q_NULLPTR);
public slots:
    void stepBy(int steps);
};

#endif // GEOMETRICSPINBOX_H
