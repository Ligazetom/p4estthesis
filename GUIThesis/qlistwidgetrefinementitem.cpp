#include "qlistwidgetrefinementitem.h"

#include "generalconstants.h"

QListWidgetRefinementItem::QListWidgetRefinementItem(int refinementMethodIndex, void *data) : QListWidgetItem(), m_refinementMethodIndex(refinementMethodIndex)
{
    switch(refinementMethodIndex)
    {
    case Refinement::GLOBAL_OTSU: m_pRefinement = std::make_unique<GLOBAL_OTSU>(data); break;
    case Refinement::MAX_MIN_DIFF: m_pRefinement = std::make_unique<MAX_MIN_DIFF>(data); break;
    case Refinement::BERNSEN: m_pRefinement = std::make_unique<BERNSEN>(data); break;
    case Refinement::SAUVOLA: m_pRefinement = std::make_unique<SAUVOLA>(data); break;
    case Refinement::NIBLACK: m_pRefinement = std::make_unique<NIBLACK_REF>(data); break;
    case Refinement::TOTAL: m_pRefinement = std::make_unique<TOTAL>(data); break;
    case Refinement::UP_TO: m_pRefinement = std::make_unique<UP_TO>(data); break;
    default: m_pRefinement = nullptr;
    }
}
