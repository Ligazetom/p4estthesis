#ifndef REFINEMENTDATATYPES_H
#define REFINEMENTDATATYPES_H

struct SGlobalOTSUThresholdParams
{
    int refineFromQuadLevel;
};

struct SMaxMinDiffThresholdParams
{
    int refineFromQuadLevel;
    int maxMinDiffThreshold;
};

struct SBernsenThresholdParams
{
    int refineFromQuadLevel;
    int radius;
    double contrastThreshold;
    double backgroundForegroundThreshold;
};

struct SSauvolaThresholdParams
{
    int refineFromQuadLevel;
    int radius;
    double kappa;
    double sigmaMax;
};

struct SNiblackThresholdParams
{
    int refineFromQuadLevel;
    int radius;
    double kappa;
    double meanOffset;
    int pipelineINProcessing;
    int pipelineINProcessingSquared;
    int pipelineINThresholding;
    int pipelineOUT;
};

struct SUpToRefinementParams
{
    int quadLevel;
};

#endif // REFINEMENTDATATYPES_H
