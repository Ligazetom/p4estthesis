#include "qgeometricspinbox.h"

QGeometricSpinBox::QGeometricSpinBox(QWidget* parent) : QSpinBox(parent) {};

void QGeometricSpinBox::stepBy(int steps)
{
    switch(steps) {
        case 1 :    setValue(value()*2);
                    break;
        case -1 :   setValue(value()/2);
                    break;
        default:    QSpinBox::stepBy(steps);
                    break;
    }
}
