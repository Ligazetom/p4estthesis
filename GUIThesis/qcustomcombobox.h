#ifndef QCUSTOMCOMBOBOX_H
#define QCUSTOMCOMBOBOX_H

#include <QComboBox>

class QCustomComboBox : public QComboBox
{
    Q_OBJECT
public:
    QCustomComboBox(int customID, QWidget *parent = nullptr);
    int GetCustomID() const { return m_customID; }

protected:
    int m_customID;
};

#endif // QCUSTOMCOMBOBOX_H
