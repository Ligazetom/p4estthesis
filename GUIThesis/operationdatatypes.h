#ifndef OPERATIONDATATYPES_H
#define OPERATIONDATATYPES_H

#include <string>

struct SLHEExplicitParams
{
    double timeStep;
    int numOfSteps;
    int pipelineIN;
    int pipelineOUT;
    bool bExportAnimation;
    std::string animationFileName;
    bool bRefine;
    int numOfIterationsToRefineAfter;
};

struct SLHEImplicitParams
{
    double timeStep;
    int numOfSteps;
    double tol;
    int pipelineIN;
    int pipelineOUT;
    bool bExportAnimation;
    std::string animationFileName;
    bool bRefine;
    int numOfIterationsToRefineAfter;
};

struct SNiblackOperationParams
{
    int radius;
    double kappa;
    double meanOffset;
    int pipelineINProcessing;
    int pipelineINProcessingSquared;
    int pipelineINThresholding;
    int pipelineOUT;
};

struct SMCFParams
{
    double timeStep;
    int numOfSteps;
    double tol;
    double epsilon;
    int pipelineIN;
    int pipelineOUT;
    bool bExportAnimation;
    std::string animationFileName;
    bool bRefine;
    int numOfIterationsToRefineAfter;
};

struct SSubsurfParams
{
    double timeStep;
    int numOfSteps;
    double tol;
    double epsilon;
    double K;
    int pipelineIN_I0;
    int pipelineIN_U0;
    int pipelineOUT;
    bool bExportAnimation;
    std::string animationFileName;
    bool bRefine;
    int numOfIterationsToRefineAfter;
};

struct SSquareParams
{
    int pipelineIN;
    int pipelineOUT;
};

struct SMaxParams
{
    int pipelineIN1;
    int pipelineIN2;
    int pipelineOUT;
};

struct SRefineByThresholdParams
{
    int pipeline;
    double threshold;
};

struct SFlatThresholdParams
{
    int pipelineIN;
    int pipelineOUT;
    double threshold;
};

#endif // OPERATIONDATATYPES_H
