#ifndef TEXTCLIPBOARDDIALOG_H
#define TEXTCLIPBOARDDIALOG_H

#include <qdialog.h>
#include "ui_TextClipboardDialog.h"
#include <string>
#include <qstring.h>

class TextClipboardDialog : public QDialog
{
    Q_OBJECT

public:
    TextClipboardDialog(QWidget* parent = Q_NULLPTR);
    void Clear();
    void HideClipboardButton(bool b = true);
    void SetTextReadOnly(bool b = true);
    QString GetText() const;

private:
    Ui::TextClipboardDialog* ui;

public slots:
    void AppendText(QString);
    void on_pushButtonOK_pressed();
    void on_pushButtonCopyToClipboard_pressed();
};

#endif // TEXTCLIPBOARDDIALOG_H
