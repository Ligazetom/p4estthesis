#ifndef QCUSTOMSPINBOX_H
#define QCUSTOMSPINBOX_H

#include <qspinbox.h>

class QCustomSpinBox : public QSpinBox
{
    Q_OBJECT

public:
    QCustomSpinBox(int customID, QWidget *parent = nullptr);
    int GetCustomID() const { return m_customID; }

protected:
    int m_customID;
};

#endif // QCUSTOMSPINBOX_H
