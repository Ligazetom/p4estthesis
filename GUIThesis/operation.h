#ifndef OPERATION_H
#define OPERATION_H

#include <qstring.h>
#include <cstdlib>
#include <vector>
#include <qobject.h>
#include <limits>
#include "BoilerplateMacros.h"
#include "qcustomspinbox.h"
#include "qcustomdoublespinbox.h"
#include "qcustomcombobox.h"
#include "generalconstants.h"

class Operation : public QObject
{
    Q_OBJECT

public:
    enum EMethod
    {
        LHE_EXPLICIT = 0,
        LHE_IMPLICIT,
        NIBLACK,
        MCF,
        SUBSURF,
        SQUARE,
        MAX,
        EXPAND_REFINEMENT,
        REFINE_BY_THRESHOLD,
        FLAT_THRESHOLD
    };

    static constexpr size_t NUMBER_OF_OPERATIONS = 10;
    static constexpr const char* OperationNames[NUMBER_OF_OPERATIONS] = {"LHE_EXPLICIT", "LHE_IMPLICIT", "NIBLACK", "MCF", "SUBSURF",
                                                                         "SQUARE", "MAX", "EXPAND_REFINEMENT", "REFINE_BY_THRESHOLD", "FLAT_THRESHOLD"};
    static constexpr const char* OperationInfo[NUMBER_OF_OPERATIONS] = {
        "Apply LHE solved by explicit scheme",
        "Apply LHE solved by implicit scheme",
        "Apply NIBLACK's algorithm. If radius = 0, expects squared values as second input",
        "Apply MEAN CURVATURE FLOW algorithm solved by semi-implicit scheme",
        "Apply SUBSURF solved by semi-implicit scheme",
        "Modifies values by the power of two",
        "Outputs higher one out of the two values",
        "Refines every second highest level quadrants by one level and then balances the forest",
        "Refines every quadrants which pipeline values is equal or higher than the threshold",
        "Thresholds image based on user provided threshold value"
    };

    Operation(void* /*data*/) : QObject() { m_bExportAnimation = false; }
    virtual ~Operation() {};

    virtual EMethod GetMethod() const = 0;
    virtual bool IsIterative() const = 0;
    virtual QString GetMethodInfo() const = 0;

    virtual size_t NumberOfDoubleParameters() const = 0;
    virtual size_t NumberOfIntParameters() const = 0;
    virtual size_t NumberOfPipelineInputs() const = 0;
    virtual size_t NumberOfPipelineOutputs() const = 0;

    virtual const char* const * GetDoubleParametersNames() const = 0;
    virtual const char* const * GetIntParametersNames() const = 0;
    virtual const char* const * GetPipelineInputsNames() const = 0;
    virtual const char* const * GetPipelineOutputsNames() const = 0;

    virtual double GetMinimalDoubleParamValue(size_t paramIndex) const = 0;
    virtual double GetMaximalDoubleParamValue(size_t paramIndex) const = 0;
    virtual double GetDefaultDoubleParamValue(size_t paramIndex) const = 0;
    virtual double GetStepDoubleParamValue(size_t paramIndex) const = 0;
    virtual int GetDecimalCountDoubleParamValue(size_t paramIndex) const = 0;

    virtual int GetMinimalIntParamValue(size_t paramIndex) const = 0;
    virtual int GetMaximalIntParamValue(size_t paramIndex) const = 0;
    virtual int GetDefaultIntParamValue(size_t paramIndex) const = 0;
    virtual int GetStepIntParamValue(size_t paramIndex) const = 0;

    virtual int GetPipelineINValue(size_t pipeIndex) const = 0;
    virtual int GetPipelineOUTValue(size_t pipeIndex) const = 0;
    virtual int GetIntParamValue(size_t paramIndex) const = 0;
    virtual double GetDoubleParamValue(size_t paramIndex) const = 0;

    virtual const std::string &GenerateCommandString() = 0;
    const std::string &GetCommandString() const { return m_commandString; }
    const std::string &GetAnimationName() const { return m_animationName; }
    const char* GetMethodName() const { return OperationNames[GetMethod()]; }
    bool IsExportingAnimation() const { return m_bExportAnimation; }

protected:
    std::string m_commandString;
    std::string m_animationName;
    bool m_bExportAnimation;

    std::string GeneratePipelineArg(int *pipeINValues, size_t pipeINValuesCount, int *pipeOUTValues, size_t pipeOUTValuesCount) const;
    std::string ConcatIntsWithSpaces(int *values, size_t valuesCount) const;

    virtual std::string GenerateParametersArg() const = 0;    
    virtual void ProcessData(void *data) = 0;
    static constexpr size_t GetValidArraySize(size_t size) { return size > 0 ? size : 1; }

public slots:
    virtual void OnPipelineINValueChanged(int pipeIndex) = 0;
    virtual void OnPipelineOUTValueChanged(int pipeIndex) = 0;
    virtual void OnIntParamValueChanged(int value) = 0;
    virtual void OnDoubleParamValueChanged(double value) = 0;
    virtual void OnAnimationNameChanged(QString text) = 0;
    virtual void OnExportAnimationChecked(bool checked) = 0;
};

class LHE_EXPLICIT : public Operation
{
    Q_OBJECT

public:
    LHE_EXPLICIT(void *data = nullptr);
    virtual ~LHE_EXPLICIT() {};

    OPERATION_PUBLIC_OVERRIDE(Operation::LHE_EXPLICIT)

    virtual bool IsIterative() const override { return true; }    

    static constexpr size_t NUM_OF_DOUBLE_PARAMS = 1;
    static constexpr size_t NUM_OF_INT_PARAMS = 1;
    static constexpr size_t NUM_OF_PIPELINE_INPUTS = 1;
    static constexpr size_t NUM_OF_PIPELINE_OUTPUTS = 1;

    static constexpr const char* PipelineInputsNames[GetValidArraySize(NUM_OF_PIPELINE_INPUTS)] = { "Input" };
    static constexpr const char* PipelineOutputsNames[GetValidArraySize(NUM_OF_PIPELINE_OUTPUTS)] = { "Output" };

    static constexpr const char* DoubleParametersNames[GetValidArraySize(NUM_OF_DOUBLE_PARAMS)] = { "Time step" };
    static constexpr double MinimalDoubleParamsValues[GetValidArraySize(NUM_OF_DOUBLE_PARAMS)] = { 0.001 };
    static constexpr double MaximalDoubleParamsValues[GetValidArraySize(NUM_OF_DOUBLE_PARAMS)] = { 99999. };  //with std::numeric_limits<double>::max() spinboxes are too long
    static constexpr double DefaultDoubleParamsValues[GetValidArraySize(NUM_OF_DOUBLE_PARAMS)] = { 1. };
    static constexpr double StepDoubleParamsValues[GetValidArraySize(NUM_OF_DOUBLE_PARAMS)] = { 0.5 };
    static constexpr int DecimalCountDoubleParamsValues[GetValidArraySize(NUM_OF_DOUBLE_PARAMS)] = { 3 };

    static constexpr const char* IntParametersNames[GetValidArraySize(NUM_OF_INT_PARAMS)] = { "Number of time steps" };
    static constexpr int MinimalIntParamsValues[GetValidArraySize(NUM_OF_INT_PARAMS)] = { 1 };
    static constexpr int MaximalIntParamsValues[GetValidArraySize(NUM_OF_INT_PARAMS)] = { 999999 };     //with std::numeric_limits<int>::max() spinboxes are too long
    static constexpr int DefaultIntParamsValues[GetValidArraySize(NUM_OF_INT_PARAMS)] = { 10 };
    static constexpr int StepIntParamsValues[GetValidArraySize(NUM_OF_INT_PARAMS)] = { 5 };

protected:
    virtual std::string GenerateParametersArg() const override;
    virtual void ProcessData(void *data) override;

public slots:
    OPERATION_SLOTS_OVERRIDE

private:
    OPERATION_PRIVATE_MEMBERS
};


class LHE_IMPLICIT : public Operation
{
    Q_OBJECT

public:
    LHE_IMPLICIT(void *data = nullptr);
    virtual ~LHE_IMPLICIT() {};

    OPERATION_PUBLIC_OVERRIDE(Operation::LHE_IMPLICIT)

    virtual bool IsIterative() const override { return true; }


    static constexpr size_t NUM_OF_DOUBLE_PARAMS = 2;
    static constexpr size_t NUM_OF_INT_PARAMS = 1;
    static constexpr size_t NUM_OF_PIPELINE_INPUTS = 1;
    static constexpr size_t NUM_OF_PIPELINE_OUTPUTS = 1;

    static constexpr const char* PipelineInputsNames[GetValidArraySize(NUM_OF_PIPELINE_INPUTS)] = { "Input" };
    static constexpr const char* PipelineOutputsNames[GetValidArraySize(NUM_OF_PIPELINE_OUTPUTS)] = { "Output" };

    static constexpr const char* DoubleParametersNames[GetValidArraySize(NUM_OF_DOUBLE_PARAMS)] = { "Time step", "Tolerance"};
    static constexpr double MinimalDoubleParamsValues[GetValidArraySize(NUM_OF_DOUBLE_PARAMS)] = { 0.001, 0. };
    static constexpr double MaximalDoubleParamsValues[GetValidArraySize(NUM_OF_DOUBLE_PARAMS)] = { 99999., 99999. };  //with std::numeric_limits<double>::max() spinboxes are too long
    static constexpr double DefaultDoubleParamsValues[GetValidArraySize(NUM_OF_DOUBLE_PARAMS)] = { 1., 0.01 };
    static constexpr double StepDoubleParamsValues[GetValidArraySize(NUM_OF_DOUBLE_PARAMS)] = { 0.5, 0.1 };
    static constexpr int DecimalCountDoubleParamsValues[GetValidArraySize(NUM_OF_DOUBLE_PARAMS)] = { 3, 4 };

    static constexpr const char* IntParametersNames[GetValidArraySize(NUM_OF_INT_PARAMS)] = { "Number of time steps" };
    static constexpr int MinimalIntParamsValues[GetValidArraySize(NUM_OF_INT_PARAMS)] = { 1 };
    static constexpr int MaximalIntParamsValues[GetValidArraySize(NUM_OF_INT_PARAMS)] = { 999999 };     //with std::numeric_limits<int>::max() spinboxes are too long
    static constexpr int DefaultIntParamsValues[GetValidArraySize(NUM_OF_INT_PARAMS)] = { 10 };
    static constexpr int StepIntParamsValues[GetValidArraySize(NUM_OF_INT_PARAMS)] = { 5 };

protected:
    virtual std::string GenerateParametersArg() const override;
    virtual void ProcessData(void *data) override;

public slots:
    OPERATION_SLOTS_OVERRIDE

private:
    OPERATION_PRIVATE_MEMBERS
};


class NIBLACK_OP : public Operation
{
    Q_OBJECT

public:
    NIBLACK_OP(void *data = nullptr);
    virtual ~NIBLACK_OP() {};

    OPERATION_PUBLIC_OVERRIDE(Operation::NIBLACK)

    virtual bool IsIterative() const override { return false; }    

    static constexpr size_t NUM_OF_DOUBLE_PARAMS = 2;
    static constexpr size_t NUM_OF_INT_PARAMS = 1;
    static constexpr size_t NUM_OF_PIPELINE_INPUTS = 3;
    static constexpr size_t NUM_OF_PIPELINE_OUTPUTS = 1;

    static constexpr const char* PipelineInputsNames[GetValidArraySize(NUM_OF_PIPELINE_INPUTS)] = { "Intensity", "Intensity squared", "Thresholding" };
    static constexpr const char* PipelineOutputsNames[GetValidArraySize(NUM_OF_PIPELINE_OUTPUTS)] = { "Output" };

    static constexpr const char* DoubleParametersNames[GetValidArraySize(NUM_OF_DOUBLE_PARAMS)] = { "Kappa coeff", "Mean offset"};
    static constexpr double MinimalDoubleParamsValues[GetValidArraySize(NUM_OF_DOUBLE_PARAMS)] = { -99999., -99999. };
    static constexpr double MaximalDoubleParamsValues[GetValidArraySize(NUM_OF_DOUBLE_PARAMS)] = { 99999., 99999. };  //with std::numeric_limits<double>::max() spinboxes are too long
    static constexpr double DefaultDoubleParamsValues[GetValidArraySize(NUM_OF_DOUBLE_PARAMS)] = { 1., 20. };
    static constexpr double StepDoubleParamsValues[GetValidArraySize(NUM_OF_DOUBLE_PARAMS)] = { 0.5, 5. };
    static constexpr int DecimalCountDoubleParamsValues[GetValidArraySize(NUM_OF_DOUBLE_PARAMS)] = { 3, 3 };

    static constexpr const char* IntParametersNames[GetValidArraySize(NUM_OF_INT_PARAMS)] = { "Radius" };
    static constexpr int MinimalIntParamsValues[GetValidArraySize(NUM_OF_INT_PARAMS)] = { 0 };
    static constexpr int MaximalIntParamsValues[GetValidArraySize(NUM_OF_INT_PARAMS)] = { 999999 };     //with std::numeric_limits<int>::max() spinboxes are too long
    static constexpr int DefaultIntParamsValues[GetValidArraySize(NUM_OF_INT_PARAMS)] = { 0 };
    static constexpr int StepIntParamsValues[GetValidArraySize(NUM_OF_INT_PARAMS)] = { 1 };

protected:
    virtual std::string GenerateParametersArg() const override;
    virtual void ProcessData(void *data) override;

public slots:
    OPERATION_SLOTS_OVERRIDE

private:
    OPERATION_PRIVATE_MEMBERS
};


class MCF : public Operation
{
    Q_OBJECT

public:
    MCF(void *data = nullptr);
    virtual ~MCF() {};

    OPERATION_PUBLIC_OVERRIDE(Operation::MCF)

    virtual bool IsIterative() const override { return true; }    

    static constexpr size_t NUM_OF_DOUBLE_PARAMS = 3;
    static constexpr size_t NUM_OF_INT_PARAMS = 1;
    static constexpr size_t NUM_OF_PIPELINE_INPUTS = 1;
    static constexpr size_t NUM_OF_PIPELINE_OUTPUTS = 1;

    static constexpr const char* PipelineInputsNames[GetValidArraySize(NUM_OF_PIPELINE_INPUTS)] = { "Input" };
    static constexpr const char* PipelineOutputsNames[GetValidArraySize(NUM_OF_PIPELINE_OUTPUTS)] = { "Output" };

    static constexpr const char* DoubleParametersNames[GetValidArraySize(NUM_OF_DOUBLE_PARAMS)] = { "Time step", "Tolerance", "Epsilon"};
    static constexpr double MinimalDoubleParamsValues[GetValidArraySize(NUM_OF_DOUBLE_PARAMS)] = { 0.001, 0., 0. };
    static constexpr double MaximalDoubleParamsValues[GetValidArraySize(NUM_OF_DOUBLE_PARAMS)] = { 99999., 99999., 99999. };  //with std::numeric_limits<double>::max() spinboxes are too long
    static constexpr double DefaultDoubleParamsValues[GetValidArraySize(NUM_OF_DOUBLE_PARAMS)] = { 1., 0.1, 0.1 };
    static constexpr double StepDoubleParamsValues[GetValidArraySize(NUM_OF_DOUBLE_PARAMS)] = { 0.5, 0.1, 0.1 };
    static constexpr int DecimalCountDoubleParamsValues[GetValidArraySize(NUM_OF_DOUBLE_PARAMS)] = { 3, 4, 3 };

    static constexpr const char* IntParametersNames[GetValidArraySize(NUM_OF_INT_PARAMS)] = { "Number of steps" };
    static constexpr int MinimalIntParamsValues[GetValidArraySize(NUM_OF_INT_PARAMS)] = { 0 };
    static constexpr int MaximalIntParamsValues[GetValidArraySize(NUM_OF_INT_PARAMS)] = { 999999 };     //with std::numeric_limits<int>::max() spinboxes are too long
    static constexpr int DefaultIntParamsValues[GetValidArraySize(NUM_OF_INT_PARAMS)] = { 0 };
    static constexpr int StepIntParamsValues[GetValidArraySize(NUM_OF_INT_PARAMS)] = { 5 };

protected:
    virtual std::string GenerateParametersArg() const override;
    virtual void ProcessData(void *data) override;

public slots:
    OPERATION_SLOTS_OVERRIDE

private:
    OPERATION_PRIVATE_MEMBERS
};


class SUBSURF : public Operation
{
    Q_OBJECT

public:
    SUBSURF(void *data = nullptr);
    virtual ~SUBSURF() {};

    OPERATION_PUBLIC_OVERRIDE(Operation::SUBSURF)

    virtual bool IsIterative() const override { return true; }    

    static constexpr size_t NUM_OF_DOUBLE_PARAMS = 4;
    static constexpr size_t NUM_OF_INT_PARAMS = 1;
    static constexpr size_t NUM_OF_PIPELINE_INPUTS = 2;
    static constexpr size_t NUM_OF_PIPELINE_OUTPUTS = 1;

    static constexpr const char* PipelineInputsNames[GetValidArraySize(NUM_OF_PIPELINE_INPUTS)] = { "I0", "u0" };
    static constexpr const char* PipelineOutputsNames[GetValidArraySize(NUM_OF_PIPELINE_OUTPUTS)] = { "Output" };

    static constexpr const char* DoubleParametersNames[GetValidArraySize(NUM_OF_DOUBLE_PARAMS)] = { "Time step", "Tolerance", "Epsilon", "K"};
    static constexpr double MinimalDoubleParamsValues[GetValidArraySize(NUM_OF_DOUBLE_PARAMS)] = { 0.001, 0., 0., 0. };
    static constexpr double MaximalDoubleParamsValues[GetValidArraySize(NUM_OF_DOUBLE_PARAMS)] = { 99999., 99999., 99999., 99999. };  //with std::numeric_limits<double>::max() spinboxes are too long
    static constexpr double DefaultDoubleParamsValues[GetValidArraySize(NUM_OF_DOUBLE_PARAMS)] = { 1., 0.1, 0.1, 20. };
    static constexpr double StepDoubleParamsValues[GetValidArraySize(NUM_OF_DOUBLE_PARAMS)] = { 0.5, 0.1, 0.1, 0.1 };
    static constexpr int DecimalCountDoubleParamsValues[GetValidArraySize(NUM_OF_DOUBLE_PARAMS)] = { 3, 4, 3, 3 };

    static constexpr const char* IntParametersNames[GetValidArraySize(NUM_OF_INT_PARAMS)] = { "Number of steps" };
    static constexpr int MinimalIntParamsValues[GetValidArraySize(NUM_OF_INT_PARAMS)] = { 0 };
    static constexpr int MaximalIntParamsValues[GetValidArraySize(NUM_OF_INT_PARAMS)] = { 999999 };     //with std::numeric_limits<int>::max() spinboxes are too long
    static constexpr int DefaultIntParamsValues[GetValidArraySize(NUM_OF_INT_PARAMS)] = { 0 };
    static constexpr int StepIntParamsValues[GetValidArraySize(NUM_OF_INT_PARAMS)] = { 5 };

protected:
    virtual std::string GenerateParametersArg() const override;
    virtual void ProcessData(void *data) override;

public slots:
    OPERATION_SLOTS_OVERRIDE

private:
    OPERATION_PRIVATE_MEMBERS
};


class SQUARE : public Operation
{
    Q_OBJECT

public:
    SQUARE(void *data = nullptr);
    virtual ~SQUARE() {};

    OPERATION_PUBLIC_OVERRIDE(Operation::SQUARE)

    virtual bool IsIterative() const override { return false; }    

    static constexpr size_t NUM_OF_DOUBLE_PARAMS = 0;
    static constexpr size_t NUM_OF_INT_PARAMS = 0;
    static constexpr size_t NUM_OF_PIPELINE_INPUTS = 1;
    static constexpr size_t NUM_OF_PIPELINE_OUTPUTS = 1;

    static constexpr const char* PipelineInputsNames[GetValidArraySize(NUM_OF_PIPELINE_INPUTS)] = { "Input" };
    static constexpr const char* PipelineOutputsNames[GetValidArraySize(NUM_OF_PIPELINE_OUTPUTS)] = { "Output" };

    static constexpr const char* DoubleParametersNames[GetValidArraySize(NUM_OF_DOUBLE_PARAMS)] = {};
    static constexpr double MinimalDoubleParamsValues[GetValidArraySize(NUM_OF_DOUBLE_PARAMS)] = {};
    static constexpr double MaximalDoubleParamsValues[GetValidArraySize(NUM_OF_DOUBLE_PARAMS)] = {};
    static constexpr double DefaultDoubleParamsValues[GetValidArraySize(NUM_OF_DOUBLE_PARAMS)] = {};
    static constexpr double StepDoubleParamsValues[GetValidArraySize(NUM_OF_DOUBLE_PARAMS)] = {};
    static constexpr int DecimalCountDoubleParamsValues[GetValidArraySize(NUM_OF_DOUBLE_PARAMS)] = {};

    static constexpr const char* IntParametersNames[GetValidArraySize(NUM_OF_INT_PARAMS)] = {};
    static constexpr int MinimalIntParamsValues[GetValidArraySize(NUM_OF_INT_PARAMS)] = {};
    static constexpr int MaximalIntParamsValues[GetValidArraySize(NUM_OF_INT_PARAMS)] = {};
    static constexpr int DefaultIntParamsValues[GetValidArraySize(NUM_OF_INT_PARAMS)] = {};
    static constexpr int StepIntParamsValues[GetValidArraySize(NUM_OF_INT_PARAMS)] = {};

protected:
    virtual std::string GenerateParametersArg() const override;
    virtual void ProcessData(void *data) override;

public slots:
    OPERATION_SLOTS_OVERRIDE

private:
    OPERATION_PRIVATE_MEMBERS
};


class MAX : public Operation
{
    Q_OBJECT

public:
    MAX(void *data = nullptr);
    virtual ~MAX() {};

    OPERATION_PUBLIC_OVERRIDE(Operation::MAX)

    virtual bool IsIterative() const override { return false; }    

    static constexpr size_t NUM_OF_DOUBLE_PARAMS = 0;
    static constexpr size_t NUM_OF_INT_PARAMS = 0;
    static constexpr size_t NUM_OF_PIPELINE_INPUTS = 2;
    static constexpr size_t NUM_OF_PIPELINE_OUTPUTS = 1;

    static constexpr const char* PipelineInputsNames[GetValidArraySize(NUM_OF_PIPELINE_INPUTS)] = { "Input1", "Input2" };
    static constexpr const char* PipelineOutputsNames[GetValidArraySize(NUM_OF_PIPELINE_OUTPUTS)] = { "Output" };

    static constexpr const char* DoubleParametersNames[GetValidArraySize(NUM_OF_DOUBLE_PARAMS)] = {};
    static constexpr double MinimalDoubleParamsValues[GetValidArraySize(NUM_OF_DOUBLE_PARAMS)] = {};
    static constexpr double MaximalDoubleParamsValues[GetValidArraySize(NUM_OF_DOUBLE_PARAMS)] = {};
    static constexpr double DefaultDoubleParamsValues[GetValidArraySize(NUM_OF_DOUBLE_PARAMS)] = {};
    static constexpr double StepDoubleParamsValues[GetValidArraySize(NUM_OF_DOUBLE_PARAMS)] = {};
    static constexpr int DecimalCountDoubleParamsValues[GetValidArraySize(NUM_OF_DOUBLE_PARAMS)] = {};

    static constexpr const char* IntParametersNames[GetValidArraySize(NUM_OF_INT_PARAMS)] = {};
    static constexpr int MinimalIntParamsValues[GetValidArraySize(NUM_OF_INT_PARAMS)] = {};
    static constexpr int MaximalIntParamsValues[GetValidArraySize(NUM_OF_INT_PARAMS)] = {};
    static constexpr int DefaultIntParamsValues[GetValidArraySize(NUM_OF_INT_PARAMS)] = {};
    static constexpr int StepIntParamsValues[GetValidArraySize(NUM_OF_INT_PARAMS)] = {};

protected:
    virtual std::string GenerateParametersArg() const override;
    virtual void ProcessData(void *data) override;

public slots:
    OPERATION_SLOTS_OVERRIDE

private:
    OPERATION_PRIVATE_MEMBERS
};


class EXPAND_REFINEMENT : public Operation
{
    Q_OBJECT

public:
    EXPAND_REFINEMENT(void *data = nullptr);
    virtual ~EXPAND_REFINEMENT() {};

    OPERATION_PUBLIC_OVERRIDE(Operation::EXPAND_REFINEMENT)

    virtual bool IsIterative() const override { return false; }    

    static constexpr size_t NUM_OF_DOUBLE_PARAMS = 0;
    static constexpr size_t NUM_OF_INT_PARAMS = 0;
    static constexpr size_t NUM_OF_PIPELINE_INPUTS = 0;
    static constexpr size_t NUM_OF_PIPELINE_OUTPUTS = 0;

    static constexpr const char* PipelineInputsNames[GetValidArraySize(NUM_OF_PIPELINE_INPUTS)] = {};
    static constexpr const char* PipelineOutputsNames[GetValidArraySize(NUM_OF_PIPELINE_OUTPUTS)] = {};

    static constexpr const char* DoubleParametersNames[GetValidArraySize(NUM_OF_DOUBLE_PARAMS)] = {};
    static constexpr double MinimalDoubleParamsValues[GetValidArraySize(NUM_OF_DOUBLE_PARAMS)] = {};
    static constexpr double MaximalDoubleParamsValues[GetValidArraySize(NUM_OF_DOUBLE_PARAMS)] = {};
    static constexpr double DefaultDoubleParamsValues[GetValidArraySize(NUM_OF_DOUBLE_PARAMS)] = {};
    static constexpr double StepDoubleParamsValues[GetValidArraySize(NUM_OF_DOUBLE_PARAMS)] = {};
    static constexpr int DecimalCountDoubleParamsValues[GetValidArraySize(NUM_OF_DOUBLE_PARAMS)] = {};

    static constexpr const char* IntParametersNames[GetValidArraySize(NUM_OF_INT_PARAMS)] = {};
    static constexpr int MinimalIntParamsValues[GetValidArraySize(NUM_OF_INT_PARAMS)] = {};
    static constexpr int MaximalIntParamsValues[GetValidArraySize(NUM_OF_INT_PARAMS)] = {};
    static constexpr int DefaultIntParamsValues[GetValidArraySize(NUM_OF_INT_PARAMS)] = {};
    static constexpr int StepIntParamsValues[GetValidArraySize(NUM_OF_INT_PARAMS)] = {};

protected:
    virtual std::string GenerateParametersArg() const override;
    virtual void ProcessData(void *) override;

public slots:
    OPERATION_SLOTS_OVERRIDE

private:
    OPERATION_PRIVATE_MEMBERS
};


class REFINE_BY_THRESHOLD : public Operation
{
    Q_OBJECT

public:
    REFINE_BY_THRESHOLD(void *data = nullptr);
    virtual ~REFINE_BY_THRESHOLD() {};

    OPERATION_PUBLIC_OVERRIDE(Operation::REFINE_BY_THRESHOLD)

    virtual bool IsIterative() const override { return false; }    

    static constexpr size_t NUM_OF_DOUBLE_PARAMS = 1;
    static constexpr size_t NUM_OF_INT_PARAMS = 0;
    static constexpr size_t NUM_OF_PIPELINE_INPUTS = 1;
    static constexpr size_t NUM_OF_PIPELINE_OUTPUTS = 0;

    static constexpr const char* PipelineInputsNames[GetValidArraySize(NUM_OF_PIPELINE_INPUTS)] = { "Input" };
    static constexpr const char* PipelineOutputsNames[GetValidArraySize(NUM_OF_PIPELINE_OUTPUTS)] = {};

    static constexpr const char* DoubleParametersNames[GetValidArraySize(NUM_OF_DOUBLE_PARAMS)] = { "Threshold" };
    static constexpr double MinimalDoubleParamsValues[GetValidArraySize(NUM_OF_DOUBLE_PARAMS)] = { 0. };
    static constexpr double MaximalDoubleParamsValues[GetValidArraySize(NUM_OF_DOUBLE_PARAMS)] = { 99999. };
    static constexpr double DefaultDoubleParamsValues[GetValidArraySize(NUM_OF_DOUBLE_PARAMS)] = { 50. };
    static constexpr double StepDoubleParamsValues[GetValidArraySize(NUM_OF_DOUBLE_PARAMS)] = { 10. };
    static constexpr int DecimalCountDoubleParamsValues[GetValidArraySize(NUM_OF_DOUBLE_PARAMS)] = { 2 };

    static constexpr const char* IntParametersNames[GetValidArraySize(NUM_OF_INT_PARAMS)] = {};
    static constexpr int MinimalIntParamsValues[GetValidArraySize(NUM_OF_INT_PARAMS)] = {};
    static constexpr int MaximalIntParamsValues[GetValidArraySize(NUM_OF_INT_PARAMS)] = {};
    static constexpr int DefaultIntParamsValues[GetValidArraySize(NUM_OF_INT_PARAMS)] = {};
    static constexpr int StepIntParamsValues[GetValidArraySize(NUM_OF_INT_PARAMS)] = {};

protected:
    virtual std::string GenerateParametersArg() const override;
    virtual void ProcessData(void *data) override;

public slots:
    OPERATION_SLOTS_OVERRIDE

private:
    OPERATION_PRIVATE_MEMBERS
};


class FLAT_THRESHOLD : public Operation
{
    Q_OBJECT

public:
    FLAT_THRESHOLD(void *data = nullptr);
    virtual ~FLAT_THRESHOLD() {};

    OPERATION_PUBLIC_OVERRIDE(Operation::FLAT_THRESHOLD)

    virtual bool IsIterative() const override { return false; }    

    static constexpr size_t NUM_OF_DOUBLE_PARAMS = 1;
    static constexpr size_t NUM_OF_INT_PARAMS = 0;
    static constexpr size_t NUM_OF_PIPELINE_INPUTS = 1;
    static constexpr size_t NUM_OF_PIPELINE_OUTPUTS = 1;

    static constexpr const char* PipelineInputsNames[GetValidArraySize(NUM_OF_PIPELINE_INPUTS)] = { "Input" };
    static constexpr const char* PipelineOutputsNames[GetValidArraySize(NUM_OF_PIPELINE_OUTPUTS)] = { "Output" };

    static constexpr const char* DoubleParametersNames[GetValidArraySize(NUM_OF_DOUBLE_PARAMS)] = { "Threshold" };
    static constexpr double MinimalDoubleParamsValues[GetValidArraySize(NUM_OF_DOUBLE_PARAMS)] = { 0. };
    static constexpr double MaximalDoubleParamsValues[GetValidArraySize(NUM_OF_DOUBLE_PARAMS)] = { 99999. };
    static constexpr double DefaultDoubleParamsValues[GetValidArraySize(NUM_OF_DOUBLE_PARAMS)] = { 50. };
    static constexpr double StepDoubleParamsValues[GetValidArraySize(NUM_OF_DOUBLE_PARAMS)] = { 10. };
    static constexpr int DecimalCountDoubleParamsValues[GetValidArraySize(NUM_OF_DOUBLE_PARAMS)] = { 2 };

    static constexpr const char* IntParametersNames[GetValidArraySize(NUM_OF_INT_PARAMS)] = {};
    static constexpr int MinimalIntParamsValues[GetValidArraySize(NUM_OF_INT_PARAMS)] = {};
    static constexpr int MaximalIntParamsValues[GetValidArraySize(NUM_OF_INT_PARAMS)] = {};
    static constexpr int DefaultIntParamsValues[GetValidArraySize(NUM_OF_INT_PARAMS)] = {};
    static constexpr int StepIntParamsValues[GetValidArraySize(NUM_OF_INT_PARAMS)] = {};

protected:
    virtual std::string GenerateParametersArg() const override;
    virtual void ProcessData(void *data) override;

public slots:
    OPERATION_SLOTS_OVERRIDE

private:
    OPERATION_PRIVATE_MEMBERS
};

#endif // OPERATION_H
