#include "argumentparser.h"

#include <qstring.h>

#include <cmath>
#include <algorithm>
#include <string>
#include <sstream>
#include <cctype>


ArgumentParser::ArgumentParser()
{
    InitializeValues();
}

ArgumentParser::~ArgumentParser()
{
    DeallocateParams();
}

bool ArgumentParser::ParseString(const QString &str)
{
    InitializeValues();

    if (str == "") return false;

    QStringList argv(PreprocessString(str));
    int argc = static_cast<int>(argv.size());

    if (argc == 1) return false;
    else if (argc == 2) return false;

    if (!PickFlagsAndArgs(argc, argv)) return false;

    for(size_t i = 0; i < m_currentFlags.size(); ++i)
    {
        switch(m_currentFlags[i].toLatin1())
        {
        case 'f': if (!Parse_f_flag()) return false; break;
        case 'i': if (!Parse_i_flag()) return false; break;
        case 'c': if (!Parse_c_flag()) return false; break;
        case 'C': break;                                    //nothing to do here
        case 'o': if (!Parse_o_flag()) return false; break;
        case 'O': if (!Parse_O_flag()) return false; break;
        case 'X': if (!Parse_X_flag()) return false; break;
        case 'B': if (!Parse_B_flag()) return false; break;
        case 'I': if (!Parse_I_flag()) return false; break;
        case 'D': if (!Parse_D_flag()) return false; break;
        case 'T': if (!Parse_T_flag()) return false; break;
        case 'r': if (!Parse_r_flag()) return false; break;
        case 'm': if (!Parse_m_flag()) return false; break;
        default:
        {
            m_logger << "Unknown flag " << m_currentFlags[i].toLatin1() << ". Program will abort!" << std::endl;
            return false;
        }
        }
    }

    if (!IsValid())
    {
        m_logger << "Mandatory flags missing: \"" << PrintMissingFlags() << "\"" << std::endl;
        return false;
    }

    if (HasDuplicates(m_animationNames))
    {
        m_logger << "Animation names cannot be identical with each other!\n";
        return false;
    }

    m_logger << "Parsing OK!\n";
    return true;
}

bool ArgumentParser::IsValid() const
{
    for(int i = 0; i < NUMBER_OF_MANDATORY_FLAGS; ++i)
    {
        if (m_flagParams.find(m_mandatoryFlags[i]) == m_flagParams.end())
        {
            return false;
        }
    }

    bool f_flag_present = m_flagParams.find('f') != m_flagParams.end();
    bool i_flag_present = m_flagParams.find('i') != m_flagParams.end();

    return f_flag_present || i_flag_present;
}

std::string ArgumentParser::PrintMissingFlags() const
{
    std::stringstream ss;

    for(int i = 0; i < NUMBER_OF_MANDATORY_FLAGS; ++i)
    {
        if (m_flagParams.find(m_mandatoryFlags[i]) == m_flagParams.end())
        {
            ss << m_mandatoryFlags[i] << " ";
        }
    }

    bool f_flag_present = m_flagParams.find('f') != m_flagParams.end();
    bool i_flag_present = m_flagParams.find('i') != m_flagParams.end();

    if (!(f_flag_present || i_flag_present)) ss << "i/f ";

    std::string temp(ss.str());

    return temp.substr(0, temp.size() - 1); //remove last " "
}

void ArgumentParser::InitializeValues()
{
    DeallocateParams();
    m_folderBatchPath = "";
    m_imageLoadPath = "";
    m_bBatchProcess = false;
    m_connectivityFilePath = "";
    m_outputFolderPath = "";
    m_bOutputVTK = true;
    m_bOutputImage = true;
    m_backgroundQuadrantLevel = -3;
    m_bSetQuadrantBackgroud = true;
    m_bSetAutomaticBackgroundValue = true;
    m_customBackgroundValue = false;
    m_bCopyOriginalImage = false;
    m_bCreateNewOutputDirectory = false;
    m_bAutoTreeSideLength = true;
    m_treeSideLength = 16;
    m_bAutoExtraPixelValue = true;
    m_extraPixelValue = 0;
    m_mpiProcessCount = 4;
    m_logger.str("");
    m_processingAlgorithms.clear();
    m_refinementAlgorithms.clear();
    m_vtkPipelinesExport.clear();
    m_imagePipelinesExport.clear();
}

QStringList ArgumentParser::PreprocessString(const QString &str)
{
    QStringList temp(SplitToArgv(str));

    if (temp.size() < 5) return QStringList();

    if (temp[1] == "-n")
    {
        bool conversionOK;
        m_mpiProcessCount = temp[2].toInt(&conversionOK);
        if (!conversionOK) m_mpiProcessCount = -1;
    }

    if (temp[0] == "mpiexec")
        for (int i = 0; i < 4; ++i) temp.removeAt(0);

    return temp;
}

bool ArgumentParser::PickFlagsAndArgs(int argc, const QStringList &argv)
{
    for(int i = 1; i < argc; ++i)
    {
        if (argv[i][0] == '-' && ((argv[i][1] >= 'A' && argv[i][1] <= 'Z') || (argv[i][1] >= 'a' && argv[i][1] <= 'z')))
        {
            m_currentFlags.push_back(argv[i][1].toLatin1());

            if (argv[i][1] == 'D' || argv[i][1] == 'I')
            {
            }
            else if (!ExtractFlagParams(i, argc, argv)) return false;
        }
    }

    return true;
}

bool ArgumentParser::Parse_f_flag()
{
    QString params(m_flagParams.at('f'));
    QStringList paramList(params.split(' ', QString::SkipEmptyParts));

    if (paramList.length() != 1)
    {
        m_logger << "Wrong number of parameters for flag '-f'. Expected is 1." << std::endl;
        return false;
    }

    m_folderBatchPath = m_flagParams.at('f');
    m_bBatchProcess = true;

    return true;
}

bool ArgumentParser::Parse_i_flag()
{
    QString params(m_flagParams.at('i'));
    QStringList paramList(params.split(' ', QString::SkipEmptyParts));

    if (paramList.length() != 1)
    {
        m_logger << "Wrong number of parameters for flag '-i'. Expects: *path* to file to be processed." << std::endl;
        return false;
    }

    m_imageLoadPath = m_flagParams.at('i');

    return true;
}

bool ArgumentParser::Parse_c_flag()
{
    QString params(m_flagParams.at('c'));
    QStringList paramList(params.split(' ', QString::SkipEmptyParts));

    if (paramList.length() != 1)
    {
        m_logger << "Wrong number of parameters for flag '-c'. Expects: *path* to file where connectivity .inp file will be created." << std::endl;
        return false;
    }

    QString extension = paramList[0].section('.', -1);
    QString result(paramList[0]);

    if (extension != "inp")
    {
        result = paramList[0].append(".inp");
    }

    m_connectivityFilePath = result;

    return true;
}

bool ArgumentParser::Parse_o_flag()
{
    QString params(m_flagParams.at('o'));
    QStringList paramList(params.split(' ', QString::SkipEmptyParts));

    if (paramList.length() != 1)
    {
        m_logger << "Wrong number of parameters for flag '-o'. Expects: *path* to folder where .vtk output will be created." << std::endl;
        return false;
    }

    params.replace('\\','/');
    if (params.back() == '/')
    {
        params.chop(1);
    }

    m_outputFolderPath = params;

    return true;
}

bool ArgumentParser::Parse_O_flag()
{
    QString params(m_flagParams.at('O'));
    QStringList paramList(params.split(' ', QString::SkipEmptyParts));

    if (paramList.length() > 3)
    {
        m_logger << "Wrong number of parameters for flag '-O'. Expects: *vtk* for vtk output, *image* for image output along with optional *binary* as output pgm format." << std::endl;
        return false;
    }

    bool presentFlag[3] = { false, false, false };
    for(int i = 0; i < paramList.length(); ++i)
    {
        if (paramList[i] == "vtk") presentFlag[0] = true;
        else if (paramList[i] == "image") presentFlag[1] = true;
        else if (paramList[i] == "binary") presentFlag[2] = true;
    }

    m_bOutputVTK = presentFlag[0];
    m_bOutputImage = presentFlag[1];

    return true;
}

bool ArgumentParser::Parse_X_flag()
{
    std::vector<std::string> outputNames;

    QString params(m_flagParams.at('X'));
    QStringList paramList(params.split(',', QString::SkipEmptyParts));

    if (paramList.length() != 2)
    {
        m_logger << "Wrong number of parameters for flag '-X'. Expects: *pipeline export options* vtk export options, *pipeline export options* image export options.";
        m_logger << "You can specify as many pipelines to be exported as needed. Default is '0 original 1 result, 1 result'." << std::endl;
        return false;
    }

    QStringList vtkParams(paramList[0].split(' ', QString::SkipEmptyParts));
    QStringList imageParams(paramList[1].split(' ', QString::SkipEmptyParts));

    if (vtkParams.length() % 2 == 1)    //we need pair number-name, so length must be even number
    {
        m_logger << "Wrong number of parameters for flag '-X' vtk part. Expects: *non-negative number < " << MAX_PIPELINE_NUMBER_STRING << "* pipeline number, *string* pipeline name.";
        m_logger << "You can specify as many pipelines to be exported as needed. Default is '0 original 1 result'." << std::endl;
        return false;
    }

    if (imageParams.length() % 2 == 1)    //we need pair number-name, so length must be even number
    {
        m_logger << "Wrong number of parameters for flag '-X' image part. Expects: *non-negative number < " << MAX_PIPELINE_NUMBER_STRING << "* pipeline number, *string* pipeline name.";
        m_logger << "You can specify as many pipelines to be exported as needed. Default is '1 result'." << std::endl;
        return false;
    }

    int numOfPipelinesToExport = vtkParams.length() / 2;
    m_vtkPipelinesExport.reserve(numOfPipelinesToExport);

    //VTK export options parsing
    for (int i = 0; i < numOfPipelinesToExport; ++i)
    {
        bool conversionOK = false;
        int pipelineNumber = vtkParams[2 * i].toInt(&conversionOK);

        if (!conversionOK || pipelineNumber < 0 || pipelineNumber >= MAX_PIPELINE_NUMBER)
        {
            m_logger << "Wrong parameter (" << 2 * i << ") format for flag '-X' vtk part. Expects: *non-negative number < " << MAX_PIPELINE_NUMBER_STRING << "* pipeline number." << std::endl;
            return false;
        }

        m_vtkPipelinesExport.push_back(std::make_pair(pipelineNumber, vtkParams[2 * i + 1]));
    }

    for(size_t i = 0; i < outputNames.size(); ++i)
    {
        if (outputNames[i] == "source")
        {
            m_logger << "VTK pipeline outputs cannot have the name 'source'!\n";
            return false;
        }
    }

    if (HasDuplicates(outputNames))
    {
        m_logger << "VTK pipeline outputs cannot have the same name!\n";
        return false;
    }

    outputNames.clear();
    numOfPipelinesToExport = imageParams.length() / 2;

    //Image export options parsing
    for (int i = 0; i < numOfPipelinesToExport; ++i)
    {
        bool conversionOK = false;
        int pipelineNumber = imageParams[2 * i].toInt(&conversionOK);

        if (!conversionOK || pipelineNumber < 0 || pipelineNumber >= MAX_PIPELINE_NUMBER)
        {
            m_logger << "Wrong parameter (" << 2 * i << ") format for flag '-X' image part. Expects: *non-negative number < " << MAX_PIPELINE_NUMBER_STRING << "* pipeline number." << std::endl;
            return false;
        }

        m_imagePipelinesExport.push_back(std::make_pair(pipelineNumber, vtkParams[2 * i + 1]));
    }

    if (HasDuplicates(outputNames))
    {
        m_logger << "IMAGE pipeline outputs cannot have the same name!\n";
        return false;
    }

    return true;
}

bool ArgumentParser::Parse_B_flag()
{
    QString params(m_flagParams.at('B'));
    QStringList paramList(params.split(' ', QString::SkipEmptyParts));

    if (paramList.length() == 0 || paramList.length() > 2)
    {
        m_logger << "Wrong number of parameters for flag '-B'. Expects: *number* level up to which quadrants will be assigned OTSU's background mean value, ";
        m_logger << "instead of mean of image part corresponding to their position (negative values will be subtracted from maximum level), ";
        m_logger << "*non-negative number <= 255* optional value to be assigned instead of OTSU's." << std::endl;
        return false;
    }

    bool conversionOK = false;
    int quadLevel = paramList[0].toInt(&conversionOK);

    if (!conversionOK)
    {
        m_logger << "Wrong first parameter format for flag '-B'. Expects: *number* level up to which quadrants will be assigned OTSU's background mean value, ";
        m_logger << "instead of mean of image part corresponding to their position. Negative values will be subtracted from maximum level." << std::endl;
        return false;
    }

    int customValue = 0;

    if (paramList.length() == 2)
    {
        customValue = paramList[1].toInt(&conversionOK);
        m_bSetAutomaticBackgroundValue = false;

        if (!conversionOK || customValue < 0 || customValue > 255)
        {
            m_logger << "Wrong second parameter format for flag '-B'. Expects: *non-negative number <= 255* optional value to be assigned instead of OTSU's." << std::endl;
            return false;
        }
    }

    m_bSetQuadrantBackgroud = true;
    m_backgroundQuadrantLevel = quadLevel;
    m_customBackgroundValue = static_cast<unsigned char>(customValue);

    return true;
}

bool ArgumentParser::Parse_I_flag()
{
    m_bCopyOriginalImage = true;
    return true;
}

bool ArgumentParser::Parse_D_flag()
{
    m_bCreateNewOutputDirectory = true;
    return true;
}

bool ArgumentParser::Parse_T_flag()
{
    QString params(m_flagParams.at('T'));
    QStringList paramList(params.split(' ', QString::SkipEmptyParts));

    if (paramList.length() == 0 || paramList.length() > 2)
    {
        m_logger << "Wrong number of parameters for flag '-T'. Expects: 2^n *positive number* of initial tree side length with optional *non-negative number* value for extra pixels.";
        m_logger << " If initial tree side length is zero, application will try to find the most optimal value. If second parameter is not specified mean of original image is used instead." << std::endl;
        return false;
    }

    bool conversionOK = false;
    int treeSideLength = paramList[0].toInt(&conversionOK);

    if (!conversionOK || treeSideLength < 0)
    {
        m_logger << "Wrong first parameter format for -T flag. Expects: 2^n * positive number* of initial tree side length.";
        m_logger << " If initial tree side length is zero, application will try to find the most optimal value." << std::endl;
        return false;
    }

    if (!IsPowerOfTwo(treeSideLength))
    {
        m_logger << "First parameter for -T flag is not 2^n positive number. Expects: 2^n *positive number* of initial tree side length.";
        m_logger << " If initial tree side length is zero, application will try to find the most optimal value." << std::endl;
        return false;
    }

    m_bAutoTreeSideLength = treeSideLength == 0;

    m_treeSideLength = treeSideLength;
    m_bAutoExtraPixelValue = true;

    if (paramList.length() == 2)
    {
        int extraPixelValue = paramList[1].toInt(&conversionOK);

        if (extraPixelValue > 255 && conversionOK)
        {
            m_logger << "Value for extra pixels exceeds value of 255. Results might not be what you expect" << std::endl;           
        }

        if (!conversionOK || extraPixelValue < 0)
        {
            m_logger << "Wrong second parameter format for -T flag. Expects: *non-negative number* value for extra pixels.";
            m_logger << " If second parameter is not specified, mean of original image is used instead." << std::endl;
            return false;
        }

        m_bAutoExtraPixelValue = false;
        m_extraPixelValue = extraPixelValue;
    }

    return true;
}

bool ArgumentParser::Parse_r_flag()
{
    QString all(m_flagParams.at('r'));
    QStringList methodsWithParams(all.split(',', QString::SkipEmptyParts));  //different methods

    for (int i = 0; i < methodsWithParams.length(); ++i)
    {
        QString methodWithParams(methodsWithParams[i]);
        QStringList tokens(methodWithParams.trimmed().split(' ', QString::SkipEmptyParts));

        if (tokens.length() == 0)
        {
            m_logger << "Wrong input for -r flag, method " << i << ". No parameters given." << std::endl;
            return false;
        }

        QString method(tokens[0]);

        //TODO: use negative numbers to specify refinement level from the maximum one

        if (method == "GLOBAL_OTSU") Parse_GLOBAL_OTSU_args(tokens, i);
        else if (method == "MAX_MIN_DIFF") Parse_MAX_MIN_DIFF_args(tokens, i);
        else if (method == "BERNSEN") Parse_BERNSEN_args(tokens, i);
        else if (method == "SAUVOLA") Parse_SAUVOLA_args(tokens, i);
        else if (method == "NIBLACK") Parse_NIBLACK_REFINE_args(tokens, i);
        else if (method == "TOTAL") Parse_TOTAL_args(tokens, i);
        else if (method == "UP_TO") Parse_UP_TO_args(tokens, i);
        else
        {
            m_logger << "Unknown method passed to -r flag!" << std::endl;
            return false;
        }
    }

    return true;
}

bool ArgumentParser::Parse_GLOBAL_OTSU_args(const QStringList &tokens, int orderIndex)
{
    if (tokens.length() != 2)
    {
        m_logger << "Wrong number of parameters for GLOBAL_OTSU(" << orderIndex << ") method. Expects: *non-negative number* quadrant level to refine from" << std::endl;
        return false;
    }

    bool conversionOK = false;
    int quadrantLevel = tokens[1].toInt(&conversionOK);

    if (!conversionOK || quadrantLevel < 0)
    {
        m_logger << "Wrong first parameter format for GLOBAL_OTSU(" << orderIndex << ") method. Expects: *non-negative number* quadrant level to refine from." << std::endl;
        return false;
    }

    SGlobalOTSUThresholdParams *params = new SGlobalOTSUThresholdParams;
    params->refineFromQuadLevel = quadrantLevel;

    m_refinementAlgorithms.push_back(std::make_pair(Refinement::GLOBAL_OTSU, params));

    return true;
}

bool ArgumentParser::Parse_MAX_MIN_DIFF_args(const QStringList &tokens, int orderIndex)
{
    if (tokens.length() != 3)
    {
        m_logger << "Wrong number of parameters for MAX_MIN_DIFF(" << orderIndex << ") method. Expects: *non-negative number* quadrant level to refine from, *non-negative number* threshold." << std::endl;
        return false;
    }

    bool conversionOK = false;
    int quadrantLevel = tokens[1].toInt(&conversionOK);

    if (!conversionOK || quadrantLevel < 0)
    {
        m_logger << "Wrong first parameter format for MAX_MIN_DIFF(" << orderIndex << ") method. Expects: *non-negative number* quadrant level to refine from." << std::endl;
        return false;
    }

    int maxMinDiffThreshold = tokens[2].toInt(&conversionOK);

    if (!conversionOK || maxMinDiffThreshold < 0)
    {
        m_logger << "Wrong second parameter format for MAX_MIN_DIFF(" << orderIndex << ") method. Expects: *non-negative number* threshold." << std::endl;
        return false;
    }

    SMaxMinDiffThresholdParams *params = new SMaxMinDiffThresholdParams;
    params->refineFromQuadLevel = quadrantLevel;
    params->maxMinDiffThreshold = maxMinDiffThreshold;

    m_refinementAlgorithms.push_back(std::make_pair(Refinement::MAX_MIN_DIFF, params));

    return true;
}

bool ArgumentParser::Parse_BERNSEN_args(const QStringList &tokens, int orderIndex)
{
    if (tokens.length() != 5)
    {
        m_logger << "Wrong number of parameters for BERNSEN(" << orderIndex << ") method. Expects: ";
        m_logger << "*non-negative number* quadrant level to refine from, *non-negative number* radius, *non-negative number* contrast threshold, *non-negative number* background-foreground threshold." << std::endl;
        return false;
    }

    bool conversionOK = false;
    int minQuadLevel = tokens[1].toInt(&conversionOK);

    if (!conversionOK || minQuadLevel < 0)
    {
        m_logger << "Wrong first parameter format for BERNSEN(" << orderIndex << ") method. Expects: *non-negative number* quadrant level to refine from." << std::endl;
        return false;
    }

    int radius = tokens[2].toInt(&conversionOK);

    if (!conversionOK || radius < 0)
    {
        m_logger << "Wrong second parameter format for BERNSEN(" << orderIndex << ") method. Expects: *non-negative number* radius." << std::endl;
        return false;
    }

    double contrastThreshold = tokens[3].toDouble(&conversionOK);

    if (!conversionOK || contrastThreshold < 0.)
    {
        m_logger << "Wrong third parameter format for BERNSEN(" << orderIndex << ") method. Expects: *non-negative number* contrast threshold." << std::endl;
        return false;
    }

    double backgroundForegroundThreshold = tokens[4].toDouble(&conversionOK);

    if (!conversionOK || backgroundForegroundThreshold < 0.)
    {
        m_logger << "Wrong fourth parameter format for BERNSEN(" << orderIndex << ") method. Expects: *non-negative number* background-foreground threshold." << std::endl;
        return false;
    }

    SBernsenThresholdParams *params = new SBernsenThresholdParams;
    params->radius = radius;
    params->contrastThreshold = contrastThreshold;
    params->backgroundForegroundThreshold = backgroundForegroundThreshold;
    params->refineFromQuadLevel = minQuadLevel;

    m_refinementAlgorithms.push_back(std::make_pair(Refinement::BERNSEN, params));

    return true;
}

bool ArgumentParser::Parse_SAUVOLA_args(const QStringList &tokens, int orderIndex)
{
    if (tokens.length() != 5)
    {
        m_logger << "Wrong number of parameters for SAUVOLA(" << orderIndex << ") method. Expects: ";
        m_logger << "*non-negative number* quadrant level to refine from, *non-negative number* radius, *non-negative number* kappa coefficient, *positive number* maximal standard deviation." << std::endl;
        return false;
    }

    bool conversionOK = false;
    int minQuadLevel = tokens[1].toInt(&conversionOK);

    if (!conversionOK || minQuadLevel < 0)
    {
        m_logger << "Wrong first parameter format for SAUVOLA(" << orderIndex << ") method. Expects: *non-negative number* quadrant level to refine from." << std::endl;
        return false;
    }

    int radius = tokens[2].toInt(&conversionOK);

    if (!conversionOK || radius < 0)
    {
        m_logger << "Wrong second parameter format for SAUVOLA(" << orderIndex << ") method. Expects: *non-negative number* radius." << std::endl;
        return false;
    }

    double kappa = tokens[3].toDouble(&conversionOK);

    if (!conversionOK || kappa < 0.)
    {
        m_logger << "Wrong third parameter format for SAUVOLA(" << orderIndex << ") method. Expects: *non-negative number* kappa coefficient." << std::endl;
        return false;
    }

    double sigmaMax = tokens[4].toDouble(&conversionOK);

    if (!conversionOK || sigmaMax <= 0.)
    {
        m_logger << "Wrong fourth parameter format for SAUVOLA(" << orderIndex << ") method. Expects: *positive number* maximal standard deviation." << std::endl;
        return false;
    }

    SSauvolaThresholdParams *params = new SSauvolaThresholdParams;
    params->radius = radius;
    params->kappa = kappa;
    params->sigmaMax = sigmaMax;
    params->refineFromQuadLevel = minQuadLevel;

    m_refinementAlgorithms.push_back(std::make_pair(Refinement::SAUVOLA, params));

    return true;
}

bool ArgumentParser::Parse_NIBLACK_REFINE_args(const QStringList &tokens, int orderIndex)
{
    if (tokens.length() != 5)
    {
        m_logger << "Wrong number of parameters for NIBLACK-refine(" << orderIndex << ") method. Expects: ";
        m_logger << "*non-negative number* quadrant level to refine from, *non-negative number* radius, *number* kappa coefficient, *number* mean offset." << std::endl;
        return false;
    }

    bool conversionOK = false;
    int minQuadLevel = tokens[1].toInt(&conversionOK);

    if (!conversionOK || minQuadLevel < 0)
    {
        m_logger << "Wrong first parameter format for NIBLACK-refine(" << orderIndex << ") method. Expects: *non-negative number* quadrant level to refine from." << std::endl;
        return false;
    }

    int radius = tokens[2].toInt(&conversionOK);

    if (!conversionOK || radius < 0)
    {
        m_logger << "Wrong second parameter format for NIBLACK-refine(" << orderIndex << ") method. Expects: *non-negative number* radius." << std::endl;
        return false;
    }

    double kappa = tokens[3].toDouble(&conversionOK);

    if (!conversionOK)
    {
        m_logger << "Wrong third parameter format for NIBLACK-refine(" << orderIndex << ") method. Expects: *number* kappa coefficient." << std::endl;
        return false;
    }

    double meanOffset = tokens[4].toDouble(&conversionOK);

    if (!conversionOK)
    {
        m_logger << "Wrong fourth parameter format for NIBLACK-refine(" << orderIndex << ") method. Expects: *number* mean offset." << std::endl;
        return false;
    }

    SNiblackThresholdParams *params = new SNiblackThresholdParams;
    params->radius = radius;
    params->kappa = kappa;
    params->meanOffset = meanOffset;
    params->refineFromQuadLevel = minQuadLevel;

    m_refinementAlgorithms.push_back(std::make_pair(Refinement::NIBLACK, params));

    return true;
}

bool ArgumentParser::Parse_TOTAL_args(const QStringList &tokens, int orderIndex)
{
    if (tokens.length() != 1)
    {
        m_logger << "Wrong number of parameters for TOTAL(" << orderIndex << ") refine method. Expects: nothing ";
        return false;
    }

    m_refinementAlgorithms.push_back(std::make_pair(Refinement::TOTAL, nullptr));

    return true;
}

bool ArgumentParser::Parse_UP_TO_args(const QStringList &tokens, int orderIndex)
{
    if (tokens.length() != 2)
    {
        m_logger << "Wrong number of parameters for UP_TO(" << orderIndex << ") refine method. Expects: *non-negative number* quadrant level up to which to refine the forest.\n";
        return false;
    }

    bool conversionOK = false;
    int quadLevel = tokens[1].toInt(&conversionOK);

    if (!conversionOK || quadLevel < 0)
    {
        m_logger << "Wrong first parameter format for UP_TO(" << orderIndex << ") refine method. Expects: *non-negative number* quadrant level up to which to refine the forest." << std::endl;
        return false;
    }

    SUpToRefinementParams *params = new SUpToRefinementParams;
    params->quadLevel = quadLevel;
    m_refinementAlgorithms.push_back(std::make_pair(Refinement::UP_TO, params));

    return true;
}

bool ArgumentParser::Parse_m_flag()
{
    QString all(m_flagParams.at('m'));
    QStringList methodsWithParams(all.split(',', QString::SkipEmptyParts));   //different methods

    for(int i = 0; i < methodsWithParams.length(); ++i)
    {
        QString allParams(methodsWithParams[i].trimmed());
        QStringList processingAndRefinement(allParams.split('*', QString::SkipEmptyParts));

        QString methodWithParams;

        if (processingAndRefinement.length() > 0)
        {
            methodWithParams = processingAndRefinement[0];
        }
        else
        {
            m_logger << "Empty section for '-m' flag(" << i << ")!\n";
            return false;
        }

        int methodEnd = methodWithParams.indexOf(' ');
        QString method;

        if (methodEnd == -1) method = methodWithParams;
        else method = QString(methodWithParams.left(methodEnd));

        if (processingAndRefinement.length() > 2)
        {
            m_logger << "Multiple continuous refinements for " << method.toStdString() << " method set!\n";
            return false;
        }

        int pipelineStart = methodWithParams.indexOf('{', 0);
        int pipelineEnd = methodWithParams.indexOf('}', pipelineStart);
        QStringList tokens;
        QStringList refinementTokens;

        if (pipelineStart != -1 && pipelineEnd != -1)
        {
            int pipelineLength = pipelineEnd - pipelineStart;
            QString pipeline(methodWithParams.mid(pipelineStart + 1, pipelineLength - 1));
            tokens = methodWithParams.right(methodWithParams.length() - pipelineEnd - 1).trimmed().split(' ', QString::SkipEmptyParts);
            tokens.prepend(pipeline);

            if (processingAndRefinement.length() == 2)
            {
                refinementTokens = processingAndRefinement[1].split(' ', QString::SkipEmptyParts);
            }
        }        

        if (method == "LHE_EXPLICIT") { if (!Parse_LHE_EXPLICIT_args(tokens, refinementTokens, i)) return false; }
        else if (method == "LHE_IMPLICIT") { if (!Parse_LHE_IMPLICIT_args(tokens, refinementTokens, i)) return false; }
        else if (method == "NIBLACK") { if (!Parse_NIBLACK_IMAGE_args(tokens, i)) return false; }
        else if (method == "MCF") { if (!Parse_MCF_args(tokens, refinementTokens, i)) return false; }
        else if (method == "SUBSURF") { if (!Parse_SUBSURF_args(tokens, refinementTokens, i)) return false; }
        else if (method == "SQUARE") { if (!Parse_SQUARE_args(tokens, i)) return false; }
        else if (method == "MAX") { if (!Parse_MAX_args(tokens, i)) return false; }
        else if (method == "EXPAND_REFINEMENT") { if (!Parse_EXPAND_REFINEMENT_args(tokens, i)) return false; }
        else if (method == "REFINE_BY_THRESHOLD") { if (!Parse_REFINE_BY_THRESHOLD_args(tokens, i)) return false; }
        else if (method == "FLAT_THRESHOLD") { if (!Parse_FLAT_THRESHOLD_args(tokens, i)) return false; }
        else
        {
            m_logger << "Unknown method passed to -m flag!" << std::endl;
            return false;
        }
    }

    return true;
}

bool ArgumentParser::Parse_LHE_EXPLICIT_args(const QStringList &tokens, const QStringList &refinementTokens, int orderIndex)
{
    if (tokens.length() != 3 && tokens.length() != 5)
    {
        m_logger << "Wrong number of parameters for LHE_EXPLICIT(" << orderIndex << ") method. Expects: *pipeline* input#output, *positive number* time step, *non-negative number* number of steps. ";
        m_logger << "Accepts optional 'anim' and *animation name* parameters.\n";
        return false;
    }

    bool conversionOK = false;
    QStringList pipeline(tokens[0].split('#', QString::SkipEmptyParts));

    if (pipeline.size() != 2)
    {
        m_logger << "Wrong first parameter format for LHE_EXPLICIT(" << orderIndex << ") method. Expects: *pipeline* input#output." << std::endl;
        return false;
    }

    int pipelineIN = pipeline[0].trimmed().toInt(&conversionOK);

    if (!conversionOK || pipelineIN >= MAX_PIPELINE_NUMBER || pipelineIN < 0)
    {
        m_logger << "Wrong pipeline input format for LHE_EXPLICIT(" << orderIndex << ") method. Expects: *non-negative number < " << MAX_PIPELINE_NUMBER_STRING << "*." << std::endl;
        return false;
    }

    int pipelineOUT = pipeline[1].trimmed().toInt(&conversionOK);

    if (!conversionOK || pipelineOUT >= MAX_PIPELINE_NUMBER || pipelineOUT < 0)
    {
        m_logger << "Wrong pipeline output format for LHE_EXPLICIT(" << orderIndex << ") method. Expects: *non-negative number < " << MAX_PIPELINE_NUMBER_STRING << "*." << std::endl;
        return false;
    }

    double timeStep = tokens[1].toDouble(&conversionOK);

    if (!conversionOK || timeStep <= 0.)
    {
        m_logger << "Wrong second parameter format for LHE_EXPLICIT(" << orderIndex << ") method. Expects: *positive number* time step." << std::endl;
        return false;
    }

    int numOfSteps = tokens[2].toInt(&conversionOK);
    if (!conversionOK || numOfSteps < 0)
    {
        m_logger << "Wrong third parameter format for LHE_EXPLICIT(" << orderIndex << ") method. Expects: *non-negative number* number of steps." << std::endl;
        return false;
    }

    bool bRefine = false;
    int numOfIterationsToRefineAfter = 0;

    if (refinementTokens.length() == 1)
    {
        bRefine = true;
        numOfIterationsToRefineAfter = refinementTokens[0].toInt(&conversionOK);

        if (!conversionOK || numOfIterationsToRefineAfter <= 0)
        {
            m_logger << "Wrong first parameter format in refinement options for LHE_EXPLICIT(" << orderIndex << ") method! ";
            m_logger << "Expects: *positive number* how often (iteration-wise) should refinement occur.\n";
            return false;
        }
    }
    else if (refinementTokens.length() > 1)
    {
        m_logger << "Too many arguments for continuous refinement in LHE_EXPLICIT(" << orderIndex << ") method!\n";
        return false;
    }

    SLHEExplicitParams *params = new SLHEExplicitParams;
    params->timeStep = timeStep;
    params->numOfSteps = numOfSteps;
    params->pipelineIN = pipelineIN;
    params->pipelineOUT = pipelineOUT;
    params->bRefine = bRefine;
    params->numOfIterationsToRefineAfter = numOfIterationsToRefineAfter;

    ParseAnimationArgs(tokens, "LHE_EXPLICIT", orderIndex, 3, params->animationFileName, params->bExportAnimation);

    m_processingAlgorithms.push_back(std::make_pair(Operation::LHE_EXPLICIT, params));

    return true;
}

bool ArgumentParser::Parse_LHE_IMPLICIT_args(const QStringList &tokens, const QStringList &refinementTokens, int orderIndex)
{
    if (tokens.length() != 4 && tokens.length() != 6)
    {
        m_logger << "Wrong number of parameters for LHE_IMPLICIT(" << orderIndex << ") method. Expects: *pipeline* input#output, ";
        m_logger << "*positive number* time step, *non-negative number* number of steps, *non-negative number* tolerance. ";
        m_logger << "Accepts optional 'anim' and *animation name* parameters.\n";;
        return false;
    }

    bool conversionOK = false;
    QStringList pipeline(tokens[0].split('#', QString::SkipEmptyParts));

    if (pipeline.size() != 2)
    {
        m_logger << "Wrong first parameter format for LHE_IMPLICIT(" << orderIndex << ") method. Expects: *pipeline* input#output." << std::endl;
        return false;
    }

    int pipelineIN = pipeline[0].trimmed().toInt(&conversionOK);

    if (!conversionOK || pipelineIN >= MAX_PIPELINE_NUMBER || pipelineIN < 0)
    {
        m_logger << "Wrong pipeline input format for LHE_IMPLICIT(" << orderIndex << ") method. Expects: *non-negative number < " << MAX_PIPELINE_NUMBER_STRING << "*." << std::endl;
        return false;
    }

    int pipelineOUT = pipeline[1].trimmed().toInt(&conversionOK);

    if (!conversionOK || pipelineOUT >= MAX_PIPELINE_NUMBER || pipelineOUT < 0)
    {
        m_logger << "Wrong pipeline output format for LHE_IMPLICIT(" << orderIndex << ") method. Expects: *non-negative number < " << MAX_PIPELINE_NUMBER_STRING << "*." << std::endl;
        return false;
    }

    double timeStep = tokens[1].toDouble(&conversionOK);

    if (!conversionOK || timeStep <= 0.)
    {
        m_logger << "Wrong second parameter format for LHE_IMPLICIT(" << orderIndex << ") method. Expects: *positive number* time step." << std::endl;
        return false;
    }

    int numOfSteps = tokens[2].toInt(&conversionOK);
    if (!conversionOK || numOfSteps < 0)
    {
        m_logger << "Wrong third parameter format for LHE_IMPLICIT(" << orderIndex << ") method. Expects: *non-negative number* number of steps." << std::endl;
        return false;
    }

    double tol = tokens[3].toDouble(&conversionOK);
    if (!conversionOK || tol < 0.)
    {
        m_logger << "Wrong fourth parameter format for LHE_IMPLICIT(" << orderIndex << ") method. Expects: *non-negative number* tolerance." << std::endl;
        return false;
    }

    bool bRefine = false;
    int numOfIterationsToRefineAfter = 0;

    if (refinementTokens.length() == 1)
    {
        bRefine = true;
        numOfIterationsToRefineAfter = refinementTokens[0].toInt(&conversionOK);

        if (!conversionOK || numOfIterationsToRefineAfter <= 0)
        {
            m_logger << "Wrong first parameter format in refinement options for LHE_IMPLICIT(" << orderIndex << ") method! ";
            m_logger << "Expects: *positive number* how often (iteration-wise) should refinement occur.\n";
            return false;
        }
    }
    else if (refinementTokens.length() > 1)
    {
        m_logger << "Too many arguments for continuous refinement in LHE_IMPLICIT(" << orderIndex << ") method!\n";
        return false;
    }

    SLHEImplicitParams *params = new SLHEImplicitParams;
    params->timeStep = timeStep;
    params->numOfSteps = numOfSteps;
    params->tol = tol;
    params->pipelineIN = pipelineIN;
    params->pipelineOUT = pipelineOUT;
    params->bRefine = bRefine;
    params->numOfIterationsToRefineAfter = numOfIterationsToRefineAfter;

    ParseAnimationArgs(tokens, "LHE_IMPLICIT", orderIndex, 4, params->animationFileName, params->bExportAnimation);

    m_processingAlgorithms.push_back(std::make_pair(Operation::LHE_IMPLICIT, params));

    return true;
}

bool ArgumentParser::Parse_NIBLACK_IMAGE_args(const QStringList &tokens, int orderIndex)
{
    if (tokens.length() != 4)
    {
        m_logger << "Wrong number of parameters for NIBLACK-image(" << orderIndex << ") method. Expects: *pipeline* input1(intensity) input2(intensitySquared) input3(thresholding)#output, ";
        m_logger << "*non-negative number* radius, *number* kappa coefficient, *number* mean offset.";
        m_logger << " If radius > 0, input2(intensitySquared) is ignored and value gets computed using neighbors." << std::endl;
        return false;
    }

    bool conversionOK = false;
    QStringList pipeline(tokens[0].split('#', QString::SkipEmptyParts));

    if (pipeline.size() != 2)
    {
        m_logger << "Wrong first parameter format for NIBLACK-image(" << orderIndex << ") method. Expects: *pipeline* input1(intensity) input2(intensitySquared) input3(thresholding)#output." << std::endl;
        return false;
    }

    QStringList inputs(pipeline[0].split(' ', QString::SkipEmptyParts));

    if (inputs.size() != 3)
    {
        m_logger << "Wrong pipeline input format for NIBLACK-image(" << orderIndex << ") method. Expects: *non-negative number < " << MAX_PIPELINE_NUMBER_STRING << "* (intensity) *non-negative number < ";
        m_logger << MAX_PIPELINE_NUMBER_STRING << "* (intensitySquared) ";
        m_logger << "*non-negative number < "<< MAX_PIPELINE_NUMBER_STRING << "* (thresholding)." << std::endl;
        return false;
    }

    int pipelineINProcessing = inputs[0].trimmed().toInt(&conversionOK);

    if (!conversionOK || pipelineINProcessing >= MAX_PIPELINE_NUMBER || pipelineINProcessing < 0)
    {
        m_logger << "Wrong pipeline input format for NIBLACK-image(" << orderIndex << ") method. Expects: *non-negative number < " << MAX_PIPELINE_NUMBER_STRING << "* (intensity) *non-negative number < ";
        m_logger << MAX_PIPELINE_NUMBER_STRING << "* (intensitySquared) ";
        m_logger << "*non-negative number < "<< MAX_PIPELINE_NUMBER_STRING << "* (thresholding)." << std::endl;
        return false;
    }

    int pipelineINProcessingSquared = inputs[1].trimmed().toInt(&conversionOK);

    if (!conversionOK || pipelineINProcessingSquared >= MAX_PIPELINE_NUMBER || pipelineINProcessingSquared < 0)
    {
        m_logger << "Wrong pipeline input format for NIBLACK-image(" << orderIndex << ") method. Expects: *non-negative number < " << MAX_PIPELINE_NUMBER_STRING << "* (intensity) *non-negative number < ";
        m_logger << MAX_PIPELINE_NUMBER_STRING << "* (intensitySquared) ";
        m_logger << "*non-negative number < "<< MAX_PIPELINE_NUMBER_STRING << "* (thresholding)." << std::endl;
        return false;
    }

    int pipelineINThresholding = inputs[2].trimmed().toInt(&conversionOK);

    if (!conversionOK || pipelineINThresholding >= MAX_PIPELINE_NUMBER || pipelineINThresholding < 0)
    {
        m_logger << "Wrong pipeline input format for NIBLACK-image(" << orderIndex << ") method. Expects: *non-negative number < " << MAX_PIPELINE_NUMBER_STRING << "* (intensity) *non-negative number < ";
        m_logger << MAX_PIPELINE_NUMBER_STRING << "* (intensitySquared) ";
        m_logger << "*non-negative number < "<< MAX_PIPELINE_NUMBER_STRING << "* (thresholding)." << std::endl;
        return false;
    }

    int pipelineOUT = pipeline[1].trimmed().toInt(&conversionOK);

    if (!conversionOK || pipelineOUT >= MAX_PIPELINE_NUMBER || pipelineOUT < 0)
    {
        m_logger << "Wrong pipeline output format for NIBLACK-image(" << orderIndex << ") method. Expects: *non-negative number < " << MAX_PIPELINE_NUMBER_STRING << "*." << std::endl;
        return false;
    }

    int radius = tokens[1].toInt(&conversionOK);

    if (!conversionOK || radius < 0)
    {
        m_logger << "Wrong second parameter format for NIBLACK-image(" << orderIndex << ") method. Expects: *non-negative number* radius." << std::endl;
        m_logger << " If radius > 0, input2(intensitySquared) is ignored and value gets computed using neighbors." << std::endl;
        return false;
    }

    double kappa = tokens[2].toDouble(&conversionOK);

    if (!conversionOK)
    {
        m_logger << "Wrong third parameter format for NIBLACK-image(" << orderIndex << ") method. Expects: *number* kappa coefficient." << std::endl;
        return false;
    }

    double meanOffset = tokens[3].toDouble(&conversionOK);

    if (!conversionOK)
    {
        m_logger << "Wrong fourth parameter format for NIBLACK-image(" << orderIndex << ") method. Expects: *number* mean offset." << std::endl;
        return false;
    }

    SNiblackOperationParams *params = new SNiblackOperationParams;
    params->radius = radius;
    params->kappa = kappa;
    params->meanOffset = meanOffset;
    params->pipelineINProcessing = pipelineINProcessing;
    params->pipelineINProcessingSquared = pipelineINProcessingSquared;
    params->pipelineINThresholding = pipelineINThresholding;
    params->pipelineOUT = pipelineOUT;

    m_processingAlgorithms.push_back(std::make_pair(Operation::NIBLACK, params));

    return true;
}

bool ArgumentParser::Parse_MCF_args(const QStringList &tokens, const QStringList &refinementTokens, int orderIndex)
{
    if (tokens.length() != 5 && tokens.length() != 7)
    {
        m_logger << "Wrong number of parameters for MCF(" << orderIndex << ") method. Expects: *pipeline* input#output, ";
        m_logger << "*positive number* time step, *non-negative number* number of steps, *non-negative number* tolerance, *non-negative number* epsilon. ";
        m_logger << "Accepts optional 'anim' and *animation name* parameters.\n";
        return false;
    }

    bool conversionOK = false;
    QStringList pipeline(tokens[0].split('#', QString::SkipEmptyParts));

    if (pipeline.size() != 2)
    {
        m_logger << "Wrong first parameter format for MCF(" << orderIndex << ") method. Expects: *pipeline* input#output." << std::endl;
        return false;
    }

    int pipelineIN = pipeline[0].trimmed().toInt(&conversionOK);

    if (!conversionOK || pipelineIN >= MAX_PIPELINE_NUMBER || pipelineIN < 0)
    {
        m_logger << "Wrong pipeline input format for MCF(" << orderIndex << ") method. Expects: *non-negative number < " << MAX_PIPELINE_NUMBER_STRING << "*." << std::endl;
        return false;
    }

    int pipelineOUT = pipeline[1].trimmed().toInt(&conversionOK);

    if (!conversionOK || pipelineOUT >= MAX_PIPELINE_NUMBER || pipelineOUT < 0)
    {
        m_logger << "Wrong pipeline output format for MCF(" << orderIndex << ") method. Expects: *non-negative number < " << MAX_PIPELINE_NUMBER_STRING << "*." << std::endl;
        return false;
    }

    double timeStep = tokens[1].toDouble(&conversionOK);

    if (!conversionOK || timeStep <= 0.)
    {
        m_logger << "Wrong second parameter format for MCF(" << orderIndex << ") method. Expects: *positive number* time step." << std::endl;
        return false;
    }

    int numOfSteps = tokens[2].toInt(&conversionOK);
    if (!conversionOK || numOfSteps < 0)
    {
        m_logger << "Wrong third parameter format for MCF(" << orderIndex << ") method. Expects: *non-negative number* number of steps." << std::endl;
        return false;
    }

    double tol = tokens[3].toDouble(&conversionOK);
    if (!conversionOK || tol < 0.)
    {
        m_logger << "Wrong fourth parameter format for MCF(" << orderIndex << ") method. Expects: *non-negative number* tolerance." << std::endl;
        return false;
    }

    double epsilon = tokens[4].toDouble(&conversionOK);
    if (!conversionOK || epsilon < 0.)
    {
        m_logger << "Wrong fifth parameter format for MCF(" << orderIndex << ") method. Expects: *non-negative number* epsilon." << std::endl;
        return false;
    }

    bool bRefine = false;
    int numOfIterationsToRefineAfter = 0;

    if (refinementTokens.length() == 1)
    {
        bRefine = true;
        numOfIterationsToRefineAfter = refinementTokens[0].toInt(&conversionOK);

        if (!conversionOK || numOfIterationsToRefineAfter <= 0)
        {
            m_logger << "Wrong first parameter format in refinement options for MCF(" << orderIndex << ") method! ";
            m_logger << "Expects: *positive number* how often (iteration-wise) should refinement occur.\n";
            return false;
        }
    }
    else if (refinementTokens.length() > 1)
    {
        m_logger << "Too many arguments for continuous refinement in MCF(" << orderIndex << ") method!\n";
        return false;
    }

    SMCFParams *params = new SMCFParams;
    params->timeStep = timeStep;
    params->numOfSteps = numOfSteps;
    params->tol = tol;
    params->epsilon = epsilon;
    params->pipelineIN = pipelineIN;
    params->pipelineOUT = pipelineOUT;
    params->bRefine = bRefine;
    params->numOfIterationsToRefineAfter = numOfIterationsToRefineAfter;

    ParseAnimationArgs(tokens, "MCF", orderIndex, 5, params->animationFileName, params->bExportAnimation);

    m_processingAlgorithms.push_back(std::make_pair(Operation::MCF, params));

    return true;
}

bool ArgumentParser::Parse_SUBSURF_args(const QStringList &tokens, const QStringList &refinementTokens, int orderIndex)
{
    if (tokens.length() != 6 && tokens.length() != 8)
    {
        m_logger << "Wrong number of parameters for SUBSURF(" << orderIndex << ") method. Expects: *pipeline* input1(I0) input2(U0)#output, ";
        m_logger << "*positive number* time step, *non-negative number* number of steps, *non-negative number* tolerance, *non-negative number* epsilon, *non-negative number* K. ";
        m_logger << "Accepts optional 'anim' and *animation name* parameters.\n";
        return false;
    }

    bool conversionOK = false;
    QStringList pipeline(tokens[0].split('#', QString::SkipEmptyParts));

    if (pipeline.size() != 2)
    {
        m_logger << "Wrong first parameter format for SUBSURF(" << orderIndex << ") method. Expects: *pipeline* *pipeline* input1(I0) input2(U0)#output." << std::endl;
        return false;
    }

    QStringList input(pipeline[0].split(' ', QString::SkipEmptyParts));

    if (input.length() != 2)
    {
        m_logger << "Wrong pipeline input format for SUBSURF(" << orderIndex << ") method. Expects: *non-negative number < " << MAX_PIPELINE_NUMBER_STRING << "* input1(I0)";
        m_logger << "*non-negative number < " << MAX_PIPELINE_NUMBER_STRING << "* input2(U0)."<< std::endl;
        return false;
    }

    int pipelineIN_I0 = input[0].trimmed().toInt(&conversionOK);

    if (!conversionOK || pipelineIN_I0 >= MAX_PIPELINE_NUMBER || pipelineIN_I0 < 0)
    {
        m_logger << "Wrong pipeline input1(I0) format for SUBSURF(" << orderIndex << ") method. Expects: *non-negative number < " << MAX_PIPELINE_NUMBER_STRING << "*." << std::endl;
        return false;
    }

    int pipelineIN_U0 = input[1].trimmed().toInt(&conversionOK);

    if (!conversionOK || pipelineIN_U0 >= MAX_PIPELINE_NUMBER || pipelineIN_U0 < 0)
    {
        m_logger << "Wrong pipeline input2(U0) format for SUBSURF(" << orderIndex << ") method. Expects: *non-negative number < " << MAX_PIPELINE_NUMBER_STRING << "*." << std::endl;
       return false;
    }

    int pipelineOUT = pipeline[1].trimmed().toInt(&conversionOK);

    if (!conversionOK || pipelineOUT >= MAX_PIPELINE_NUMBER || pipelineOUT < 0)
    {
        m_logger << "Wrong pipeline output format for SUBSURF(" << orderIndex << ") method. Expects: *non-negative number < " << MAX_PIPELINE_NUMBER_STRING << "*." << std::endl;
        return false;
    }

    double timeStep = tokens[1].toDouble(&conversionOK);

    if (!conversionOK || timeStep <= 0.)
    {
        m_logger << "Wrong second parameter format for SUBSURF(" << orderIndex << ") method. Expects: *positive number* time step." << std::endl;
        return false;
    }

    int numOfSteps = tokens[2].toInt(&conversionOK);
    if (!conversionOK || numOfSteps < 0)
    {
        m_logger << "Wrong third parameter format for SUBSURF(" << orderIndex << ") method. Expects: *non-negative number* number of steps." << std::endl;
        return false;
    }

    double tol = tokens[3].toDouble(&conversionOK);
    if (!conversionOK || tol < 0.)
    {
        m_logger << "Wrong fourth parameter format for SUBSURF(" << orderIndex << ") method. Expects: *non-negative number* tolerance." << std::endl;
        return false;
    }

    double epsilon = tokens[4].toDouble(&conversionOK);
    if (!conversionOK || epsilon < 0.)
    {
        m_logger << "Wrong fifth parameter format for SUBSURF(" << orderIndex << ") method. Expects: *non-negative number* epsilon." << std::endl;
        return false;
    }

    double K = tokens[5].toDouble(&conversionOK);
    if (!conversionOK || K < 0.)
    {
        m_logger << "Wrong sixth parameter format for SUBSURF(" << orderIndex << ") method. Expects: *non-negative number* K." << std::endl;
        return false;
    }

    bool bRefine = false;
    int numOfIterationsToRefineAfter = 0;

    if (refinementTokens.length() == 1)
    {
        bRefine = true;
        numOfIterationsToRefineAfter = refinementTokens[0].toInt(&conversionOK);

        if (!conversionOK || numOfIterationsToRefineAfter <= 0)
        {
            m_logger << "Wrong first parameter format in refinement options for SUBSURF(" << orderIndex << ") method! ";
            m_logger << "Expects: *positive number* how often (iteration-wise) should refinement occur.\n";
            return false;
        }
    }
    else if (refinementTokens.length() > 1)
    {
        m_logger << "Too many arguments for continuous refinement in SUBSURF(" << orderIndex << ") method!\n";
        return false;
    }

    SSubsurfParams *params = new SSubsurfParams;
    params->timeStep = timeStep;
    params->numOfSteps = numOfSteps;
    params->tol = tol;
    params->epsilon = epsilon;
    params->K = K;
    params->pipelineIN_I0 = pipelineIN_I0;
    params->pipelineIN_U0 = pipelineIN_U0;
    params->pipelineOUT = pipelineOUT;
    params->bRefine = bRefine;
    params->numOfIterationsToRefineAfter = numOfIterationsToRefineAfter;

    ParseAnimationArgs(tokens, "SUBSURF", orderIndex, 6, params->animationFileName, params->bExportAnimation);

    m_processingAlgorithms.push_back(std::make_pair(Operation::SUBSURF, params));

    return true;
}

bool ArgumentParser::Parse_SQUARE_args(const QStringList &tokens, int orderIndex)
{
    if (tokens.length() != 1)
    {
        m_logger << "Wrong number of parameters for SQUARE(" << orderIndex << ") method. Expects: *pipeline* input/output.\n";
        return false;
    }

    bool conversionOK = false;
    QStringList pipeline(tokens[0].split('#', QString::SkipEmptyParts));

    if (pipeline.size() != 2)
    {
        m_logger << "Wrong first parameter format for SQUARE(" << orderIndex << ") method. Expects: *pipeline* input/output." << std::endl;
        return false;
    }

    int pipelineIN = pipeline[0].trimmed().toInt(&conversionOK);

    if (!conversionOK || pipelineIN >= MAX_PIPELINE_NUMBER || pipelineIN < 0)
    {
        m_logger << "Wrong pipeline input format for SQUARE(" << orderIndex << ") method. Expects: *non-negative number < " << MAX_PIPELINE_NUMBER_STRING << "*." << std::endl;
        return false;
    }

    int pipelineOUT = pipeline[1].trimmed().toInt(&conversionOK);

    if (!conversionOK || pipelineOUT >= MAX_PIPELINE_NUMBER || pipelineOUT < 0)
    {
        m_logger << "Wrong pipeline output format for SQUARE(" << orderIndex << ") method. Expects: *non-negative number < " << MAX_PIPELINE_NUMBER_STRING << "*." << std::endl;
        return false;
    }

    SSquareParams *params = new SSquareParams;
    params->pipelineIN = pipelineIN;
    params->pipelineOUT = pipelineOUT;

    m_processingAlgorithms.push_back(std::make_pair(Operation::SQUARE, params));

    return true;
}

bool ArgumentParser::Parse_MAX_args(const QStringList &tokens, int orderIndex)
{
    if (tokens.length() != 1)
    {
        m_logger << "Wrong number of parameters for MAX(" << orderIndex << ") method. Expects: *pipeline* input1 input2/output.\n";
        return false;
    }

    bool conversionOK = false;
    QStringList pipeline(tokens[0].split('#', QString::SkipEmptyParts));

    if (pipeline.size() != 2)
    {
        m_logger << "Wrong first parameter format for MAX(" << orderIndex << ") method. Expects: *pipeline* input1 input2/output." << std::endl;
        return false;
    }

    QStringList input(pipeline[0].split(' ', QString::SkipEmptyParts));

    if (input.length() != 2)
    {
        m_logger << "Wrong pipeline input format for MAX(" << orderIndex << ") method. Expects: *non-negative number < " << MAX_PIPELINE_NUMBER_STRING << "* input1";
        m_logger << "*non-negative number < " << MAX_PIPELINE_NUMBER_STRING << "* input2."<< std::endl;
        return false;
    }

    int pipelineIN1 = input[0].trimmed().toInt(&conversionOK);

    if (!conversionOK || pipelineIN1 >= MAX_PIPELINE_NUMBER || pipelineIN1 < 0)
    {
        m_logger << "Wrong pipeline input1 format for MAX(" << orderIndex << ") method. Expects: *non-negative number < " << MAX_PIPELINE_NUMBER_STRING << "*." << std::endl;
        return false;
    }

    int pipelineIN2 = input[1].trimmed().toInt(&conversionOK);

    if (!conversionOK || pipelineIN2 >= MAX_PIPELINE_NUMBER || pipelineIN2 < 0)
    {
        m_logger << "Wrong pipeline input2 format for MAX(" << orderIndex << ") method. Expects: *non-negative number < " << MAX_PIPELINE_NUMBER_STRING << "*." << std::endl;
        return false;
    }

    int pipelineOUT = pipeline[1].trimmed().toInt(&conversionOK);

    if (!conversionOK || pipelineOUT >= MAX_PIPELINE_NUMBER || pipelineOUT < 0)
    {
        m_logger << "Wrong pipeline output format for MAX(" << orderIndex << ") method. Expects: *non-negative number < " << MAX_PIPELINE_NUMBER_STRING << "*." << std::endl;
        return false;
    }

    SMaxParams *params = new SMaxParams;
    params->pipelineIN1 = pipelineIN1;
    params->pipelineIN2 = pipelineIN2;
    params->pipelineOUT = pipelineOUT;

    m_processingAlgorithms.push_back(std::make_pair(Operation::MAX, params));

    return true;
}

bool ArgumentParser::Parse_EXPAND_REFINEMENT_args(const QStringList &tokens, int orderIndex)
{
    if (tokens.length() != 0)
    {
        m_logger << "Wrong number of parameters for EXPAND_REFINEMENT(" << orderIndex << ") method. Expects: nothing.\n";
        return false;
    }

    m_processingAlgorithms.push_back(std::make_pair(Operation::EXPAND_REFINEMENT, nullptr));

    return true;
}

bool ArgumentParser::Parse_REFINE_BY_THRESHOLD_args(const QStringList &tokens, int orderIndex)
{
    if (tokens.length() != 2)
    {
        m_logger << "Wrong number of parameters for REFINE_BY_THRESHOLD(" << orderIndex << ") method.";
        m_logger << " Expects: *pipeline* input, *number* threshold.\n";
        return false;
    }

    bool conversionOK = false;
    int pipeline = tokens[0].trimmed().toInt(&conversionOK);

    if (!conversionOK || pipeline >= MAX_PIPELINE_NUMBER || pipeline < 0)
    {
        m_logger << "Wrong pipeline parameter format for REFINE_BY_THRESHOLD(" << orderIndex << ") method. Expects: *non-negative number < " << MAX_PIPELINE_NUMBER_STRING << "*." << std::endl;
        return false;
    }

    double threshold = tokens[1].trimmed().toDouble(&conversionOK);

    if (!conversionOK)
    {
        m_logger << "Wrong second parameter format for REFINE_BY_THRESHOLD(" << orderIndex << ") method. Expects: *number*." << std::endl;
        return false;
    }

    SRefineByThresholdParams *params = new SRefineByThresholdParams;
    params->pipeline = pipeline;
    params->threshold = threshold;

    m_processingAlgorithms.push_back(std::make_pair(Operation::REFINE_BY_THRESHOLD, params));

    return true;
}

bool ArgumentParser::Parse_FLAT_THRESHOLD_args(const QStringList &tokens, int orderIndex)
{
    if (tokens.length() != 2)
    {
        m_logger << "Wrong number of parameters for FLAT_THRESHOLD(" << orderIndex << ") method.";
        m_logger << " Expects: *pipeline* input/output, *number* threshold.\n";        
        return false;
    }

    bool conversionOK = false;
    QStringList pipeline(tokens[0].split('#', QString::SkipEmptyParts));

    if (pipeline.size() != 2)
    {
        m_logger << "Wrong first parameter format for FLAT_THRESHOLD(" << orderIndex << ") method. Expects: *pipeline* input/output." << std::endl;
        return false;
    }

    int pipelineIN = pipeline[0].trimmed().toInt(&conversionOK);

    if (!conversionOK || pipelineIN >= MAX_PIPELINE_NUMBER || pipelineIN < 0)
    {
        m_logger << "Wrong pipeline input format for FLAT_THRESHOLD(" << orderIndex << ") method. Expects: *non-negative number < " << MAX_PIPELINE_NUMBER_STRING << "*." << std::endl;
        return false;
    }

    int pipelineOUT = pipeline[1].trimmed().toInt(&conversionOK);

    if (!conversionOK || pipelineOUT >= MAX_PIPELINE_NUMBER || pipelineOUT < 0)
    {
        m_logger << "Wrong pipeline output format for FLAT_THRESHOLD(" << orderIndex << ") method. Expects: *non-negative number < " << MAX_PIPELINE_NUMBER_STRING << "*." << std::endl;
        return false;
    }

    double threshold = tokens[1].trimmed().toDouble(&conversionOK);

    if (!conversionOK)
    {
        m_logger << "Wrong second parameter format for FLAT_THRESHOLD(" << orderIndex << ") method. Expects: *number*." << std::endl;
        return false;
    }

    SFlatThresholdParams *params = new SFlatThresholdParams;
    params->pipelineIN = pipelineIN;
    params->pipelineOUT = pipelineOUT;
    params->threshold = threshold;

    m_processingAlgorithms.push_back(std::make_pair(Operation::FLAT_THRESHOLD, params));

    return true;
}

bool ArgumentParser::ParseAnimationArgs(const QStringList &tokens, const char *method,
                                        int orderIndex, int atIndex,
                                        std::string &animationName, bool &exportAnimation)
{
    if (tokens.length() <= atIndex)
    {
        exportAnimation = false;
        animationName = "";
        return true;
    }

    if (tokens[atIndex] == "anim")
    {
        if (tokens.length() == atIndex + 1)
        {
            m_logger << "Missing animation name for " << method << "(" << orderIndex << ") method.\n";
            return false;
        }

        if (tokens.length() > atIndex + 2)
        {
            m_logger << "More arguments than expected for " << method << "(" << orderIndex << ") method.\n";
            return false;
        }

        m_animationNames.push_back(tokens[atIndex + 1].toStdString());
        animationName = tokens[atIndex + 1].toStdString();
        exportAnimation = true;
    }
    else
    {
        m_logger << "Wrong animation parameter format for " << method << "(" << orderIndex << ") method.\n";
        return false;
    }

    return true;
}

bool ArgumentParser::HasDuplicates(std::vector<std::string> &names) const
{
    if (names.size() == 0) return false;

    std::sort(names.begin(), names.end());

    for (size_t i = 0; i < names.size() - 1; ++i)
    {
        if (names[i] == names[i + 1]) return true;
    }

    return false;
}

bool ArgumentParser::IsPowerOfTwo(int number) const
{
    if (number == 0) return true;

    double res = log2(number);
    return ceil(res) - floor(res) == 0.;
}

bool ArgumentParser::ExtractFlagParams(int i, int argc, const QStringList &argv)
{
    if (i + 1 >= argc)
    {
        m_logger << "Missing parameter/s for flag " << argv[i][1].toLatin1() << std::endl;
        return false;
    }
    else if (i + 2 < argc && argv[i + 2][0] != '-')
    {
        m_logger << "Multiple parameters for single flag must be inside \" \". Fix required for flag " << argv[i][1].toLatin1() << std::endl;
        return false;
    }

    m_flagParams[argv[i][1]] = argv[i + 1];
    return true;
}

QStringList ArgumentParser::SplitToArgv(const QString &str) const
{
    QStringList list;

    int lastStart = -1;
    bool bMultiWord = false;
    int wordLengthCounter = 0;

    for (int i = 0; i < str.size(); ++i)
    {
        QString buff("");
        QChar curChar = str[i];

        if (curChar.isSpace())
        {
            if (lastStart != -1 && !bMultiWord)
            {
                buff = str.mid(lastStart, wordLengthCounter);
                wordLengthCounter = 0;
                lastStart = -1;
            }
            else if (bMultiWord)
            {
                ++wordLengthCounter;
                continue;
            }
            else continue;
        }
        else if (curChar == '"')
        {
            if (bMultiWord)
            {
                bMultiWord = false;
            }
            else bMultiWord = true;

            if (bMultiWord)
            {
                continue;
            }
            else if (lastStart != -1)
            {
                buff = str.mid(lastStart, wordLengthCounter);
                wordLengthCounter = 0;
                lastStart = -1;
            }
            else lastStart = i;
        }
        else
        {
            ++wordLengthCounter;

            if (lastStart == -1)
            {
                lastStart = i;
            }
        }

        if (buff != "")
            list.push_back(buff);
    }

    return list;
}

void ArgumentParser::DeallocateParams()
{
    for (size_t i = 0; i < m_refinementAlgorithms.size(); ++i)
    {
        switch(m_refinementAlgorithms[i].first)
        {
        case Refinement::GLOBAL_OTSU: delete reinterpret_cast<SGlobalOTSUThresholdParams*>(m_refinementAlgorithms[i].second); break;
        case Refinement::MAX_MIN_DIFF: delete reinterpret_cast<SMaxMinDiffThresholdParams*>(m_refinementAlgorithms[i].second); break;
        case Refinement::BERNSEN: delete reinterpret_cast<SBernsenThresholdParams*>(m_refinementAlgorithms[i].second); break;
        case Refinement::SAUVOLA: delete reinterpret_cast<SSauvolaThresholdParams*>(m_refinementAlgorithms[i].second); break;
        case Refinement::NIBLACK: delete reinterpret_cast<SNiblackThresholdParams*>(m_refinementAlgorithms[i].second); break;
        case Refinement::TOTAL: break;
        case Refinement::UP_TO: delete reinterpret_cast<SUpToRefinementParams*>(m_refinementAlgorithms[i].second); break;
        default: continue;
        }
    }

    for (size_t i = 0; i < m_processingAlgorithms.size(); ++i)
    {
        switch(m_processingAlgorithms[i].first)
        {
        case Operation::LHE_EXPLICIT: delete reinterpret_cast<SLHEExplicitParams*>(m_processingAlgorithms[i].second); break;
        case Operation::LHE_IMPLICIT: delete reinterpret_cast<SLHEImplicitParams*>(m_processingAlgorithms[i].second); break;
        case Operation::NIBLACK: delete reinterpret_cast<SNiblackOperationParams*>(m_processingAlgorithms[i].second); break;
        case Operation::MCF: delete reinterpret_cast<SMCFParams*>(m_processingAlgorithms[i].second); break;
        case Operation::SUBSURF: delete reinterpret_cast<SSubsurfParams*>(m_processingAlgorithms[i].second); break;
        case Operation::SQUARE: delete reinterpret_cast<SSquareParams*>(m_processingAlgorithms[i].second); break;
        case Operation::MAX: delete reinterpret_cast<SMaxParams*>(m_processingAlgorithms[i].second); break;
        case Operation::EXPAND_REFINEMENT: break;
        case Operation::REFINE_BY_THRESHOLD: delete reinterpret_cast<SRefineByThresholdParams*>(m_processingAlgorithms[i].second); break;
        case Operation::FLAT_THRESHOLD: delete reinterpret_cast<SFlatThresholdParams*>(m_processingAlgorithms[i].second); break;
        default: continue;
        }
    }
}
































