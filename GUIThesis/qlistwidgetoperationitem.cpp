#include "qlistwidgetoperationitem.h"

QListWidgetOperationItem::QListWidgetOperationItem(int operationMethodIndex, void *data) : QListWidgetItem(), m_operationMethodIndex(operationMethodIndex)
{
    switch(operationMethodIndex)
    {
    case Operation::LHE_EXPLICIT: m_pOperation = std::make_unique<LHE_EXPLICIT>(data); break;
    case Operation::LHE_IMPLICIT: m_pOperation = std::make_unique<LHE_IMPLICIT>(data); break;
    case Operation::NIBLACK: m_pOperation = std::make_unique<NIBLACK_OP>(data); break;
    case Operation::MCF: m_pOperation = std::make_unique<MCF>(data); break;
    case Operation::SUBSURF: m_pOperation = std::make_unique<SUBSURF>(data); break;
    case Operation::SQUARE: m_pOperation = std::make_unique<SQUARE>(data); break;
    case Operation::MAX: m_pOperation = std::make_unique<MAX>(data); break;
    case Operation::EXPAND_REFINEMENT: m_pOperation = std::make_unique<EXPAND_REFINEMENT>(data); break;
    case Operation::REFINE_BY_THRESHOLD: m_pOperation = std::make_unique<REFINE_BY_THRESHOLD>(data); break;
    case Operation::FLAT_THRESHOLD: m_pOperation = std::make_unique<FLAT_THRESHOLD>(data); break;
    default: m_pOperation = nullptr;
    }
}
