#include "mainwindow.h"
#include "ui_mainwindow.h"

#include "consoleoutputdialog.h"
#include "pstream.h"

#include <sstream>
#include <cmath>

#include <qlayout.h>
#include <qdebug.h>
#include <qmessagebox.h>
#include <qfiledialog.h>

#include "qlistwidgetrefinementitem.h"
#include "qlistwidgetoperationitem.h"
#include "qcustomcombobox.h"
#include "qcustomspinbox.h"
#include "qcustomdoublespinbox.h"
#include "textclipboarddialog.h"
#include "qpipelineexportwidget.h"
#include "qlineeditdialog.h"

constexpr int OPERATION_PARAMS_SPACING_FACTOR = 2;
constexpr int OPERATION_PIPELINE_SPACING_FACTOR = 3;
constexpr int REFINEMENT_PARAMS_SPACING_FACTOR = 2;

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);    
    m_pConsoleOutputThread = nullptr;
    m_bCanRunAnotherInstance = true;
    m_pDialog = nullptr;

    InitializeRefinementCombobox();
    InitializeOperationCombobox();

    connect(ui->listWidgetOperationStack,
            SIGNAL(currentItemChanged(QListWidgetItem*, QListWidgetItem*)),
            this,
            SLOT(onListWidgetOperationStack_currentItemChanged(QListWidgetItem*, QListWidgetItem*)), Qt::QueuedConnection);
    connect(ui->listWidgetRefinementStack,
            SIGNAL(currentItemChanged(QListWidgetItem*, QListWidgetItem*)),
            this,
            SLOT(onListWidgetRefinementStack_currentItemChanged(QListWidgetItem*, QListWidgetItem*)), Qt::QueuedConnection);

    ui->groupBoxOperationDetails->setLayout(new QHBoxLayout(ui->groupBoxOperationDetails));
    ui->groupBoxRefinementDetails->setLayout(new QHBoxLayout(ui->groupBoxRefinementDetails));

    QVBoxLayout *layout = new QVBoxLayout(ui->scrollAreaVTKExport);
    layout->setAlignment(Qt::AlignTop);
    ui->scrollAreaVTKExport->widget()->setLayout(layout);

    layout = new QVBoxLayout(ui->scrollAreaImageExport);
    layout->setAlignment(Qt::AlignTop);
    ui->scrollAreaImageExport->widget()->setLayout(layout);

    ProcessConfigurationString(m_settings.value("app_config").toString());
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::GenerateCommandString()
{
    QString commandBuff = QString("mpiexec -n %1 %2 -C %3 %4 %5 %6 %7 %8 %9");

    commandBuff = commandBuff.arg(ui->spinBoxMPIProcCount->value());    //%1
    commandBuff = commandBuff.arg(m_settings.value("p4est_app_path", "").toString()); //%2
    commandBuff = commandBuff.arg(ui->checkBoxBatchProcessing->isChecked() ?
                                      QString("-f \"%1\"").arg(ui->lineEditFolderInputPath->text()) : QString("-i \"%1\"").arg(ui->lineEditFilePath->text())); //%3
    commandBuff = commandBuff.arg(QString("-o \"%1\"").arg(ui->lineEditFolderOutputPath->text())); //%4
    commandBuff = commandBuff.arg(QString("-c \"%1\"").arg(ui->lineEditConnectivityPath->text())); //%5
    commandBuff = commandBuff.arg(GenerateRefinementString()); //%6
    commandBuff = commandBuff.arg(GenerateOperationString()); //%7
    commandBuff = commandBuff.arg(GenerateExportString()); //%8
    commandBuff = commandBuff.arg(GenerateGeneralOptionsString()); //%9

    m_commandString = commandBuff;
}

QString MainWindow::GenerateRefinementString()
{
    std::stringstream ss;

    if (ui->listWidgetRefinementStack->count() > 0)
    {
        ss << "-r ";
        ss << "\"";
    }

    for (int i = 0; i < ui->listWidgetRefinementStack->count(); ++i)
    {
        QListWidgetRefinementItem *item = reinterpret_cast<QListWidgetRefinementItem*>(ui->listWidgetRefinementStack->item(i));
        ss << item->GetRefinement()->GenerateCommandString() << (i == ui->listWidgetRefinementStack->count() - 1 ? "" : ", ");
    }

    if (ui->listWidgetRefinementStack->count() > 0) ss << "\"";

    return ss.str().c_str();
}

QString MainWindow::GenerateOperationString()
{
    std::stringstream ss;

    if (ui->listWidgetOperationStack->count() > 0)
    {
        ss << "-m ";
        ss << "\"";
    }

    for (int i = 0; i < ui->listWidgetOperationStack->count(); ++i)
    {
        QListWidgetOperationItem *item = reinterpret_cast<QListWidgetOperationItem*>(ui->listWidgetOperationStack->item(i));
        ss << item->GetOperation()->GenerateCommandString() << (i == ui->listWidgetOperationStack->count() - 1 ? "" : ", ");
    }

    if (ui->listWidgetOperationStack->count() > 0) ss << "\"";

    return ss.str().c_str();
}

QString MainWindow::GenerateExportString()
{
    std::stringstream ss;
    std::string exportOptions;

    if (ui->checkBoxExportVTK->isChecked() && ui->checkBoxExportImage->isChecked()) exportOptions = "-O \"vtk image\"";
    else if (ui->checkBoxExportVTK->isChecked()) exportOptions = "-O \"vtk\"";
    else if (ui->checkBoxExportImage->isChecked()) exportOptions = "-O \"image\"";
    else exportOptions = "";

    ss << exportOptions << " " << "-X \"";

    QObjectList children(ui->scrollAreaVTKExport->widget()->children());

    for (int i = 0; i < children.size(); ++i)
    {
        QPipelineExportWidget *child = dynamic_cast<QPipelineExportWidget*>(children[i]);

        if (child)
            ss << child->GetPipelineIndex() << " " << child->GetExportName().toStdString() << " ";
    }

    if (children.size() == 1) ss << "1 result";

    ss << ", ";

    children = ui->scrollAreaImageExport->widget()->children();

    for (int i = 0; i < children.size(); ++i)
    {
        QPipelineExportWidget *child = dynamic_cast<QPipelineExportWidget*>(children[i]);

        if (child)
            ss << child->GetPipelineIndex() << " " << child->GetExportName().toStdString() << " ";
    }

    if (children.size() == 1) ss << "1 result";

    ss << "\"";

    return ss.str().c_str();
}

QString MainWindow::GenerateGeneralOptionsString()
{
    std::stringstream ss;

    if (ui->checkBoxExportOriginalImage->isChecked()) ss << "-I ";
    if (ui->checkBoxCreateFolders->isChecked()) ss << "-D ";
    if (ui->checkBoxSetBackgroundValue->isChecked()) ss << "-B \"" << ui->spinBoxBackgroundOTSU->value() << " ";
    if (ui->checkBoxBackgroundOptional->isChecked()) ss << ui->spinBoxBackgroundOptional->value();

    ss << "\" " ;

    if (ui->checkBoxAutomaticTreeSideLength->isChecked() && !ui->checkBoxExtraPixelsValue->isChecked())
    {
        ss << "-T \"0 " << ui->spinBoxExtraPixelsValue->value() << "\"";
    }
    else
    {
        if (!ui->checkBoxAutomaticTreeSideLength->isChecked()) ss << "-T \"" << ui->spinBoxTreeSideLength->value() << " ";
        if (!ui->checkBoxExtraPixelsValue->isChecked()) ss << ui->spinBoxExtraPixelsValue->value() << "\"";
    }

    return ss.str().c_str();
}

void MainWindow::InitializeRefinementCombobox()
{
    ui->comboBoxRefinement->clear();

    for (size_t i = 0; i < Refinement::NUMBER_OF_REFINEMENTS; ++i)
    {
        ui->comboBoxRefinement->addItem(Refinement::RefinementNames[i]);
    }
}

void MainWindow::InitializeOperationCombobox()
{
    ui->comboBoxOperation->clear();

    for (size_t i = 0; i < Operation::NUMBER_OF_OPERATIONS; ++i)
    {
        ui->comboBoxOperation->addItem(Operation::OperationNames[i]);
    }
}

void MainWindow::InitializePipelineCombobox(QComboBox *comboBox, int maxPipelineCount) const
{
    comboBox->clear();

    for (int i = 0; i < maxPipelineCount; ++i)
    {
        comboBox->addItem(QString("%1").arg(i));
    }
}

void MainWindow::UpdateRefinementDetailsPanel(const Refinement *refinement)
{
    for(int i = ui->groupBoxRefinementDetails->layout()->count() - 1; i >= 0 ; --i)
    {
        QLayoutItem *layoutItem = ui->groupBoxRefinementDetails->layout()->takeAt(i);

        ui->groupBoxRefinementDetails->layout()->removeItem(layoutItem);
        delete layoutItem->widget();
        delete layoutItem;
    }

    if (refinement == nullptr)   return;

    QWidget *filler = new QWidget();
    QVBoxLayout *verLayout = new QVBoxLayout(filler);

    QGroupBox *params = new QGroupBox("Parameters", filler);

    params->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Expanding);

    verLayout->addWidget(params);

    QGridLayout *gridLayout = new QGridLayout();
    params->setLayout(gridLayout);

    for (size_t i = 0; i < refinement->NumberOfIntParameters(); ++i)
    {
        QHBoxLayout *subHorLayout = new QHBoxLayout();
        QCustomSpinBox *spinBox = new QCustomSpinBox(i, filler);
        spinBox->setSizePolicy(QSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed));

        spinBox->setMaximum(refinement->GetMaximalIntParamValue(i));
        spinBox->setMinimum(refinement->GetMinimalIntParamValue(i));
        spinBox->setValue(refinement->GetIntParamValue(i));
        spinBox->setSingleStep(refinement->GetStepIntParamValue(i));
        spinBox->setAlignment(Qt::AlignCenter);

        connect(spinBox, SIGNAL(valueChanged(int)), refinement, SLOT(OnIntParamValueChanged(int)));

        subHorLayout->addWidget(new QLabel(refinement->GetIntParametersNames()[i] + QString(":"), filler));
        subHorLayout->addWidget(spinBox);
        gridLayout->addLayout(subHorLayout,
                              i / REFINEMENT_PARAMS_SPACING_FACTOR,
                              i % REFINEMENT_PARAMS_SPACING_FACTOR,
                              GetRefinementParamsGridAlignment(i % REFINEMENT_PARAMS_SPACING_FACTOR));
    }

    int gridOffset = refinement->NumberOfIntParameters();

    for (size_t i = 0; i < refinement->NumberOfDoubleParameters(); ++i)
    {
        QHBoxLayout *subHorLayout = new QHBoxLayout();
        QCustomDoubleSpinBox *spinBox = new QCustomDoubleSpinBox(i, filler);
        spinBox->setSizePolicy(QSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed));

        spinBox->setMaximum(refinement->GetMaximalDoubleParamValue(i));
        spinBox->setMinimum(refinement->GetMinimalDoubleParamValue(i));
        spinBox->setValue(refinement->GetDoubleParamValue(i));
        spinBox->setSingleStep(refinement->GetStepDoubleParamValue(i));
        spinBox->setDecimals(refinement->GetDecimalCountDoubleParamValue(i));
        spinBox->setAlignment(Qt::AlignCenter);

        connect(spinBox, SIGNAL(valueChanged(double)), refinement, SLOT(OnDoubleParamValueChanged(double)));

        subHorLayout->addWidget(new QLabel(refinement->GetDoubleParametersNames()[i] + QString(":"), filler));
        subHorLayout->addWidget(spinBox);
        gridLayout->addLayout(subHorLayout,
                              (i + gridOffset) / REFINEMENT_PARAMS_SPACING_FACTOR,
                              (i + gridOffset) % REFINEMENT_PARAMS_SPACING_FACTOR,
                              GetRefinementParamsGridAlignment((i + gridOffset) % REFINEMENT_PARAMS_SPACING_FACTOR));
    }

    ui->groupBoxRefinementDetails->layout()->addWidget(filler);
}

void MainWindow::UpdateOperationDetailsPanel(const Operation *operation)
{
    for(int i = ui->groupBoxOperationDetails->layout()->count() - 1; i >= 0 ; --i)
    {
        QLayoutItem *layoutItem = ui->groupBoxOperationDetails->layout()->takeAt(i);

        ui->groupBoxOperationDetails->layout()->removeItem(layoutItem);
        delete layoutItem->widget();
        delete layoutItem;
    }

    if (operation == nullptr)   return;

    QWidget *filler = new QWidget();
    QVBoxLayout *verLayout = new QVBoxLayout(filler);

    QGroupBox *pipelines = new QGroupBox("Pipelines", filler);
    QGroupBox *params = new QGroupBox("Parameters", filler);
    QGroupBox *animation = nullptr;

    params->setSizePolicy(QSizePolicy(QSizePolicy::Preferred, QSizePolicy::Expanding));

    verLayout->addWidget(pipelines);
    verLayout->addWidget(params);

    if (operation->IsIterative())
    {
        animation = new QGroupBox("Animation", filler);
        verLayout->addWidget(animation);
    }

    QGridLayout *gridLayout = new QGridLayout();
    pipelines->setLayout(gridLayout);

    for (size_t i = 0; i < operation->NumberOfPipelineInputs(); ++i)
    {
        QHBoxLayout *subHorLayout = new QHBoxLayout();
        QCustomComboBox *comboBox = new QCustomComboBox(i, filler);

        comboBox->setSizePolicy(QSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed));
        InitializePipelineCombobox(comboBox, MAX_PIPELINE_NUMBER);
        comboBox->setCurrentIndex(operation->GetPipelineINValue(i));

        connect(comboBox, SIGNAL(currentIndexChanged(int)), operation, SLOT(OnPipelineINValueChanged(int)));

        subHorLayout->addWidget(new QLabel(operation->GetPipelineInputsNames()[i] + QString(":"), filler));
        subHorLayout->addWidget(comboBox);
        gridLayout->addLayout(subHorLayout,
                              i / OPERATION_PIPELINE_SPACING_FACTOR,
                              i % OPERATION_PIPELINE_SPACING_FACTOR,
                              GetOperationPipelineGridAlignment(i % OPERATION_PIPELINE_SPACING_FACTOR));
    }

    int gridOffset = operation->NumberOfPipelineInputs();

    for (size_t i = 0; i < operation->NumberOfPipelineOutputs(); ++i)
    {
        QHBoxLayout *subHorLayout = new QHBoxLayout();
        QCustomComboBox *comboBox = new QCustomComboBox(i, filler);

        comboBox->setSizePolicy(QSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed));
        InitializePipelineCombobox(comboBox, MAX_PIPELINE_NUMBER);
        comboBox->setCurrentIndex(operation->GetPipelineOUTValue(i));

        connect(comboBox, SIGNAL(currentIndexChanged(int)), operation, SLOT(OnPipelineOUTValueChanged(int)));

        subHorLayout->addWidget(new QLabel(operation->GetPipelineOutputsNames()[i] + QString(":"), filler));
        subHorLayout->addWidget(comboBox);
        gridLayout->addLayout(subHorLayout,
                              (i + gridOffset) / OPERATION_PIPELINE_SPACING_FACTOR,
                              (i + gridOffset) % OPERATION_PIPELINE_SPACING_FACTOR,
                              GetOperationPipelineGridAlignment((i + gridOffset) % OPERATION_PIPELINE_SPACING_FACTOR));
    }

    gridLayout = new QGridLayout();
    params->setLayout(gridLayout);

    for (size_t i = 0; i < operation->NumberOfIntParameters(); ++i)
    {
        QHBoxLayout *subHorLayout = new QHBoxLayout();
        QCustomSpinBox *spinBox = new QCustomSpinBox(i, filler);
        spinBox->setSizePolicy(QSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed));

        spinBox->setMaximum(operation->GetMaximalIntParamValue(i));
        spinBox->setMinimum(operation->GetMinimalIntParamValue(i));
        spinBox->setValue(operation->GetIntParamValue(i));
        spinBox->setSingleStep(operation->GetStepIntParamValue(i));
        spinBox->setAlignment(Qt::AlignCenter);

        connect(spinBox, SIGNAL(valueChanged(int)), operation, SLOT(OnIntParamValueChanged(int)));

        subHorLayout->addWidget(new QLabel(operation->GetIntParametersNames()[i] + QString(":"), filler));
        subHorLayout->addWidget(spinBox);
        gridLayout->addLayout(subHorLayout,
                              i / OPERATION_PARAMS_SPACING_FACTOR,
                              i % OPERATION_PARAMS_SPACING_FACTOR,
                              GetOperationParamsGridAlignment(i % OPERATION_PARAMS_SPACING_FACTOR));
    }

    gridOffset = operation->NumberOfIntParameters();

    for (size_t i = 0; i < operation->NumberOfDoubleParameters(); ++i)
    {
        QHBoxLayout *subHorLayout = new QHBoxLayout();
        QCustomDoubleSpinBox *spinBox = new QCustomDoubleSpinBox(i, filler);
        spinBox->setSizePolicy(QSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed));

        spinBox->setMaximum(operation->GetMaximalDoubleParamValue(i));
        spinBox->setMinimum(operation->GetMinimalDoubleParamValue(i));
        spinBox->setValue(operation->GetDoubleParamValue(i));
        spinBox->setSingleStep(operation->GetStepDoubleParamValue(i));
        spinBox->setDecimals(operation->GetDecimalCountDoubleParamValue(i));
        spinBox->setAlignment(Qt::AlignCenter);

        connect(spinBox, SIGNAL(valueChanged(double)), operation, SLOT(OnDoubleParamValueChanged(double)));

        subHorLayout->addWidget(new QLabel(operation->GetDoubleParametersNames()[i] + QString(":"), filler));
        subHorLayout->addWidget(spinBox);
        gridLayout->addLayout(subHorLayout,
                              (i + gridOffset) / OPERATION_PARAMS_SPACING_FACTOR,
                              (i + gridOffset) % OPERATION_PARAMS_SPACING_FACTOR,
                              GetOperationParamsGridAlignment((i + gridOffset) % OPERATION_PARAMS_SPACING_FACTOR));
    }

    if (operation->IsIterative())
    {
        QHBoxLayout *animLayout = new QHBoxLayout();

        QLineEdit *animName = new QLineEdit(filler);
        QCheckBox *exportAnimation = new QCheckBox(filler);
        exportAnimation->setText("Export animation");

        animLayout->addWidget(new QLabel("Animation name:", filler));
        animLayout->addWidget(animName);
        animLayout->addWidget(exportAnimation);

        animName->setText(operation->GetAnimationName().c_str());
        exportAnimation->setChecked(operation->IsExportingAnimation());
        animName->setEnabled(operation->IsExportingAnimation());

        connect(animName, SIGNAL(textChanged(QString)), operation, SLOT(OnAnimationNameChanged(QString)));
        connect(exportAnimation, SIGNAL(toggled(bool)), operation, SLOT(OnExportAnimationChecked(bool)));
        connect(exportAnimation, SIGNAL(toggled(bool)), animName, SLOT(setEnabled(bool)));

        animation->setLayout(animLayout);
    }

    ui->groupBoxOperationDetails->layout()->addWidget(filler);
}

Qt::Alignment MainWindow::GetRefinementParamsGridAlignment(int column) const
{
    if (REFINEMENT_PARAMS_SPACING_FACTOR == 2)
    {
        if (column == 0) return Qt::AlignLeft;
        else if (column == 1) return Qt::AlignRight;
    }
    else if (REFINEMENT_PARAMS_SPACING_FACTOR == 3)
    {
        if (column == 0) return Qt::AlignLeft;
        else if (column == 1) return Qt::AlignCenter;
        else if (column == 2) return Qt::AlignRight;
    }

    return Qt::AlignLeft;
}

Qt::Alignment MainWindow::GetOperationParamsGridAlignment(int column) const
{
    if (OPERATION_PARAMS_SPACING_FACTOR == 2)
    {
        if (column == 0) return Qt::AlignLeft;
        else if (column == 1) return Qt::AlignRight;
    }
    else if (OPERATION_PARAMS_SPACING_FACTOR == 3)
    {
        if (column == 0) return Qt::AlignLeft;
        else if (column == 1) return Qt::AlignCenter;
        else if (column == 2) return Qt::AlignRight;
    }

    return Qt::AlignLeft;
}

Qt::Alignment MainWindow::GetOperationPipelineGridAlignment(int column) const
{
    if (OPERATION_PIPELINE_SPACING_FACTOR == 2)
    {
        if (column == 0) return Qt::AlignLeft;
        else if (column == 1) return Qt::AlignRight;
    }
    else if (OPERATION_PIPELINE_SPACING_FACTOR == 3)
    {
        if (column == 0) return Qt::AlignLeft;
        else if (column == 1) return Qt::AlignCenter;
        else if (column == 2) return Qt::AlignRight;
    }

    return Qt::AlignLeft;
}

bool MainWindow::ProcessConfigurationString(const QString &str)
{
    ArgumentParser parser;

    if (!parser.ParseString(str)) return false;

    CleanExportTab();
    CleanOperationTab();
    CleanRefinementTab();

    //Reset
    InitializeFileIOTabFromConfig(parser);
    InitializeRefinementTabFromConfig(parser);
    InitializeInitializationTabFromConfig(parser);
    InitializeOperationsTabFromConfig(parser);
    InitializeExportTabFromConfig(parser);
    InitializeMPITabFromConfig(parser);

    return true;
}

void MainWindow::InitializeFileIOTabFromConfig(const ArgumentParser &parser)
{
    ui->lineEditFilePath->setText(parser.GetImageLoadPath());
    ui->lineEditFolderInputPath->setText(parser.GetFolderBatchPath());
    ui->checkBoxBatchProcessing->setChecked(parser.GetBatchProcess());
    ui->lineEditConnectivityPath->setText(parser.GetConnectivityFilePath());
    ui->lineEditFolderOutputPath->setText(parser.GetOutputFolderPath());
}

void MainWindow::InitializeRefinementTabFromConfig(const ArgumentParser &parser)
{
    std::vector<std::pair<Refinement::EMethod, void*>> data(parser.GetRefinementAlgorithms());

    for (size_t i = 0; i < data.size(); ++i)
    {
        QListWidgetRefinementItem *listItem = new QListWidgetRefinementItem(data[i].first, data[i].second);
        listItem->setText(Refinement::RefinementNames[data[i].first]);

        ui->listWidgetRefinementStack->addItem(listItem);
        ui->listWidgetRefinementStack->setCurrentItem(listItem);
    }
}

void MainWindow::InitializeInitializationTabFromConfig(const ArgumentParser &parser)
{
    ui->spinBoxBackgroundOTSU->setValue(parser.GetBackgroundQuadrantLevel());
    ui->checkBoxSetBackgroundValue->setChecked(parser.GetSetQuadrantBackground());
    ui->checkBoxBackgroundOptional->setChecked(!parser.GetSetAutomaticBackgroundValue());
    ui->spinBoxBackgroundOptional->setValue(parser.GetCustomBackgroundValue());
    ui->spinBoxTreeSideLength->setValue(parser.GetTreeSideLength());
    ui->checkBoxAutomaticTreeSideLength->setChecked(parser.GetAutoTreeSideLength());
    ui->spinBoxExtraPixelsValue->setValue(parser.GetExtraPixelValue());
    ui->checkBoxExtraPixelsValue->setChecked(parser.GetAutoExtraPixelValue());
}

void MainWindow::InitializeOperationsTabFromConfig(const ArgumentParser &parser)
{
    std::vector<std::pair<Operation::EMethod, void*>> data(parser.GetProcessingAlgorithms());

    for (size_t i = 0; i < data.size(); ++i)
    {
        QListWidgetOperationItem *listItem = new QListWidgetOperationItem(data[i].first, data[i].second);
        listItem->setText(Operation::OperationNames[data[i].first]);

        ui->listWidgetOperationStack->addItem(listItem);
        ui->listWidgetOperationStack->setCurrentItem(listItem);
    }
}

void MainWindow::InitializeExportTabFromConfig(const ArgumentParser &parser)
{
    CleanExportTab();

    ui->checkBoxExportVTK->setChecked(parser.GetOutputVTK());
    ui->checkBoxExportImage->setChecked(parser.GetOutputImage());

    std::vector<std::pair<int, QString>> vtkPipelines(parser.GetVtkPipelinesExport());

    for(size_t i = 0; i < vtkPipelines.size(); ++i)
    {
        QWidget *widget = ui->scrollAreaVTKExport->widget();
        QVBoxLayout *verticalLayout = dynamic_cast<QVBoxLayout*>(widget->layout());
        verticalLayout->addWidget(new QPipelineExportWidget(vtkPipelines[i].first, vtkPipelines[i].second.toLatin1(), widget), 0, Qt::AlignTop);
    }

    std::vector<std::pair<int, QString>> imagePipelines(parser.GetImagePipelinesExport());

    for(size_t i = 0; i < imagePipelines.size(); ++i)
    {
        QWidget *widget = ui->scrollAreaImageExport->widget();
        QVBoxLayout *verticalLayout = dynamic_cast<QVBoxLayout*>(widget->layout());
        verticalLayout->addWidget(new QPipelineExportWidget(imagePipelines[i].first, imagePipelines[i].second.toLatin1(), widget), 0, Qt::AlignTop);
    }
}

void MainWindow::InitializeMPITabFromConfig(const ArgumentParser &parser)
{
    ui->spinBoxMPIProcCount->setValue(parser.GetMPIProcessCount());
}

void MainWindow::Run()
{
    GenerateCommandString();
    m_settings.setValue("app_config", m_commandString);

    if (!m_bCanRunAnotherInstance)
    {
        QMessageBox msgBox(QMessageBox::Warning, "Multiple instances", "Instance of p4est is already running!");
        msgBox.exec();
        return;
    }

    ConsoleOutputDialog *dialog = nullptr;

    if (m_pDialog == nullptr)
    {
        m_pDialog = new ConsoleOutputDialog(this);
        dialog = m_pDialog;
        dialog->setAttribute(Qt::WA_DeleteOnClose);
    }
    else
    {
        if (ui->checkBoxKeepPreviousConsoleOutput->isChecked())
        {
            dialog = m_pDialog;
        }
        else
        {
            dialog = new ConsoleOutputDialog(this);
            dialog->setAttribute((Qt::WA_DeleteOnClose));
        }
    }

    connect(this, SIGNAL(GotConsoleLine(QString)), dialog, SLOT(AppendText(QString)));
    connect(this, SIGNAL(ForestStarted()), this, SLOT(on_forestStarted()));
    connect(this, SIGNAL(ForestEnded(QDialog*)), this, SLOT(on_forestEnded(QDialog*)));
    connect(this, SIGNAL(ErrorWhileExecutingForest()), this, SLOT(on_forestExecutionError()));
    connect(this, SIGNAL(ErrorWhileRunningForest()), this, SLOT(on_forestRunningError()));

    m_pConsoleOutputThread = std::make_unique<std::thread>(([this, dialog]()
    {
        if (m_commandString == "")
        {
            emit ErrorWhileExecutingForest();
            return;
        }

        redi::ipstream proc(m_commandString.toStdString(), redi::pstreams::pstdout | redi::pstreams::pstderr);
        std::string line;

        if (!proc.is_open())
        {
            qDebug() << "Is not opened\n";
            emit ErrorWhileExecutingForest();
            return;
        }

        emit ForestStarted();

        char buf[1024];
        std::streamsize n;
        bool finished[2] = { false, false };
        bool bOutHadNothing = true;
        bool bExecutionError = false;
        bool bRunningError = false;

        while (!finished[0] || !finished[1])
        {
            if (!finished[0])
            {
                while ((n = proc.err().readsome(buf, sizeof(buf))) > 0)
                {
                    if (bOutHadNothing) bExecutionError = true;
                    bRunningError = true;
                    emit GotConsoleLine(QString(std::string(buf, n).c_str()));
                }

                if (proc.eof())
                {
                    finished[0] = true;
                    if (!finished[1])
                        proc.clear();
                }
            }

            if (!finished[1])
            {
                while ((n = proc.out().readsome(buf, sizeof(buf))) > 0)
                {
                    bOutHadNothing = false;
                    emit GotConsoleLine(QString(std::string(buf, n).c_str()));
                }

                if (proc.eof())
                {
                    finished[1] = true;
                    if (!finished[0])
                        proc.clear();
                }
            }
        }

        if (bExecutionError) emit ErrorWhileExecutingForest();
        if (bRunningError) emit ErrorWhileRunningForest();

        emit ForestEnded(dialog);
    }));

    m_pConsoleOutputThread->detach();

    if (ui->checkBoxConsoleOutput->isChecked())
        dialog->show();
}

void MainWindow::CleanExportTab()
{
    QWidget *buff = new QWidget(ui->scrollAreaVTKExport);
    delete ui->scrollAreaVTKExport->widget();
    ui->scrollAreaVTKExport->setWidget(buff);

    QVBoxLayout *layout = new QVBoxLayout(buff);
    layout->setAlignment(Qt::AlignTop);
    buff->setLayout(layout);

    buff = new QWidget(ui->scrollAreaImageExport);
    delete ui->scrollAreaImageExport->widget();
    ui->scrollAreaImageExport->setWidget(buff);

    layout = new QVBoxLayout(buff);
    layout->setAlignment(Qt::AlignTop);
    buff->setLayout(layout);
}

void MainWindow::CleanRefinementTab()
{
    ui->listWidgetRefinementStack->clear();
}

void MainWindow::CleanOperationTab()
{
    ui->listWidgetOperationStack->clear();
}

bool MainWindow::CheckValidExportNames() const
{
    QObjectList children(ui->scrollAreaVTKExport->widget()->children());

    for (int i = 0; i < children.size(); ++i)
    {
        QPipelineExportWidget *child = dynamic_cast<QPipelineExportWidget*>(children[i]);

        if (child && child->GetExportName().trimmed() == "") return false;
    }

    children = ui->scrollAreaImageExport->widget()->children();

    for (int i = 0; i < children.size(); ++i)
    {
        QPipelineExportWidget *child = dynamic_cast<QPipelineExportWidget*>(children[i]);

        if (child && child->GetExportName().trimmed() == "") return false;
    }

    return true;
}

void MainWindow::on_pushButtonRunForest_pressed()
{
    if (m_settings.value("p4est_app_path", "") == "")
    {
        QMessageBox msgBox(QMessageBox::Warning, "p4est error", "Invalid path to p4est application!");
        msgBox.exec();

        return;
    }

    if (!CheckValidExportNames())
    {
        QMessageBox msgBox(QMessageBox::Warning, "Export names empty", "One or more pipeline export names are empty!");
        msgBox.exec();

        return;
    }

    Run();
}

void MainWindow::on_checkBoxBatchProcessing_toggled(bool b)
{
    ui->lineEditFilePath->setEnabled(!b);
    ui->pushButtonFilePath->setEnabled(!b);
    ui->lineEditFolderInputPath->setEnabled(b);
    ui->pushButtonFolderInputPath->setEnabled(b);
}

void MainWindow::on_pushButtonFilePath_pressed()
{
    QString folder;

    if (ui->lineEditFilePath->text() != "")
    {
        QFileInfo fi(ui->lineEditFilePath->text());
        folder = fi.absoluteDir().absolutePath();
    }
    else folder = m_settings.value("folder_img_load_path", "").toString();

    QString fileFilter = "Image data (*.pgm)";      //TODO: extend with additional supported formats
    QString filePath = QFileDialog::getOpenFileName(this, "Load image", folder, fileFilter);

    if (filePath != "")
    {
        QFileInfo fi(filePath);
        m_settings.setValue("folder_img_load_path", fi.absoluteDir().absolutePath());
        ui->lineEditFilePath->setText(filePath);
    }
}

void MainWindow::on_pushButtonFolderInputPath_pressed()
{
    QString folder;

    if (ui->lineEditFolderInputPath->text() != "")
    {
        QFileInfo fi(ui->lineEditFolderInputPath->text());
        folder = fi.absoluteDir().absolutePath();
    }
    else folder = m_settings.value("folder_img_load_path", "").toString();

    QString folderPath = QFileDialog::getExistingDirectory(this, "Select folder", folder, QFileDialog::ShowDirsOnly);

    if (folderPath != "")
    {
        QFileInfo fi(folderPath);
        m_settings.setValue("folder_img_load_path", fi.absoluteDir().absolutePath());
        ui->lineEditFolderInputPath->setText(folderPath);
    }
}

void MainWindow::on_pushButtonConnectivityPath_pressed()
{
    QString folder;

    if (ui->lineEditConnectivityPath->text() != "")
    {
        QFileInfo fi(ui->lineEditConnectivityPath->text());
        folder = fi.absoluteDir().absolutePath();
    }
    else folder = m_settings.value("folder_connectivity_path", "").toString();

    QString fileFilter = "ABAQUS (*.inp)";
    QString filePath = QFileDialog::getSaveFileName(this, "Load image", folder, fileFilter);

    if (filePath != "")
    {
        if (filePath.split('.').back() != "inp")
        {
            filePath += ".inp";
        }

        QFileInfo fi(filePath);
        m_settings.setValue("folder_connectivity_path", fi.absoluteDir().absolutePath());
        ui->lineEditConnectivityPath->setText(filePath);
    }
}

void MainWindow::on_pushButtonFolderOutputPath_pressed()
{
    QString folder;

    if (ui->lineEditFolderOutputPath->text() != "")
    {
        QFileInfo fi(ui->lineEditFolderOutputPath->text());
        folder = fi.absoluteDir().absolutePath();
    }
    else folder = m_settings.value("folder_output_path", "").toString();

    QString folderPath = QFileDialog::getExistingDirectory(this, "Select folder", folder, QFileDialog::ShowDirsOnly);

    if (folderPath != "")
    {
        QFileInfo fi(folderPath);
        m_settings.setValue("folder_output_path", fi.absoluteDir().absolutePath());
        ui->lineEditFolderOutputPath->setText(folderPath);
    }
}

void MainWindow::on_pushButtonAddRefinement_pressed()
{
    QListWidgetRefinementItem *listItem = new QListWidgetRefinementItem(ui->comboBoxRefinement->currentIndex());
    listItem->setText(Refinement::RefinementNames[ui->comboBoxRefinement->currentIndex()]);

    ui->listWidgetRefinementStack->addItem(listItem);
    ui->listWidgetRefinementStack->setCurrentItem(listItem);
}

void MainWindow::onListWidgetRefinementStack_currentItemChanged(QListWidgetItem *current, QListWidgetItem*)
{
    if (current == nullptr)
    {
        ui->pushButtonMoveRefinementUp->setEnabled(false);
        ui->pushButtonMoveRefinementDown->setEnabled(false);
        ui->pushButtonRemoveRefinement->setEnabled(false);

        UpdateRefinementDetailsPanel(nullptr);

        return;
    }
    else
    {
        int currentRow = current->listWidget()->row(current);
        int rowCount = current->listWidget()->count();

        ui->pushButtonMoveRefinementUp->setEnabled(currentRow != 0);
        ui->pushButtonMoveRefinementDown->setEnabled(currentRow != rowCount - 1);
        ui->pushButtonRemoveRefinement->setEnabled(true);
    }

    QListWidgetRefinementItem *curItem = reinterpret_cast<QListWidgetRefinementItem*>(current);

    UpdateRefinementDetailsPanel(curItem->GetRefinement());
}

void MainWindow::on_pushButtonMoveRefinementUp_pressed()
{
    int currentRow = ui->listWidgetRefinementStack->currentRow();
    QListWidgetItem *item = ui->listWidgetRefinementStack->takeItem(currentRow);

    ui->listWidgetRefinementStack->insertItem(currentRow - 1, item);
    ui->listWidgetRefinementStack->setCurrentRow(currentRow - 1);
}

void MainWindow::on_pushButtonMoveRefinementDown_pressed()
{
    int currentRow = ui->listWidgetRefinementStack->currentRow();
    QListWidgetItem *item = ui->listWidgetRefinementStack->takeItem(currentRow);

    ui->listWidgetRefinementStack->insertItem(currentRow + 1, item);
    ui->listWidgetRefinementStack->setCurrentRow(currentRow + 1);
}

void MainWindow::on_pushButtonRemoveRefinement_pressed()
{
    delete ui->listWidgetRefinementStack->currentItem();
    ui->listWidgetRefinementStack->setCurrentRow(ui->listWidgetRefinementStack->count() - 1);
}

void MainWindow::on_pushButtonRefinementHelp_pressed()
{
    QMessageBox msgBox(QMessageBox::Information, "Refinement info", Refinement::RefinementInfo[ui->comboBoxRefinement->currentIndex()]);
    msgBox.exec();
}

void MainWindow::on_pushButtonAddOperation_pressed()
{
    QListWidgetOperationItem *listItem = new QListWidgetOperationItem(ui->comboBoxOperation->currentIndex());
    listItem->setText(Operation::OperationNames[ui->comboBoxOperation->currentIndex()]);

    ui->listWidgetOperationStack->addItem(listItem);
    ui->listWidgetOperationStack->setCurrentItem(listItem);
}

void MainWindow::onListWidgetOperationStack_currentItemChanged(QListWidgetItem *current, QListWidgetItem*)
{
    if (current == nullptr)
    {
        ui->pushButtonMoveOperationUp->setEnabled(false);
        ui->pushButtonMoveOperationDown->setEnabled(false);
        ui->pushButtonRemoveOperation->setEnabled(false);

        UpdateOperationDetailsPanel(nullptr);

        return;
    }
    else
    {
        int currentRow = current->listWidget()->row(current);
        int rowCount = current->listWidget()->count();

        ui->pushButtonMoveOperationUp->setEnabled(currentRow != 0);
        ui->pushButtonMoveOperationDown->setEnabled(currentRow != rowCount - 1);
        ui->pushButtonRemoveOperation->setEnabled(true);
    }

    QListWidgetOperationItem *curItem = reinterpret_cast<QListWidgetOperationItem*>(current);

    UpdateOperationDetailsPanel(curItem->GetOperation());
}

void MainWindow::on_pushButtonMoveOperationUp_pressed()
{
    int currentRow = ui->listWidgetOperationStack->currentRow();
    QListWidgetItem *item = ui->listWidgetOperationStack->takeItem(currentRow);

    ui->listWidgetOperationStack->insertItem(currentRow - 1, item);
    ui->listWidgetOperationStack->setCurrentRow(currentRow - 1);
}

void MainWindow::on_pushButtonMoveOperationDown_pressed()
{
    int currentRow = ui->listWidgetOperationStack->currentRow();
    QListWidgetItem *item = ui->listWidgetOperationStack->takeItem(currentRow);

    ui->listWidgetOperationStack->insertItem(currentRow + 1, item);
    ui->listWidgetOperationStack->setCurrentRow(currentRow + 1);
}

void MainWindow::on_pushButtonRemoveOperation_pressed()
{
    delete ui->listWidgetOperationStack->currentItem();
    ui->listWidgetOperationStack->setCurrentRow(ui->listWidgetOperationStack->count() - 1);
}

void MainWindow::on_pushButtonOperationHelp_pressed()
{
    QMessageBox msgBox(QMessageBox::Information, "Operation info", Operation::OperationInfo[ui->comboBoxOperation->currentIndex()]);
    msgBox.exec();
}

void MainWindow::on_pushButtonAddVTKExport_pressed()
{
    QWidget *widget = ui->scrollAreaVTKExport->widget();
    QVBoxLayout *verticalLayout = dynamic_cast<QVBoxLayout*>(widget->layout());
    verticalLayout->addWidget(new QPipelineExportWidget(1, "", widget), 0, Qt::AlignTop);
}

void MainWindow::on_pushButtonAddImageExport_pressed()
{
    QWidget *widget = ui->scrollAreaImageExport->widget();
    QVBoxLayout *verticalLayout = dynamic_cast<QVBoxLayout*>(widget->layout());
    verticalLayout->addWidget(new QPipelineExportWidget(1, "", widget), 0, Qt::AlignTop);
}

void MainWindow::on_checkBoxExportVTK_toggled(bool b)
{
    ui->pushButtonAddVTKExport->setEnabled(b);
    ui->scrollAreaVTKExport->setEnabled(b);
}

void MainWindow::on_checkBoxExportImage_toggled(bool b)
{
    ui->pushButtonAddImageExport->setEnabled(b);
    ui->scrollAreaImageExport->setEnabled(b);
}

void MainWindow::on_spinBoxTreeSideLength_valueChanged(int value)
{
    double logRes = log2(value);
    int topLogRes = static_cast<int>(std::ceil(logRes));
    int botLogRes = static_cast<int>(std::floor(logRes));

    int topVal = 1 << topLogRes;
    int botVal = 1 << botLogRes;

    ui->spinBoxTreeSideLength->setValue(std::abs(topVal - value) <= std::abs(botVal - value) ? topVal : botVal);
}

void MainWindow::on_checkBoxSetBackgroundValue_toggled(bool b)
{
    ui->spinBoxBackgroundOTSU->setEnabled(b);
    ui->checkBoxBackgroundOptional->setEnabled(b);
}

void MainWindow::on_checkBoxBackgroundOptional_toggled(bool b)
{
    ui->spinBoxBackgroundOptional->setEnabled(b);
}

void MainWindow::on_checkBoxExtraPixelsValue_toggled(bool b)
{
    ui->spinBoxExtraPixelsValue->setEnabled(!b);
}

void MainWindow::on_checkBoxAutomaticTreeSideLength_toggled(bool b)
{
    ui->spinBoxTreeSideLength->setEnabled(!b);
}

void MainWindow::on_actionRun_triggered()
{
    if (m_settings.value("p4est_app_path", "") == "")
    {
        QMessageBox msgBox(QMessageBox::Warning, "p4est error", "Invalid path to p4est application!");
        msgBox.exec();

        return;
    }

    if (!CheckValidExportNames())
    {
        QMessageBox msgBox(QMessageBox::Warning, "Export names empty", "One or more pipeline export names are empty!");
        msgBox.exec();

        return;
    }

    Run();
}

void MainWindow::on_actionGenerateCommandString_triggered()
{
    GenerateCommandString();
    TextClipboardDialog *dialog = new TextClipboardDialog(this);
    dialog->setAttribute(Qt::WA_DeleteOnClose);

    dialog->Clear();
    dialog->AppendText(m_commandString);

    dialog->show();
}

void MainWindow::on_actionLoad_configuration_triggered()
{
    TextClipboardDialog dialog;

    dialog.SetTextReadOnly(false);
    dialog.HideClipboardButton();
    dialog.exec();

    QString config(dialog.GetText());

    if (!ProcessConfigurationString(config))
    {
        QMessageBox msgBox(QMessageBox::Critical, "Configuration error", "Invalid configuration string!");
        msgBox.exec();
    }
}

void MainWindow::on_actionPath_triggered()
{
    QLineEditDialog dialog("Path to p4est application:", "Set path", this);
    dialog.exec();
}

void MainWindow::on_actionReset_triggered()
{
    QMessageBox::StandardButton reply(QMessageBox::question(this, "Attention", "Do you really wish to reset configuration?", QMessageBox::Yes|QMessageBox::No));

    if (reply != QMessageBox::Yes) return;

    CleanExportTab();
    CleanOperationTab();
    CleanRefinementTab();

    ArgumentParser parser;

    InitializeFileIOTabFromConfig(parser);
    InitializeRefinementTabFromConfig(parser);
    InitializeInitializationTabFromConfig(parser);
    InitializeOperationsTabFromConfig(parser);
    InitializeExportTabFromConfig(parser);
    InitializeMPITabFromConfig(parser);
}

void MainWindow::on_forestStarted()
{
    m_bCanRunAnotherInstance = false;
    ui->statusBar->showMessage("Running p4est");
}

void MainWindow::on_forestEnded(QDialog *dialog)
{
    m_bCanRunAnotherInstance = true;
    dialog->disconnect(this, nullptr, nullptr, nullptr);
    ui->statusBar->clearMessage();
    connect(m_pDialog, SIGNAL(destroyed(QObject*)), this, SLOT(on_dialogDestroyed(QObject*)));
}

void MainWindow::on_forestExecutionError()
{
    QMessageBox msgBox(QMessageBox::Critical, "p4est error", "Unable to execute p4est application!");
    msgBox.exec();
}

void MainWindow::on_forestRunningError()
{
    QMessageBox msgBox(QMessageBox::Critical, "p4est error", "Error occured while running p4est application!");
    msgBox.exec();
}

void MainWindow::on_dialogDestroyed(QObject*)
{
    m_pDialog = nullptr;
}
