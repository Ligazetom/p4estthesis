#ifndef QPIPELINEEXPORTWIDGET_H
#define QPIPELINEEXPORTWIDGET_H

#include <qwidget.h>
#include <qcombobox.h>
#include <qlabel.h>
#include <qlineedit.h>
#include <qlayout.h>
#include <qstring.h>

class QPipelineExportWidget : public QWidget
{
    Q_OBJECT
public:
    explicit QPipelineExportWidget(int comboValue = 1, const char *exportName = "", QWidget *parent = nullptr);

    QString GetExportName() const { return m_exportName.text(); }
    int GetPipelineIndex() const { return m_comboBox.currentIndex(); }

private:
    QComboBox m_comboBox;
    QLineEdit m_exportName;
    QHBoxLayout m_layout;

    void InitializeComboBoxValues(int currentIndex);

private slots:
    void on_pushButtonRemove_pressed();
signals:

};

#endif // QPIPELINEEXPORTWIDGET_H
