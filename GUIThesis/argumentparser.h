#ifndef ARGUMENTPARSER_H
#define ARGUMENTPARSER_H

#include "generalconstants.h"
#include "refinement.h"
#include "refinementdatatypes.h"
#include "operation.h"
#include "operationdatatypes.h"

#include <qlist.h>
#include <qstring.h>
#include <vector>
#include <map>
#include <sstream>

constexpr int NUMBER_OF_MANDATORY_FLAGS = 1;

class ArgumentParser
{
public:
    ArgumentParser();
    ~ArgumentParser();

    bool ParseString(const QString &str);
    bool IsValid() const;
    std::string PrintMissingFlags() const;
    std::string GetLoggerText() const { return m_logger.str(); }

    QString GetFolderBatchPath() const { return m_folderBatchPath; }
    QString GetImageLoadPath() const { return m_imageLoadPath; }
    bool GetBatchProcess() const { return m_bBatchProcess; }
    QString GetConnectivityFilePath() const { return m_connectivityFilePath; }
    QString GetOutputFolderPath() const { return m_outputFolderPath; }
    bool GetOutputVTK() const { return m_bOutputVTK; }
    bool GetOutputImage() const { return m_bOutputImage; }
    std::vector<std::pair<int, QString>> GetVtkPipelinesExport() const { return m_vtkPipelinesExport; }
    std::vector<std::pair<int, QString>> GetImagePipelinesExport() const { return m_imagePipelinesExport; }
    int GetBackgroundQuadrantLevel() const { return m_backgroundQuadrantLevel; }
    bool GetSetQuadrantBackground() const { return m_bSetQuadrantBackgroud; }
    bool GetSetAutomaticBackgroundValue() const { return m_bSetAutomaticBackgroundValue; }
    unsigned char GetCustomBackgroundValue() const { return m_customBackgroundValue; }
    bool GetCopyOriginalImage() const { return m_bCopyOriginalImage; }
    bool GetCreateNewOutputDirectory() const { return m_bCreateNewOutputDirectory; }
    bool GetAutoTreeSideLength() const { return m_bAutoTreeSideLength; }
    int GetTreeSideLength() const { return m_treeSideLength; }
    bool GetAutoExtraPixelValue() const { return m_bAutoExtraPixelValue; }
    unsigned char GetExtraPixelValue() const { return m_extraPixelValue; }
    int GetMPIProcessCount() const { return m_mpiProcessCount; }
    std::vector<std::pair<Refinement::EMethod, void*>> GetRefinementAlgorithms() const { return m_refinementAlgorithms; }
    std::vector<std::pair<Operation::EMethod, void*>> GetProcessingAlgorithms() const { return m_processingAlgorithms; }

private:
    QString m_folderBatchPath;
    QString m_imageLoadPath;
    bool m_bBatchProcess;
    QString m_connectivityFilePath;
    QString m_outputFolderPath;
    bool m_bOutputVTK;
    bool m_bOutputImage;
    std::vector<std::pair<int, QString>> m_vtkPipelinesExport;
    std::vector<std::pair<int, QString>> m_imagePipelinesExport;
    int m_backgroundQuadrantLevel;
    bool m_bSetQuadrantBackgroud;
    bool m_bSetAutomaticBackgroundValue;
    unsigned char m_customBackgroundValue;
    bool m_bCopyOriginalImage;
    bool m_bCreateNewOutputDirectory;
    bool m_bAutoTreeSideLength;
    int m_treeSideLength;
    bool m_bAutoExtraPixelValue;
    unsigned char m_extraPixelValue;
    int m_mpiProcessCount;
    std::vector<std::pair<Refinement::EMethod, void*>> m_refinementAlgorithms;
    std::vector<std::pair<Operation::EMethod, void*>> m_processingAlgorithms;

    std::stringstream m_logger;
    bool m_bIsValid = false;
    std::vector<QChar> m_currentFlags;
    std::map<QChar, QString> m_flagParams;
    std::vector<std::string> m_animationNames;   

    const char m_mandatoryFlags[NUMBER_OF_MANDATORY_FLAGS] = {'o'}; //{f, i} only one of the must be present

    void InitializeValues();

    QStringList PreprocessString(const QString &str);

    bool PickFlagsAndArgs(int argc, const QStringList &argv);
    bool Parse_f_flag();
    bool Parse_i_flag();
    bool Parse_c_flag();
    bool Parse_o_flag();
    bool Parse_O_flag();
    bool Parse_X_flag();
    bool Parse_B_flag();
    bool Parse_I_flag();
    bool Parse_D_flag();
    bool Parse_T_flag();
    bool Parse_r_flag();
    bool Parse_m_flag();

    bool Parse_GLOBAL_OTSU_args(const QStringList &tokens, int orderIndex);
    bool Parse_MAX_MIN_DIFF_args(const QStringList &tokens, int orderIndex);
    bool Parse_BERNSEN_args(const QStringList &tokens, int orderIndex);
    bool Parse_SAUVOLA_args(const QStringList &tokens, int orderIndex);
    bool Parse_NIBLACK_REFINE_args(const QStringList &tokens, int orderIndex);
    bool Parse_TOTAL_args(const QStringList &tokens, int orderIndex);
    bool Parse_UP_TO_args(const QStringList &tokens, int orderIndex);

    bool Parse_LHE_EXPLICIT_args(const QStringList &tokens, const QStringList &refinementTokens, int orderIndex);
    bool Parse_LHE_IMPLICIT_args(const QStringList &tokens, const QStringList &refinementTokens, int orderIndex);
    bool Parse_NIBLACK_IMAGE_args(const QStringList &tokens, int orderIndex);
    bool Parse_MCF_args(const QStringList &tokens, const QStringList &refinementTokens, int orderIndex);
    bool Parse_SUBSURF_args(const QStringList &tokens, const QStringList &refinementTokens, int orderIndex);
    bool Parse_SQUARE_args(const QStringList &tokens, int orderIndex);
    bool Parse_MAX_args(const QStringList &tokens, int orderIndex);
    bool Parse_EXPAND_REFINEMENT_args(const QStringList &tokens, int orderIndex);
    bool Parse_REFINE_BY_THRESHOLD_args(const QStringList &tokens, int orderIndex);
    bool Parse_FLAT_THRESHOLD_args(const QStringList &tokens, int orderIndex);

    bool ParseAnimationArgs(const QStringList &tokens, const char *method,
                            int orderIndex, int atIndex,
                            std::string &animationName, bool &exportAnimation);
    bool HasDuplicates(std::vector<std::string> &names) const;

    bool IsPowerOfTwo(int number) const;
    bool ExtractFlagParams(int i, int argc, const QStringList &argv);
    QStringList SplitToArgv(const QString &str) const;
    void DeallocateParams();

};

#endif // ARGUMENTPARSER_H

