#ifndef REFINEMENT_H
#define REFINEMENT_H

#include <cstdlib>
#include <qstring.h>

#include "BoilerplateMacros.h"
#include "qcustomspinbox.h"
#include "qcustomdoublespinbox.h"
#include "qcustomcombobox.h"
#include "generalconstants.h"

class Refinement : public QObject
{
     Q_OBJECT

public:
    enum EMethod
    {
        GLOBAL_OTSU = 0,
        MAX_MIN_DIFF,
        BERNSEN,
        SAUVOLA,
        NIBLACK,
        TOTAL,
        UP_TO
    };

    static constexpr size_t NUMBER_OF_REFINEMENTS = 7;
    static constexpr const char* RefinementNames[NUMBER_OF_REFINEMENTS] = {"GLOBAL_OTSU", "MAX_MIN_DIFF", "BERNSEN", "SAUVOLA", "NIBLACK", "TOTAL", "UP_TO"};
    static constexpr const char* RefinementInfo[NUMBER_OF_REFINEMENTS] = {
        "Refine grid according to OTSU's algorithm",
        "Finds maximum and minimum pixel intensity for quadrant, if difference is bigger or equal to user provided threshold, quadrant will get refined",
        "Refine grid according to BERNSEN's algorithm",
        "Refine grid according to SAUVOLA's algorithm",
        "Refine grid according to NIBLACK's algorithm",
        "Refines every quadrant to the maximum possible level of refinement",
        "Refines every quadrant to no more than the specified quadrant level"
    };

    Refinement(void* /*data*/) : QObject() {};
    virtual ~Refinement() {};
    virtual EMethod GetMethod() const = 0;
    virtual QString GetMethodInfo() const = 0;

    virtual size_t NumberOfDoubleParameters() const = 0;
    virtual size_t NumberOfIntParameters() const = 0;

    virtual const char* const * GetDoubleParametersNames() const = 0;
    virtual const char* const * GetIntParametersNames() const = 0;

    virtual double GetMinimalDoubleParamValue(size_t paramIndex) const = 0;
    virtual double GetMaximalDoubleParamValue(size_t paramIndex) const = 0;
    virtual double GetDefaultDoubleParamValue(size_t paramIndex) const = 0;
    virtual double GetStepDoubleParamValue(size_t paramIndex) const = 0;
    virtual int GetDecimalCountDoubleParamValue(size_t paramIndex) const = 0;

    virtual int GetMinimalIntParamValue(size_t paramIndex) const = 0;
    virtual int GetMaximalIntParamValue(size_t paramIndex) const = 0;
    virtual int GetDefaultIntParamValue(size_t paramIndex) const = 0;
    virtual int GetStepIntParamValue(size_t paramIndex) const = 0;

    virtual int GetIntParamValue(size_t paramIndex) const = 0;
    virtual double GetDoubleParamValue(size_t paramIndex) const = 0;

    virtual const std::string &GenerateCommandString() = 0;
    const std::string &GetCommandString() const { return m_commandString; }
    const char* GetMethodName() const { return RefinementNames[GetMethod()]; }

protected:
    std::string m_commandString;
    virtual std::string GenerateParametersArg() const = 0;
    virtual void ProcessData(void *data) = 0;
    static constexpr size_t GetValidArraySize(size_t size) { return size > 0 ? size : 1; }

public slots:
    virtual void OnIntParamValueChanged(int value) = 0;
    virtual void OnDoubleParamValueChanged(double value) = 0;
};


class GLOBAL_OTSU : public Refinement
{
    Q_OBJECT

public:
    GLOBAL_OTSU(void *data = nullptr);
    virtual ~GLOBAL_OTSU() {};

    REFINEMENT_PUBLIC_OVERRIDE(Refinement::GLOBAL_OTSU)

    static constexpr size_t NUM_OF_DOUBLE_PARAMS = 0;
    static constexpr size_t NUM_OF_INT_PARAMS = 1;

    static constexpr const char* DoubleParametersNames[GetValidArraySize(NUM_OF_DOUBLE_PARAMS)] = {};
    static constexpr double MinimalDoubleParamsValues[GetValidArraySize(NUM_OF_DOUBLE_PARAMS)] = {};
    static constexpr double MaximalDoubleParamsValues[GetValidArraySize(NUM_OF_DOUBLE_PARAMS)] = {};
    static constexpr double DefaultDoubleParamsValues[GetValidArraySize(NUM_OF_DOUBLE_PARAMS)] = {};
    static constexpr double StepDoubleParamsValues[GetValidArraySize(NUM_OF_DOUBLE_PARAMS)] = {};
    static constexpr int DecimalCountDoubleParamsValues[GetValidArraySize(NUM_OF_DOUBLE_PARAMS)] = {};

    static constexpr const char* IntParametersNames[GetValidArraySize(NUM_OF_INT_PARAMS)] = { "Quadrant level to refine from" };
    static constexpr int MinimalIntParamsValues[GetValidArraySize(NUM_OF_INT_PARAMS)] = { 0 };
    static constexpr int MaximalIntParamsValues[GetValidArraySize(NUM_OF_INT_PARAMS)] = { 99999 };     //with std::numeric_limits<int>::max() spinboxes are too long
    static constexpr int DefaultIntParamsValues[GetValidArraySize(NUM_OF_INT_PARAMS)] = { 0 };
    static constexpr int StepIntParamsValues[GetValidArraySize(NUM_OF_INT_PARAMS)] = { 1 };

protected:
    virtual std::string GenerateParametersArg() const override;
    virtual void ProcessData(void *data) override;

public slots:
    REFINEMENT_SLOTS_OVERRIDE

private:
    REFINEMENT_PRIVATE_MEMBERS
};


class MAX_MIN_DIFF : public Refinement
{
    Q_OBJECT

public:
    MAX_MIN_DIFF(void *data = nullptr);
    virtual ~MAX_MIN_DIFF() {};

    REFINEMENT_PUBLIC_OVERRIDE(Refinement::MAX_MIN_DIFF)

    static constexpr size_t NUM_OF_DOUBLE_PARAMS = 0;
    static constexpr size_t NUM_OF_INT_PARAMS = 2;

    static constexpr const char* DoubleParametersNames[GetValidArraySize(NUM_OF_DOUBLE_PARAMS)] = {};
    static constexpr double MinimalDoubleParamsValues[GetValidArraySize(NUM_OF_DOUBLE_PARAMS)] = {};
    static constexpr double MaximalDoubleParamsValues[GetValidArraySize(NUM_OF_DOUBLE_PARAMS)] = {};
    static constexpr double DefaultDoubleParamsValues[GetValidArraySize(NUM_OF_DOUBLE_PARAMS)] = {};
    static constexpr double StepDoubleParamsValues[GetValidArraySize(NUM_OF_DOUBLE_PARAMS)] = {};
    static constexpr int DecimalCountDoubleParamsValues[GetValidArraySize(NUM_OF_DOUBLE_PARAMS)] = {};

    static constexpr const char* IntParametersNames[GetValidArraySize(NUM_OF_INT_PARAMS)] = { "Quadrant level to refine from", "Threshold" };
    static constexpr int MinimalIntParamsValues[GetValidArraySize(NUM_OF_INT_PARAMS)] = { 0, 0 };
    static constexpr int MaximalIntParamsValues[GetValidArraySize(NUM_OF_INT_PARAMS)] = { 99999, 99999 };     //with std::numeric_limits<int>::max() spinboxes are too long
    static constexpr int DefaultIntParamsValues[GetValidArraySize(NUM_OF_INT_PARAMS)] = { 0, 30 };
    static constexpr int StepIntParamsValues[GetValidArraySize(NUM_OF_INT_PARAMS)] = { 1, 5 };

protected:
    virtual std::string GenerateParametersArg() const override;
    virtual void ProcessData(void *data) override;

public slots:
    REFINEMENT_SLOTS_OVERRIDE

private:
    REFINEMENT_PRIVATE_MEMBERS
};


class BERNSEN : public Refinement
{
    Q_OBJECT

public:
    BERNSEN(void *data = nullptr);
    virtual ~BERNSEN() {};

    REFINEMENT_PUBLIC_OVERRIDE(Refinement::BERNSEN)

    static constexpr size_t NUM_OF_DOUBLE_PARAMS = 2;
    static constexpr size_t NUM_OF_INT_PARAMS = 2;

    static constexpr const char* DoubleParametersNames[GetValidArraySize(NUM_OF_DOUBLE_PARAMS)] = { "Contrast threshold", "Background-foreground threshold" };
    static constexpr double MinimalDoubleParamsValues[GetValidArraySize(NUM_OF_DOUBLE_PARAMS)] = { 0., 0. };
    static constexpr double MaximalDoubleParamsValues[GetValidArraySize(NUM_OF_DOUBLE_PARAMS)] = { 99999., 99999. };
    static constexpr double DefaultDoubleParamsValues[GetValidArraySize(NUM_OF_DOUBLE_PARAMS)] = { 10., 10. };
    static constexpr double StepDoubleParamsValues[GetValidArraySize(NUM_OF_DOUBLE_PARAMS)] = { 5., 5. };
    static constexpr int DecimalCountDoubleParamsValues[GetValidArraySize(NUM_OF_DOUBLE_PARAMS)] = { 3, 3 };

    static constexpr const char* IntParametersNames[GetValidArraySize(NUM_OF_INT_PARAMS)] = { "Quadrant level to refine from", "Radius" };
    static constexpr int MinimalIntParamsValues[GetValidArraySize(NUM_OF_INT_PARAMS)] = { 0, 0 };
    static constexpr int MaximalIntParamsValues[GetValidArraySize(NUM_OF_INT_PARAMS)] = { 99999, 99999 };     //with std::numeric_limits<int>::max() spinboxes are too long
    static constexpr int DefaultIntParamsValues[GetValidArraySize(NUM_OF_INT_PARAMS)] = { 0, 3 };
    static constexpr int StepIntParamsValues[GetValidArraySize(NUM_OF_INT_PARAMS)] = { 1, 1 };

protected:
    virtual std::string GenerateParametersArg() const override;
    virtual void ProcessData(void *data) override;

public slots:
    REFINEMENT_SLOTS_OVERRIDE

private:
    REFINEMENT_PRIVATE_MEMBERS
};


class SAUVOLA : public Refinement
{
    Q_OBJECT

public:
    SAUVOLA(void *data = nullptr);
    virtual ~SAUVOLA() {};

    REFINEMENT_PUBLIC_OVERRIDE(Refinement::SAUVOLA)

    static constexpr size_t NUM_OF_DOUBLE_PARAMS = 2;
    static constexpr size_t NUM_OF_INT_PARAMS = 2;

    static constexpr const char* DoubleParametersNames[GetValidArraySize(NUM_OF_DOUBLE_PARAMS)] = { "Kappa coeff", "Maximal std" };
    static constexpr double MinimalDoubleParamsValues[GetValidArraySize(NUM_OF_DOUBLE_PARAMS)] = { 0., 0.001 };
    static constexpr double MaximalDoubleParamsValues[GetValidArraySize(NUM_OF_DOUBLE_PARAMS)] = { 99999., 99999. };
    static constexpr double DefaultDoubleParamsValues[GetValidArraySize(NUM_OF_DOUBLE_PARAMS)] = { 1., 10. };
    static constexpr double StepDoubleParamsValues[GetValidArraySize(NUM_OF_DOUBLE_PARAMS)] = { 5., 5. };
    static constexpr int DecimalCountDoubleParamsValues[GetValidArraySize(NUM_OF_DOUBLE_PARAMS)] = { 3, 3 };

    static constexpr const char* IntParametersNames[GetValidArraySize(NUM_OF_INT_PARAMS)] = { "Quadrant level to refine from", "Radius" };
    static constexpr int MinimalIntParamsValues[GetValidArraySize(NUM_OF_INT_PARAMS)] = { 0, 0 };
    static constexpr int MaximalIntParamsValues[GetValidArraySize(NUM_OF_INT_PARAMS)] = { 99999, 99999 };     //with std::numeric_limits<int>::max() spinboxes are too long
    static constexpr int DefaultIntParamsValues[GetValidArraySize(NUM_OF_INT_PARAMS)] = { 0, 3 };
    static constexpr int StepIntParamsValues[GetValidArraySize(NUM_OF_INT_PARAMS)] = { 1, 1 };

protected:
    virtual std::string GenerateParametersArg() const override;
    virtual void ProcessData(void *data) override;

public slots:
    REFINEMENT_SLOTS_OVERRIDE

private:
    REFINEMENT_PRIVATE_MEMBERS
};


class NIBLACK_REF : public Refinement
{
    Q_OBJECT

public:
    NIBLACK_REF(void *data = nullptr);
    virtual ~NIBLACK_REF() {};

    REFINEMENT_PUBLIC_OVERRIDE(Refinement::NIBLACK)

    static constexpr size_t NUM_OF_DOUBLE_PARAMS = 2;
    static constexpr size_t NUM_OF_INT_PARAMS = 2;

    static constexpr const char* DoubleParametersNames[GetValidArraySize(NUM_OF_DOUBLE_PARAMS)] = { "Kappa coeff", "Mean offset" };
    static constexpr double MinimalDoubleParamsValues[GetValidArraySize(NUM_OF_DOUBLE_PARAMS)] = { -99999., -99999. };
    static constexpr double MaximalDoubleParamsValues[GetValidArraySize(NUM_OF_DOUBLE_PARAMS)] = { 99999., 99999. };
    static constexpr double DefaultDoubleParamsValues[GetValidArraySize(NUM_OF_DOUBLE_PARAMS)] = { 1., 2. };
    static constexpr double StepDoubleParamsValues[GetValidArraySize(NUM_OF_DOUBLE_PARAMS)] = { 5., 5. };
    static constexpr int DecimalCountDoubleParamsValues[GetValidArraySize(NUM_OF_DOUBLE_PARAMS)] = { 3, 3 };

    static constexpr const char* IntParametersNames[GetValidArraySize(NUM_OF_INT_PARAMS)] = { "Quadrant level to refine from", "Radius" };
    static constexpr int MinimalIntParamsValues[GetValidArraySize(NUM_OF_INT_PARAMS)] = { 0, 0 };
    static constexpr int MaximalIntParamsValues[GetValidArraySize(NUM_OF_INT_PARAMS)] = { 99999, 99999 };     //with std::numeric_limits<int>::max() spinboxes are too long
    static constexpr int DefaultIntParamsValues[GetValidArraySize(NUM_OF_INT_PARAMS)] = { 0, 3 };
    static constexpr int StepIntParamsValues[GetValidArraySize(NUM_OF_INT_PARAMS)] = { 1, 1 };

protected:
    virtual std::string GenerateParametersArg() const override;
    virtual void ProcessData(void *data) override;

public slots:
    REFINEMENT_SLOTS_OVERRIDE

private:
    REFINEMENT_PRIVATE_MEMBERS
};


class TOTAL : public Refinement
{
    Q_OBJECT

public:
    TOTAL(void *data = nullptr);
    virtual ~TOTAL() {};

    REFINEMENT_PUBLIC_OVERRIDE(Refinement::TOTAL)

    static constexpr size_t NUM_OF_DOUBLE_PARAMS = 0;
    static constexpr size_t NUM_OF_INT_PARAMS = 0;

    static constexpr const char* DoubleParametersNames[GetValidArraySize(NUM_OF_DOUBLE_PARAMS)] = {};
    static constexpr double MinimalDoubleParamsValues[GetValidArraySize(NUM_OF_DOUBLE_PARAMS)] = {};
    static constexpr double MaximalDoubleParamsValues[GetValidArraySize(NUM_OF_DOUBLE_PARAMS)] = {};
    static constexpr double DefaultDoubleParamsValues[GetValidArraySize(NUM_OF_DOUBLE_PARAMS)] = {};
    static constexpr double StepDoubleParamsValues[GetValidArraySize(NUM_OF_DOUBLE_PARAMS)] = {};
    static constexpr int DecimalCountDoubleParamsValues[GetValidArraySize(NUM_OF_DOUBLE_PARAMS)] = {};

    static constexpr const char* IntParametersNames[GetValidArraySize(NUM_OF_INT_PARAMS)] = {};
    static constexpr int MinimalIntParamsValues[GetValidArraySize(NUM_OF_INT_PARAMS)] = {};
    static constexpr int MaximalIntParamsValues[GetValidArraySize(NUM_OF_INT_PARAMS)] = {};     //with std::numeric_limits<int>::max() spinboxes are too long
    static constexpr int DefaultIntParamsValues[GetValidArraySize(NUM_OF_INT_PARAMS)] = {};
    static constexpr int StepIntParamsValues[GetValidArraySize(NUM_OF_INT_PARAMS)] = {};

protected:
    virtual std::string GenerateParametersArg() const override;
    virtual void ProcessData(void *) override;

public slots:
    REFINEMENT_SLOTS_OVERRIDE

private:
    REFINEMENT_PRIVATE_MEMBERS
};


class UP_TO : public Refinement
{
    Q_OBJECT

public:
    UP_TO(void *data = nullptr);
    virtual ~UP_TO() {};

    REFINEMENT_PUBLIC_OVERRIDE(Refinement::UP_TO)

    static constexpr size_t NUM_OF_DOUBLE_PARAMS = 0;
    static constexpr size_t NUM_OF_INT_PARAMS = 1;

    static constexpr const char* DoubleParametersNames[GetValidArraySize(NUM_OF_DOUBLE_PARAMS)] = {};
    static constexpr double MinimalDoubleParamsValues[GetValidArraySize(NUM_OF_DOUBLE_PARAMS)] = {};
    static constexpr double MaximalDoubleParamsValues[GetValidArraySize(NUM_OF_DOUBLE_PARAMS)] = {};
    static constexpr double DefaultDoubleParamsValues[GetValidArraySize(NUM_OF_DOUBLE_PARAMS)] = {};
    static constexpr double StepDoubleParamsValues[GetValidArraySize(NUM_OF_DOUBLE_PARAMS)] = {};
    static constexpr int DecimalCountDoubleParamsValues[GetValidArraySize(NUM_OF_DOUBLE_PARAMS)] = {};

    static constexpr const char* IntParametersNames[GetValidArraySize(NUM_OF_INT_PARAMS)] = { "Quadrant level to refine to:"};
    static constexpr int MinimalIntParamsValues[GetValidArraySize(NUM_OF_INT_PARAMS)] = { 0 };
    static constexpr int MaximalIntParamsValues[GetValidArraySize(NUM_OF_INT_PARAMS)] = { 99999 };     //with std::numeric_limits<int>::max() spinboxes are too long
    static constexpr int DefaultIntParamsValues[GetValidArraySize(NUM_OF_INT_PARAMS)] = { 3 };
    static constexpr int StepIntParamsValues[GetValidArraySize(NUM_OF_INT_PARAMS)] = { 1 };

protected:
    virtual std::string GenerateParametersArg() const override;
    virtual void ProcessData(void *data) override;

public slots:
    REFINEMENT_SLOTS_OVERRIDE

private:
    REFINEMENT_PRIVATE_MEMBERS
};
#endif // REFINEMENT_H
