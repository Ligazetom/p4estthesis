#ifndef QLISTWIDGETREFINEMENTITEM_H
#define QLISTWIDGETREFINEMENTITEM_H

#include <QListWidgetItem>
#include "refinement.h"
#include <memory>

class QListWidgetRefinementItem : public QListWidgetItem
{
public:
    explicit QListWidgetRefinementItem(int refinementMethodIndex, void *data = nullptr);
    int GetRefinementMethodIndex() const { return m_refinementMethodIndex; }
    Refinement *GetRefinement() { return m_pRefinement.get(); }

protected:
    int m_refinementMethodIndex;
    std::unique_ptr<Refinement> m_pRefinement;
};

#endif // QLISTWIDGETREFINEMENTITEM_H
