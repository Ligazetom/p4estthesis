#include "consoleoutputdialog.h"

#include <qclipboard.h>

ConsoleOutputDialog::ConsoleOutputDialog(QWidget* parent) : QDialog(parent), ui(new Ui::ConsoleOutputDialog)
{
    ui->setupUi(this);
}

void ConsoleOutputDialog::Clear()
{
    ui->textEditConsoleOutput->clear();
}

void ConsoleOutputDialog::AppendText(QString text)
{
    ui->textEditConsoleOutput->moveCursor(QTextCursor::End);
    text = text.replace("\033[31m", "");
    text = text.replace("\033[0m", "");     //color information for console output
    ui->textEditConsoleOutput->insertPlainText(text);
}

void ConsoleOutputDialog::on_pushButtonCopy_pressed()
{
    QClipboard *clipboard = QGuiApplication::clipboard();

    clipboard->setText(ui->textEditConsoleOutput->toPlainText());
}

void ConsoleOutputDialog::on_pushButtonClear_pressed()
{
    Clear();
}
