#-------------------------------------------------
#
# Project created by QtCreator 2020-10-11T21:49:11
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = GUIThesis
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

unix {
    INCLUDEPATH += /usr/lib/gcc/x86_64-linux-gnu/9/include
}

SOURCES += \
    argumentparser.cpp \
    consoleoutputdialog.cpp \
        main.cpp \
        mainwindow.cpp \
    operation.cpp \
    qcustomcombobox.cpp \
    qcustomdoublespinbox.cpp \
    qcustomspinbox.cpp \
    qgeometricspinbox.cpp \
    qlineeditdialog.cpp \
    qlistwidgetoperationitem.cpp \
    qlistwidgetrefinementitem.cpp \
    qpipelineexportwidget.cpp \
    refinement.cpp \
    textclipboarddialog.cpp

HEADERS += \
    BoilerplateMacros.h \
    argumentparser.h \
    consoleoutputdialog.h \
    generalconstants.h \
        mainwindow.h \
    operation.h \
    operationdatatypes.h \
    pstream.h \
    qcustomcombobox.h \
    qcustomdoublespinbox.h \
    qcustomspinbox.h \
    qgeometricspinbox.h \
    qlineeditdialog.h \
    qlistwidgetoperationitem.h \
    qlistwidgetrefinementitem.h \
    qpipelineexportwidget.h \
    refinement.h \
    refinementdatatypes.h \
    textclipboarddialog.h

FORMS += \
        ConsoleOutputDialog.ui \
        LineEditDialog.ui \
        TextClipboardDialog.ui \
        mainwindow.ui
