#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "consoleoutputdialog.h"
#include <QMainWindow>
#include <iostream>
#include <cstdlib>
#include <fstream>
#include <string>
#include <algorithm>
#include <thread>
#include <QListWidgetItem>
#include <qcombobox.h>
#include <qsettings.h>

#include "operation.h"
#include "refinement.h"
#include "argumentparser.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private:
    Ui::MainWindow *ui;
    QSettings m_settings;

    std::unique_ptr<std::thread> m_pConsoleOutputThread;
    bool m_bCanRunAnotherInstance;
    QString m_commandString;
    ConsoleOutputDialog *m_pDialog;

    void GenerateCommandString();
    QString GenerateRefinementString();
    QString GenerateOperationString();
    QString GenerateExportString();
    QString GenerateGeneralOptionsString();

    void InitializeRefinementCombobox();
    void InitializeOperationCombobox();
    void InitializePipelineCombobox(QComboBox *comboBox, int maxPipelineCount) const;
    void UpdateRefinementDetailsPanel(const Refinement *refinement);
    void UpdateOperationDetailsPanel(const Operation *operation);

    Qt::Alignment GetRefinementParamsGridAlignment(int column) const;
    Qt::Alignment GetOperationParamsGridAlignment(int column) const;
    Qt::Alignment GetOperationPipelineGridAlignment(int column) const;

    bool ProcessConfigurationString(const QString &str);
    void InitializeFileIOTabFromConfig(const ArgumentParser &parser);
    void InitializeRefinementTabFromConfig(const ArgumentParser &parser);
    void InitializeInitializationTabFromConfig(const ArgumentParser &parser);
    void InitializeOperationsTabFromConfig(const ArgumentParser &parser);
    void InitializeExportTabFromConfig(const ArgumentParser &parser);
    void InitializeMPITabFromConfig(const ArgumentParser &parser);
    void Run();

    void CleanExportTab();
    void CleanRefinementTab();
    void CleanOperationTab();

    bool CheckValidExportNames() const;
private slots:
    void on_pushButtonRunForest_pressed();

    void on_checkBoxBatchProcessing_toggled(bool b);
    void on_pushButtonFilePath_pressed();
    void on_pushButtonFolderInputPath_pressed();
    void on_pushButtonConnectivityPath_pressed();
    void on_pushButtonFolderOutputPath_pressed();

    void on_pushButtonAddRefinement_pressed();
    void onListWidgetRefinementStack_currentItemChanged(QListWidgetItem *current, QListWidgetItem *previous);   //queued connection because count() is not yet updated during delete
    void on_pushButtonMoveRefinementUp_pressed();
    void on_pushButtonMoveRefinementDown_pressed();
    void on_pushButtonRemoveRefinement_pressed();
    void on_pushButtonRefinementHelp_pressed();

    void on_pushButtonAddOperation_pressed();
    void onListWidgetOperationStack_currentItemChanged(QListWidgetItem *current, QListWidgetItem *previous);    //queued connection because count() is not yet updated during delete
    void on_pushButtonMoveOperationUp_pressed();
    void on_pushButtonMoveOperationDown_pressed();
    void on_pushButtonRemoveOperation_pressed();
    void on_pushButtonOperationHelp_pressed();

    void on_pushButtonAddVTKExport_pressed();
    void on_pushButtonAddImageExport_pressed();
    void on_checkBoxExportVTK_toggled(bool b);
    void on_checkBoxExportImage_toggled(bool b);
    void on_checkBoxAutomaticTreeSideLength_toggled(bool b);

    void on_spinBoxTreeSideLength_valueChanged(int value);
    void on_checkBoxSetBackgroundValue_toggled(bool b);
    void on_checkBoxBackgroundOptional_toggled(bool b);
    void on_checkBoxExtraPixelsValue_toggled(bool b);

    void on_actionRun_triggered();
    void on_actionGenerateCommandString_triggered();
    void on_actionLoad_configuration_triggered();
    void on_actionPath_triggered();
    void on_actionReset_triggered();

    void on_forestStarted();
    void on_forestEnded(QDialog *dialog);
    void on_forestExecutionError();
    void on_forestRunningError();
    void on_dialogDestroyed(QObject*);

signals:
    void ForestStarted();
    void ForestEnded(QDialog*);
    void GotConsoleLine(QString);
    void ErrorWhileExecutingForest();
    void ErrorWhileRunningForest();
};

#endif // MAINWINDOW_H
