#ifndef QCUSTOMDOUBLESPINBOX_H
#define QCUSTOMDOUBLESPINBOX_H

#include <qspinbox.h>

class QCustomDoubleSpinBox : public QDoubleSpinBox
{
    Q_OBJECT

public:
    QCustomDoubleSpinBox(int customID, QWidget *parent = nullptr);
    int GetCustomID() const { return m_customID; }

protected:
    int m_customID;
};

#endif // QCUSTOMDOUBLESPINBOX_H
