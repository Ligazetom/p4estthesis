#ifndef CONSOLEOUTPUTDIALOG_H
#define CONSOLEOUTPUTDIALOG_H

#include <qdialog.h>
#include "ui_ConsoleOutputDialog.h"
#include <string>
#include <qstring.h>

class ConsoleOutputDialog : public QDialog
{
    Q_OBJECT

public:
    ConsoleOutputDialog(QWidget* parent = Q_NULLPTR);
    void Clear();

private:
    Ui::ConsoleOutputDialog* ui;

public slots:
    void AppendText(QString);

private slots:
    void on_pushButtonCopy_pressed();
    void on_pushButtonClear_pressed();
};

#endif // CONSOLEOUTPUTDIALOG_H
