#ifndef QLISTWIDGETOPERATIONITEM_H
#define QLISTWIDGETOPERATIONITEM_H

#include <QListWidgetItem>
#include "operation.h"
#include <memory>

class QListWidgetOperationItem : public QListWidgetItem
{
public:
    QListWidgetOperationItem(int operationMethodIndex, void *data = nullptr);
    int GetOperationMethodIndex() const { return m_operationMethodIndex; }
    Operation *GetOperation() { return m_pOperation.get(); }

protected:
    int m_operationMethodIndex;
    std::unique_ptr<Operation> m_pOperation;
};

#endif // QLISTWIDGETOPERATIONITEM_H
