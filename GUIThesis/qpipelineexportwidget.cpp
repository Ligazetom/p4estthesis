#include "qpipelineexportwidget.h"

#include <qpushbutton.h>
#include <qlayout.h>
#include <qlabel.h>

#include "generalconstants.h"

QPipelineExportWidget::QPipelineExportWidget(int comboValue, const char *exportName, QWidget *parent) : QWidget(parent)
{
    setLayout(&m_layout);
    m_layout.addWidget(new QLabel("Pipeline:", this));

    InitializeComboBoxValues(comboValue);
    m_exportName.setText(exportName);

    m_layout.addWidget(&m_comboBox);
    m_exportName.setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Fixed);
    m_layout.addWidget(&m_exportName);

    QPushButton *pushButton = new QPushButton("Remove", this);
    connect(pushButton, SIGNAL(pressed()), this, SLOT(on_pushButtonRemove_pressed()));

    m_layout.addWidget(pushButton);
}

void QPipelineExportWidget::InitializeComboBoxValues(int currentIndex)
{
    for (int i = 0; i < MAX_PIPELINE_NUMBER; ++i)
    {
        m_comboBox.addItem(QString("%1").arg(i));
    }

    m_comboBox.setCurrentIndex(currentIndex);
}

void QPipelineExportWidget::on_pushButtonRemove_pressed()
{
    deleteLater();
}
