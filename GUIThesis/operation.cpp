#include "operation.h"
#include <qobject.h>

#include <qdebug.h>
#include <sstream>

#include "operationdatatypes.h"

////////////////////////////////////
////////////////////////////////////
//OPERATION_PRIVATE_MEMBERS_IMPL (*className*, {*arrayInit for pipeIN*}, {*arrayInit for pipeOUT*})
////////////////////////////////////
////////////////////////////////////


constexpr const char* Operation::OperationNames[];
constexpr const char* Operation::OperationInfo[];

std::string Operation::GeneratePipelineArg(int *pipeINValues, size_t pipeINValuesCount, int *pipeOUTValues, size_t pipeOUTValuesCount) const
{
    std::stringstream ss;

    ss << "{ " << ConcatIntsWithSpaces(pipeINValues, pipeINValuesCount) << "# " << ConcatIntsWithSpaces(pipeOUTValues, pipeOUTValuesCount) << "}";
    return ss.str();
}

std::string Operation::ConcatIntsWithSpaces(int *values, size_t valuesCount) const
{
    std::stringstream ss;

    for (size_t i = 0; i < valuesCount; ++i)
    {
        ss << values[i] << " ";
    }

    return ss.str();
}


OPERATION_PRIVATE_MEMBERS_IMPL(LHE_EXPLICIT, 0, 1)

std::string LHE_EXPLICIT::GenerateParametersArg() const
{
    std::stringstream ss;

    ss << m_doubleParams[0] << " " << m_intParams[0];

    if (m_bExportAnimation) ss << " anim " << m_animationName;

    return ss.str();
}

void LHE_EXPLICIT::ProcessData(void *data)
{
    if (data == nullptr) return;
    SLHEExplicitParams *params = reinterpret_cast<SLHEExplicitParams*>(data);

    m_doubleParams[0] = params->timeStep;
    m_intParams[0] = params->numOfSteps;
    m_pipelinesIN[0] = params->pipelineIN;
    m_pipelinesOUT[0] = params->pipelineOUT;
    m_bExportAnimation = params->bExportAnimation;
    m_animationName = params->animationFileName;
}


OPERATION_PRIVATE_MEMBERS_IMPL(LHE_IMPLICIT, 0, 1)

std::string LHE_IMPLICIT::GenerateParametersArg() const
{
    std::stringstream ss;

    ss << m_doubleParams[0] << " " << m_intParams[0] << " " << m_doubleParams[1];

    if (m_bExportAnimation) ss << " anim " << m_animationName;

    return ss.str();
}

void LHE_IMPLICIT::ProcessData(void *data)
{
    if (data == nullptr) return;
    SLHEImplicitParams *params = reinterpret_cast<SLHEImplicitParams*>(data);

    m_doubleParams[0] = params->timeStep;
    m_doubleParams[1] = params->tol;
    m_intParams[0] = params->numOfSteps;
    m_pipelinesIN[0] = params->pipelineIN;
    m_pipelinesOUT[0] = params->pipelineOUT;
    m_bExportAnimation = params->bExportAnimation;
    m_animationName = params->animationFileName;
}


OPERATION_PRIVATE_MEMBERS_IMPL(NIBLACK_OP, ESC(0, 1, 2), 3)    //use ESC with multiple values for single parameter

std::string NIBLACK_OP::GenerateParametersArg() const
{
    std::stringstream ss;

    ss << m_intParams[0] << " " << m_doubleParams[0] << " " << m_doubleParams[1];
    return ss.str();
}

void NIBLACK_OP::ProcessData(void *data)
{
    if (data == nullptr) return;
    SNiblackOperationParams *params = reinterpret_cast<SNiblackOperationParams*>(data);

    m_doubleParams[0] = params->kappa;
    m_doubleParams[1] = params->meanOffset;
    m_intParams[0] = params->radius;
    m_pipelinesIN[0] = params->pipelineINProcessing;
    m_pipelinesIN[1] = params->pipelineINProcessingSquared;
    m_pipelinesIN[2] = params->pipelineINThresholding;
    m_pipelinesOUT[0] = params->pipelineOUT;
}


OPERATION_PRIVATE_MEMBERS_IMPL(MCF, 0, 1)

std::string MCF::GenerateParametersArg() const
{
    std::stringstream ss;

    ss << m_doubleParams[0] << " " << m_intParams[0] << " " << m_doubleParams[1] << " " << m_doubleParams[2];

    if (m_bExportAnimation) ss << " anim " << m_animationName;

    return ss.str();
}

void MCF::ProcessData(void *data)
{
    if (data == nullptr) return;
    SMCFParams *params = reinterpret_cast<SMCFParams*>(data);

    m_doubleParams[0] = params->timeStep;
    m_doubleParams[1] = params->tol;
    m_doubleParams[2] = params->epsilon;
    m_intParams[0] = params->numOfSteps;
    m_pipelinesIN[0] = params->pipelineIN;
    m_pipelinesOUT[0] = params->pipelineOUT;
    m_bExportAnimation = params->bExportAnimation;
    m_animationName = params->animationFileName;
}


OPERATION_PRIVATE_MEMBERS_IMPL(SUBSURF, ESC(0, 1), 2)    //use ESC with multiple values for single parameter

std::string SUBSURF::GenerateParametersArg() const
{
    std::stringstream ss;

    ss << m_doubleParams[0] << " " << m_intParams[0] << " " << m_doubleParams[1] << " " << m_doubleParams[2] << " " << m_doubleParams[3];

    if (m_bExportAnimation) ss << " anim " << m_animationName;

    return ss.str();
}

void SUBSURF::ProcessData(void *data)
{
    if (data == nullptr) return;
    SSubsurfParams *params = reinterpret_cast<SSubsurfParams*>(data);

    m_doubleParams[0] = params->timeStep;
    m_doubleParams[1] = params->tol;
    m_doubleParams[2] = params->epsilon;
    m_doubleParams[3] = params->K;
    m_intParams[0] = params->numOfSteps;
    m_pipelinesIN[0] = params->pipelineIN_I0;
    m_pipelinesIN[1] = params->pipelineIN_U0;
    m_pipelinesOUT[0] = params->pipelineOUT;
    m_bExportAnimation = params->bExportAnimation;
    m_animationName = params->animationFileName;
}


OPERATION_PRIVATE_MEMBERS_IMPL(SQUARE, 0, 1)

std::string SQUARE::GenerateParametersArg() const
{
    return "";
}

void SQUARE::ProcessData(void *data)
{
    if (data == nullptr) return;
    SSquareParams *params = reinterpret_cast<SSquareParams*>(data);

    m_pipelinesIN[0] = params->pipelineIN;
    m_pipelinesOUT[0] = params->pipelineOUT;
}


OPERATION_PRIVATE_MEMBERS_IMPL(MAX, ESC(0, 1), 2)    //use ESC with multiple values for single parameter

std::string MAX::GenerateParametersArg() const
{
    return "";
}

void MAX::ProcessData(void *data)
{
    if (data == nullptr) return;
    SMaxParams *params = reinterpret_cast<SMaxParams*>(data);

    m_pipelinesIN[0] = params->pipelineIN1;
    m_pipelinesIN[1] = params->pipelineIN2;
    m_pipelinesOUT[0] = params->pipelineOUT;
}


OPERATION_PRIVATE_MEMBERS_IMPL(EXPAND_REFINEMENT, ESC(), ESC())    //use ESC with no values

std::string EXPAND_REFINEMENT::GenerateParametersArg() const
{
    return "";
}

void EXPAND_REFINEMENT::ProcessData(void *)
{
}


OPERATION_PRIVATE_MEMBERS_IMPL(REFINE_BY_THRESHOLD, 0, ESC())    //use ESC with no values

std::string REFINE_BY_THRESHOLD::GenerateParametersArg() const
{
    std::stringstream ss;

    ss << m_doubleParams[0];

    return ss.str();
}

void REFINE_BY_THRESHOLD::ProcessData(void *data)
{
    if (data == nullptr) return;
    SRefineByThresholdParams *params = reinterpret_cast<SRefineByThresholdParams*>(data);

    m_doubleParams[0] = params->threshold;
    m_pipelinesIN[0] = params->pipeline;
}


OPERATION_PRIVATE_MEMBERS_IMPL(FLAT_THRESHOLD, 0, 1)

std::string FLAT_THRESHOLD::GenerateParametersArg() const
{
    std::stringstream ss;

    ss << m_doubleParams[0];

    return ss.str();
}

void FLAT_THRESHOLD::ProcessData(void *data)
{
    if (data == nullptr) return;
    SFlatThresholdParams *params = reinterpret_cast<SFlatThresholdParams*>(data);

    m_doubleParams[0] = params->threshold;
    m_pipelinesIN[0] = params->pipelineIN;
    m_pipelinesOUT[0] = params->pipelineOUT;
}
