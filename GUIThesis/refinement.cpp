#include "refinement.h"

#include <sstream>

#include "refinementdatatypes.h"

////////////////////////////////////
////////////////////////////////////
//REFINEMENT_PRIVATE_MEMBERS_IMPL (*className*)
////////////////////////////////////
////////////////////////////////////

constexpr const char* Refinement::RefinementNames[];
constexpr const char* Refinement::RefinementInfo[];


REFINEMENT_PRIVATE_MEMBERS_IMPL(GLOBAL_OTSU)

std::string GLOBAL_OTSU::GenerateParametersArg() const
{
    std::stringstream ss;

    ss << m_intParams[0];

    return ss.str();
}

void GLOBAL_OTSU::ProcessData(void *data)
{
    if (data == nullptr) return;
    SGlobalOTSUThresholdParams *params = reinterpret_cast<SGlobalOTSUThresholdParams*>(data);

    m_intParams[0] = params->refineFromQuadLevel;
}


REFINEMENT_PRIVATE_MEMBERS_IMPL(MAX_MIN_DIFF)

std::string MAX_MIN_DIFF::GenerateParametersArg() const
{
    std::stringstream ss;

    ss << m_intParams[0] << " " << m_intParams[1];

    return ss.str();
}

void MAX_MIN_DIFF::ProcessData(void *data)
{
    if (data == nullptr) return;
    SMaxMinDiffThresholdParams *params = reinterpret_cast<SMaxMinDiffThresholdParams*>(data);

    m_intParams[0] = params->refineFromQuadLevel;
    m_intParams[1] = params->maxMinDiffThreshold;
}


REFINEMENT_PRIVATE_MEMBERS_IMPL(BERNSEN)

std::string BERNSEN::GenerateParametersArg() const
{
    std::stringstream ss;

    ss << m_intParams[0] << " " << m_intParams[1] << " " << m_doubleParams[0] << " " << m_doubleParams[1];

    return ss.str();
}

void BERNSEN::ProcessData(void *data)
{
    if (data == nullptr) return;
    SBernsenThresholdParams *params = reinterpret_cast<SBernsenThresholdParams*>(data);

    m_doubleParams[0] = params->contrastThreshold;
    m_doubleParams[1] = params->backgroundForegroundThreshold;
    m_intParams[0] = params->refineFromQuadLevel;
    m_intParams[1] = params->radius;
}


REFINEMENT_PRIVATE_MEMBERS_IMPL(SAUVOLA)

std::string SAUVOLA::GenerateParametersArg() const
{
    std::stringstream ss;

    ss << m_intParams[0] << " " << m_intParams[1] << " " << m_doubleParams[0] << " " << m_doubleParams[1];

    return ss.str();
}

void SAUVOLA::ProcessData(void *data)
{
    if (data == nullptr) return;
    SSauvolaThresholdParams *params = reinterpret_cast<SSauvolaThresholdParams*>(data);

    m_doubleParams[0] = params->kappa;
    m_doubleParams[1] = params->sigmaMax;
    m_intParams[0] = params->refineFromQuadLevel;
    m_intParams[1] = params->radius;
}


REFINEMENT_PRIVATE_MEMBERS_IMPL(NIBLACK_REF)

std::string NIBLACK_REF::GenerateParametersArg() const
{
    std::stringstream ss;

    ss << m_intParams[0] << " " << m_intParams[1] << " " << m_doubleParams[0] << " " << m_doubleParams[1];

    return ss.str();
}

void NIBLACK_REF::ProcessData(void *data)
{
    if (data == nullptr) return;
    SNiblackThresholdParams *params = reinterpret_cast<SNiblackThresholdParams*>(data);

    m_doubleParams[0] = params->kappa;
    m_doubleParams[1] = params->meanOffset;
    m_intParams[0] = params->refineFromQuadLevel;
    m_intParams[1] = params->radius;
}


REFINEMENT_PRIVATE_MEMBERS_IMPL(TOTAL)

std::string TOTAL::GenerateParametersArg() const
{
    return "";
}

void TOTAL::ProcessData(void *)
{

}


REFINEMENT_PRIVATE_MEMBERS_IMPL(UP_TO)

std::string UP_TO::GenerateParametersArg() const
{
    std::stringstream ss;

    ss << m_intParams[0];

    return ss.str();
}

void UP_TO::ProcessData(void *data)
{
    if (data == nullptr) return;
    SUpToRefinementParams *params = reinterpret_cast<SUpToRefinementParams*>(data);

    m_intParams[0] = params->quadLevel;
}
