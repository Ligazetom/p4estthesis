# README - p4est thesis

## What is this

GUI + Console application for solving differential equations on images (currently supported only '.pgm' format).
Main feature is adaptive generation of simplified representation of the image with the help of **p4est** library, to lower the number of finite volumes needed to be processed. This, along with MPI integration, allows for shorter computation times and lower memory footprints.

This application was used for macrophage segmentation of *danio rerio* embryos, where image dataset contained a lot of sparse data:

![Input example](https://bitbucket.org/Ligazetom/p4estthesis/raw/4ba2be9ca279b39f9587eb5636d900b9f32cbdb5/TexThesis/figures/bigcrop.png)

If we were to solve equations on this image in it's raw form, we would have to work with **393 216** elements (768 x 512 px). Using adaptively generated mesh, refining only in places around the macrophages, we were able to reduce the number of elements to **7674**:

![Preprocessed representation](https://bitbucket.org/Ligazetom/p4estthesis/raw/4ba2be9ca279b39f9587eb5636d900b9f32cbdb5/TexThesis/figures/bigcrop_balanced.png)

Whole application was built with modularity in mind, so every operation is thought of as a block. You can chain these blocks to process the image in whatever way you want. For example, in our case the chain for segmentation looked like this:

![Methods application](https://bitbucket.org/Ligazetom/p4estthesis/raw/0dbabd85986cba23b063d7fe3ca7ee76da1aee8f/TexThesis/figures/eng_method_application.png)

Core application is controlled via command line arguments, which GUI will generate for you.