﻿Vstupné dáta vyzerajú takto. Biele škvrny sú makrofágy, ktoré chceme vysegmentovať. Bližšie jednotlivé makrofágy vyzerajú takto. Majú rôzne tvary a premenlivé intenzity, tiež sa na pozadí vyskytuje rôzne intenzívny šum. My chceme binarizovať daný obraz a jasne priradiť aká časť makrofágom je a aká časť obrazu je pozadie.

Takouto segmentáciou sa už zaoberalo zopár predošlých prác a naším hlavným prínosom je aplikovať už z časti overené metódy na nerovnomerné diskretizácie. Keď sa znovu pozrieme na vstupný obraz, tak vidíme že makrofágy zaberajú zlomok celkovej oblasti obrazu a teda riešiť numerické schémy na rovnomernej mriežke je neefektívne. Preto chceme vygenerovať takú diskretizáciu, ktorá bude čo najjemnejšia v okolí makrofágov a v oblastiach bez dôležitých informácií nám postačí hrubšie delenie.

Pre naše prípady taká diskretizácia vyzerá napríklad takto. 

Na vytvorenie a prácu s takouto diskretizáciou sme použili knižnicu p4est. Je to skratka zo slov parallel forest, čo naznačuje, že ponúka nejakú formu paralelizácie a forest ako "les" sa odkazuje na štruktúru kvadrantových alebo oktantových stromov, záleží či pracujeme v 2D alebo v 3D, v ktorých jednotlivé elementy knižnica reprezentuje. Teda lesom sa nazýva viacero takýchto stromov.

Veľmi zbežne, je to knižnica napísaná v programovacom jazyku C, podporuje MPI a prácu v 2D aj v 3D a je open-source. Tak úplne v skratke taký krátky postup:

1) V 2D je elementom štvorec

2) My knižnici poskytneme počiatočnú diskretizáciu našej oblasti, ktorú pokryjeme štvorcami. Z dôvodov, na ktorých vysvetlenie nemám čas je veľmi výhodné, ak pri práci s obrazmi to sú naozaj štvorce a nie len štvoruholníky a dĺžka ich hrany je mocnina 2

3) Každý takto na začiatku vytvorený štvorec sa považuje za koreň stromu a všetky ďalšie štvorce môžu vzniknúť len ich zjemnením, pričom už navždy budú priradené danému stromu. Zjemnenie je operácia, ktorá z jedného štvorca vytvorí štyri nové, menšie štvorce. Teda na obraze to bude vyzerať ako tento obrázok naľavo a v abstraktnej predstave, v akej si to uchováva knižnica je to vo forme kvadrantového stromu vizualizovaného napravo. Čísla reprezentujú úroveň zjemnenia. Komplexnejší príklad je napríklad takýto. 

4) A nech sa tomu celému dá nejaká hlava a päta tak sú jednotlivé kvadranty uložené v poradí Z-krivky. Teda tam kde je makrofág chceme zjemniť diskretizáciu čo najviac a v iných oblastiach ju chceme mať čo najbližšie k veľkosti koreňa stromu.

5) Ak by sme zjemňovali štvorce len v miestach makrofágov, tak by nám mohla nastať situácia aká je naľavo. Čísla znovu reprezentujú úrovne zjemnenia. Kvôli algoritmom, ktoré sme implementovali a rovnako aj kvôli niektorým funkcionalitám samotnej knižnice je nutné aby sme po každom zjemnení zabezpečili tzv. vyváženie lesa, čo je operácia ktorá vyžaduje aby sa susedné kvadranty nelíšili úrovňou zjemnenia nikdy o viac ako 1. Ak sa líšia, dané kvadranty sa zjemnia a dostaneme diskretizáciu vpravo.

Na našich najväčších vstupných dátach výsledná diskretizácia vyzerá takto. Tu môžeme vidieť farebne odlíšené jednotlivé stromy, ktorých tu je 648 a len zdôrazňujem, že raz ako vytvoríme počiatočnú diskretizáciu, tak všetky následne zjemnením vytvorené štvorce majú stále farbu stromu, z ktorého vznikli.

Pomocou MPI dokážeme jednotlivé elementy rovnomerne rozvrhnúť medzi aktívne procesy a pracovať s nimi. Toto je obrázok, kde farby hovoria o ranku procesu. Program bol spustený na 8 jadrách a aj keď sa Vám môže zdať, že napríklad červenej je tam najviac, tak čo sa týka plochy to je možno pravda, ale keďže naše operácie trvajú rovnako dlho bez ohľadu na plochu, ktorú element zaberá, rovnomerným rozdelením medzi procesy sa myslí počet elementov. Teda niektoré procesy majú oveľa viac menších elementov.

Týmto by som skončil rozprávaním o knižnici a prešiel k numerike. Postup, z ktorého vychádzame je založený na hrubšom prahovaní makrofágu, ktorého výsledok spracujeme pomocou SUBSURF metódy, ktorá by mala finálny produkt "doladiť". 

1) Na toto "hrubé" prahovanie používame Niblackom inšpirovanú metódu. Táto metóda vypočíta pre každý pixel jeho vlastný prah na základe intenzít v okolí a potom porovnaním pôvodnej intenzity pixelu s týmto prahom rozhodneme či priradíme pixel do pozadia alebo nie. Máme teda masku so zadaným polomerom a pixely nachádzajúce sa v tejto maske prispievajú do výpočtu prahu pre stredný pixel.

Tu v týchto rovniciach je len odvodený vzťah, ktorý hovorí, že na výpočet smerodajnej odchýlky nám stačí poznať priemer intenzít v maske a umocniť ho na druhú, to je ten pravý člen a ako druhú hodnotu potrebujeme poznať priemer druhých mocnín intenzít v maske, to je ten prvý člen. Teda klasicky vypočítame aritmetický priemer a ten umocníme, potom spravíme druhé mocniny pôvodných hodnôt a z nich spravíme aritmetický priemer. Táto operácia je veľmi priamočiara pre rovnomerné diskretizácie, no práca s oknom v p4este nie je priamo podporovaná. Podarilo sa nám to obísť, ale za cenu dlhšieho výpočtového času.

2) Preto sme implementovali aj schému na výpočet rovnice vedenia tepla, ktorej zhladzovací efekt nám spraví síce vážený priemer a nie aritmetický, no kompletne sa vyhneme práci s oknom. Klasicky, pomocou aproximácie časovej derivácie spätnou diferenciou a využítím MKO dostaneme tento vzťah. Znovu sa tu však vyskytuje problém spôsobený nerovnomernou diskretizáciou. Kvôli vyváženiu lesa máme zabezpečené, že jeden štvorec bude mať cez hranu maximálne dvoch susedov, no v tom prípade sa táto schéma nedá použiť. Preto zavádzame pomocnú premennú u-sigma. Vďaka tejto aproximácii sme schopní sa vyhnúť problémom s dvomi susedmi a bonusom je tiež možnosť počítať všetky veci ako sú gradienty alebo difúzne koeficienty, ktoré sú závislé od susedov, lokálne. Nemusíme pristupovať k susedom. Vzorce na výpočet u-sigiem vyplývajú z rovnice pre ktorú ich počítame a sú založené na princípe konzervativity.

Knižnica relatívne jednoducho umožňuje prácu s priamymi susedmi a preto je jednoduchšie najprv aplikovať rovnicu vedenia tepla a jej výsledok použiť ako vstup do Niblacka, než sa trápiť s oknom.

3) SUBSURF - ako som spomínal toto je naša dolaďovacia operácia. Odvodenie je veľmi podobné implicitnej schéme rovnice vedenia tepla a aproximácie susedných hodnôt sa tiež veľmi nelíšia, no sú tam zmeny samozrejme. Dôležitý rozdiel je že nám tu vystupujú gradienty a rovnako tak aj dve varianty vstupných dát. u ako funkcia intenzity s počiatočnou podmienkou u0 je klasický vstup, ktorý nám prichádza z niblacka, no pre presnejšiu detekciu hrán používame na výpočet normy gradientov vstupujúcich do funkcie g dáta I0, ktoré sú taktiež výstupom z niblacka, no výsledok je získaný pomocou iných parametrov.

V jednoduchosti, I0 je výsledok niblacka, ktorého polomer masky by bol menší a tým by sme mali presnejšie zachytiť tvar makrofágu, zatiaľ čo u0 je výsledok maxima z niblacka, ktorého polomer masky by bol väčší a I0, aby sme zabezpečili čo najväčšie pokrytie makrofágu. Subsurf potom pomocou týchto dvoch vstupov by mal "nafúknuté" hranice tlačiť k presnejšie vysegmentovaným hraniciam získaných z I0 a ak to nepreženieme s časom, makrofág by mal zostať ucelený. Stáva sa totiž že počas segmentácie sa makrofág roztrhne a vzniknú 2 alebo viacero samostatných častí. A keďže namiesto masiek používame rovnicu vedenia tepla, tak pre I0 ju aplikujeme po kratší čas ako pre u0.

Samotná aplikácia je naprogramovaná v C++, kvôli podpore MPI je to konzolová aplikácia, je napísaná s myšlienkou, že ju niekto bude chcieť v budúcnosti rozšíriť. Každá operácia je svoj samostatný blok a viete ich ľubovoľne kombinovať, teda nič nie je napevno naprogramované a nie je nutná kompilácia a teda samotná aplikácia sa dá tým pádom použiť aj na iné problémy. Táto schéma popisuje naše použitie.

Kompletne celá sa ovláda pomocou spúštacích argumentov, čo napríklad vyzerá takto. Sú tam nejaké prepínače zadefinované, help je zabudovaný v rámci aplikácie tiež aj s príkladom ako na to a najväčšia časť programu bolo spraviť práve parser na tieto argumenty, rozumne a robustne. Bolo a ešte aj je asi pre budúcnosť spraviť GUI k tomu, ktoré bude generovať tieto príkazy ale na to mi nejak nezostal čas.

Všetky výsledky boli spracované tými istými parametrami.

Záver:


Otázky:
Implicitné schémy zabrali najviac času, operácie ako square, niblack, max atď. teda nie iteračné algoritmy nezabrali ani milisekundu, čo znamená že celým lesom s počtom elementov 20 050 sa dá prejsť veľmi rýchlo, tam za rýchlosť vďačím aj manuálnej správe pamäti pre kvadrantové dáta, ktoré sú uložené kontinuálne v jednom bloku.

Vidíme že sparalelnením výpočty jednoznačne urýchľujeme a síce pri lokálnom paralelnom spúšťaní a takto malých dátach to nezaváži, je dobré si všimnúť, že čas inicializácie rastie s počtom jadier.